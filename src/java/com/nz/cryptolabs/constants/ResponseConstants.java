/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.constants;

/**
 *
 * @author Administrator
 */
public class ResponseConstants {

    final public static String LATER = "later";
    //resgister
    final public static String SUCCESSFUL_CREATED_ACCOUNT = "successful.created.account";
    final public static String SUCCESSFUL_UPDATED_ACCOUNT = "successful.updated.account";
    final public static String DUPLICATE_EMAIL = "duplicate.email";
    final public static String DUPLICATE_PHONE = "duplicate.phone";
    final public static String SUCCESSFULL_ASSIGNED_ROLE = "successful.assigned.role";
    final public static String SUCCESSFULL_ASSIGNED_COMMISSION = "successful.assigned.commission";
    final public static String SENT_RESET_PASSWORD_MAIL = "sent.reset.password.mail";
    //login
    final public static String CONGRATS_VERIFIED_LOGIN = "congrats.verified.login";
    final public static String SORRY_INVALID_LOGIN = "sorry.invalid.login";
    //chef
    final public static String SUCCESSFUL_CREATED_NEWDISH = "successful.created.newdish";
    final public static String SUCCESSFUL_UPDATED_OLDDISH = "successful.updated.olddish";
    final public static String SUCCESSFUL_UPDATED_ORDER_STATUS = "successful.updated.order_status";
    final public static String FAILED_UPDATE_ORDER_STATUS = "failed.update.order_status";
    final public static String SUCCESSFUL_MENTION_QUANTITY = "successful.mention.quantity";
    //client
    final public static String SUCCESSFUL_CREATED_NEWORDER = "successful.created.neworder";
    final public static String SUCCESSFUL_UPDATED_OLDORDER = "successful.updated.oldorder";

}
