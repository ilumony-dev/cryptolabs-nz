/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.constants;

import java.math.BigDecimal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author Administrator
 */
public interface Constants {

    String depositParticulars = "Deposit-(Purchased Investments)";
    String withdrawlParticulars = "Withdrawal-(Sold Investments)";

    String kickboxApiKey = "live_48e510a673dd08002c45f039145c3e895e2598122ac132de1217424b284a1bc6"; //fill in
    String kickboxAppCode = "AM-gCN-viQrBx3YcqFGv";

    String bittrexApiKey = "e191dbf4e17b475ebaead51e1cdb7b2f";
    String bittrexSecret = "94c41d791d6947dcac98904a41b0b600";
    String poliniexApiKey = "";
    String poliniexSecret = "";

    BigDecimal HUNDRED = new BigDecimal(100);
    String ENV_VAL = "DEV";
    String ENV_DEV = "DEV";
    String ENV_PROD = "PROD";

    GrantedAuthority superAdminRole = new SimpleGrantedAuthority("ROLE_SUPER_ADMIN");
    GrantedAuthority adminRole = new SimpleGrantedAuthority("ROLE_ADMIN");
    GrantedAuthority userRole = new SimpleGrantedAuthority("ROLE_USER");

    public static GrantedAuthority superAdminRole() {
        return superAdminRole;
    }

    public static GrantedAuthority adminRole() {
        return adminRole;
    }

}
