/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.otp;

/**
 *
 * @author Administrator
 */
public class OTPResponsePOJO {

    private String carrier;
    private boolean is_cellphone;
    private String message;
    private int seconds_to_expire;
    private String uuid;
    private boolean success;

    /**
     * @return the carrier
     */
    public String getCarrier() {
        return carrier;
    }

    /**
     * @param carrier the carrier to set
     */
    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    /**
     * @return the is_cellphone
     */
    public boolean isIs_cellphone() {
        return is_cellphone;
    }

    /**
     * @param is_cellphone the is_cellphone to set
     */
    public void setIs_cellphone(boolean is_cellphone) {
        this.is_cellphone = is_cellphone;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the seconds_to_expire
     */
    public int getSeconds_to_expire() {
        return seconds_to_expire;
    }

    /**
     * @param seconds_to_expire the seconds_to_expire to set
     */
    public void setSeconds_to_expire(int seconds_to_expire) {
        this.seconds_to_expire = seconds_to_expire;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }
}
