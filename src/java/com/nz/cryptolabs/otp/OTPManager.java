/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.otp;


import com.nz.cryptolabs.components.HTTPRequestManager;
import com.nz.cryptolabs.services.ObjectCastService;
import java.io.IOException;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class OTPManager {

    @Autowired
    private HTTPRequestManager httpRequestManager;
    @Autowired
    private ObjectCastService objectCastService;

    private HashMap<String, OTPEntity> otpEntityMap;
    private final String api_key;

    public OTPManager() {
        this.otpEntityMap = new HashMap<String, OTPEntity>();
        this.api_key = "WObTojKf93zL3iijJcW3pAbf1ryqOsTD";
    }
//    for otp generate method

    public OTPResponsePOJO OTPGeneration(String senderType, String countryCode, String phone) throws IOException {
        OTPEntity otpEntity = new OTPEntity();
        otpEntity.setSenderType(senderType);
        otpEntity.setCountryCode(countryCode);
        otpEntity.setPhone(phone);
        return queued(otpEntity);
    }

    public OTPResponsePOJO queued(OTPEntity otpEntity) throws IOException {
        otpEntityMap.put(otpEntity.getPhone(), otpEntity);
        String url = "https://api.authy.com/protected/json/phones/verification/start?"
                + "api_key=" + api_key + "&via=" + otpEntity.getSenderType() + "&country_code=" + otpEntity.getCountryCode() + "&phone_number=" + otpEntity.getPhone();
        String json = httpRequestManager.sendHTTPPostRequest(url, null);
        OTPResponsePOJO otpResponse = (OTPResponsePOJO) objectCastService.jSONcast(OTPResponsePOJO.class, json);
        return otpResponse;
    }

//    for verify otp method
    public OTPVerifyResponsePOJO verifyOTP(String phone, String otp) throws IOException {
        OTPEntity otpEntity = otpEntityMap.get(phone);
        if (otpEntity == null) {
            return null;
        }
        otpEntity.setOtp(otp);
        String url = "https://api.authy.com/protected/json/phones/verification/check";
        HashMap<String, String> params = new HashMap<>();
        params.put("api_key", api_key);
        params.put("via", otpEntity.getSenderType());
        params.put("phone_number", otpEntity.getPhone());
        params.put("country_code", otpEntity.getCountryCode());
        params.put("verification_code", otpEntity.getOtp());
        String json = httpRequestManager.sendHTTPGetRequest(url, params);
        OTPVerifyResponsePOJO otpVerifyResponse = objectCastService.jSONcast(OTPVerifyResponsePOJO.class, json);
        return otpVerifyResponse;
    }

}
