/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.components.HTTPRequestManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author palo12
 */
@Service
public class CoinMarketCapAPIServiceImpl implements CoinMarketCapAPIService {

    @Autowired
    private HTTPRequestManager httpRequestManager;

    @Override
    public String getAllCoinMarketCap(Integer limit) {
        try {
            String url = "https://api.coinmarketcap.com/v1/ticker/?convert=USD";
            if (limit != null && limit > 0) {
                url += "& limit = " + limit;
            }
            String json = httpRequestManager.sendHTTPGetRequest(url);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode readTree = mapper.readTree(json);
            if (readTree instanceof ArrayNode) {
                ArrayNode arrayTree = (ArrayNode) readTree;
                for (Iterator<JsonNode> iter = arrayTree.elements(); iter.hasNext();) {
                    ObjectNode objNode = (ObjectNode) iter.next();
                    JsonNode _24h_volume_usd = objNode.get("24h_volume_usd");
                    objNode.put("volume_usd_24h", _24h_volume_usd);
//                    JsonNode _24h_volume_eur = objNode.get("24h_volume_eur");
//                    objNode.put("volume_eur_24h", _24h_volume_eur);
                }
                return arrayTree.toString();
            }
            return "[]";
        } catch (IOException iox) {
            return null;
        }
    }

    @Override
    public String getSingleCoinMarketCap(String uniqueId) {
        String url = "https://api.coinmarketcap.com/v1/ticker/" + uniqueId + "?convert=USD";
        String json = httpRequestManager.sendHTTPGetRequest(url);
        return json;
    }

    private CryptoCoin getCryptoCoin(ObjectNode objNode) {
        CryptoCoin coin = new CryptoCoin();
        JsonNode idNode = objNode.get("id");
        coin.setUniqueId(idNode.asText());
        JsonNode nameNode = objNode.get("name");
        coin.setName(nameNode.asText());
        JsonNode symbolNode = objNode.get("symbol");
        coin.setSymbol(symbolNode.asText());
        JsonNode rankNode = objNode.get("rank");
        coin.setRank(rankNode.asText());
        JsonNode btcPriceNode = objNode.get("price_btc");
        coin.setBtcPrice(btcPriceNode.asDouble());
        JsonNode usdPriceNode = objNode.get("price_usd");
        coin.setUsdPrice(usdPriceNode.asDouble());
        JsonNode percent1hrNode = objNode.get("percent_change_1h");
        coin.setPercent1hr(percent1hrNode.asDouble());
        JsonNode percent24hrNode = objNode.get("percent_change_24h");
        coin.setPercent24hr(percent24hrNode.asDouble());
        JsonNode percent7dNode = objNode.get("percent_change_7d");
        coin.setPercent7d(percent7dNode.asDouble());
        JsonNode lastUpdatedNode = objNode.get("last_updated");
        long lastUpdated = lastUpdatedNode.asLong();
        coin.setLastUpdated(new Date(lastUpdated));
        return coin;
    }

    @Override
    public List<CryptoCoin> getAllCoinMarketCapList(Integer limit) throws IOException {
        String json = getAllCoinMarketCap(limit);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ArrayNode) {
            ArrayNode arrayTree = (ArrayNode) readTree;
            List<CryptoCoin> coinList = new ArrayList<>();
            for (Iterator<JsonNode> iter = arrayTree.elements(); iter.hasNext();) {
                ObjectNode objNode = (ObjectNode) iter.next();
                CryptoCoin coin = getCryptoCoin(objNode);
                coinList.add(coin);
            }
            return coinList;
        }
        return null;
    }

    @Override
    public CryptoCoin getSingleCoinMarketCapCoin(String uniqueId) throws IOException {
        String json = getSingleCoinMarketCap(uniqueId);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ArrayNode) {
            ArrayNode arrayTree = (ArrayNode) readTree;
            if (arrayTree.size() > 0) {
                ObjectNode objNode = (ObjectNode) arrayTree.get(0);
                CryptoCoin coin = getCryptoCoin(objNode);
                return coin;
            }
        }
        return null;
    }

    @Override
    public String getMarketPrice(String uniqueId) throws IOException {
        String json = getSingleCoinMarketCap(uniqueId);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ArrayNode) {
            ArrayNode arrayTree = (ArrayNode) readTree;
            if (arrayTree.size() > 0) {
                ObjectNode objNode = (ObjectNode) arrayTree.get(0);
                JsonNode usdPriceNode = objNode.get("price_usd");
                String price = (usdPriceNode.asText());
                return price;
            }
        }
        return "0.00";
    }

    @Override
    public String getValueForCoin(String uniqueId, String... params) throws IOException {
        String json = getSingleCoinMarketCap(uniqueId);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ArrayNode) {
            ArrayNode arrayTree = (ArrayNode) readTree;
            if (arrayTree.size() > 0) {
                ObjectNode objNode = (ObjectNode) arrayTree.get(0);
                JsonNode usdPriceNode = objNode.get("price_usd");
                String price = (usdPriceNode.asText());
                return price;
            }
        }
        return "0.00";
    }

    @Override
    public ObjectNode getRateForPair(String currency0, String currency1) throws IOException {
//        String url = "https://shapeshift.io/rate/" + pair + "/";
//        String url = "https://min-api.cryptocompare.com/data/price?fsym=" + currency0 + "&tsyms=" + currency1;
        String url = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=" + currency0 + "&tsyms=" + currency1 + "&e=CCCAGG&tryConversion=true";
        String json = httpRequestManager.sendHTTPGetRequest(url);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ObjectNode) {
            ObjectNode objNode = (ObjectNode) readTree;
            return objNode;
        }
        return null;
    }

    @Override
    public ObjectNode getRateForPair(String currency0, String currency1, Long timestamp) throws IOException {
        String url = "https://min-api.cryptocompare.com/data/pricehistorical?fsym=" + currency0
                + "&tsyms=" + currency1
                + "&e=CCCAGG&tryConversion=true&ts=" + timestamp + "&extraParams=NotAvailable&calculationType=Close&sign=false";
        String json = httpRequestManager.sendHTTPGetRequest(url);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ObjectNode) {
            ObjectNode objNode = (ObjectNode) readTree;
            return objNode;
        }
        return null;
    }

    /**
     *
     * @param currencyPair is pair of the country currency like USD_NZD, INR_USD etc.
     * @param date
     * @return 
     * @throws java.io.IOException
     */
    @Override
    public ObjectNode currencyConversionRate(String currencyPair, String date) throws IOException {
        String url = "http://free.currencyconverterapi.com/api/v3/convert?q=" + currencyPair + "&compact=ultra&date=" + date;
        String json = httpRequestManager.sendHTTPGetRequest(url);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode readTree = mapper.readTree(json);
        if (readTree instanceof ObjectNode) {
            ObjectNode objNode = (ObjectNode) readTree;
            return objNode;
        }
        return null;
    }

}
