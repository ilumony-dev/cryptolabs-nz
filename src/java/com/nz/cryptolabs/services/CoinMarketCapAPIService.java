/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.CryptoCoin;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author palo12
 */
public interface CoinMarketCapAPIService {

    public String getAllCoinMarketCap(Integer limit);

    public String getSingleCoinMarketCap(String uniqueId);

    public List<CryptoCoin> getAllCoinMarketCapList(Integer limit) throws IOException;

    public CryptoCoin getSingleCoinMarketCapCoin(String uniqueId) throws IOException;

    public String getMarketPrice(String uniqueId) throws IOException;

    public String getValueForCoin(String uniqueId, String... params) throws IOException;

    public ObjectNode getRateForPair(String currency0, String currency1) throws IOException;

    public ObjectNode getRateForPair(String currency0, String currency1, Long timestamp) throws IOException;

    public ObjectNode currencyConversionRate(String currencyPair, String date) throws IOException;
}
