/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.payments.ChargeRequest;
import com.nz.cryptolabs.payments.StripeService;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service
public class DatabaseServiceImpl implements DatabaseService {

    @Override
    public List<ShareFund> fundActualShares(String fundId, String investmentAmount) {
        List<ShareFund> shares = repository.sharesByFund(fundId);
        List<ShareFund> actualShareList = new ArrayList<>();
        BigDecimal amount = new BigDecimal(investmentAmount);
        for (ShareFund share : shares) {
            BigDecimal percentage = new BigDecimal(share.getPercentage() != null ? share.getPercentage() : "0.00");
            BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, 8, RoundingMode.HALF_UP).multiply(amount);
            share.setShareAmount(shareAmount.toPlainString());
            BigDecimal quantity = shareAmount.divide(new BigDecimal(share.getPrice()), 8, RoundingMode.HALF_UP);
            share.setQuantity(quantity.toPlainString());
            actualShareList.add(share);
        }
        List<ShareFund> coins = repository.coinsByFund(fundId);
        for (ShareFund coin : coins) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal percentage = new BigDecimal(coin.getPercentage() != null ? coin.getPercentage() : "0.00");
                BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, 8, RoundingMode.HALF_UP).multiply(amount);
                coin.setShareAmount(shareAmount.toPlainString());
                coin.setPrice(String.valueOf(cryptoCoin.getUsdPrice()));
                BigDecimal quantity = shareAmount.divide(new BigDecimal(coin.getPrice()), 8, RoundingMode.HALF_UP);
                coin.setQuantity(quantity.toPlainString());
                actualShareList.add(coin);
            }

        }
        return actualShareList;
    }

    @Override
    public List<ShareFund> investmentActualDetails(String investmentId) {
        List<ShareFund> shares = repository.pendingSharesByInvestment(investmentId);
        List<ShareFund> investmentActualDetails = new ArrayList<>();
        for (ShareFund share : shares) {
            investmentActualDetails.add(share);
        }
        List<ShareFund> coins = repository.pendingCoinsByInvestment(investmentId);
        if (!coins.isEmpty()) {
            HashMap<String, String> coinPriceMap = new HashMap();
            for (ShareFund coin : coins) {
                String usdPrice = null;
                try {
                    usdPrice = coinPriceMap.get(coin.getCoinId());
                    if (usdPrice == null) {
                        usdPrice = coinMarketCapAPIService.getMarketPrice(coin.getCoinId());
                        coinPriceMap.put(coin.getCoinId(), usdPrice);
                    }
                    BigDecimal shareAmount = new BigDecimal(coin.getShareAmount());
                    BigDecimal price = new BigDecimal(usdPrice).setScale(8, RoundingMode.HALF_UP);
                    BigDecimal quantity = shareAmount.divide(price, 8, RoundingMode.HALF_UP);
                    BigDecimal investmentAmount = price.multiply(quantity).setScale(2, RoundingMode.HALF_UP);
                    coin.setPrice(price.toPlainString());
                    coin.setQuantity(quantity.toPlainString());
                    coin.setShareAmount(investmentAmount.toPlainString());
                } catch (IOException ex) {
                    Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                investmentActualDetails.add(coin);
            }
        }
        return investmentActualDetails;
    }

    @Override
    public List<ShareFund> sharesByInvestment(String investmentId) {
        List<ShareFund> shares = repository.sharesByInvestment(investmentId);
        if (!shares.isEmpty()) {
            HashMap<String, String> coinPriceMap = new HashMap();
            for (ShareFund share : shares) {
                if (share.getCoinId() != null && !share.getCoinId().isEmpty()) {
                    ShareFund coin = share;
                    String usdPrice = null;
                    try {
                        usdPrice = coinPriceMap.get(share.getCoinId());
                        if (usdPrice == null) {
                            usdPrice = coinMarketCapAPIService.getMarketPrice(coin.getCoinId());
                            coinPriceMap.put(coin.getCoinId(), usdPrice);
                        }
                        BigDecimal quantity = new BigDecimal(coin.getQuantity());
                        BigDecimal price = new BigDecimal(usdPrice).setScale(8, RoundingMode.HALF_UP);
                        BigDecimal shareAmount = price.multiply(quantity).setScale(2, RoundingMode.HALF_UP);
                        coin.setPrice(price.toPlainString());
                        coin.setQuantity(quantity.toPlainString());
                        coin.setShareAmount(shareAmount.toPlainString());
                    } catch (IOException ex) {
                        Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return shares;
    }

    @Override
    public List<ShareFund> actualSharesByAmount(String investmentId, String userId, String investmentAmount) {
        List<ShareFund> shares = repository.actualSharePercentages(investmentId);
        List<ShareFund> actualShareList = new ArrayList<>();
        BigDecimal amount = new BigDecimal(investmentAmount);
        for (ShareFund share : shares) {
            BigDecimal percentage = new BigDecimal(share.getPercentage() != null ? share.getPercentage() : "0.00");
            BigDecimal shareAmount = percentage.divide(Constants.HUNDRED, 8, RoundingMode.HALF_UP).multiply(amount);
            share.setShareAmount(shareAmount.toPlainString());
            BigDecimal quantity = shareAmount.divide(new BigDecimal(share.getPrice()), 8, RoundingMode.HALF_UP);
            share.setQuantity(quantity.toPlainString());
            actualShareList.add(share);
        }
        List<ShareFund> coins = repository.actualCoinPercentages(investmentId);
        if (!coins.isEmpty()) {
            HashMap<String, String> coinPriceMap = new HashMap();
            for (ShareFund coin : coins) {
                coin.setName(coin.getCoinId());
                BigDecimal percentage = new BigDecimal(coin.getPercentage() != null ? coin.getPercentage() : "0.00");
                BigDecimal coinAmount = percentage.divide(Constants.HUNDRED, 8, RoundingMode.HALF_UP).multiply(amount);
                coin.setShareAmount(coinAmount.toPlainString());
                String price = null;
                try {
                    price = coinPriceMap.get(coin.getCoinId());
                    if (price == null) {
                        price = coinMarketCapAPIService.getMarketPrice(coin.getCoinId());
                    }
                    coin.setPrice(price);
                    coinPriceMap.put(coin.getCoinId(), coin.getPrice());
                } catch (IOException ex) {
                    Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                BigDecimal quantity = coinAmount.divide(new BigDecimal(coin.getPrice()), 8, RoundingMode.HALF_UP);
                coin.setQuantity(quantity.toPlainString());
                actualShareList.add(coin);
            }
        }
        return actualShareList;
    }

    @Override
    public List<StateCity> countries(Boolean states, Boolean cities) {
        List<StateCity> countries = repository.countries(states, cities);
        HashMap<String, StateCity> map = new HashMap<>();
        for (StateCity curr : countries) {
            StateCity country = map.get(curr.getCode());
            if (country != null) {
                List<StateCity> details = country.getDetails();
                if (details == null) {
                    details = new ArrayList<>();
                }
                details.add(curr);
                country.setDetails(details);
            } else {
                map.put(curr.getCode(), curr);
            }
        }
        List<StateCity> list = new LinkedList<>();
        Set<Map.Entry<String, StateCity>> set = map.entrySet();
        for (Map.Entry<String, StateCity> entry : set) {
            list.add(entry.getValue());
        }
        return list;
    }

    private List<InvestmentBean> investmentList(List<InvestmentBean> investments, Boolean coins, String currency) throws IOException {
        HashMap<String, List<InvestmentBean>> map = new HashMap<>();
        for (InvestmentBean investment : investments) {
            List<InvestmentBean> list = map.get(investment.getInvestmentId());
            if (list == null) {
                list = new LinkedList<>();
            }
            list.add(investment);
            map.put(investment.getInvestmentId(), list);
        }
        HashMap<String, CryptoCoin> coinPriceMap = new HashMap();
        List<InvestmentBean> investmentList = new LinkedList<>();
        Set<Map.Entry<String, List<InvestmentBean>>> set = map.entrySet();
        for (Map.Entry<String, List<InvestmentBean>> entry : set) {
            List<InvestmentBean> list = entry.getValue();
            InvestmentBean inv = list.get(0);
            List<ShareFund> shareList = new LinkedList<>();
            BigDecimal currAmount = BigDecimal.ZERO;
            BigDecimal investedAmount = BigDecimal.ZERO;
            for (InvestmentBean fundShare : list) {
                ShareFund share = new ShareFund();
                share.setShareName(fundShare.getShareName());
                share.setShareId(fundShare.getShareId());
                share.setQuantity(fundShare.getQuantity());
                share.setPrice(fundShare.getPrice());
                BigDecimal value = new BigDecimal(fundShare.getValue());
                if (!currency.equalsIgnoreCase("USD")) {
                    if (currency.equalsIgnoreCase(inv.getCurrency())) {
                        value = new BigDecimal(fundShare.getLocalValue());
                    } else {
                        BigDecimal localPrice = currencyConversionRate("USD_" + currency, fundShare.getCreatedDate());
                        value = value.multiply(localPrice);
                    }
                }
                value = value.setScale(2, BigDecimal.ROUND_HALF_UP);
                share.setInvestedAmount(value.toPlainString());
                share.setShareAmount(fundShare.getValue());
                share.setCreatedDate(fundShare.getCreatedDate());
                if (coins) {
                    share.setCoinId(fundShare.getShareId());
                    CryptoCoin cryptoCoin = null;
                    try {
                        cryptoCoin = coinPriceMap.get(share.getCoinId());
                        if (cryptoCoin == null) {
                            cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(share.getCoinId());
                            coinPriceMap.put(share.getCoinId(), cryptoCoin);
                        }
                        share.setPercent1hr(cryptoCoin.getPercent1hr());
                        share.setPercent24hr(cryptoCoin.getPercent24hr());
                        share.setPercent7d(cryptoCoin.getPercent7d());
                        share.setPrice(String.valueOf(cryptoCoin.getUsdPrice()));
                        BigDecimal amount = new BigDecimal(share.getQuantity()).multiply(new BigDecimal(share.getPrice()));
                        amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
                        share.setShareAmount(amount.toString());
                    } catch (IOException ex) {
                        Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                BigDecimal getShareAmount = new BigDecimal(share.getShareAmount());
                if (share.getCreatedDate() != null) {
                    if (!currency.equalsIgnoreCase("USD")) {
                        BigDecimal localPrice = currencyConversionRate("USD_" + currency, null);
                        getShareAmount = getShareAmount.multiply(localPrice);
                        getShareAmount = getShareAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    }
                }
                share.setShareAmount(getShareAmount.toPlainString());
                currAmount = currAmount.add(getShareAmount);
                investedAmount = investedAmount.add(new BigDecimal(share.getInvestedAmount()));
                shareList.add(share);
            }
            currAmount = currAmount.setScale(2, BigDecimal.ROUND_CEILING);
            investedAmount = investedAmount.setScale(2, BigDecimal.ROUND_DOWN);
            inv.setValue(currAmount.toPlainString());
            inv.setInvestedAmount(investedAmount.toPlainString());
            inv.setFundActualShares(shareList);
            investmentList.add(inv);
        }
        for (InvestmentBean inv : investmentList) {
            if (!currency.equalsIgnoreCase("USD")) {
                if (currency.equalsIgnoreCase(inv.getCurrency())) {
                    inv.setInvestmentAmount(inv.getLocalInvestmentAmount());
                } else {
                    BigDecimal investmentAmount = new BigDecimal(inv.getInvestmentAmount());
                    BigDecimal localPrice = currencyConversionRate("USD_" + currency, inv.getCreatedDate());
                    investmentAmount = investmentAmount.multiply(localPrice);
                    investmentAmount = investmentAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    inv.setInvestmentAmount(investmentAmount.toPlainString());
                }
            }
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId, String currency) {
        List<InvestmentBean> investments = repository.purchasedFundInvestments(userId, investmentId, fundId);
        List<InvestmentBean> investmentList = null;
        try {
            investmentList = investmentList(investments, false, currency);
        } catch (IOException ex) {
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId, String currency) {
        List<InvestmentBean> investments = repository.purchasedPortfolioInvestments(userId, investmentId, fundId);
        List<InvestmentBean> investmentList = null;
        try {
            investmentList = investmentList(investments, true, currency);
        } catch (IOException ex) {
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> purchasedInvestments(String userId, String investmentId, String fundId, String currency) {
        List<InvestmentBean> fundInvestments = purchasedFundInvestments(userId, investmentId, fundId, currency);
        List<InvestmentBean> portfolioInvestments = purchasedPortfolioInvestments(userId, investmentId, fundId, currency);
        List<InvestmentBean> investmentList = new ArrayList<>();
        investmentList.addAll(fundInvestments);
        investmentList.addAll(portfolioInvestments);
        for (InvestmentBean investment : investmentList) {
            BigDecimal value = new BigDecimal(investment.getValue());
            BigDecimal investmentAmount = new BigDecimal(investment.getInvestmentAmount());
            String createdDate = investment.getCreatedDate();
            Date parseDate = common.parseDate(createdDate, CommonMethods.format2);
        }
        return investmentList;
    }

    @Override
    public List<InvestmentBean> coinsByInvestments(String userId, String investmentId, Boolean total, String fundId) {
        String currency = "USD";
        currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : "USD";
        List<InvestmentBean> investments = purchasedPortfolioInvestments(userId, investmentId, fundId, currency);
        Set<String> set = new HashSet<>();
        for (ListIterator<InvestmentBean> it = investments.listIterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            List<ShareFund> shares = inv.getFundActualShares();
            for (ListIterator<ShareFund> it1 = shares.listIterator(); it1.hasNext();) {
                ShareFund coin = it1.next();
                set.add(coin.getCoinId());
            }
        }
        for (ListIterator<InvestmentBean> it = investments.listIterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            HashMap<String, String> coinQuantityMap = new HashMap<>();
            for (Iterator<String> i = set.iterator(); i.hasNext();) {
                coinQuantityMap.put(i.next(), "0.000");
            }
            List<ShareFund> shares = inv.getFundActualShares();
            for (ListIterator<ShareFund> it1 = shares.listIterator(); it1.hasNext();) {
                ShareFund coin = it1.next();
                if (coinQuantityMap.containsKey(coin.getCoinId())) {
                    BigDecimal quantity = new BigDecimal(coinQuantityMap.get(coin.getCoinId()));
                    quantity = quantity.add(new BigDecimal(coin.getQuantity()));
                    coinQuantityMap.put(coin.getCoinId(), quantity.toPlainString());
                }
            }
            inv.setMap(coinQuantityMap);
        }
        if (total) {
            HashMap<String, String> coinQuantityMap = new HashMap<>();
            for (Iterator<String> i = set.iterator(); i.hasNext();) {
                coinQuantityMap.put(i.next(), "0.000");
            }
            for (Iterator<InvestmentBean> it = investments.iterator(); it.hasNext();) {
                InvestmentBean inv = it.next();
                Map<String, String> map = inv.getMap();
                Set<String> coins = coinQuantityMap.keySet();
                for (Iterator<String> it1 = coins.iterator(); it1.hasNext();) {
                    String coin = it1.next();
                    BigDecimal coinAmount = new BigDecimal(map.get(coin));
                    if (coinAmount.compareTo(BigDecimal.ZERO) == 1) {
                        BigDecimal coinQuantityAmount = new BigDecimal(coinQuantityMap.get(coin));
                        coinQuantityMap.put(coin, coinQuantityAmount.add(coinAmount).toPlainString());
                    }
                }
            }
            InvestmentBean totalBean = new InvestmentBean();
            totalBean.setFundName("Total");
            totalBean.setAction("background: #f0ad4e; color: #fff;");

            totalBean.setMap(coinQuantityMap);
            investments.add(totalBean);
        }
        return investments;
    }

    @Override
    public List<ShareFund> totalUnitsById(String fundId, String userId, String investmentId) {
        List<ShareFund> totalUnitsById = repository.totalUnitsById(fundId, userId, investmentId);
        double usdPrice = 0d;
        double nzdPrice = 0d;
        try {
            ObjectNode rateForPair = rateForPair("BTC", "USD,NZD", null);
            JsonNode curr = rateForPair.get("BTC");
            usdPrice = curr.get("USD").asDouble();
            nzdPrice = curr.get("NZD").asDouble();
        } catch (IOException ex) {
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        BigDecimal usd = new BigDecimal(usdPrice);
        BigDecimal nzd = new BigDecimal(nzdPrice);
        for (ShareFund fp : totalUnitsById) {
            BigDecimal quantity = new BigDecimal(fp.getQuantity()).setScale(16, RoundingMode.HALF_UP);
            fp.setUsd(usd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
            fp.setLocal(nzd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
        }
        return totalUnitsById;
    }

    @Override
    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate) {
        List<ShareFund> userUnitSummary = repository.userUnitSummary(userId, fromDate, toDate);
        double usdPrice = 0d;
        double nzdPrice = 0d;
        Map<String, ObjectNode> map = new HashMap<>();
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        for (ShareFund fp : userUnitSummary) {
            try {
                ObjectNode rateForPair = map.get(fp.getCreatedDate());
                if (rateForPair == null) {
                    Date dateTime = common.parseDate(fp.getCreatedDate(), dateFormat);
                    rateForPair = rateForPair("BTC", "USD,NZD", dateTime.getTime());
                    map.put(fp.getCreatedDate(), rateForPair);
                }
                if (rateForPair != null) {
                    JsonNode curr = rateForPair.get("BTC");
                    usdPrice = curr.get("USD").asDouble();
                    nzdPrice = curr.get("NZD").asDouble();
                }
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            BigDecimal usd = new BigDecimal(usdPrice);
            BigDecimal nzd = new BigDecimal(nzdPrice);
            BigDecimal quantity = new BigDecimal(fp.getQuantity()).setScale(16, RoundingMode.HALF_UP);
            fp.setUsd(usd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
            fp.setLocal(nzd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
        }
        return userUnitSummary;
    }

    @Override
    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals) {
        List<ShareFund> dailyUpdates = repository.dailyUpdates(fundId, userId, investmentId, individuals);
        Map<String, ObjectNode> map = new HashMap<>();
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        for (ShareFund fp : dailyUpdates) {
            String usdPrice = "0";
            String nzdPrice = "0";
            try {
                ObjectNode rateForPair = map.get(fp.getCreatedDate());
                if (rateForPair == null) {
                    Date dateTime = common.parseDate(fp.getCreatedDate(), dateFormat);
                    rateForPair = rateForPair("BTC", "USD,NZD", dateTime.getTime());
                    map.put(fp.getCreatedDate(), rateForPair);
                }
                if (rateForPair != null) {
                    JsonNode curr = rateForPair.get("BTC");
                    usdPrice = curr.get("USD").asText();
                    nzdPrice = curr.get("NZD").asText();
                }
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            BigDecimal usd = new BigDecimal(usdPrice);
            BigDecimal nzd = new BigDecimal(nzdPrice);
            BigDecimal quantity = new BigDecimal(fp.getQuantity()).setScale(16, RoundingMode.HALF_UP);
            fp.setUsd(usd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
            fp.setLocal(nzd.multiply(quantity).setScale(4, RoundingMode.HALF_UP).toPlainString());
        }
        return dailyUpdates;
    }

    @Override
    public Map<String, Object> saveStripeCharge(ChargeRequest chargeRequest, String userId) throws StripeException {
        try {
            Map<String, Object> map = new HashMap<>();
            Charge charge = stripeService.charge(chargeRequest);
            map.put("charge", charge);
            StripeCharge stripeCharge = new StripeCharge(charge);
            stripeCharge.setStripe_created_ts(common.dateFormat(new Date(charge.getCreated())));
            stripeCharge.setCreated_ts(common.dateFormat(new Date()));
            stripeCharge.setUser_id(userId);
            stripeCharge.setFund_id(chargeRequest.getFundId());
            int id = repository.saveStripeCharge(stripeCharge);
            stripeCharge.setId(String.valueOf(id));
            map.put("stripeCharge", stripeCharge);
            TransactionBean walletTxn = new TransactionBean(stripeCharge);
            double amountInCents = Double.parseDouble(stripeCharge.getAmount());
            double amountInDollars = amountInCents / 100;
            walletTxn.setLocal_amount(String.valueOf(amountInDollars));
            String timestamp = common.dateFormat(new Date());
            if (stripeCharge.getCurrency() != null
                    && !stripeCharge.getCurrency().isEmpty()
                    && !stripeCharge.getCurrency().equalsIgnoreCase("USD")) {
                BigDecimal price = currencyConversionRate(stripeCharge.getCurrency() + "_USD", timestamp);
                amountInDollars = amountInDollars * price.doubleValue();
            }
            walletTxn.setCreated_ts(timestamp);
            walletTxn.setAmount(String.valueOf(amountInDollars));
            walletTxn.setActive("Y");
            walletTxn.setInc_dec("Inc");
            walletTxn.setBrand("STRIPE");
            int tid = repository.saveWalletTransaction(walletTxn);
            walletTxn.setId(String.valueOf(tid));
            map.put("walletTxn", walletTxn);
            return map;
        } catch (IOException ex) {
            Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public ShareFund findFundPortfolio(String fundId
    ) {
        ShareFund fund = repository.fundById(fundId);
        if (fund == null) {
            fund = repository.portfolioById(fundId);
        }
        return fund;
    }

    @Override
    public List<ShareFund> totalCoins(String userId, String investmentId
    ) {
        List<ShareFund> coins = repository.totalCoins(userId, investmentId);
        BigDecimal totalMarketPrice = BigDecimal.ZERO;
        for (ShareFund coin : coins) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
            } catch (IOException ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal mktPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(cryptoCoin.getUsdPrice()));
                mktPrice = mktPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
                totalMarketPrice = totalMarketPrice.add(mktPrice);
                coin.setPrice(mktPrice.toString());
            }
        }
        for (ShareFund coin : coins) {
            coin.setShareName(coin.getCoinId().toUpperCase());
            BigDecimal percentage = new BigDecimal(coin.getPrice()).multiply(Constants.HUNDRED).divide(totalMarketPrice, 8, RoundingMode.HALF_UP);
            percentage = percentage.setScale(2, BigDecimal.ROUND_HALF_UP);
            coin.setPercentage(percentage.toString());
        }
        return coins;
    }

    @Override
    public Map<Date, BigDecimal> latestBalanceData(String type, String userId,
            String investmentId
    ) {
        List<ShareFund> latestBalanceData = repository.latestBalanceData(type, userId, investmentId);
        Map<String, CryptoCoin> coinMap = new LinkedHashMap<>();
        Map<Date, List<ShareFund>> map = new LinkedHashMap<>();
        for (ShareFund coin : latestBalanceData) {
            CryptoCoin cryptoCoin = null;
            try {
                cryptoCoin = coinMap.get(coin.getCoinId());
                if (cryptoCoin == null) {
                    cryptoCoin = coinMarketCapAPIService.getSingleCoinMarketCapCoin(coin.getCoinId());
                    coinMap.put(coin.getCoinId(), cryptoCoin);
                }
            } catch (Exception ex) {
                Logger.getLogger(DatabaseServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (cryptoCoin != null) {
                BigDecimal mktPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(cryptoCoin.getUsdPrice()));
                mktPrice = mktPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
                coin.setShareAmount(mktPrice.toString());
                BigDecimal invPrice = new BigDecimal(coin.getQuantity()).multiply(new BigDecimal(coin.getPrice()));
                invPrice = invPrice.setScale(2, BigDecimal.ROUND_HALF_UP);
                coin.setPrice(invPrice.toString());
            }
            Date date = common.parseDate(coin.getCreatedDate(), common.format2);
            date.setHours(0);
            date.setMinutes(0);
            date.setSeconds(0);
            List<ShareFund> list = map.get(date);
            if (list == null) {
                list = new LinkedList();
            }
            list.add(coin);
            map.put(date, list);
        }
        Map<Date, BigDecimal> map1 = new LinkedHashMap<>();
        Set<Map.Entry<Date, List<ShareFund>>> set = map.entrySet();
        BigDecimal amount = BigDecimal.ZERO;
        for (Iterator<Map.Entry<Date, List<ShareFund>>> it = set.iterator(); it.hasNext();) {
            Map.Entry<Date, List<ShareFund>> entry = it.next();
            Date key = entry.getKey();
            List<ShareFund> list = entry.getValue();
            BigDecimal value = BigDecimal.ZERO;
            for (ShareFund coin : list) {
                value = value.add(new BigDecimal(coin.getShareAmount()));
                amount = amount.add(new BigDecimal(coin.getPrice()));
            }
            map1.put(key, value);
        }
        return map1;
    }

    @Override
    public List<ShareFund> detailsByInvestment(String userId, String investmentId,
            String action
    ) {
        List<ShareFund> shares = repository.detailsByInvestment(userId, investmentId, action);
        for (ShareFund share : shares) {
            if (share.getShareId() == null) {
                share.setShareName(share.getCoinId());
            }
        }
        return shares;
    }

    @Override
    public void addTrade(ShareFund trade) {
        Date date = common.parseDate(trade.getCreatedDate(), "yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, trade.getHh());
        cal.set(Calendar.MINUTE, trade.getMm());
        cal.set(Calendar.SECOND, trade.getSs());
        String dateFormat = common.dateFormat(cal.getTime());
        List<ShareFund> totalUnitsById = repository.totalUnitsById(trade.getFundId(), null, null);
        BigDecimal totalMinto = BigDecimal.ZERO;
        BigDecimal totalBtc = new BigDecimal(trade.getCloseBal());
        for (ShareFund unit : totalUnitsById) {
            totalMinto = totalMinto.add(new BigDecimal(unit.getMinto()));
        }
//      currentBtc =  individualMinto / totalMinto * totalBtc;
        for (ShareFund unit : totalUnitsById) {
            BigDecimal individualMinto = new BigDecimal(unit.getMinto());
            BigDecimal currentBtc = individualMinto.divide(totalMinto, 16, RoundingMode.HALF_UP).multiply(totalBtc);
            unit.setBtc(currentBtc.toPlainString());
            unit.setCreatedDate(dateFormat);
        }
        trade.setCreatedDate(dateFormat);
        repository.addTrade(trade, totalUnitsById);
    }

    @Override
    public void saveTodayUpdates() {
        String currency = "USD";
        List<UserFundsCommand> userFunds = repository.userFunds(null, null, null);
        for (UserFundsCommand userFund : userFunds) {
            userFund.setInvestment_amount("0");
            List<InvestmentBean> purchasedInvestments = purchasedInvestments(userFund.getUser_id(), userFund.getInvestment_id(), userFund.getFund_id(), currency);
            InvestmentBean inv = purchasedInvestments.get(0);
            userFund.setInvestment_amount(inv.getInvestmentAmount());
        }
        repository.saveUserTodayStatus(userFunds);
    }

    @Override
    public ObjectNode rateForPair(String currency0, String currency1,
            Long timestamp) throws IOException {
        if (timestamp != null && timestamp > 0) {
            ObjectNode rateForPair = coinMarketCapAPIService.getRateForPair(currency0, currency1, timestamp);
            return rateForPair;
        } else {
            ObjectNode rateForPair = coinMarketCapAPIService.getRateForPair(currency0, currency1);
            return rateForPair;
        }
    }

    @Override
    public BigDecimal currencyConversionRate(String currencyPair, String ts) throws IOException {
        String date = common.ts2date(ts);
        ShareFund conversionRate = repository.currencyConversionRate(currencyPair, date);
        String price = null;
        if (conversionRate != null && conversionRate.getId() != null) {
            price = conversionRate.getPrice();
            if (price == null) {
                ObjectNode response = coinMarketCapAPIService.currencyConversionRate(currencyPair, date);
                JsonNode rateNode = response.get(currencyPair);
                price = rateNode.asText();
                repository.updateCurrencyConversionRate(currencyPair, conversionRate.getId(), price);
            }
            if (price != null) {
                return new BigDecimal(price);
            }
        } else {
            ObjectNode response = coinMarketCapAPIService.currencyConversionRate(currencyPair, date);
            JsonNode rateNode = response.get(currencyPair);
            price = rateNode.asText();
            if (price != null) {
                repository.saveCurrencyConversionRate(currencyPair, date, price);
                return new BigDecimal(price);
            }
        }
        return null;
    }

    @Autowired
    private StripeService stripeService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private BittrexAccountAPIService bittrexAccountAPIService;
}
