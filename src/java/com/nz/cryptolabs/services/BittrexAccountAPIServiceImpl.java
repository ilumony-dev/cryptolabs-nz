package com.nz.cryptolabs.services;

import com.google.gson.Gson;
import com.nz.cryptolabs.bittrex.BittrexExchangeService;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalanceContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalancesContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetDepositAddressContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetDepositHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetWithdrawlHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.marketapi.GetOpenOrdersContainer;
import com.nz.cryptolabs.constants.Constants;
import org.springframework.stereotype.Service;

@Service
public class BittrexAccountAPIServiceImpl implements BittrexAccountAPIService {

    private final String userApiKey = Constants.bittrexApiKey;
    private final String userSecret = Constants.bittrexSecret;
    private final Gson gson = new Gson();
    private final BittrexExchangeService bittrex = new BittrexExchangeService(userApiKey, userSecret);

    @Override
    public String buyLimit(String market, Double quanity, Double rate) {
        String response = bittrex.buyLimit(market, quanity, rate);
        return response;
    }

    @Override
    public String sellLimit(String market, Double quanity, Double rate) {
        String response = bittrex.sellLimit(market, quanity, rate);
        return response;
    }

    @Override
    public String cancel(String uuid) {
        String response = bittrex.cancel(uuid);
        return response;
    }

    @Override
    public GetOpenOrdersContainer getOpenOrders() {
        String response = bittrex.getOpenOrders();
        GetOpenOrdersContainer getOpenOrdersContainer = gson.fromJson(response, GetOpenOrdersContainer.class);
        return getOpenOrdersContainer;
    }

    @Override
    public GetOpenOrdersContainer getOpenOrders(String market) {
        String response = bittrex.getOpenOrders(market);
        GetOpenOrdersContainer getOpenOrdersContainer = gson.fromJson(response, GetOpenOrdersContainer.class);
        return getOpenOrdersContainer;
    }

    @Override
    public GetBalancesContainer getBalances() throws NullPointerException {
        String response = bittrex.getBalances();
        GetBalancesContainer getBalancesContainer = gson.fromJson(response, GetBalancesContainer.class);
        return getBalancesContainer;
    }

    @Override
    public GetBalanceContainer getBalance(String currency) throws NullPointerException {
        String response = bittrex.getBalance(currency);
        GetBalanceContainer getBalanceContainer = gson.fromJson(response, GetBalanceContainer.class);
        return getBalanceContainer;
    }

    @Override
    public String badCoinGetBalance() {
        String badcoin = "SHTCOIN";
        String response = bittrex.getBalance(badcoin);
        return response;
    }

    @Override
    public GetDepositAddressContainer getDepositAddress(String currency) {
        String response = bittrex.getBalance(currency);
        GetDepositAddressContainer getDepositAddressContainer = gson.fromJson(response, GetDepositAddressContainer.class);
        return getDepositAddressContainer;
    }

    //@Test
    public void testWithdraw() {
        //Assert.assertTrue(badCrendentialMsg, checkCredentials());
    }

    @Override
    public GetOrderContainer getOrder(String uuid) {
        String response = bittrex.getOrder(uuid);
        GetOrderContainer getOrderContainer = gson.fromJson(response, GetOrderContainer.class);
        return getOrderContainer;
    }

//     If you want to test the validity of of the function, fill in the uuid with a valid uuid from a past account order. Then uncomment
//    //@Test
//    public void testGetOrderValidUUID() {
//        //Assert.assertTrue(badCrendentialMsg, checkCredentials());
//        String uuid = ""; //fill in
//        String response = bittrex.getOrder(uuid);
//        //Assert.assertNotNull(nullResponseMsg, response);
//        GetOrderContainer getOrderContainer = gson.fromJson(response, GetOrderContainer.class);
//        //Assert.assertNotNull(objNullMsg("GetOrderContainer"), getOrderContainer);
//        Order order = getOrderContainer.getOrder();
//        //Assert.assertNotNull(objNullMsg("Order"), order);
//        //Assert.assertNotNull("Exchange", order.getExchange());
//        //Assert.assertEquals("Uuids should match", uuid, order.getOrderUuid());
//    }
    @Override
    public GetOrderHistoryContainer getOrderHistory() {
        String response = bittrex.getOrderHistory();
        GetOrderHistoryContainer getOrderHistoryContainer = gson.fromJson(response, GetOrderHistoryContainer.class);
        return getOrderHistoryContainer;
    }

    @Override
    public GetWithdrawlHistoryContainer getWithdrawlHistory() {
        String response = bittrex.getWithdrawalHistory();
        GetWithdrawlHistoryContainer getWithdrawlHistoryContainer = gson.fromJson(response, GetWithdrawlHistoryContainer.class);
        return getWithdrawlHistoryContainer;
    }

    @Override
    public GetWithdrawlHistoryContainer getWithdrawlHistoryCurrency(String currency) {
        String response = bittrex.getWithdrawalHistory(currency);
        GetWithdrawlHistoryContainer getWithdrawlHistoryContainer = gson.fromJson(response, GetWithdrawlHistoryContainer.class);
        return getWithdrawlHistoryContainer;
    }

    @Override
    public GetWithdrawlHistoryContainer getWithdrawlHistoryBTC() {
        return getWithdrawlHistoryCurrency("BTC");
    }

    @Override
    public GetDepositHistoryContainer getDepositHistory() {
        String response = bittrex.getDepositHistory();
        GetDepositHistoryContainer getDepositHistoryContainer = gson.fromJson(response, GetDepositHistoryContainer.class);
        return getDepositHistoryContainer;
    }

    @Override
    public GetDepositHistoryContainer getDepositHistoryCurrency(String coin) {
        String response = bittrex.getDepositHistory(coin);
        GetDepositHistoryContainer getDepositHistoryContainer = gson.fromJson(response, GetDepositHistoryContainer.class);
        return getDepositHistoryContainer;
    }

    @Override
    public GetDepositHistoryContainer getDepositHistoryBTC() {
        return getDepositHistoryCurrency("BTC");
    }

    private boolean checkCredentials() {
        if (userSecret.isEmpty() || userApiKey.isEmpty()) {
            return false;
        } else {
            GetBalanceContainer gbc = gson.fromJson(bittrex.getBalance("BTC"), GetBalanceContainer.class);
            return gbc.getSuccess();
        }
    }

    private String objNullMsg(String objectName) {
        return objectName + " should not be null. Please review discrepencies in JSON architecture between Bittrex and this package.";
    }

    private final String badCrendentialMsg = "Invalid Apikey and/or Secret. Valid credentials required for account queries.";

    private final String falseFlagMsg = "Success flag should be true";

    private final String falseFlagConsistentMsg = "False flag should result in same message";

    private final String nullResponseMsg = "Response from Bittrex should not be null";
}
