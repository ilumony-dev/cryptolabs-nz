package com.nz.cryptolabs.services;

import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.repositories.CommonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import java.util.Date;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private CommonRepository userRepository;
    @Autowired
    private CommonMethods common;

    @Override
    @Transactional(readOnly = false)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserInfo userInfo = userRepository.findByUsername(username);
        if (userInfo != null) {
            Configuration config = userRepository.findConfiguration(userInfo.getUserId());
            userRepository.save(userInfo.getUserId(), "LOGGED IN");
            GrantedAuthority authority = new SimpleGrantedAuthority(userInfo.getRole());
            SecuredUser user = new SecuredUser(userInfo.getEmail(),
                    userInfo.getPassword(), Arrays.asList(authority));
            user.setEmail(userInfo.getEmail());
            user.setUserId(userInfo.getUserId());
            user.setFullName(userInfo.getFullName());
            user.setDob(userInfo.getDob());
            user.setCreatedTs(userInfo.getCreatedTs());
            Date createdTs = common.parseDate(user.getCreatedTs(), CommonMethods.format2);
            user.setCreatedTime(createdTs.getTime());
            user.setMobileNo(userInfo.getMobileNo());
            user.setRefId(userInfo.getRefId());
            user.setUser("ROLE_USER".equalsIgnoreCase(userInfo.getRole()));
            user.setAdmin("ROLE_ADMIN".equalsIgnoreCase(userInfo.getRole()));
            user.setConfig(config);
            return user;
        } else {
            return null;
        }
    }
}
