package com.nz.cryptolabs.services;

import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalanceContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalancesContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetDepositAddressContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetDepositHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetWithdrawlHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.marketapi.GetOpenOrdersContainer;

public interface BittrexAccountAPIService {

    public String buyLimit(String market, Double quanity, Double rate);

    public String sellLimit(String market, Double quanity, Double rate);

    public String cancel(String uuid);

    public GetOpenOrdersContainer getOpenOrders();

    public GetOpenOrdersContainer getOpenOrders(String market);

    public GetBalancesContainer getBalances() throws NullPointerException;

    public GetBalanceContainer getBalance(String currency) throws NullPointerException;

    public String badCoinGetBalance();

    public GetDepositAddressContainer getDepositAddress(String currency);

    public GetOrderContainer getOrder(String uuid);

    public GetOrderHistoryContainer getOrderHistory();

    public GetWithdrawlHistoryContainer getWithdrawlHistory();

    public GetWithdrawlHistoryContainer getWithdrawlHistoryCurrency(String currency);

    public GetWithdrawlHistoryContainer getWithdrawlHistoryBTC();

    public GetDepositHistoryContainer getDepositHistory();

    public GetDepositHistoryContainer getDepositHistoryCurrency(String currency);

    public GetDepositHistoryContainer getDepositHistoryBTC();
}
