/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.json.JSONObject;

/**
 *
 * @author Administrator
 */
public interface ObjectCastService {

    public <T> T jSONcast(Class<T> clazz, String jsonInString) throws IOException;

    public <T> T jSONcast(Class<T> clazz, File jsonInFile) throws IOException;

    public <T> T jSONcast(Class<T> clazz, URL jsonInURL) throws IOException;

    public <T> T cast(Class<T> clazz, Object object);

    public JSONObject parseJson(String jsonString);
}
