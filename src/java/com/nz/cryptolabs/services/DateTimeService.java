/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.services;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Maninderjit Singh
 */
public interface DateTimeService {

    public void setZoneId(String zoneId);

    public Date today0000();

    public Date today1200();

    public Date now();

    public long timeDate();

    public Calendar calendar();
}
