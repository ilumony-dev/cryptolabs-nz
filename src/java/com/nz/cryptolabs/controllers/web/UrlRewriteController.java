/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.services.SessionManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.repositories.CommonRepository;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Maninderjit
 */
@Controller
public class UrlRewriteController {

    @Autowired
    private SessionManagementService sessionManagementService;

    @Autowired
    private CommonRepository repository;

    @RequestMapping(value = {"/welcome", "/dashboard", "/home"}, method = RequestMethod.GET)
    public String welcome(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getAuthorities().contains(Constants.superAdminRole)) {
                return "forward:/admin-dashboard";
            } else if (user.getAuthorities().contains(Constants.adminRole)) {
                return "forward:/admin-dashboard";
            } else {
                Verification verification = repository.findVerification(user.getUserId());
                if (verification != null) {
                    return "forward:/user-dashboard";
                } else {
                    return "forward:/verification";
                }
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/loggedin"}, method = RequestMethod.GET)
    public String loggedin(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getAuthorities().contains(Constants.superAdminRole) || user.getAuthorities().contains(Constants.adminRole)) {
                return "forward:/admin-dashboard";
            } else {
                Verification verification = repository.findVerification(user.getUserId());
                if (verification != null) {
                    return "forward:/investments";
                } else {
                    return "forward:/verification";
                }
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/inoetreoirpewirp", "/1"}, method = RequestMethod.GET)
    public String urlrewrite1(ModelMap modelMap) {
        return "forward:/investments";
    }

    @RequestMapping(value = {"/inksdjflsdjrjwel", "/2"}, method = RequestMethod.GET)
    public String urlrewrite2(ModelMap modelMap) {
        return "forward:/insights";
    }

    @RequestMapping(value = {"/prsdldfjlsjrjewli", "/3"}, method = RequestMethod.GET)
    public String urlrewrite3(ModelMap modelMap) {
        return "forward:/profile";
    }

    @RequestMapping(value = {"/paksdjfljdsfudskd", "/4"}, method = RequestMethod.GET)
    public String urlrewrite4(ModelMap modelMap) {
        return "forward:/payment";
    }

    @RequestMapping(value = {"/vedjjfldshjfldskjf", "/5"}, method = RequestMethod.GET)
    public String urlrewrite5(ModelMap modelMap) {
        return "forward:/verification";
    }

    @RequestMapping(value = {"/suflsdjweafkjsdlkj", "/6"}, method = RequestMethod.GET)
    public String urlrewrite6(ModelMap modelMap,
            @RequestParam("ok") String id,
            @RequestParam(value = "un", required = false) String uid) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            if (user.getAdmin() && uid != null) {
                return "forward:/summary?investmentId=" + id + "&userId=" + uid;
            } else {
                return "forward:/summary?investmentId=" + id;
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/rehjkgjgtfdqwgfss", "/7"}, method = RequestMethod.GET)
    public String urlrewrite7(ModelMap modelMap) {
        return "forward:/refferal";
    }

    @RequestMapping(value = {"/coasdflkerewrweasd", "/8"}, method = RequestMethod.GET)
    public String urlrewrite8(ModelMap modelMap) {
        return "forward:/config";
    }

    @RequestMapping(value = {"/heasdfjsdajflsdeay", "/9"}, method = RequestMethod.GET)
    public String urlrewrite9(ModelMap modelMap) {
        return "forward:/headway";
    }

}
