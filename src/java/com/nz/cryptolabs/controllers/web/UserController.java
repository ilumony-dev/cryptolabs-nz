/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.mail.MailManager;
import com.nz.cryptolabs.services.SessionManagementService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Maninderjit
 */
@Controller
@Secured(value = {"ROLE_USER", "ROLE_ADMIN"})
public class UserController {

    public void progressBar(ModelMap modelMap, String userId) {
        Verification ver = repository.findVerification(userId);
        Integer progress = 0;
        if (ver.getInvite_code() != null && !ver.getInvite_code().isEmpty()) {
            progress += 5;
        }
        if (ver.getAddress() != null && !ver.getAddress().isEmpty()) {
            progress += 5;
        }
        if (ver.getPp_passport_number() != null && !ver.getPp_passport_number().isEmpty()) {
            progress += 5;
        }
        if (ver.getPa_number() != null && !ver.getPa_number().isEmpty()) {
            progress += 5;
        }
        if (ver.getDl_license_number() != null && !ver.getDl_license_number().isEmpty()) {
            progress += 5;
        }
        List<UserFundsCommand> userFunds = repository.userFunds(null, userId, null);
        if (!userFunds.isEmpty()) {
            progress += 25;
        }
        String currency = "USD";
        currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : "USD";
        List<InvestmentBean> userInvestments = databaseService.purchasedInvestments(userId, null, null, currency);
        if (!userInvestments.isEmpty()) {
            progress += 25;
        }
        List<ShareFund> totalCoins = databaseService.totalCoins(userId, null);
        if (!totalCoins.isEmpty()) {
            progress += 25;
        }
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        Date date = cal.getTime();
        List<UserFundsCommand> latestWeek = repository.getUserTodayStatus(null, userId, null, common.dateFormat(date, CommonMethods.format3), null);
        modelMap.addAttribute("progress", progress);
        modelMap.addAttribute("verification", ver);
        modelMap.addAttribute("userFunds", userFunds);
        modelMap.addAttribute("userInvestments", userInvestments);
        modelMap.addAttribute("totalcoins", totalCoins);
        modelMap.addAttribute("currency", currency);
        modelMap.addAttribute("currSymbol", common.currSymbol().get(currency));
        modelMap.addAttribute("latestWeek", latestWeek);
    }
    @Autowired
    private MailManager mailManager;

    @RequestMapping(value = {"/user-dashboard"}, method = RequestMethod.GET)
    public String userDashboard(ModelMap modelMap, Locale loc,
            @RequestParam(value = "un", required = false) String userId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
//            mailManager.sendConfirmationMailToEmail("maninderjit@ice.red", "Maninderjit Singh");
//            mailManager.sendWelcomeToNewClient("maninderjit@ice.red", "Maninderjit Singh");
            if (user.getAdmin() && userId != null) {
                UserInfo endUser = repository.findByUserId(userId);
                Date createdTs = common.parseDate(user.getCreatedTs(), CommonMethods.format2);
                endUser.setCreatedTime(createdTs.getTime());
                progressBar(modelMap, endUser.getUserId());
                modelMap.addAttribute("endUser", endUser);
            } else {
                progressBar(modelMap, user.getUserId());
                modelMap.addAttribute("endUser", user);
            }
            return "user-dashboard";

        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"summary"}, method = RequestMethod.GET)
    public String investmentDashboard(ModelMap modelMap,
            @RequestParam("investmentId") String investmentId,
            @RequestParam(value = "userId", required = false) String userId) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            if (user.getUser()) {
                userId = user.getUserId();
            }
            String currency = "USD";
            currency = common.userCurrency().get(userId) != null ? common.userCurrency().get(userId) : "USD";
            if ((user.getUser() || user.getAdmin()) && userId != null) {
                InvestmentBean inv = databaseService.purchasedInvestments(userId, investmentId, null, currency).get(0);
                List<ShareFund> fundActualShares = new LinkedList<>();
                List<ShareFund> shares = inv.getFundActualShares();
                if (shares != null && !shares.isEmpty()) {
                    Map<String, List<ShareFund>> map = new HashMap<>();
                    for (ShareFund share : shares) {
                        String shareId = share.getShareId();
                        if (map.get(shareId) == null) {
                            map.put(shareId, new LinkedList<>());
                        }
                        List<ShareFund> list = map.get(shareId);
                        list.add(share);
                        map.put(shareId, list);
                    }
                    Set<Map.Entry<String, List<ShareFund>>> entrySet = map.entrySet();
                    for (Iterator<Map.Entry<String, List<ShareFund>>> iterator = entrySet.iterator(); iterator.hasNext();) {
                        Map.Entry<String, List<ShareFund>> entry = iterator.next();
                        List<ShareFund> list = entry.getValue();
                        ShareFund share = list.get(0);
                        if (list.size() > 1) {
                            BigDecimal sA = new BigDecimal(share.getShareAmount());
                            BigDecimal iA = new BigDecimal(share.getInvestedAmount());
                            for (int i = 1; i < list.size(); i++) {
                                ShareFund shr = list.get(i);
                                BigDecimal sA1 = new BigDecimal(shr.getShareAmount());
                                sA = sA.add(sA1);
                                BigDecimal iA1 = new BigDecimal(shr.getInvestedAmount());
                                iA = iA.add(iA1);
                            }
                            share.setShareAmount(sA.toPlainString());
                            share.setInvestedAmount(iA.toPlainString());
                            fundActualShares.add(share);
                        } else {
                            fundActualShares.add(share);
                        }
                    }
                    Collections.sort(fundActualShares, new Comparator<ShareFund>() {
                        @Override
                        public int compare(ShareFund o1, ShareFund o2) {
                            return new BigDecimal(o2.getShareAmount()).compareTo(new BigDecimal(o1.getShareAmount()));
                        }
                    });
                }

                List<ShareFund> coinsByFund = repository.coinsByFund(inv.getFundId());
                for (ShareFund share : fundActualShares) {
                    share.sameNameUpdatePercentage(coinsByFund);
                }
                inv.setFundActualShares(fundActualShares);
                List< ShareFund> top5 = null;
                if (fundActualShares.size() > 4) {
                    top5 = fundActualShares.subList(0, 5);
                } else {
                    top5 = fundActualShares;
                }
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_MONTH, -7);
                Date date = cal.getTime();
                List<UserFundsCommand> latestWeek = repository.getUserTodayStatus(null, userId, investmentId, common.dateFormat(date, CommonMethods.format3), null);
                modelMap.addAttribute("investment", inv);
                modelMap.addAttribute("portfolio", repository.portfolioById(inv.getFundId()));
                modelMap.addAttribute("top5Shares", top5);
                modelMap.addAttribute("coinsByFund", coinsByFund);
                modelMap.addAttribute("currency", currency);
                modelMap.addAttribute("currSymbol", common.currSymbol().get(currency));
                modelMap.addAttribute("latestWeek", latestWeek);
            }
            return "investment-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/investments", method = RequestMethod.GET)
    public String investments(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            List<ShareFund> funds = repository.funds();
            List<ShareFund> portfolios = repository.portfolios();
            modelMap.addAttribute("funds", funds);
            modelMap.addAttribute("portfolios", portfolios);
            modelMap.addAttribute("amount", 0); // in cents
            modelMap.addAttribute("stripePublicKey", stripePublicKey);
            String currency = paymentCurrency != null && !paymentCurrency.isEmpty() ? paymentCurrency : "USD";
            modelMap.addAttribute("paymentCurrency", currency);
            modelMap.addAttribute("currSymbol", common.currSymbol().get(currency));
            modelMap.addAttribute("domain", domain);
            List<TransactionBean> walletSummary = repository.userWalletTransactions(user.getUserId());
            if (!walletSummary.isEmpty()) {
                modelMap.addAttribute("walletSummary", walletSummary);
                modelMap.addAttribute("walletBalance", walletSummary.get(walletSummary.size() - 1).getBalance());
                modelMap.addAttribute("lastDeposite", walletSummary.get(walletSummary.size() - 1).getAmount());
            }
            return "investments";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/insights"}, method = RequestMethod.GET)
    public String insights(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            return "insights";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/profile"}, method = RequestMethod.GET)
    public String profile(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            return "profile";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/payment"}, method = RequestMethod.GET)
    public String payment(ModelMap modelMap) throws IOException, JSONException {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            List<TransactionBean> accountSummary = repository.userAccountSummary(user.getUserId());
            modelMap.addAttribute("accountSummary", accountSummary);
            List<TransactionBean> walletSummary = repository.userWalletTransactions(user.getUserId());
            if (!walletSummary.isEmpty()) {
                modelMap.addAttribute("walletSummary", walletSummary);
                modelMap.addAttribute("walletBalance", walletSummary.get(walletSummary.size() - 1).getBalance());
                modelMap.addAttribute("lastDeposite", walletSummary.get(walletSummary.size() - 1).getAmount());
            }
            modelMap.addAttribute("amount", 0); // in cents
            modelMap.addAttribute("stripePublicKey", stripePublicKey);
            String currency = "USD";
            modelMap.addAttribute("currency", currency);
            modelMap.addAttribute("currSymbol", common.currSymbol().get(currency));
            return "payment";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verification"}, method = RequestMethod.GET)
    public String verification(ModelMap model) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            return "verification";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verify-new"}, method = RequestMethod.GET)
    public String verifyNew(ModelMap model) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            return "verification2";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/refferal"}, method = RequestMethod.GET)
    public String refferal(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            modelMap.addAttribute("email", user.getUsername());
            modelMap.addAttribute("fullName", user.getFullName());
            return "referral";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/config"}, method = RequestMethod.GET)
    public String config(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            return "config";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/add-investment", "/bank-transfer-add-investment"}, method = RequestMethod.POST)
    public String bankTransferAddInvestment(ModelMap model, @ModelAttribute("investment") InvestmentBean invBean) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            invBean.setUserId(user.getUserId());
            invBean.setCreatedDate(common.dateFormat(new Date()));
            if (invBean.getTimeFrame() == null) {
                invBean.setTimeFrame("FIXED");
            }
            List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
            invBean.setFundActualShares(fundActualShares);
            repository.saveInvestment(invBean);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/via-wallet-add-investment"}, method = RequestMethod.POST)
    public String viaWalletAddInvestment(ModelMap model, @ModelAttribute("investment") InvestmentBean invBean) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            invBean.setUserId(user.getUserId());
            invBean.setCreatedDate(common.dateFormat(new Date()));
            if (invBean.getTimeFrame() == null) {
                invBean.setTimeFrame("FIXED");
            }
            List<ShareFund> fundActualShares = databaseService.fundActualShares(invBean.getFundId(), invBean.getInvestmentAmount());
            invBean.setFundActualShares(fundActualShares);
            repository.saveInvestment(invBean);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String accountEvent(ModelMap model, @ModelAttribute("event") AccountEventCommand UserEvent) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            UserEvent.setUserId(user.getUserId());
            repository.saveAccountingEvent(UserEvent);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/verification"}, method = RequestMethod.POST)
    public String verification(ModelMap model, @ModelAttribute("verificaiton") Verification verification) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            verification.setUser_id(user.getUserId());
            verification.setCreated_ts(common.dateFormat(new Date()));
            repository.saveVerification(verification);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/config"}, method = RequestMethod.POST)
    public String config(ModelMap model, @ModelAttribute("config") Configuration config) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            config.setUserId(user.getUserId());
            config.setCreatedTs(common.dateFormat(new Date()));
            repository.saveConfiguration(config);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/currency"}, method = RequestMethod.POST)
    public String config(ModelMap model, @ModelAttribute("currency") String currency) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            common.userCurrency().put(user.getUserId(), currency);
            return "redirect:/welcome";
        } else {
            return "redirect:/login";
        }
    }

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private String domain;
    @Autowired
    private String paymentCurrency;

    final private String stripePublicKey = "pk_test_zrCXgtyeFkNkUj4HjXvEdnah";

}
