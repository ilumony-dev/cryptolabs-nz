/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.SecurityService;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.HTTPRequestManager;
import com.nz.cryptolabs.constants.Constants;
import com.nz.cryptolabs.mail.ExpiryDate;
import com.nz.cryptolabs.mail.MailConstants;
import com.nz.cryptolabs.mail.MailManager;
import com.nz.cryptolabs.mail.ResetPasswordForm;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Administrator
 */
@Controller(value = "/")
public class RegisterController {

    private User getPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof User) {
                return ((User) principal);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
    public String register(Model model) {
        model.addAttribute("adminEmailId", adminEmailId);
        model.addAttribute("kickboxApiKey", Constants.kickboxApiKey);
        model.addAttribute("kickboxAppCode", Constants.kickboxAppCode);
        return "registration";
    }

    @RequestMapping(value = {"/register"}, method = RequestMethod.POST)
    public String registerPost(Model model, @ModelAttribute("user") UserInfo user) {
        if (repository.findByUsername(user.getEmail().trim()) != null) {
            return "forward:/forgetpwd";
        }
        user.setRole("ROLE_USER");
        user.setCreatedTs(common.dateFormat(new Date()));
        user.setMobileNo(user.getCountryCode() + user.getMobileNo());
        repository.save(user);
        securityService.autologin(user.getEmail(), user.getPassword());
//        mailManager.sendConfirmationMailToEmail("maninderjit@ice.red", "Maninderjit Singh");
//        mailManager.sendWelcomeToNewClient(user.getEmail(), user.getFullName());
        return "redirect:/loggedin";
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String redirect(ModelMap model) {
        return "redirect:/login";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(ModelMap model, @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {
        if (error != null) {
            model.addAttribute("title", "Error");
            model.addAttribute("message", "Invalid username and password!");
        }
        if (logout != null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null) {
//                Object principal = auth.getPrincipal();
//                if (principal instanceof SecuredUser) {
//                    SecuredUser user = ((SecuredUser) principal);
//                    repository.save(user.getUserId(), "LOGGED OUT");
//                }
                SecurityContextHolder.getContext().setAuthentication(null);
            }
            model.addAttribute("title", "Logout");
            model.addAttribute("message", "You've been logged out successfully.");
        }
        if (getPrincipal() != null) {
            return "redirect:/welcome";
        }
        return "login";
    }

    //for 403 access denied page
    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String accesssDenied(ModelMap model) {
        return "403";
    }

    @RequestMapping(value = {"/forgetpwd"}, method = RequestMethod.GET)
    public String forgetPasswordPage(ModelMap modelMap) {
        return "forgetPassword";
    }

    @RequestMapping(value = {"/forgetpwd"}, method = RequestMethod.POST)
    public String forgetPasswordPage(ModelMap modelMap, @RequestParam("email") String email) {
        UserInfo user = repository.findByUsername(email);
        if (user != null && user.getEmail() != null) {
            mailManager.sendResetToUser(user.getEmail(), user.getFullName());
            modelMap.addAttribute("title", "Sent Message");
            modelMap.addAttribute("message", "We have sent a link on your email.");
            return "login";
        } else {
            modelMap.addAttribute("title", "Invalid Email");
            modelMap.addAttribute("message", "That email has not found.");
            return "login";
        }
    }

    @RequestMapping(value = {"/resetpwd"}, method = RequestMethod.GET)
    public String resetPasswordPage(ModelMap modelMap, @RequestParam("token") String token,
            @RequestParam("csrf") String csrf) {
        ExpiryDate date = MailConstants.map.get(token);
        if (date != null) {
            UserInfo user = repository.findByUsername(date.getEmail());
            if (user != null && user.getEmail() != null) {
                if (date.getExpiryDate().before(new Date())) {
                    modelMap.addAttribute("message", "The token has expired.");
                    return "login";
                }
//                RandomStringGenerator generator = new RandomStringGenerator();
//                String newPassword = generator.generate(16);
                ResetPasswordForm resetPwdForm = new ResetPasswordForm();
                resetPwdForm.setUserId(user.getUserId());
                resetPwdForm.setCsrf(csrf);
                resetPwdForm.setToken(token);
//                resetPwdForm.setNewPassword(newPassword);
//                resetPwdForm.setRePassword(newPassword);
                modelMap.addAttribute("resetPwdForm", resetPwdForm);
                return "resetPassword";
            }
        }
        return "login";
    }

    @RequestMapping(value = {"/resetpwd"}, method = RequestMethod.POST)
    public String resetPasswordPage(ModelMap modelMap, @ModelAttribute("resetPwdForm") ResetPasswordForm resetPwdForm) {
        ExpiryDate date = MailConstants.map.get(resetPwdForm.getToken());
        if (date != null) {
            UserInfo user = repository.findByUsername(date.getEmail());
            if (date.getExpiryDate().before(new Date())) {
                modelMap.addAttribute("title", "Token Expired");
                modelMap.addAttribute("message", "The token has expired.");
                return "login";
            }
            if (user != null
                    && resetPwdForm.getUserId() != null
                    && !resetPwdForm.getNewPassword().isEmpty()
                    && !resetPwdForm.getRePassword().isEmpty()
                    && resetPwdForm.getNewPassword().equals(resetPwdForm.getRePassword())) {
                user.setPassword(resetPwdForm.getNewPassword());
                repository.updateUserPassword(user);
                modelMap.addAttribute("title", "Password Updated");
                modelMap.addAttribute("message", "You have updated your password.");
                return "login";
            }
        }
        return "login";
    }
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private MailManager mailManager;
    @Autowired
    private HTTPRequestManager httpRequestManager;
    @Autowired
    private String adminEmailId;
}
