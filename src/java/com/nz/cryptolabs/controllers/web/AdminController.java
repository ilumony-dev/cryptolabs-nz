/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.nz.cryptolabs.poloniex.PoloniexExchangeService;
import com.nz.cryptolabs.beans.CryptoCoin;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.bittrex.modal.accountapi.Balance;
import com.nz.cryptolabs.services.BittrexAccountAPIService;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetBalancesContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.GetOrderHistoryContainer;
import com.nz.cryptolabs.bittrex.modal.accountapi.PastOrder;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.components.CommonMethods;
import com.nz.cryptolabs.components.TimeManager;
import com.nz.cryptolabs.services.SessionManagementService;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.nz.cryptolabs.constants.Constants;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import com.nz.cryptolabs.mail.AdminMailManager;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.DateTimeService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Controller
public class AdminController implements Constants {

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private CommonMethods common;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private DateTimeService dateTimeService;
    @Autowired
    private AdminMailManager mailManager;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;
    @Autowired
    private BittrexAccountAPIService bittrexAccountAPIService;
    @Autowired
    private TimeManager timeManager;

    private void coinsByInvestments(ModelMap model) {
        List<InvestmentBean> invs = databaseService.coinsByInvestments(null, null, false, null);
        Set<String> fundNameSet = new LinkedHashSet<>();
        Set<String> coinNameSet = new LinkedHashSet<>();
        Set<String> refIdSet = new LinkedHashSet<>();
        for (Iterator<InvestmentBean> it = invs.iterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            if (inv.getRefId() != null) {
                refIdSet.add("\"" + inv.getRefId() + "\"");
            }
            if (!"Total".equalsIgnoreCase(inv.getFundName())) {
                fundNameSet.add("\"" + inv.getFundName() + "\"");
                if (inv.getFundActualShares() != null) {
                    for (Iterator<ShareFund> it1 = inv.getFundActualShares().iterator(); it1.hasNext();) {
                        ShareFund share = it1.next();
                        coinNameSet.add("\"" + share.getCoinId() + "\"");
                    }
                }
            }
        }
        model.addAttribute("investments", invs);
        model.addAttribute("refIds", new ArrayList(refIdSet));
        model.addAttribute("funds", new ArrayList(fundNameSet));
        model.addAttribute("coins", new ArrayList(coinNameSet));
    }

    @RequestMapping(value = {"/admin-dashboard"}, method = RequestMethod.GET)
    public String adminDashboard(ModelMap model) throws IOException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("funds", repository.funds());
            model.addAttribute("totalUnits", databaseService.totalUnitsById(null, null, null));
            coinsByInvestments(model);
            model.addAttribute("pendingInvestments", repository.pendingInvestments(null));
            model.addAttribute("pendingTransactions", repository.pendingTransactions(null));
            List<Map<String, Object>> totals = repository.totals();
            for (Map<String, Object> map : totals) {
                String tbl = (String) map.get("tbl");
                if (tbl.equals("user_master")) {
                    model.addAttribute("totalCustomers", map.get("total"));
                } else if (tbl.equals("investment")) {
                    model.addAttribute("totalInvestments", map.get("total"));
                } else if (tbl.equals("user_shares_transaction")) {
                    model.addAttribute("totalSharesValue", map.get("total"));
                }
            }
            {   //Algo 5
//                GetBalancesContainer balancesContainer = bittrexAccountAPIService.getBalances();
//                List<Balance> balances = balancesContainer.getBalances();
//                List<Balance> totalBalances = new LinkedList<>();
//                StringBuilder currency = new StringBuilder();
//                for (int i = 0; i < balances.size(); i++) {
//                    Balance balance = balances.get(i);
//                    if (balance.getBalance() > 0) {
//                        currency.append(balance.getCurrency()).append(i == balances.size() - 1 ? "" : ",");
//                        totalBalances.add(balance);
//                    }
//                }
//                ObjectNode rateForPair = databaseService.rateForPair(currency.toString(), "BTC", null);
//                BigDecimal totalBtcValue = BigDecimal.ZERO;
//                for (Balance balance : totalBalances) {
//                    String coin = balance.getCurrency();
//                    JsonNode curr = rateForPair.get(coin);
//                    BigDecimal btcPrice = new BigDecimal(curr.get("BTC").asDouble());
//                    BigDecimal currValue = new BigDecimal(balance.getBalance());
//                    BigDecimal btcValue = btcPrice.multiply(currValue);
//                    balance.setBtcValue(btcValue.doubleValue());
//                    totalBtcValue = totalBtcValue.add(btcValue);
//                }
//                model.addAttribute("totalBtcValue", totalBtcValue);
                List<ShareFund> dailyUpdates = databaseService.dailyUpdates(null, null, null, false);
                model.addAttribute("dailyUpdates", dailyUpdates);
            }
            return "admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-invite-codes"}, method = RequestMethod.GET)
    public String adminInviteCodes(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("funds", repository.funds());
            model.addAttribute("inviteCodes", repository.inviteCodes(true, false, false));
            return "admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-user-accounts"}, method = RequestMethod.GET)
    public String adminUA(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
//            try {
            model.addAttribute("info", admin);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("totalUsers", repository.users());
//                model.addAttribute("funds", repository.funds());
//                model.addAttribute("portfolios", repository.portfolios());
//                List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
//                model.addAttribute("coins", coinList);
            return "admin-show-user-accounts";
//            } catch (IOException ex) {
//                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
//                return "forward:/welcome";
//            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-portfolios"}, method = RequestMethod.GET)
    public String adminC(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            try {
                model.addAttribute("info", admin);
                model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
                model.addAttribute("shares", repository.shares());
                model.addAttribute("funds", repository.funds());
                model.addAttribute("portfolios", repository.portfolios());
                model.addAttribute("currencies", repository.currencies());
                List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
                model.addAttribute("coins", coinList);
                return "admin-show-portfolios";
            } catch (IOException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
                return "forward:/admin-dashboard";
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-shares"}, method = RequestMethod.GET)
    public String adminShowShares(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("shares", repository.shares());
            model.addAttribute("funds", repository.funds());
            return "admin-show-shares";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-funds"}, method = RequestMethod.GET)
    public String adminShowFunds(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            try {
                model.addAttribute("info", admin);
                model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
                model.addAttribute("shares", repository.shares());
                model.addAttribute("funds", repository.funds());
                List<CryptoCoin> coinList = coinMarketCapAPIService.getAllCoinMarketCapList(null);
                model.addAttribute("coins", coinList);
                return "admin-show-funds";
            } catch (IOException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
                return "forward:/admin-dashboard";
            }
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-portfolio"}, method = RequestMethod.GET)
    public String adminShowPortfolio(ModelMap model, @RequestParam("id") String id) throws IOException {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
//            try {
//                String main2 = poloniexExchangeService.main2();
//                if (main2.contains("<script type=\"text/javascript\" src=\"/cdn-cgi/scripts/cf.challenge.js\"") && main2.contains("</html>")) {
//                    main2 = main2.replaceAll("\"/cdn-cgi/scripts", "\"https://www.poloniex.com/cdn-cgi/scripts");
//                    main2 = main2.replaceAll("\"/cdn-cgi/l", "\"./admin-cdn-cgi/l");
//                    model.addAttribute("htmlCode", main2);
////                    return "emptyfile";
//                    return "redirect:https://www.poloniex.com/public?command=returnTicker";
//                }
//            } catch (Exception ex) {
//                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            if ("19".equalsIgnoreCase(id)) {
                GetBalancesContainer balancesContainer = bittrexAccountAPIService.getBalances();
                List<Balance> balances = balancesContainer.getBalances();
                List<Balance> totalBalances = new LinkedList<>();
                StringBuilder currency = new StringBuilder();
                for (int i = 0; i < balances.size(); i++) {
                    Balance balance = balances.get(i);
                    if (balance.getBalance() > 0) {
                        currency.append(balance.getCurrency()).append(i == balances.size() - 1 ? "" : ",");
                        totalBalances.add(balance);
                    }
                }
                ObjectNode rateForPair = databaseService.rateForPair(currency.toString(), "BTC", null);
                Double totalBtcValue = 0d;
                for (Balance balance : totalBalances) {
                    JsonNode curr = rateForPair.get(balance.getCurrency());
                    double btcPrice = curr.get("BTC").asDouble();
                    double currValue = balance.getBalance();
                    double btcValue = btcPrice * currValue;
                    balance.setBtcValue(btcValue);
                    totalBtcValue += btcValue;
                }
                model.addAttribute("totalBtcValue", totalBtcValue);
                GetOrderHistoryContainer orderHistoryContainer = bittrexAccountAPIService.getOrderHistory();
                Date today0000 = dateTimeService.today0000();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(today0000);
                calendar.add(Calendar.DAY_OF_MONTH, -1);
//                Date yesterday0000 = calendar.getTime();
                List<PastOrder> pastOrders = orderHistoryContainer.getPastOrders();
                List<PastOrder> todayOrders = new LinkedList<>();
                for (PastOrder order : pastOrders) {
                    Date dateTime = order.getDateTime();
                    if (dateTime.after(today0000)) {
//                    if (dateTime.after(today0000)) {
                        todayOrders.add(order);
                    }
                }
                model.addAttribute("todayOrders", todayOrders);
            }
            model.addAttribute("info", admin);
            model.addAttribute("trade", repository.tradeByFundId(id));
            model.addAttribute("portfolio", repository.portfolioById(id));
            ShareFund totalUnits = repository.totalUnitsById(id, null);
            model.addAttribute("totalUnits", totalUnits);
            model.addAttribute("hh", common.hours());
            model.addAttribute("mm", common.minutes());
            return "admin-show-portfolio";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-show-currencies"}, method = RequestMethod.GET)
    public String adminShowCurrencies(ModelMap model) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            model.addAttribute("info", admin);
            model.addAttribute("onlineUsers", sessionManagementService.getAllOnlineUsers());
            model.addAttribute("currencies", repository.currencies());
            model.addAttribute("funds", repository.funds());
            return "admin-show-currencies";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-cdn-cgi/l/chk_captcha"}, method = {RequestMethod.POST, RequestMethod.GET})
    public String adminCdnCgilChkCaptcha(ModelMap model,
            @RequestParam("id") String id,
            @RequestParam("g-recaptcha-response") String gRecaptchaResponse) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            return "redirect:https://www.poloniex.com/cdn-cgi/l/chk_captcha?id=" + id + "&g-recaptcha-response" + gRecaptchaResponse;
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/portfolio-report"}, method = RequestMethod.GET)
    public String headway(ModelMap modelMap) {
        SecuredUser user = sessionManagementService.getCurrentUser();
        if (user != null) {
            modelMap.addAttribute("info", user);
            modelMap.addAttribute("title", "Top 10 Portfolio Holding Report");
            return "portfolio-rep";
        } else {
            return "redirect:/login";
        }
    }
    
    @RequestMapping(value = {"/admin-add-bitcoin"}, method = RequestMethod.POST)
    public String adminAddBitcoin(ModelMap modelMap, @ModelAttribute("bitcoin") ShareFund bitcoin) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            Date date = new Date();
            bitcoin.setCreatedDate(common.dateFormat(date));
            repository.addBitcoin(bitcoin);
            return "redirect:/admin-show-portfolio?id=" + bitcoin.getFundId();
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-trade"}, method = RequestMethod.POST)
    public String adminAddTrade(ModelMap modelMap, @ModelAttribute("trade") ShareFund trade) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            databaseService.addTrade(trade);
            return "redirect:/admin-show-portfolio?id=" + trade.getFundId();
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-invite-code"}, method = RequestMethod.POST)
    public String adminAddInviteCode(ModelMap modelMap, @ModelAttribute("code") InviteCode code) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            code.setCreatedBy(admin.getUserId());
            Date date = new Date();
            code.setCreatedTs(common.dateFormat(date));
            Integer year = common.parseInt(code.getTimeFrame());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.YEAR, year);
            date = cal.getTime();
            code.setExpiryTs(common.dateFormat(date));
            repository.save(code);
            if (code.getId() != null && code.getEmailId() != null && code.getInviteCode() != null) {
                mailManager.allocateEmail(admin, code.getEmailId(), code.getInviteCode());
            }
            return "redirect:/admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-allocate-email"}, method = RequestMethod.POST)
    public String adminAllocateEmail(ModelMap modelMap, @ModelAttribute("code") InviteCode code) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.allocateEmail(code);
            if (code.getId() != null && code.getEmailId() != null && code.getInviteCode() != null) {
                mailManager.allocateEmail(admin, code.getEmailId(), code.getInviteCode());
            }
            return "redirect:/admin-invite-codes";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-share"}, method = RequestMethod.POST)
    public String adminAddShare(ModelMap modelMap, @ModelAttribute("share") ShareFund share) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            share.setCreatedBy(admin.getUserId());
            Integer id = common.parseInt(share.getShareId());
            if (id > 0) {
                repository.updateShare(share);
            } else {
                repository.addShare(share);
            }
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-currency"}, method = RequestMethod.POST)
    public String adminAddCurrency(ModelMap modelMap, @ModelAttribute("currency") ShareFund currency) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            currency.setCreatedBy(admin.getUserId());
            repository.addCurrency(currency);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-conversion-pairs"}, method = RequestMethod.POST)
    public String adminUpdateConversionPairs(ModelMap modelMap, @ModelAttribute("currency") ShareFund currency) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            currency.setCreatedBy(admin.getUserId());
            repository.addCurrency(currency);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-share-price"}, method = RequestMethod.POST)
    public String adminUpdateSharePrice(ModelMap modelMap, @ModelAttribute("share") ShareFund share) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            share.setCreatedDate(common.dateFormat(new Date()));
            share.setCreatedBy(admin.getUserId());
            repository.updateSharePrice(share);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-add-fund"}, method = RequestMethod.POST)
    public String adminAddFund(ModelMap modelMap,
            @ModelAttribute("fund") ShareFund fund,
            final @RequestPart(value = "file", required = false) MultipartFile file) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
//            if (!file.isEmpty()) {
//                String original = file.getOriginalFilename();
//                int lastIndexOf = original.lastIndexOf(".");
//                String ext = original.substring(lastIndexOf);
//                String name = userPOJO.getName().trim().concat(String.valueOf("_" + userPOJO.getId())).concat(ext);
//                userPOJO.setImageName(name);
//                userPOJO.setIconName(name);
//            }
            fund.setCreatedBy(admin.getUserId());
            Integer id = common.parseInt(fund.getFundId());
            if (id > 0) {
                repository.updateFund(fund);
            } else {
                repository.addFund(fund);
            }
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-purchase-pending-shares"}, method = RequestMethod.POST)
    public String adminPurchasePendingShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            investmentShares.setCreatedDate(common.dateFormat(new Date()));
            investmentShares.setCreatedBy(admin.getUserId());
            repository.updatePendingShares(investmentShares);
            repository.purchaseShares(investmentShares);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-update-purchased-shares"}, method = RequestMethod.POST)
    public String adminUpdatePurchasedShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund updatedShares) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            repository.updatePurchasedShares(updatedShares);
            return "redirect:/admin-show-user-accounts";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-sell-shares"}, method = RequestMethod.POST)
    public String adminSellShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            investmentShares.setCreatedDate(common.dateFormat(new Date()));
            investmentShares.setCreatedBy(admin.getUserId());
            repository.sellShares(investmentShares);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(value = {"/admin-purchase-shares"}, method = RequestMethod.POST)
    public String adminPurchaseShares(ModelMap modelMap,
            @ModelAttribute("shares") ShareFund investmentShares
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin != null) {
            investmentShares.setCreatedDate(common.dateFormat(new Date()));
            investmentShares.setCreatedBy(admin.getUserId());
            repository.purchaseShares(investmentShares);
            return "redirect:/admin-dashboard";
        } else {
            return "redirect:/login";
        }
    }

}
