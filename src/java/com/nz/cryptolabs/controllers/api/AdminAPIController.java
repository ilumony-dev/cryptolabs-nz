/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.controllers.api;

import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.SecuredUser;
import com.nz.cryptolabs.beans.ShareFund;
import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserInfo;
import static com.nz.cryptolabs.constants.Constants.adminRole;
import com.nz.cryptolabs.repositories.CommonRepository;
import com.nz.cryptolabs.services.DatabaseService;
import com.nz.cryptolabs.services.CoinMarketCapAPIService;
import com.nz.cryptolabs.services.SessionManagementService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Administrator
 */
@RestController
@RequestMapping(value = "/rest/cryptolabs/api")
public class AdminAPIController {

    @Autowired
    private SessionManagementService sessionManagementService;
    @Autowired
    private DatabaseService databaseService;
    @Autowired
    private CommonRepository repository;
    @Autowired
    private CoinMarketCapAPIService coinMarketCapAPIService;

    @RequestMapping(value = {"/onlineUsers", "/oluuytrerty"}, method = RequestMethod.GET)
    @ResponseBody
    public String getOnlineUsers(@RequestParam(name = "api_key", required = false) String apiKey) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        List<SecuredUser> principals = (List) sessionManagementService.getAllOnlineUsers();
        return toString(principals);
    }

    @RequestMapping(value = {"/deActiveShare", "/met98765678w356"}, method = RequestMethod.GET)
    @ResponseBody
    public String deActiveShare(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "sId") String shareId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        Boolean result = repository.deActiveShare(shareId);
        return result.toString();
    }

    @RequestMapping(value = {"/deActiveFund", "/met5456876w789"}, method = RequestMethod.GET)
    @ResponseBody
    public String deActiveFund(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(name = "fId") String fundId) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "false";
        }
        Boolean result = repository.deActiveFund(fundId);
        return result.toString();
    }

    @RequestMapping(value = {"/balChart"}, method = RequestMethod.GET)
    @ResponseBody
    public String balChart(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam("type") String type) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        Map<Date, BigDecimal> map = databaseService.latestBalanceData(type, null, null);
        String values = new String();
        String labels = new String();
        String totalBalance = new String();
        Set<Map.Entry<Date, BigDecimal>> set = map.entrySet();
        for (Iterator<Map.Entry<Date, BigDecimal>> iterator = set.iterator(); iterator.hasNext();) {
            int i = 0;
            Map.Entry<Date, BigDecimal> next = iterator.next();
            values = values + ((i > 0 && i != map.size()) ? ", " : "") + next.getValue();
            labels = labels + ((i > 0 && i != map.size()) ? ", " : "") + ("\"" + next.getKey() + "\"");
            i++;
        }
//        if (i == list.size() - 1) {
//            totalBalance = "" + map.get("balance");
//        }
        return "{"
                + "\"totalBalance\":" + totalBalance + " ,\n"
                + "\"labels\":[" + labels + "] ,\n"
                + "\"values\":[" + values
                + "]\n"
                + "}";
    }

    @RequestMapping(value = {"/userAccountSummary"}, method = RequestMethod.GET)
    @ResponseBody
    public String userAccountSummary(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<TransactionBean> trans = repository.userAccountSummary(userInfo.getUserId());
            return toString(trans);
        }
        return null;
    }

    @RequestMapping(value = {"/userUnitSummary"}, method = RequestMethod.GET)
    @ResponseBody
    public String userUnitSummary(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username,
            @RequestParam(value = "fdt", required = false) String fromDate,
            @RequestParam(value = "tdt", required = false) String toDate
    ) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<ShareFund> units = databaseService.userUnitSummary(userInfo.getUserId(), fromDate, toDate);
            fromDate = (fromDate != null && !fromDate.isEmpty()) ? fromDate : "First Day";
            toDate = (toDate != null && !toDate.isEmpty()) ? toDate : "Present Day";
            return "{"
                    + "\"units\":" + toString(units) + " ,\n"
                    + "\"refId\":" + checkNull(userInfo.getRefId()) + " ,\n"
                    + "\"fromDate\":" + checkNull(fromDate) + " ,\n"
                    + "\"toDate\":" + checkNull(toDate) + " "
                    + "}";

        }
        return null;
    }

    @RequestMapping(value = {"/userDailyUpdates"}, method = RequestMethod.GET)
    @ResponseBody
    public String userDailyUpdates(@RequestParam(name = "api_key", required = false) String apiKey,
            @RequestParam(value = "un", required = false) String username) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        UserInfo userInfo = repository.findByUsername(username);
        if (userInfo != null) {
            List<ShareFund> dailyUpdates = databaseService.dailyUpdates(null, userInfo.getUserId(), null, true);
            return toString(dailyUpdates);
        }
        return null;
    }

    @RequestMapping(value = {"/coinsByAllInvestments", "/qwergnuyhalltinyhkgyi"}, method = RequestMethod.GET)
    @ResponseBody
    public String coinsByInvestments(@RequestParam(name = "api_key", required = false) String apiKey) {
        SecuredUser admin = sessionManagementService.getCurrentUser(adminRole);
        if (admin == null) {
            return "[]";
        }
        List<InvestmentBean> invs = databaseService.coinsByInvestments(null, null, false, null);
        Set<String> fundNameSet = new LinkedHashSet<>();
        Set<String> coinNameSet = new LinkedHashSet<>();
        Set<String> refIdSet = new LinkedHashSet<>();
        for (Iterator<InvestmentBean> it = invs.iterator(); it.hasNext();) {
            InvestmentBean inv = it.next();
            if (inv.getRefId() != null) {
                refIdSet.add("\"" + inv.getRefId() + "\"");
            }
            if (!"Total".equalsIgnoreCase(inv.getFundName())) {
                fundNameSet.add("\"" + inv.getFundName() + "\"");
                if (inv.getFundActualShares() != null) {
                    for (Iterator<ShareFund> it1 = inv.getFundActualShares().iterator(); it1.hasNext();) {
                        ShareFund share = it1.next();
                        coinNameSet.add("\"" + share.getCoinId() + "\"");
                    }
                }
            }
        }
        return "{"
                + "\"investments\":" + toString(invs) + " ,\n"
                + "\"refIds\":" + toString(new ArrayList(refIdSet)) + " ,\n"
                + "\"funds\":" + toString(new ArrayList(fundNameSet)) + " ,\n"
                + "\"coins\":" + toString(new ArrayList(coinNameSet)) + "\n"
                + "}";
    }

    public String toString(List list) {
        if (list == null) {
            return null;
        }
        String objects = "";
        for (int i = 0; i < list.size(); i++) {
            Object obj = list.get(i);
            objects = objects + ((i > 0 && i != list.size()) ? "," : "") + obj.toString();
        }
        return "[" + objects + "]";

    }

}
