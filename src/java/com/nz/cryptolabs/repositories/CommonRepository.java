/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.beans.Verification;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface CommonRepository {

    public void save(String userId, String activity);

    public void save(InviteCode code);

    public void allocateEmail(InviteCode code);

    public void save(UserInfo user);

    public UserInfo findByUsername(String username);

    public UserInfo findByUserId(String userId);

    public UserInfo updateUserPassword(UserInfo user);

    public void addShare(ShareFund share);

    public ShareFund shareById(String shareId);

    public void updateShare(ShareFund share);

    public void updateSharePrice(ShareFund share);

    public List<ShareFund> shares();

    public void addFund(ShareFund fund);

    public void updateFund(ShareFund fund);

    public List<ShareFund> funds();

    public List<ShareFund> portfolios();

    public List<UserFundsCommand> userFunds(String fundId, String userId, String investmentId);

    public List<ShareFund> sharesByFund(String fundId);

    public List<ShareFund> coinsByFund(String fundId);

    public void saveInvestment(InvestmentBean bean);

    public void saveAccountingEvent(AccountEventCommand event);

    public List<InvestmentBean> pendingInvestments(String userId);

    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId);

    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId);

//    public List<InvestmentBean> purchasedInvestments(String userId, String investmentId, String fundId);
    public List<AccountEventCommand> latestInvestments(String userId);

    public List<ShareFund> pendingSharesByInvestment(String investmentId);

    public List<ShareFund> pendingCoinsByInvestment(String investmentId);

    public void updatePendingShares(ShareFund investmentShares);

    public void purchaseShares(ShareFund investmentShares);

    public void updatePurchasedShares(ShareFund updatedShares);

    public List<ShareFund> sharesByInvestment(String investmentId);

    public List<ShareFund> detailsByInvestment(String userId, String investmentId, String action);

    public List<InvestmentBean> pendingTransactions(String userId);

    public void sellShares(ShareFund investmentShares);

    public List<ShareFund> actualSharePercentages(String investmentId);

    public List<ShareFund> actualCoinPercentages(String investmentId);

    public List<Map<String, Object>> totals();

    public List<ShareFund> latestBalanceData(String type, String userId, String investmentId);

    public List<TransactionBean> userAccountSummary(String userId);

    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate);

    public List<InviteCode> inviteCodes(Boolean available, Boolean expired, Boolean used);

    public List<InviteCode> checkInviteCode(String code, String type);

    public List<StateCity> countries(Boolean states, Boolean cities);

    public Boolean deActiveShare(String shareId);

    public Boolean deActiveFund(String fundId);

    public void saveVerification(Verification verification);

    public Verification findVerification(String userId);

    public int saveStripeCharge(StripeCharge charge);

    public int saveWalletTransaction(TransactionBean txn);

    public List<TransactionBean> userWalletTransactions(String userId);

    public ShareFund fundById(String fundId);

    public ShareFund portfolioById(String portfolioId);

    public void addReferenceNo(UserReference reference);

    public UserReference findReferenceNo(String token, String ref);

    public void updatedAmounted(String ref);

    public List<ShareFund> totalCoins(String userId, String investmentId);

    public List<UserInfo> users();

    public void addTrade(ShareFund trade, List<ShareFund> totalUnits);

    public ShareFund tradeByFundId(String id);

    public ShareFund totalUnitsById(String fundId, String invesmentId);

    public List<ShareFund> totalUnitsById(String fundId, String userId, String invesmentId);

    public void addBitcoin(ShareFund bitcoin);

    public List<ShareFund> currencies();

    public void addCurrency(ShareFund currency);

    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals);

    public void saveConfiguration(Configuration config);

    public Configuration findConfiguration(String userId);

    public void saveCurrencyConversionRate(String currencyPair, String date, String price);

    public ShareFund currencyConversionRate(String currencyPair, String date);

    public void updateCurrencyConversionRate(String currencyPair, String date, String price);

    public void saveUserTodayStatus(List<UserFundsCommand> userFunds);

    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate);
}
