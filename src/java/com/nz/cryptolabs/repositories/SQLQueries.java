/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

/**
 *
 * @author Administrator
 */
public interface SQLQueries {

    String insertUserActivity = "INSERT INTO `user_activity` (`user_id`, `activity`, `date_time`, `active`)\n"
            + "VALUES (?, ?, now(), 'Y');";

    String updateUserActivity = "UPDATE `user_activity` SET `active` = 'N' WHERE `id` > 0 and `user_id` = ";

    String insertTransaction = "INSERT INTO `transactions`"
            + " (`created_ts`, `user_id`, `investment_id`, `particulars`, `inc_dec`, `amount`, `active`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y');";

    String insertUnitTransaction = " INSERT INTO `user_inv_units_txn`"
            + " (`created_ts`, `user_id`, `investment_id`, `nzd`, `usd`, `btc`, `minto`, `active`, `action`)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y', ?); ";

    String insertInviteCode = " INSERT INTO `invite_code`"
            + " (`invite_code`, `expiry_ts`, `created_by`, `created_ts`, `expired`, `used`, `active`, `purpose`, `email`)"
            + " VALUES (?, ?, ?, ?, 'N', 'N', 'Y', ?, ?); ";

    String inviteCode = " SELECT * FROM invite_code WHERE used = 'N' AND invite_code = ?; ";

    String usedInviteCode = " UPDATE `invite_code` SET `used` = 'Y' WHERE `used` = 'N' AND `invite_code` = ?; ";

    String allocateEmail = " UPDATE `invite_code` SET `email`=? WHERE `id`=?; ";

    String insertUser = "INSERT INTO `user_master`"
            + "(`full_name`, `dob`, `email`, `password`, `invite_code`, `mobile_no`, `created_ts`, `active`, `ref_id`)"
            + "VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, 'Y', ?);";

    String insertRole = "INSERT INTO `role_master` (`user_id`, `role`) VALUES (?, ?);";

    String updatepasswordsql = "update user_master U set U.password=? where U.user_id = ?;";

    String loginSQL = "SELECT U.*, R.role"
            //            + ", C.currency"
            + " FROM user_master U "
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) "
            //            + " LEFT JOIN config C ON (U.user_id = C.user_id and C.active = 'Y') "
            + " WHERE U.email = ?;";

    String userById = "SELECT U.*, R.role FROM user_master U"
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) WHERE U.user_id = ?;";

    String insertShare = " INSERT INTO `share` "
            + " (`name`, `desc`, `active`, `approved`, `created_by`, `exchange_code`, `custodian_id`) "
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?)";

    String insertCurrency = " INSERT INTO `currency` "
            + " (`name`, `symbol`, `active`, `approved`, `desc`, `created_by`, `exchange_code`, `custodian_id`) "
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?, ?);";

    String shareById = "SELECT * FROM `share` WHERE `active`= 'Y' AND `id` = ?";

    String updateShare = " UPDATE `share` SET `name`=?, `desc`=?, `exchange_code`=?, `custodian_id`=? WHERE `id`=?;";

    String deActiveShare = " UPDATE `share` SET `active`='N' WHERE `id`=?;";

    String insertFund = " INSERT INTO `fund`"
            + " (`name`, `desc`, `active`, `approved`, `created_by`, `custodian_id`, `exchange_code`, `master`)"
            + " VALUES (?, ?, 'Y', 'Y', ?, ?, ?, ?); ";

    String updateFund = " UPDATE `fund` SET `name`=?, `desc`=?, `exchange_code`=?, `custodian_id`=? WHERE `id`=?;";

    String deActiveFund = " UPDATE `fund` SET `active`='N' WHERE `id`=?;";

    String inActiveFundDetail = " UPDATE `fund_detail` SET `active`='N' WHERE `fund_id`=?; ";

    String insertFundDetail = " INSERT INTO `fund_detail`"
            + " (`fund_id`, `share_id`, `percentage`, `quantity`, `cdate`, `active`, `coin_id`)"
            + " VALUES (?, ?, ?, ?, ?, 'Y', ?);";

    String insertInvestment = "INSERT INTO `investment`"
            + " (`user_id`, `customer_name`, `investment_amount`, `bank_acc`, `reference_no`, `fund_id`, `active`, `created_ts`, `time_frame`, `bank_name`, `years`, `regularly_amount`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?, ?, ?, ?);";

    String updateInvestmentAmount = " UPDATE `investment` SET `investment_amount`= `investment_amount` + ? WHERE `id` = ?;";

    String insertInvestmentPendingShares = "INSERT INTO `user_shares_pending_transaction`"
            + " (`created_ts`, `user_id`, `investment_id`, `share_id`, `price`, `quantity`, `active`, `action`, `coin_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?);";

    String insertFundEvent = "INSERT INTO `investment_request`"
            + " (user_id, investment_id, amount, action, created_ts, description, status, active)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y');";

    String insertInvestmentUpdatedShares = "INSERT INTO `user_shares_transaction`"
            + " (`created_ts`, `user_id`, `investment_id`, `share_id`, `price`, `quantity`, `active`, `action`, `coin_id`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y', ?, ?);";

    String updatePurchasedShares = "UPDATE `user_shares_transaction` "
            + "SET `price`= ?, `quantity`=? WHERE `id`= ?;";

    String updateOldSharePrice = "UPDATE `share_price` SET `active` = 'N' WHERE `share_id` = ?";

    String insertSharePrice = "INSERT INTO `share_price`"
            + " (`share_id`, `price`, `created_ts`, `active`)"
            + " VALUES (?, ?, ?, 'Y');";

    String insertUserRefNo = "INSERT INTO `user_ref_no` "
            + " (`ref_no`, `stripe_token`, `poli_token`, `user_id`, `fund_id`, `price_in_nzd`, `created_ts`, `active`,`amounted`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y', 'N');";

    String insertVerification = "INSERT INTO `verification` "
            + " (`pp_passport_number`, `pp_passport_expiry_date`, `pp_filename`, `pa_same_res_addr`, `pa_is_your_pa_street_addr`, "
            + " `pa_postal_agency`, `pa_po_box`, `pa_private_bag`, `pa_special_service`, `pa_number`, `pa_postal_office`, `pa_postal_code`, "
            + " `pa_filename`, `dl_first_name`, `dl_middle_name`, `dl_last_name`, `dl_dob`, `dl_street`, `dl_street_name`, `dl_street_number`, "
            + " `dl_license_number`, `dl_license_version`, `dl_filename`, `address`, `user_id`, `created_ts`)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    String updateOldConfig = "UPDATE `config` SET `active` = 'N' WHERE `user_id` = ?";

    String insertConfig = "INSERT INTO `config` "
            + " (`local_currency`, `show_currency`, `user_id`, `created_ts`, `active`) "
            + " VALUES (?, ?, ?, 'Y');";

    String insertStripeCharge = "INSERT INTO `stripe_charge` "
            + " (`stripe_charge_id`, `user_id`, `bal_txn_id`, `created_ts`, `stripe_created_ts`, `amount`, `amount_refunded`, `currency`, "
            + " `status`, `fail_code`, `fail_message`, `description`, `fund_id`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    String insertWalletTransaction = "INSERT INTO `wallet_txns` "
            + " (`created_ts`, `user_id`, `txn_id`, `brand`, `particulars`, `inc_dec`, `amount`, `active`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, 'Y');";

    String funds = " SELECT F.*, group_concat(V.name) as shares FROM fund F "
            + " LEFT JOIN ( "
            + " SELECT FD.*, S.name FROM fund_detail FD "
            + " INNER JOIN share S ON (S.active= 'Y' AND S.id = FD.share_id) "
            + " WHERE FD.active = 'Y') V ON (F.id = V.fund_id) "
            + " WHERE F.active = 'Y' AND F.approved = 'Y' AND `master` = 'FUND' "
            + " GROUP BY F.id; ";

    String portfolios = " SELECT F.*, group_concat(FD.coin_id) as shares FROM fund F "
            + " LEFT JOIN fund_detail FD ON (FD.fund_id = F.id AND FD.active = 'Y') "
            + " WHERE F.active = 'Y' AND F.approved = 'Y' AND master = 'PORTFOLIO' GROUP BY F.id; ";

    String fundById = " SELECT F.*, group_concat(V.name) as shares FROM fund F "
            + " LEFT JOIN ("
            + " SELECT FD.*, S.name FROM fund_detail FD "
            + " INNER JOIN share S ON (S.active= 'Y' AND S.id = FD.share_id)"
            + " WHERE FD.active = 'Y') V ON (F.id = V.fund_id) WHERE F.active = 'Y' AND `master` = 'FUND' AND `F.id` = ? "
            + " GROUP BY F.id; ";

    String portfolioById = " SELECT F.*, group_concat(FD.coin_id) as shares FROM fund F "
            + " LEFT JOIN fund_detail FD ON (FD.fund_id = F.id AND FD.active = 'Y') "
            + " WHERE F.active = 'Y' AND master = 'PORTFOLIO' AND F.id = ? "
            + " GROUP BY F.id; ";

    String shares = " SELECT * FROM `share` WHERE `active`= 'Y' ";

    String currencies = " SELECT * FROM `currency` WHERE `active`= 'Y' ";

    String users = " SELECT U.*, R.role, UA.date_time FROM user_master U \n"
            + " INNER JOIN role_master R ON (U.user_id = R.user_id) \n"
            + " LEFT JOIN user_activity UA ON (U.user_id = UA.user_id AND UA.active = 'Y')\n"
            + " WHERE U.active = 'Y'; ";

    String insertFundTrade = " INSERT INTO `fund_trade` "
            + " (`created_ts`, `fund_id`, `op_bal`, `close_bal`, `pur_trades`, `sold_trades`, `pur_coins`, `sold_coins`, `active`, `unit_price`) "
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, 'Y', ?); ";

    String insertConversionRate = " INSERT INTO `conversion_rate` "
            + " (`created_ts`, `usd_nzd`, `usd_gbp`, `usd_inr`, `usd_aud`) "
            + " VALUES (?, ?, ?, ?, ?); ";

    String insertUserCurrBtc = " INSERT INTO `user_curr_btc` "
            + " (`created_ts`, `fund_id`, `user_id`, `investment_id`, `bitcoin`, `minto`, `active`)"
            + " VALUES (?, ?, ?, ?, ?, ?, 'Y') ";

    String insertUserTodayStatus = " INSERT INTO `user_today_status` "
            + " (`curr_date`, `user_id`, `investment_id`, `fund_id`, `investment_amount`, `active`) "
            + " VALUES (?, ?, ?, ?, ?, 'Y') ";

}
