/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.repositories;

import com.nz.cryptolabs.beans.AccountEventCommand;
import com.nz.cryptolabs.beans.Configuration;
import com.nz.cryptolabs.beans.ShareFund;
import com.nz.cryptolabs.beans.InvestmentBean;
import com.nz.cryptolabs.beans.InviteCode;
import com.nz.cryptolabs.beans.StateCity;
import com.nz.cryptolabs.beans.StripeCharge;
import com.nz.cryptolabs.beans.TransactionBean;
import com.nz.cryptolabs.beans.UserFundsCommand;
import com.nz.cryptolabs.beans.UserInfo;
import com.nz.cryptolabs.beans.UserReference;
import com.nz.cryptolabs.beans.Verification;
import com.nz.cryptolabs.constants.Constants;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 *
 * @author Administrator
 */
@Repository
public class CommonRepositoryImpl implements CommonRepository {

    @Autowired
    private DataSourceTransactionManager transactionManager;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private void save(TransactionBean tran) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getInvestment_id());
            pstmnt.setString(4, tran.getParticulars());
            pstmnt.setString(5, tran.getInc_dec());
            pstmnt.setString(6, tran.getAmount());
            return pstmnt;
        });
    }

    private void saveUnitTransaction(TransactionBean tran) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUnitTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getInvestment_id());
            pstmnt.setString(4, tran.getLocal());
            pstmnt.setString(5, tran.getUsd());
            pstmnt.setString(6, tran.getBtc());
            pstmnt.setString(7, tran.getMinto());
            pstmnt.setString(8, tran.getParticulars());
            return pstmnt;
        });
    }

    @Override
    public void save(String userId, String activity) {
        String sql = SQLQueries.updateUserActivity + userId;
        jdbcTemplate.update(sql);
        jdbcTemplate.update(SQLQueries.insertUserActivity, new Object[]{userId, activity});
    }

    @Override
    public void save(InviteCode code) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInviteCode, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, code.getInviteCode());
            pstmnt.setString(2, code.getExpiryTs());
            pstmnt.setString(3, code.getCreatedBy());
            pstmnt.setString(4, code.getCreatedTs());
            pstmnt.setString(5, code.getPurpose());
            pstmnt.setString(6, code.getEmailId());
            return pstmnt;
        }, holder);
        String id = String.valueOf(holder.getKey().intValue());
        code.setId(id);
    }

    @Override
    public void allocateEmail(InviteCode code) {
        jdbcTemplate.update(SQLQueries.allocateEmail, new Object[]{code.getEmailId(), code.getId()});
    }

    @Override
    public void save(UserInfo user) {
        Object totalCustomers = null;
        List<Map<String, Object>> totals = totals();
        for (Map<String, Object> map : totals) {
            String tbl = (String) map.get("tbl");
            if (tbl.equals("user_master")) {
                totalCustomers = map.get("total");
                break;
            }
        }
        Object refId = "";
        try {
            int i = new BigInteger(totalCustomers.toString()).intValue() + 1;
            if (i > 0) {
                refId = "M-" + String.valueOf(i * 4) + user.getFullName().substring(0, 2).toUpperCase();
            }
        } catch (Exception ex) {
            int i = 1;
            if (i > 0) {
                refId = "M-" + String.valueOf(i * 4) + user.getFullName().substring(0, 2).toUpperCase();
            }
        }
        user.setRefId(refId.toString());
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUser, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getFullName());
            pstmnt.setString(2, user.getDob());
            pstmnt.setString(3, user.getEmail());
            pstmnt.setString(4, user.getPassword());
            pstmnt.setString(5, user.getInviteCode());
            pstmnt.setString(6, user.getMobileNo());
            pstmnt.setString(7, user.getCreatedTs());
            pstmnt.setString(8, user.getRefId());
            return pstmnt;
        }, holder);
        final int user_id = holder.getKey().intValue();
        user.setUserId(String.valueOf(user_id));
        System.out.println("primaryKey user_id  ---- " + user_id);

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertRole, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getUserId());
            pstmnt.setString(2, user.getRole() != null ? user.getRole() : "ROLE_USER");
            return pstmnt;
        });

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.usedInviteCode, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, user.getInviteCode());
            return pstmnt;
        });

        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(user.getCreatedTs());
        tran.setUser_id(user.getUserId());
        tran.setParticulars("Opening Account...");
        tran.setInc_dec("Inc");
        tran.setAmount("0.00");
        save(tran);
    }

    @Override
    public List<UserInfo> users() {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.users, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setInviteCode(rs.getString("invite_code"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            user.setDateTime(rs.getString("date_time"));
            return user;
        });
        return list;
    }

    @Override
    public UserInfo findByUsername(String username) {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.loginSQL, new Object[]{username}, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setCreatedTs(rs.getString("created_ts"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            return user;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public UserInfo findByUserId(String userId) {
        List<UserInfo> list = jdbcTemplate.query(SQLQueries.userById, new Object[]{userId}, (ResultSet rs, int rowNum) -> {
            UserInfo user = new UserInfo();
            user.setUserId(rs.getString("user_id"));
            user.setFullName(rs.getString("full_name"));
            user.setDob(rs.getString("dob"));
            user.setCreatedTs(rs.getString("created_ts"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            user.setMobileNo(rs.getString("mobile_no"));
            user.setRefId(rs.getString("ref_id"));
            user.setRole(rs.getString("role"));
            return user;
        });
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public UserInfo updateUserPassword(UserInfo user) {
        try {
            jdbcTemplate.update((Connection con) -> {
                PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updatepasswordsql, 1);
//                String cryptedPassword = "";
//                cryptedPassword = new BCryptPasswordEncoder().encode(user.getPerson_password().trim());
                pstmnt.setString(1, user.getPassword());
                pstmnt.setString(2, user.getUserId());
//                System.out.println(user.getPerson_ID());
                return pstmnt;
            });
            System.out.println("person update complete  ---- ");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exception ---- " + e);
        } finally {
            return user;
        }
    }

    @Override
    public void addShare(ShareFund share) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertShare, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, share.getName());
            pstmnt.setString(2, share.getDescription());
            pstmnt.setString(3, share.getCreatedBy());
            pstmnt.setString(4, share.getExchangeCode());
            pstmnt.setString(5, share.getCustodianId());
            return pstmnt;
        });
    }

    @Override
    public void addCurrency(ShareFund currency) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertCurrency, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, currency.getName());
            pstmnt.setString(2, currency.getCurrencySymbol());
            pstmnt.setString(3, currency.getDescription());
            pstmnt.setString(4, currency.getCreatedBy());
            pstmnt.setString(5, currency.getExchangeCode());
            pstmnt.setString(6, currency.getCustodianId());
            return pstmnt;
        });
    }

    @Override
    public ShareFund shareById(String shareId) {
        ShareFund shareById = jdbcTemplate.queryForObject(SQLQueries.shareById, new Object[]{shareId}, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            return share;
        }//id, name, desc, active, approved, created_by, exchange_code, custodian_id
        );
        return shareById;
    }

    @Override
    public void updateShare(ShareFund share) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updateShare, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, share.getName());
            pstmnt.setString(2, share.getDescription());
            pstmnt.setString(3, share.getExchangeCode());
            pstmnt.setString(4, share.getCustodianId());
            pstmnt.setString(5, share.getShareId());
            return pstmnt;
        });
    }

    @Override
    public void updateSharePrice(ShareFund share) {
        jdbcTemplate.update(SQLQueries.updateOldSharePrice, new Object[]{share.getShareId()});
        jdbcTemplate.update(SQLQueries.insertSharePrice, new Object[]{share.getShareId(), share.getPrice(), share.getCreatedDate()});
    }

    @Override
    public List<ShareFund> shares() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.shares, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> currencies() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.currencies, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setCurrencySymbol(rs.getString("symbol"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            return share;
        });
        return list;
    }

    @Override
    public void addFund(ShareFund fund) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFund, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, fund.getName());
            pstmnt.setString(2, fund.getDescription());
            pstmnt.setString(3, fund.getCreatedBy());
            pstmnt.setString(4, fund.getCustodianId());
            pstmnt.setString(5, fund.getExchangeCode());
            pstmnt.setString(6, fund.getMaster());
            return pstmnt;
        }, holder);
        final String fundId = String.valueOf(holder.getKey().intValue());
        final String[] shareIds = fund.getShareId() != null ? fund.getShareId().split(",") : new String[]{};
        final String[] coinIds = fund.getCoinId() != null ? fund.getCoinId().split(",") : new String[]{};
        final String[] percentages = fund.getPercentage().split(",");
        final String[] quantities = fund.getQuantity().split(",");
        int length = shareIds.length > 0 ? shareIds.length : coinIds.length;

        jdbcTemplate.batchUpdate(SQLQueries.insertFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                boolean shbool = shareIds.length - 1 >= i;
                boolean cobool = coinIds.length - 1 >= i;
                ps.setString(1, fundId);
                ps.setString(2, shbool ? shareIds[i] : null);
                ps.setString(3, percentages[i]);
                ps.setString(4, quantities[i]);
                ps.setString(5, fund.getCreatedDate());
                ps.setString(6, cobool ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });
    }

    @Override
    public void updateFund(ShareFund fund) {
        final String fundId = fund.getFundId();
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.updateFund, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, fund.getName());
            pstmnt.setString(2, fund.getDescription());
            pstmnt.setString(3, fund.getCustodianId());
            pstmnt.setString(4, fund.getExchangeCode());
            pstmnt.setString(5, fund.getFundId());
            return pstmnt;
        }, holder);
        jdbcTemplate.update(SQLQueries.inActiveFundDetail, new Object[]{fundId});

        final String[] shareIds = fund.getShareId() != null ? fund.getShareId().split(",") : new String[]{};
        final String[] coinIds = fund.getCoinId() != null ? fund.getCoinId().split(",") : new String[]{};
        final String[] percentages = fund.getPercentage().split(",");
        final String[] quantities = fund.getQuantity().split(",");
        int length = shareIds.length > 0 ? shareIds.length : coinIds.length;
        jdbcTemplate.batchUpdate(SQLQueries.insertFundDetail, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                boolean shbool = shareIds.length - 1 >= i;
                boolean cobool = coinIds.length - 1 >= i;
                ps.setString(1, fundId);
                ps.setString(2, shbool ? shareIds[i] : null);
                ps.setString(3, percentages[i]);
                ps.setString(4, quantities[i]);
                ps.setString(5, fund.getCreatedDate());
                ps.setString(6, cobool ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return length;
            }
        });
    }

    @Override
    public List<ShareFund> funds() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.funds, (ResultSet rs, int i) -> {
            ShareFund fund = new ShareFund();
            fund.setFundId(rs.getString("id"));
            fund.setName(rs.getString("name"));
            fund.setDescription(rs.getString("desc"));
            fund.setExchangeCode(rs.getString("exchange_code"));
            fund.setCustodianId(rs.getString("custodian_id"));
            fund.setPercentage(rs.getString("interest_rate"));
            fund.setShares(rs.getString("shares"));//count of registered shares with fund
            return fund;
        });
        return list;
    }

    @Override
    public ShareFund fundById(String fundId) {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.fundById, (ResultSet rs, int i) -> {
            ShareFund fund = new ShareFund();
            fund.setFundId(rs.getString("id"));
            fund.setName(rs.getString("name"));
            fund.setDescription(rs.getString("desc"));
            fund.setExchangeCode(rs.getString("exchange_code"));
            fund.setCustodianId(rs.getString("custodian_id"));
            fund.setPercentage(rs.getString("interest_rate"));
            fund.setShares(rs.getString("shares"));//count of registered shares with fund
            return fund;
        }, new Object[]{fundId});
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<ShareFund> portfolios() {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.portfolios, (ResultSet rs, int i) -> {
            ShareFund fund = new ShareFund();
            fund.setFundId(rs.getString("id"));
            fund.setName(rs.getString("name"));
            fund.setCurrencySymbol(rs.getString("curr_sym"));
            fund.setDescription(rs.getString("desc"));
            fund.setExchangeCode(rs.getString("exchange_code"));
            fund.setCustodianId(rs.getString("custodian_id"));
            fund.setPercentage(rs.getString("interest_rate"));
            String image = rs.getString("img");
            fund.setMaster(image != null ? image : "crypto-coins1.png");
            fund.setShares(rs.getString("shares"));//count of registered shares with fund
            return fund;
        });
        return list;
    }

    @Override
    public ShareFund portfolioById(String portfolioId) {
        List<ShareFund> list = jdbcTemplate.query(SQLQueries.portfolioById, (ResultSet rs, int i) -> {
            ShareFund fund = new ShareFund();
            fund.setFundId(rs.getString("id"));
            fund.setName(rs.getString("name"));
            fund.setDescription(rs.getString("desc"));
            fund.setExchangeCode(rs.getString("exchange_code"));
            fund.setCustodianId(rs.getString("custodian_id"));
            fund.setPercentage(rs.getString("interest_rate"));
            fund.setShares(rs.getString("shares"));//count of registered shares with fund
            return fund;
        }, new Object[]{portfolioId});
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<UserFundsCommand> userFunds(String fundId, String userId, String investmentId) {
        String sql = " SELECT IM.user_id, IM.id investment_ID, IM.customer_name, IM.investment_amount, IM.fund_id, F.name AS investment_name, F.desc FROM `investment` IM \n"
                + " INNER JOIN fund F ON (F.active = 'Y' AND F.id = IM.fund_id) WHERE IM.active = 'Y' ";
        if (fundId != null) {
            sql += " AND IM.fund_id  = " + fundId;
        }
        if (userId != null) {
            sql += " AND IM.user_id  = " + userId;
        }
        if (investmentId != null) {
            sql += " AND IM.id  = " + investmentId;
        }
        List<UserFundsCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserFundsCommand fcm = new UserFundsCommand();
            fcm.setUser_id(rs.getString("user_id"));
            fcm.setName(rs.getString("investment_name"));
            fcm.setInvestment_id(rs.getString("investment_ID"));
            fcm.setInvestment_amount(rs.getString("investment_amount"));
            fcm.setFund_id(rs.getString("fund_id"));
            return fcm;
        });
        return list;
    }

    @Override
    public List<ShareFund> sharesByFund(String fundId) {
        String sql = " SELECT S.*,SP.price,FD.percentage,FD.quantity,FD.fund_id, F.name as fund_name FROM fund_detail FD\n "
                + " inner join share_price SP ON (FD.share_id = SP.share_id and SP.active = 'Y')\n "
                + " inner join share S ON (S.active= 'Y' AND SP.share_id = S.id)\n "
                + " inner join fund F ON (F.active = 'Y' AND FD.fund_id = F.id)\n "
                + " WHERE FD.active = 'Y' AND FD.fund_id = " + fundId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            share.setPrice(rs.getString("price"));
            share.setPercentage(rs.getString("percentage"));
            share.setQuantity(rs.getString("quantity"));
            share.setFundId(rs.getString("fund_id"));
            share.setShareName(rs.getString("name"));
            share.setFundName(rs.getString("fund_name"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> coinsByFund(String fundId) {
        String sql = " SELECT FD.*, F.name as fund_name FROM fund_detail FD \n"
                + " inner join fund F ON (F.active = 'Y' AND FD.fund_id = F.id) \n"
                + " WHERE FD.active = 'Y' AND FD.fund_id = " + fundId;
        //id, fund_id, share_id, percentage, cdate, active, quantity, coin_id
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
//                share.setShareId(rs.getString("id"));
//                share.setName(rs.getString("name"));
//                share.setDescription(rs.getString("desc"));
//                share.setExchangeCode(rs.getString("exchange_code"));
//                share.setCustodianId(rs.getString("custodian_id"));
//                share.setPrice(rs.getString("price"));
            share.setFundId(rs.getString("fund_id"));
            share.setPercentage(rs.getString("percentage"));
            share.setQuantity(rs.getString("quantity"));
            share.setCoinId(rs.getString("coin_id"));
            share.setShareName(rs.getString("coin_id"));
            share.setFundName(rs.getString("fund_name"));
            return share;
        });
        return list;
    }

    @Override
    public void saveInvestment(InvestmentBean bean) {
        String sql = "SELECT CONVERT(id, CHAR(11)) as investment_id FROM investment IM WHERE IM.user_id = " + bean.getUserId() + " and fund_id = " + bean.getFundId();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        String investmentId = null;
        if (!list.isEmpty()) {
            Map<String, Object> fR = list.get(0);
            Object obj = fR.get("investment_id");
            investmentId = (String) obj;
        }
        if (investmentId != null) {
            jdbcTemplate.update(SQLQueries.updateInvestmentAmount, new Object[]{bean.getInvestmentAmount(), investmentId});
        } else {
            GeneratedKeyHolder holder = new GeneratedKeyHolder();
            jdbcTemplate.update((java.sql.Connection con) -> {
                PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertInvestment, Statement.RETURN_GENERATED_KEYS);
                pstmnt.setString(1, bean.getUserId());
                pstmnt.setString(2, bean.getCustomerName());
                pstmnt.setString(3, bean.getInvestmentAmount());
                pstmnt.setString(4, bean.getBankAccount());
                pstmnt.setString(5, bean.getReferenceNo());
                pstmnt.setString(6, bean.getFundId());
                pstmnt.setString(7, bean.getCreatedDate());
                pstmnt.setString(8, bean.getTimeFrame());
                pstmnt.setString(9, bean.getBankName());
                pstmnt.setString(10, bean.getYears());
                pstmnt.setString(11, bean.getRegularlyAmount());
                return pstmnt;
            }, holder);
            investmentId = String.valueOf(holder.getKey().intValue());
        }
        bean.setInvestmentId(investmentId);
        List<ShareFund> fundActualShares = bean.getFundActualShares();
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentPendingShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, bean.getCreatedDate());
                ps.setString(2, bean.getUserId());
                ps.setString(3, bean.getInvestmentId());
                ps.setString(4, fundActualShares.get(i).getShareId());
                ps.setString(5, fundActualShares.get(i).getPrice());
                ps.setString(6, fundActualShares.get(i).getQuantity());
                ps.setString(7, "PENDING");
                ps.setString(8, fundActualShares.get(i).getCoinId());
            }

            @Override
            public int getBatchSize() {
                return fundActualShares.size();
            }
        });
    }

    @Override
    public List<InvestmentBean> pendingInvestments(String userId) {
        String sql = " SELECT IM.user_id, IM.id as investment_id, IM.customer_name, F.name as investment_name, IM.investment_amount, IM.time_frame, shares, action, U.ref_id\n"
                + " FROM investment IM \n"
                + " inner join user_master U ON(IM.user_id = U.user_id) \n"
                + " inner join \n"
                + " (\n"
                + " select UST.investment_id, group_concat(S.name) as shares, UST.action from user_shares_pending_transaction UST \n"
                + " inner join share S ON(S.active= 'Y' AND S.id = UST.share_id)\n"
                + " where UST.`action` = 'PENDING' AND UST.`active` = 'Y'\n"
                + " group by UST.investment_id\n"
                + " union all\n"
                + " select UST.investment_id, group_concat(UST.coin_id) as shares, UST.action from user_shares_pending_transaction UST \n"
                + " where UST.`action` = 'PENDING' AND UST.`active` = 'Y' AND UST.`coin_id` is not null\n"
                + " group by UST.investment_id\n"
                + " ) A on (A.investment_id= IM.id)\n"
                + " inner join fund F on(F.active = 'Y' AND F.id = IM.fund_id)\n"
                + " where A.action = 'PENDING'  ";
        if (userId != null) {
            sql += " and IM.user_id = " + userId;
        }

        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setShares(rs.getString("shares"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setAction(rs.getString("action"));
            investment.setRefId(rs.getString("ref_id"));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> purchasedFundInvestments(String userId, String investmentId, String fundId) {
        String sql = "   SELECT IM.*, F.name as fund_name, S.name as share_name, UST.share_id, UST.quantity, UST.price, "
                + " UST.price * UST.quantity as value, UST.local_value FROM investment IM\n"
                + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                + " INNER JOIN share S ON(S.active= 'Y' AND UST.share_id = S.id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'FUND')\n"
                + " WHERE IM.active = 'Y' \n";
        if (fundId != null) {
            sql += " AND IM.fund_id = " + fundId + " \n";
        }
        if (userId != null) {
            sql += " AND IM.user_id = " + userId + " \n";
        }
        if (investmentId != null) {
            sql += " AND IM.id = " + investmentId + " \n";
        }
        sql += " ORDER BY id;  ";
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setInvestmentId(rs.getString("id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setRegularlyAmount(rs.getString("regularly_amount"));
            investment.setInterestRate(rs.getString("interest_rate"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setYears(rs.getString("years"));
            investment.setBankAccount(rs.getString("bank_acc"));
            investment.setBankName(rs.getString("bank_name"));
            investment.setReferenceNo(rs.getString("reference_no"));
            investment.setFundName(rs.getString("fund_name"));
            investment.setFundId(rs.getString("fund_id"));
            investment.setShareName(rs.getString("share_name"));
            investment.setShareId(rs.getString("share_id"));
            investment.setQuantity(rs.getString("quantity"));
            investment.setPrice(rs.getString("price"));
            investment.setValue(rs.getString("value"));
            investment.setLocalValue(rs.getString("local_value"));
            investment.setCurrency(rs.getString("currency"));
            return investment;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> purchasedPortfolioInvestments(String userId, String investmentId, String fundId) {
        StringBuilder sql = new StringBuilder(" SELECT U.ref_id, U.email, IM.*, F.name as fund_name, F.img as fund_image, UST.created_ts as created_date, UST.coin_id as share_name, UST.coin_id as share_id, UST.quantity, "
                + " UST.price, UST.price * UST.quantity as value, UST.local_value, UT.units_qty\n"
                + " FROM investment IM \n"
                + " INNER JOIN user_master U ON(IM.user_id = U.user_id) \n"
                //                + " INNER JOIN (SELECT id, created_ts, user_id, investment_id, share_id, price, local_price, (CASE WHEN action = 'SOLD' THEN -quantity ELSE quantity END) AS quantity, active, action, coin_id FROM user_shares_transaction) UST ON (UST.investment_id = IM.id) \n"
                + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'PORTFOLIO') \n"
                + " LEFT JOIN (SELECT user_id, investment_id, SUM(CASE WHEN action = 'SOLD' THEN -minto ELSE minto END) AS units_qty FROM user_inv_units_txn GROUP BY user_id, investment_id) UT ON(UT.user_id = IM.user_id and UT.investment_id = IM.id)\n"
                + " WHERE IM.active = 'Y' \n");
        if (userId != null) {
            sql.append(" AND IM.user_id = ").append(userId).append(" \n");
        }
        if (investmentId != null) {
            sql.append(" AND IM.id = ").append(investmentId).append(" \n");
        }
        sql.append(" ORDER BY id; ");
        List<InvestmentBean> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setRefId(rs.getString("ref_id"));
            investment.setEmail(rs.getString("email"));
            investment.setInvestmentId(rs.getString("id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setInvestmentAmount(rs.getString("investment_amount"));
            investment.setLocalInvestmentAmount(rs.getString("local_investment_amount"));
            investment.setRegularlyAmount(rs.getString("regularly_amount"));
            investment.setInterestRate(rs.getString("interest_rate"));
            investment.setTimeFrame(rs.getString("time_frame"));
            investment.setYears(rs.getString("years"));
            investment.setBankAccount(rs.getString("bank_acc"));
            investment.setBankName(rs.getString("bank_name"));
            investment.setReferenceNo(rs.getString("reference_no"));
            investment.setFundName(rs.getString("fund_name"));
            String image = rs.getString("fund_image");
            image = image != null ? image : "crypto-coins1.png";
            investment.setFundImage(image);
            investment.setFundId(rs.getString("fund_id"));
            investment.setCreatedDate(rs.getString("created_date"));
            investment.setShareName(rs.getString("share_name"));
            investment.setShareId(rs.getString("share_id"));
            investment.setQuantity(rs.getString("quantity"));
            investment.setPrice(rs.getString("price"));
            investment.setValue(rs.getString("value"));
            investment.setLocalValue(rs.getString("local_value"));
            investment.setCurrency(rs.getString("currency"));
            investment.setUnits(rs.getString("units_qty"));
            return investment;
        });
        return list;
    }

    @Override
    public List<ShareFund> totalCoins(String userId, String investmentId) {
        StringBuilder sql
                = new StringBuilder("SELECT C.coin_id, sum(C.quantity) as quantity FROM\n"
                        + " (SELECT UST.coin_id, UST.quantity\n"
                        + " FROM investment IM\n"
                        + " INNER JOIN user_master U ON(IM.user_id = U.user_id)\n"
                        + " INNER JOIN user_shares_transaction UST ON(UST.investment_id = IM.id) \n"
                        + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id AND F.master = 'PORTFOLIO')\n"
                        + " WHERE IM.active = 'Y' \n");
        if (userId != null) {
            sql.append(" AND IM.user_id = ").append(userId).append(" \n");
        }
        if (investmentId != null) {
            sql.append(" AND IM.id = ").append(investmentId).append(" \n");
        }
        sql.append(" ) C GROUP BY coin_id;  ");
        List<ShareFund> list = jdbcTemplate.query(sql.toString(), (ResultSet rs, int i) -> {
            ShareFund coin = new ShareFund();
            coin.setCoinId(rs.getString("coin_id"));
            coin.setQuantity(rs.getString("quantity"));
            return coin;
        });
        return list;

    }

    @Override
    public List<ShareFund> pendingSharesByInvestment(String investmentId) {
        String sql = " SELECT user_id, investment_id, share_id, S.name as share_name, USPT.price, USPT.quantity, USPT.price * USPT.quantity AS amount\n"
                + " FROM `user_shares_pending_transaction` USPT\n"
                + " INNER JOIN `share` S ON (S.active= 'Y' AND S.id = USPT.share_id)\n"
                + " WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' AND USPT.investment_id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setName(rs.getString("share_name"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> pendingCoinsByInvestment(String investmentId) {
        String sql = "  SELECT user_id, investment_id, coin_id, USPT.price, USPT.quantity, USPT.price * USPT.quantity AS amount\n"
                + " FROM `user_shares_pending_transaction` USPT\n"
                + " WHERE USPT.`action` = 'PENDING' AND USPT.`active` = 'Y' AND USPT.investment_id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setCoinId(rs.getString("coin_id"));
            share.setName(rs.getString("coin_id"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            return share;
        });
        return list;
    }

    @Override
    public void updatePendingShares(ShareFund investmentShares) {
        String sql = "UPDATE `user_shares_pending_transaction` SET `active` = 'N'"
                + " WHERE `user_id` = ? and `investment_id` = ?"
                //                + " and `share_id` IN (" + investmentShares.getShareId() + ")"
                + " and `action` = 'PENDING'";
        jdbcTemplate.update(sql, new Object[]{investmentShares.getUserId(), investmentShares.getInvestmentId()});

        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentPendingShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "DONE");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
    }

    @Override
    public void saveAccountingEvent(AccountEventCommand event) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        long creationTime = System.currentTimeMillis();
        java.sql.Timestamp timestamp = new java.sql.Timestamp(creationTime);
        event.setCreateDate(timestamp.toString());
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFundEvent, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, event.getUserId());
            pstmnt.setString(2, event.getInvestmentId());
            pstmnt.setString(3, event.getAmount());
            pstmnt.setString(4, event.getActionType());
            pstmnt.setString(5, event.getCreateDate());
            pstmnt.setString(6, event.getDescription());
            pstmnt.setString(7, "PENDING");
            return pstmnt;
        }, holder);
    }

    @Override
    public void purchaseShares(ShareFund investmentShares) {
        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentUpdatedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "PURCHASED");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < maxlength; i++) {
            total = total.add(new BigDecimal(prices[i]).multiply(new BigDecimal(quantities[i])));
        }
        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(investmentShares.getCreatedDate());
        tran.setUser_id(investmentShares.getUserId());
        tran.setInvestment_id(investmentShares.getInvestmentId());
        tran.setParticulars(Constants.depositParticulars);
        tran.setInc_dec("Inc");
        tran.setAmount(total.toPlainString());
        tran.setLocal(!investmentShares.getLocal().isEmpty() ? investmentShares.getLocal() : null);
        tran.setUsd(!investmentShares.getUsd().isEmpty() ? investmentShares.getUsd() : null);
        tran.setBtc(!investmentShares.getBtc().isEmpty() ? investmentShares.getBtc() : null);
        tran.setMinto(!investmentShares.getMinto().isEmpty() ? investmentShares.getMinto() : null);
        save(tran);
        if (tran.getMinto() != null) {
//            BigDecimal unitPrice = null;
//            try {
//                ShareFund totalUnits = totalUnitsById(null, investmentShares.getInvestmentId());
//                unitPrice = new BigDecimal(totalUnits.getPrice());
//            } catch (Exception ex) {
//                unitPrice = BigDecimal.TEN;
//            }
//            BigDecimal units = total.divide(unitPrice, 8, RoundingMode.HALF_UP);
//            tran.setUnits(units.toPlainString());
            tran.setParticulars("PURCHASED");
            saveUnitTransaction(tran);
        }
        tran.setParticulars("Purchased Shares/ Coins");
        tran.setInc_dec("Dec");
        saveWalletTransaction(tran);
        try {
            if (investmentShares.getReqId() != null && Integer.parseInt(investmentShares.getReqId()) > 0) {
                jdbcTemplate.update("UPDATE `investment_request` SET `status`='DONE' WHERE `id`=?;", new Object[]{investmentShares.getReqId()});
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public void updatePurchasedShares(ShareFund updatedShares) {
        String[] ids = updatedShares.getId().split(",");
        String[] prices = updatedShares.getPrice().split(",");
        String[] quantities = updatedShares.getQuantity().split(",");
        jdbcTemplate.batchUpdate(SQLQueries.updatePurchasedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, prices[i]);
                ps.setString(2, quantities[i]);
                ps.setString(3, ids[i]);
            }

            @Override
            public int getBatchSize() {
                return ids.length;
            }
        });
    }

    @Override
    public List<ShareFund> sharesByInvestment(String investmentId) {
        String sql = "  SELECT UST.id, UST.action, user_id, investment_id, share_id, S.name as share_name, UST.price, \n"
                + " sum(CASE WHEN UST.action = 'PURCHASED' THEN + UST.quantity ELSE - UST.quantity END) as quantity, \n"
                + " sum(UST.price * CASE WHEN UST.action = 'PURCHASED' THEN + UST.quantity ELSE - UST.quantity END) AS amount, coin_id\n"
                + " FROM `user_shares_transaction` UST\n"
                + " LEFT JOIN `share` S ON (S.active= 'Y' AND S.id = UST.share_id)\n"
                + " WHERE UST.active = 'Y' and  UST.investment_id =" + investmentId
                + " \nGROUP BY share_id;";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setName(rs.getString("share_name"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            share.setCoinId(rs.getString("coin_id"));
            return share;
        });
        return list;
    }
//id, created_ts, user_id, investment_id, share_id, price, quantity, amount, active, action, coin_id, share_name

    @Override
    public List<ShareFund> detailsByInvestment(String userId, String investmentId, String action) {
        String sql = " SELECT UST.id, UST.created_ts, UST.user_id, UST.investment_id, UST.share_id, UST.price, UST.quantity,UST.price * UST.quantity as amount, UST.active, UST.action, UST.coin_id, S.name as share_name FROM user_shares_transaction UST\n"
                + "left join share S ON (UST.share_id = S.id) \n"
                + "where UST.user_id = " + userId;
        if (investmentId != null) {
            sql += " and UST.investment_id = " + investmentId;
        }
        if (action != null) {
            sql += " and UST.action = '" + action + "'";
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setId(rs.getString("id"));
            share.setCreatedDate(rs.getString("created_ts"));
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("share_id"));
            share.setPrice(rs.getString("price"));
            share.setQuantity(rs.getString("quantity"));
            share.setShareAmount(rs.getString("amount"));
            share.setMaster(rs.getString("action"));
            share.setCoinId(rs.getString("coin_id"));
            share.setName(rs.getString("share_name"));
            return share;
        });
        return list;
    }

    @Override
    public List<InvestmentBean> pendingTransactions(String userId) {
        String sql = " SELECT IR.id as req_id, IR.user_id, IR.investment_id, IM.customer_name, F.name as investment_name, IR.amount, IR.action, U.ref_id \n"
                + " FROM investment_request IR\n"
                + " INNER JOIN investment IM ON(IR.`investment_id` = IM.`id`)\n"
                + " INNER JOIN user_master U ON(U.user_id = IM.user_id)\n"
                + " INNER JOIN fund F ON(F.active = 'Y' AND F.id = IM.fund_id)\n"
                + " WHERE IR.`active` = 'Y' AND IR.`status` = 'PENDING' ";
        if (userId != null) {
            sql += " AND IM.user_id = " + userId;
        }
        List<InvestmentBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            InvestmentBean investment = new InvestmentBean();
            investment.setReqId(rs.getString("req_id"));
            investment.setUserId(rs.getString("user_id"));
            investment.setInvestmentId(rs.getString("investment_id"));
            investment.setCustomerName(rs.getString("customer_name"));
            investment.setFundName(rs.getString("investment_name"));
            investment.setInvestmentAmount(rs.getString("amount"));
            investment.setAction(rs.getString("action"));
            investment.setRefId(rs.getString("ref_id"));
            return investment;
        });
        return list;
    }

    @Override
    public void sellShares(ShareFund investmentShares) {
        String[] shareIds = investmentShares.getShareId().split(",");
        String[] prices = investmentShares.getPrice().split(",");
        String[] quantities = investmentShares.getQuantity().split(",");
        String[] coinIds = investmentShares.getCoinId().split(",");
        int maxlength = shareIds.length > 0 ? shareIds.length : coinIds.length;
        for (int i = 0; i < maxlength; i++) {
            shareIds[i] = "null".equalsIgnoreCase(shareIds[i]) ? null : shareIds[i];
            coinIds[i] = "null".equalsIgnoreCase(coinIds[i]) ? null : coinIds[i];
        }
        jdbcTemplate.batchUpdate(SQLQueries.insertInvestmentUpdatedShares, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, investmentShares.getCreatedDate());
                ps.setString(2, investmentShares.getUserId());
                ps.setString(3, investmentShares.getInvestmentId());
                ps.setString(4, shareIds.length > i ? shareIds[i] : null);
                ps.setString(5, prices[i]);
                ps.setString(6, quantities[i]);
                ps.setString(7, "SOLD");
                ps.setString(8, coinIds.length > i ? coinIds[i] : null);
            }

            @Override
            public int getBatchSize() {
                return maxlength;
            }
        });
        BigDecimal total = BigDecimal.ZERO;
        for (int i = 0; i < maxlength; i++) {
            total = total.add(new BigDecimal(prices[i]).multiply(new BigDecimal(quantities[i])));
        }
        TransactionBean tran = new TransactionBean();
        tran.setCreated_ts(investmentShares.getCreatedDate());
        tran.setUser_id(investmentShares.getUserId());
        tran.setInvestment_id(investmentShares.getInvestmentId());
        tran.setParticulars(Constants.withdrawlParticulars);
        tran.setInc_dec("Dec");
        tran.setAmount(total.toPlainString());
        save(tran);
        BigDecimal unitPrice = null;
        try {
            ShareFund totalUnits = totalUnitsById(null, investmentShares.getInvestmentId());
            unitPrice = new BigDecimal(totalUnits.getPrice());
        } catch (Exception ex) {
            unitPrice = BigDecimal.TEN;
        }
        BigDecimal units = total.divide(unitPrice, 8, RoundingMode.HALF_UP);
        tran.setUnits(units.toPlainString());
        tran.setParticulars("SOLD");
        saveUnitTransaction(tran);
        jdbcTemplate.update("UPDATE `investment` SET `investment_amount`=`investment_amount` - ? WHERE `id`=?;", new Object[]{total, investmentShares.getInvestmentId()});
        try {
            if (investmentShares.getReqId() != null && Integer.parseInt(investmentShares.getReqId()) > 0) {
                jdbcTemplate.update("UPDATE `investment_request` SET `status`='DONE' WHERE `id`=?;", new Object[]{investmentShares.getReqId()});
            }
        } catch (Exception ex) {

        }
    }

    @Override
    public List<ShareFund> actualSharePercentages(String investmentId) {
        String sql = " select IM.user_id, IM.id as investment_id, S.*, SP.price, FD.percentage from investment IM \n"
                + "inner join fund_detail FD ON(IM.fund_id = FD.fund_id)\n"
                + "inner join share S ON(S.active= 'Y' AND FD.share_id = S.id)\n"
                + "inner join share_price SP ON (SP.share_id = S.id) \n"
                + "where IM.id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
            share.setShareId(rs.getString("id"));
            share.setName(rs.getString("name"));
            share.setDescription(rs.getString("desc"));
            share.setExchangeCode(rs.getString("exchange_code"));
            share.setCustodianId(rs.getString("custodian_id"));
            share.setPrice(rs.getString("price"));
            share.setPercentage(rs.getString("percentage"));
            return share;
        });
        return list;
    }

    @Override
    public List<ShareFund> actualCoinPercentages(String investmentId) {
        String sql = " select IM.user_id, IM.id as investment_id, FD.fund_id, FD.coin_id, FD.percentage from investment IM \n"
                + "inner join fund_detail FD ON(IM.fund_id = FD.fund_id and  FD.active = 'Y')\n"
                + "inner join fund F ON(IM.fund_id = F.id and F.master = 'PORTFOLIO') where IM.id = " + investmentId;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund share = new ShareFund();
            share.setUserId(rs.getString("user_id"));
            share.setInvestmentId(rs.getString("investment_id"));
//                share.setShareId(rs.getString("id"));
//                share.setName(rs.getString("name"));
//                share.setDescription(rs.getString("desc"));
//                share.setExchangeCode(rs.getString("exchange_code"));
//                share.setCustodianId(rs.getString("custodian_id"));
//                share.setPrice(rs.getString("price"));
            share.setCoinId(rs.getString("coin_id"));
            share.setPercentage(rs.getString("percentage"));
            return share;
        });
        return list;
    }

    @Override
    public List<AccountEventCommand> latestInvestments(String userId) {
        String sql = " select * FROM investment_request\n"
                + " where user_id = " + userId + " limit 5";

        List<AccountEventCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            AccountEventCommand trns = new AccountEventCommand();
            trns.setActionType(rs.getString("action"));
            trns.setStatus(rs.getString("status"));
            trns.setAmount(rs.getString("amount"));
            trns.setCreateDate(rs.getString("created_ts"));
            trns.setDescription(rs.getString("description"));
            return trns;
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> totals() {
        String sql = " SELECT FLOOR(count(*)) as total, 'user_master' AS tbl FROM user_master where active = 'Y'\n"
                + " UNION ALL\n"
                + " SELECT FLOOR(count(*)) as total, 'investment' as tbl FROM investment where active = 'Y'\n"
                + " UNION ALL\n"
                + " SELECT FLOOR(SUM(CASE WHEN action = 'PURCHASED' THEN + (price *quantity) ELSE - (price * quantity) END)) as total, 'user_shares_transaction' as tbl FROM user_shares_transaction WHERE active = 'Y'; ";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<ShareFund> latestBalanceData(String type, String userId, String investmentId) {
        String sql = " SELECT id, created_ts, user_id, investment_id, share_id, price, quantity, active, action, coin_id FROM user_shares_transaction  "
                + " WHERE coin_id in (SELECT coin_id FROM user_shares_transaction WHERE active = 'Y' ";
        if (userId != null) {
            sql += " and user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and investment_id = " + investmentId;
        }
        sql += " group by coin_id) ";
        if (userId != null) {
            sql += " and user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and investment_id = " + investmentId;
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund coin = new ShareFund();
            coin.setCreatedDate(rs.getString("created_ts"));
            coin.setUserId(rs.getString("user_id"));
            coin.setInvestmentId(rs.getString("investment_id"));
            coin.setPrice(rs.getString("price"));
            coin.setQuantity(rs.getString("quantity"));
            coin.setCoinId(rs.getString("coin_id"));
            coin.setMaster(rs.getString("action"));
            return coin;
        });
        return list;
    }

    @Override
    public List<TransactionBean> userAccountSummary(String userId) {
        String sql = " SELECT T1.*, SUM(CASE WHEN T2.inc_dec = 'Inc' THEN T2.amount ELSE - T2.amount END) AS balance FROM transactions T1\n"
                + " INNER JOIN transactions T2 ON (T2.id <= T1.id and T1.user_id = T2.user_id) \n"
                + " WHERE T1.user_id = " + userId
                + " \nGROUP BY T1.id; ";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("created_ts"));
            tran.setUser_id(rs.getString("user_id"));
            tran.setInvestment_id(rs.getString("investment_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setBalance(rs.getString("balance"));
            return tran;
        }//id, created_ts, user_id, investment_id, particulars, inc_dec, amount, active, balance
        );
        return list;
    }
//ref_id, user_id, investment_id, name, fund_id, minto, btc, created_ts, bitcoin

    @Override
    public List<ShareFund> userUnitSummary(String userId, String fromDate, String toDate) {
//        String sql = "  SELECT UT.*, UCB.created_ts, UCB.bitcoin FROM\n"
//                + " (SELECT U.ref_id, UT.user_id, UT.investment_id, F.name, F.id as fund_id, \n"
//                + " sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as minto, \n"
//                + " sum(case when action = 'SOLD' then -UT.btc else UT.btc end) as btc\n"
//                + " FROM user_inv_units_txn UT \n"
//                + " inner join investment IM on(UT.investment_id = IM.id) \n"
//                + " inner join fund F on(F.id = IM.fund_id) \n"
//                + " inner join user_master U on(U.user_id = UT.user_id) \n"
//                + " where UT.user_id = " + userId + " and UT.created_ts <= '" + date + "' \n"
//                + " group by ref_id, user_id, investment_id, name) UT \n"
//                + " left join user_curr_btc UCB on(UCB.user_id = UT.user_id and UCB.investment_id = UT.investment_id and UCB.created_ts <= '" + date + "')\n"
//                + "  ";
        String sql = "  SELECT UT.*, FT.unit_price FROM( SELECT UT.*, UCB.created_ts, UCB.bitcoin FROM\n"
                + " (SELECT U.ref_id, UT.user_id, UT.investment_id, F.name, F.id as fund_id, \n"
                + " sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as minto, \n"
                + " sum(case when action = 'SOLD' then -UT.btc else UT.btc end) as btc\n"
                + " FROM user_inv_units_txn UT \n"
                + " inner join investment IM on(UT.investment_id = IM.id) \n"
                + " inner join fund F on(F.id = IM.fund_id) \n"
                + " inner join user_master U on(U.user_id = UT.user_id) \n"
                + " where UT.user_id = " + userId;
        if (fromDate != null && !fromDate.isEmpty()) {
            sql += " and UT.created_ts >= '" + fromDate + "' \n";
        }
        if (toDate != null && !toDate.isEmpty()) {
            sql += " and UT.created_ts <= '" + toDate + "' \n";
        }
        sql += " group by ref_id, user_id, investment_id, name) UT \n"
                + " left join user_curr_btc UCB on(UCB.user_id = UT.user_id and UCB.investment_id = UT.investment_id \n"
                + " and UCB.created_ts <= " + (toDate != null && !toDate.isEmpty() ? "'" + toDate + "'" : " now() ") + ")) UT \n"
                + " left join fund_trade FT on(FT.fund_id = UT.fund_id and FT.created_ts = UT.created_ts) \n"
                + " GROUP BY ref_id, user_id, investment_id, name, fund_id, minto, btc, created_ts, bitcoin, unit_price\n"
                + " ";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund unit = new ShareFund();
            unit.setCreatedDate(rs.getString("created_ts"));
            unit.setReqId(rs.getString("ref_id"));
            unit.setUserId(rs.getString("user_id"));
            unit.setInvestmentId(rs.getString("investment_id"));
            unit.setFundName(rs.getString("name"));
            unit.setFundId(rs.getString("fund_id"));
            unit.setMinto(rs.getString("minto"));
            unit.setBtc(rs.getString("btc"));
            unit.setQuantity(rs.getString("bitcoin"));
            unit.setPrice(rs.getString("unit_price"));
            return unit;
        });
        return list;
    }

    private static final RowMapper<InviteCode> inviteCodeRowMapper = (ResultSet rs, int i) -> {
        InviteCode code = new InviteCode();
        code.setId(rs.getString("id"));
        code.setInviteCode(rs.getString("invite_code"));
        code.setExpiryTs(rs.getString("expiry_ts"));
        code.setCreatedBy(rs.getString("created_by"));
        code.setCreatedTs(rs.getString("created_ts"));
        code.setExpired(rs.getString("expired"));
        code.setUsed(rs.getString("used"));
        code.setActive(rs.getString("active"));
        code.setPurpose(rs.getString("purpose"));
        code.setEmailId(rs.getString("email"));
        return code;
    };

    @Override
    public List<InviteCode> inviteCodes(Boolean available, Boolean expired, Boolean used) {
        String sql = " SELECT * FROM invite_code where active = 'Y'; ";
        List<InviteCode> list = jdbcTemplate.query(sql, inviteCodeRowMapper);
        return list;
    }

    @Override
    public List<InviteCode> checkInviteCode(String code, String type) {
        String sql = " select * from invite_code where active = 'Y' and expired = 'N' and used = 'N' and expiry_ts > now()"
                + " and invite_code = '" + code + "' and purpose = '" + type + "' ; ";
        List<InviteCode> list = jdbcTemplate.query(sql, inviteCodeRowMapper);
        return list;
    }

    @Override
    public List<StateCity> countries(Boolean states, Boolean cities) {
        String sql = " select C.id, C.sortname code, C.name country, C.phonecode, S.id as state_id, S.name state from countries C\n"
                + "inner join states S on(C.id = S.country_id) order by id asc; ";
        List<StateCity> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            StateCity code = new StateCity();
            code.setId(rs.getString("id"));
            code.setCode(rs.getString("code"));
            code.setName(rs.getString("country"));
            code.setPhoneCode(rs.getString("phonecode"));
            code.setDetName(rs.getString("state"));
            code.setDetId(rs.getString("state_id"));
            return code;
        });
        return list;
    }

    @Override
    public Boolean deActiveShare(String shareId) {
        int id = jdbcTemplate.update(SQLQueries.deActiveShare, new Object[]{shareId});
        return id > 0;
    }

    @Override
    public Boolean deActiveFund(String fundId) {
        int id = jdbcTemplate.update(SQLQueries.deActiveFund, new Object[]{fundId});
        return id > 0;
    }

    @Override
    public void saveVerification(Verification ver) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertVerification, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, ver.getPp_passport_number());
            pstmnt.setString(2, ver.getPp_passport_expiry_date());
            pstmnt.setString(3, ver.getPp_filename());
            pstmnt.setString(4, ver.getPa_same_res_addr());
            pstmnt.setString(5, ver.getPa_is_your_pa_street_addr());
            pstmnt.setString(6, ver.getPa_postal_agency());
            pstmnt.setString(7, ver.getPa_po_box());
            pstmnt.setString(8, ver.getPa_private_bag());
            pstmnt.setString(9, ver.getPa_special_service());
            pstmnt.setString(10, ver.getPa_number());
            pstmnt.setString(11, ver.getPa_postal_office());
            pstmnt.setString(12, ver.getPa_postal_code());
            pstmnt.setString(13, ver.getPa_filename());
            pstmnt.setString(14, ver.getDl_first_name());
            pstmnt.setString(15, ver.getDl_middle_name());
            pstmnt.setString(16, ver.getDl_last_name());
            pstmnt.setString(17, ver.getDl_dob());
            pstmnt.setString(18, ver.getDl_street());
            pstmnt.setString(19, ver.getDl_street_name());
            pstmnt.setString(20, ver.getDl_street_number());
            pstmnt.setString(21, ver.getDl_license_number());
            pstmnt.setString(22, ver.getDl_license_version());
            pstmnt.setString(23, ver.getDl_filename());
            pstmnt.setString(24, ver.getAddress());
            pstmnt.setString(25, ver.getUser_id());
            pstmnt.setString(26, ver.getCreated_ts());
            return pstmnt;
        });
    }

    @Override
    public Verification findVerification(String userId) {
        String sql = " SELECT V.*, U.invite_code FROM verification V\n"
                + " INNER JOIN user_master U on(V.user_id = U.user_id)\n"
                + " WHERE U.user_id = " + userId;
        List<Verification> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            Verification ver = new Verification();
            ver.setPp_passport_number(rs.getString("pp_passport_number"));
            ver.setPp_passport_expiry_date(rs.getString("pp_passport_expiry_date"));
            ver.setPp_filename(rs.getString("pp_filename"));
            ver.setPa_same_res_addr(rs.getString("pa_same_res_addr"));
            ver.setPa_is_your_pa_street_addr(rs.getString("pa_is_your_pa_street_addr"));
            ver.setPa_postal_agency(rs.getString("pa_postal_agency"));
            ver.setPa_po_box(rs.getString("pa_po_box"));
            ver.setPa_private_bag(rs.getString("pa_private_bag"));
            ver.setPa_special_service(rs.getString("pa_special_service"));
            ver.setPa_number(rs.getString("pa_number"));
            ver.setPa_postal_office(rs.getString("pa_postal_office"));
            ver.setPa_postal_code(rs.getString("pa_postal_code"));
            ver.setPa_filename(rs.getString("pa_filename"));
            ver.setDl_first_name(rs.getString("dl_first_name"));
            ver.setDl_middle_name(rs.getString("dl_middle_name"));
            ver.setDl_last_name(rs.getString("dl_last_name"));
            ver.setDl_dob(rs.getString("dl_dob"));
            ver.setDl_street(rs.getString("dl_street"));
            ver.setDl_street_name(rs.getString("dl_street_name"));
            ver.setDl_street_number(rs.getString("dl_street_number"));
            ver.setDl_license_number(rs.getString("dl_license_number"));
            ver.setDl_license_version(rs.getString("dl_license_version"));
            ver.setDl_filename(rs.getString("dl_filename"));
            ver.setAddress(rs.getString("address"));
            ver.setUser_id(rs.getString("user_id"));
            ver.setCreated_ts(rs.getString("created_ts"));
            ver.setInvite_code(rs.getString("invite_code"));
            return ver;
        });
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void saveConfiguration(Configuration config) {
        jdbcTemplate.update(SQLQueries.updateOldConfig, new Object[]{config.getUserId()});
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertConfig, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, config.getLocalCurrency());
            pstmnt.setString(2, config.getShowCurrency());
            pstmnt.setString(3, config.getUserId());
            pstmnt.setString(4, config.getCreatedTs());
            return pstmnt;
        });
    }

    @Override
    public Configuration findConfiguration(String userId) {
        String sql = " SELECT C.* FROM config C WHERE C.active = 'Y' and C.user_id = " + userId;
        List<Configuration> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            Configuration config = new Configuration();
            config.setLocalCurrency(rs.getString("local_currency"));
            config.setShowCurrency(rs.getString("show_currency"));
            config.setUserId(rs.getString("user_id"));
            config.setCreatedTs(rs.getString("created_ts"));
            return config;
        });
        return (list != null && !list.isEmpty()) ? list.get(0) : null;
    }

    @Override
    public int saveStripeCharge(StripeCharge chg) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertStripeCharge, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, chg.getTxn_id());
            pstmnt.setString(2, chg.getUser_id());
            pstmnt.setString(3, chg.getBal_txn_id());
            pstmnt.setString(4, chg.getCreated_ts());
            pstmnt.setString(5, chg.getStripe_created_ts());
            pstmnt.setString(6, chg.getAmount());
            pstmnt.setString(7, chg.getAmount_refunded());
            pstmnt.setString(8, chg.getCurrency());
            pstmnt.setString(9, chg.getStatus());
            pstmnt.setString(10, chg.getFail_code());
            pstmnt.setString(11, chg.getFail_message());
            pstmnt.setString(12, chg.getDescription());
            pstmnt.setString(13, chg.getFund_id());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public int saveWalletTransaction(TransactionBean tran) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertWalletTransaction, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, tran.getCreated_ts());
            pstmnt.setString(2, tran.getUser_id());
            pstmnt.setString(3, tran.getTxn_id());
            pstmnt.setString(4, tran.getBrand());
            pstmnt.setString(5, tran.getParticulars());
            pstmnt.setString(6, tran.getInc_dec());
            pstmnt.setString(7, tran.getAmount());
            return pstmnt;
        }, holder);
        int id = holder.getKey().intValue();
        return id;
    }

    @Override
    public List<TransactionBean> userWalletTransactions(String userId) {
        String sql = "  SELECT T1.*, SUM(CASE WHEN T2.inc_dec = 'Inc' THEN T2.amount ELSE - T2.amount END) AS balance FROM wallet_txns T1\n"
                + " INNER JOIN wallet_txns T2 ON (T2.id <= T1.id and T1.user_id = T2.user_id) \n"
                + " WHERE T1.user_id =" + userId
                + " GROUP BY T1.id; ";
        List<TransactionBean> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            TransactionBean tran = new TransactionBean();
            tran.setId(rs.getString("id"));
            tran.setCreated_ts(rs.getString("created_ts"));
            tran.setUser_id(rs.getString("user_id"));
//                tran.setInvestment_id(rs.getString("investment_id"));
            tran.setParticulars(rs.getString("particulars"));
            tran.setInc_dec(rs.getString("inc_dec"));
            tran.setAmount(rs.getString("amount"));
            tran.setBalance(rs.getString("balance"));
            return tran;
        });
        return list;
    }

    @Override
    public void addReferenceNo(UserReference reference) {
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertUserRefNo, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, reference.getReferenceNo());
            pstmnt.setString(2, reference.getStripeToken());
            pstmnt.setString(3, reference.getPoliToken());
            pstmnt.setString(4, reference.getUserId());
            pstmnt.setString(5, reference.getFundId());
            pstmnt.setString(6, reference.getPrice());
            pstmnt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            return pstmnt;
        });
    }

    @Override
    public UserReference findReferenceNo(String token, String ref) {
        String sql = "SELECT * FROM user_ref_no WHERE active ='Y'";
        if (ref != null) {
            sql += " and ref_no = '" + ref + "'";
        }
        if (token != null) {
            sql += " and poli_token = '" + token + "'";
        }
        List<UserReference> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserReference tran = new UserReference();
            tran.setReferenceNo(rs.getString("ref_no"));
            tran.setStripeToken(rs.getString("stripe_token"));
            tran.setPoliToken(rs.getString("poli_token"));
            tran.setUserId(rs.getString("user_id"));
            tran.setFundId(rs.getString("fund_id"));
            tran.setPrice(rs.getString("price_in_nzd"));
            tran.setAmounted(rs.getString("amounted"));
            return tran;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public void updatedAmounted(String ref) {
        String sql = "UPDATE `user_ref_no` SET `amounted`='Y' WHERE `ref_no`= ? ;";
        jdbcTemplate.update(sql, new Object[]{ref});
    }

    @Override
    public void addTrade(ShareFund trade, List<ShareFund> totalUnits) {
        String sql = "UPDATE `fund_trade` SET `active`='N' WHERE `id`>'0' and fund_id =" + trade.getFundId();
        jdbcTemplate.update(sql);

        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFundTrade, Statement.RETURN_GENERATED_KEYS);
            pstmnt.setString(1, trade.getCreatedDate());
            pstmnt.setString(2, trade.getFundId());
            pstmnt.setString(3, trade.getOpBal());
            pstmnt.setString(4, trade.getCloseBal());
            pstmnt.setString(5, trade.getPurTrades());
            pstmnt.setString(6, trade.getSoldTrades());
            pstmnt.setString(7, trade.getPurCoins());
            pstmnt.setString(8, trade.getSoldCoins());
            pstmnt.setString(9, trade.getPrice());
            return pstmnt;
        });

        sql = "UPDATE `user_curr_btc` SET `active`='N' WHERE `id`>'0' and fund_id =" + trade.getFundId();
        jdbcTemplate.update(sql);

        jdbcTemplate.batchUpdate(SQLQueries.insertUserCurrBtc, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ShareFund unit = totalUnits.get(i);
                ps.setString(1, trade.getCreatedDate());
                ps.setString(2, unit.getFundId());
                ps.setString(3, unit.getUserId());
                ps.setString(4, unit.getInvestmentId());
                ps.setString(5, unit.getBtc());
                ps.setString(6, unit.getMinto());
            }

            @Override
            public int getBatchSize() {
                return totalUnits.size();
            }
        });
    }

    @Override
    public void addBitcoin(ShareFund bitcoin) {
//        String sql = "UPDATE `fund_bitcoin` SET `active`='N' WHERE `id`>'0' and fund_id =" + bitcoin.getFundId();
//        jdbcTemplate.update(sql);
//
//        jdbcTemplate.update((java.sql.Connection con) -> {
//            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertFundBitcoin, Statement.RETURN_GENERATED_KEYS);
//            pstmnt.setString(1, bitcoin.getCreatedDate());
//            pstmnt.setString(2, bitcoin.getFundId());
//            pstmnt.setString(3, bitcoin.getQuantity());
//            pstmnt.setString(4, bitcoin.getPrice());
//            pstmnt.setString(5, bitcoin.getShareAmount());
//            return pstmnt;
//        });
    }

    @Override
    public ShareFund tradeByFundId(String id) {
        String sql = " SELECT * FROM fund_trade WHERE active ='Y' and fund_id = " + id;
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setFundId(rs.getString("fund_id"));
            trade.setOpBal(rs.getString("op_bal"));
            trade.setCloseBal(rs.getString("close_bal"));
            trade.setPurTrades(rs.getString("pur_trades"));
            trade.setSoldTrades(rs.getString("sold_trades"));
            trade.setPurCoins(rs.getString("pur_coins"));
            trade.setSoldCoins(rs.getString("sold_coins"));
            return trade;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public ShareFund totalUnitsById(String fundId, String invesmentId) {
        if (fundId == null && invesmentId != null) {
            Map<String, Object> map = jdbcTemplate.queryForMap("SELECT IM.fund_id from investment IM where IM.id =" + invesmentId);
            fundId = String.valueOf(map.get("fund_id"));
        }
        String sql = "  SELECT F.id as fund_id, sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as total_units, 0 as close_bal, 0 AS unit_price\n"
                + " FROM user_inv_units_txn UT \n"
                + " inner join investment IM on(UT.investment_id = IM.id)\n"
                + " inner join fund F on(F.id = IM.fund_id)\n"
                + " inner join user_master U on(U.user_id = UT.user_id)\n"
                + " where UT.active = 'Y' AND F.id = " + fundId
                + "\n group by F.id ";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund portfolio = new ShareFund();
            portfolio.setQuantity(rs.getString("total_units"));
            portfolio.setCloseBal(rs.getString("close_bal"));
            portfolio.setPrice(rs.getString("unit_price"));
            return portfolio;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<ShareFund> totalUnitsById(String fundId, String userId, String investmentId) {
        String sql = "  SELECT U.ref_id, UT.user_id, UT.investment_id, F.name, F.id as fund_id, \n"
                + " sum(case when action = 'SOLD' then -UT.minto else UT.minto end) as minto, \n"
                + " sum(case when action = 'SOLD' then -UT.btc else UT.btc end) as btc, \n"
                + " coalesce(sum(UCB.bitcoin), 0) as bitcoin, \n"
                + " FT.unit_price as unit_price \n"
                + " FROM user_inv_units_txn UT \n"
                + " inner join investment IM on(UT.investment_id = IM.id) \n"
                + " inner join fund F on(F.id = IM.fund_id) \n"
                + " inner join user_master U on(U.user_id = UT.user_id) \n"
                + " left join user_curr_btc UCB on(UCB.user_id = UT.user_id and UCB.investment_id = UT.investment_id and UCB.active = 'Y') \n"
                + " left join fund_trade FT on(FT.fund_id = F.id and FT.active = 'Y') \n"
                + " where UT.active = 'Y'\n";
        if (fundId != null) {
            sql += " and F.id = " + fundId;
        }
        if (userId != null) {
            sql += " and U.user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and IM.id = " + investmentId;
        }
        sql += " group by ref_id, user_id, investment_id, name, fund_id, unit_price;  ";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setReqId(rs.getString("ref_id"));
            trade.setUserId(rs.getString("user_id"));
            trade.setInvestmentId(rs.getString("investment_id"));
            trade.setName(rs.getString("name"));
            trade.setFundId(rs.getString("fund_id"));
            trade.setMinto(rs.getString("minto"));
            trade.setBtc(rs.getString("btc"));
            trade.setQuantity(rs.getString("bitcoin"));
            trade.setPrice(rs.getString("unit_price"));
            return trade;
        });
        return list;
    }

    @Override
    public void saveUserTodayStatus(List<UserFundsCommand> userFunds) {
        jdbcTemplate.batchUpdate(SQLQueries.insertUserTodayStatus, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                UserFundsCommand userFund = userFunds.get(i);
                ps.setString(1, userFund.getCurrent_date());
                ps.setString(2, userFund.getUser_id());
                ps.setString(3, userFund.getInvestment_id());
                ps.setString(4, userFund.getFund_id());
                ps.setString(5, userFund.getInvestment_amount());
            }

            @Override
            public int getBatchSize() {
                return userFunds.size();
            }
        });
    }

    @Override
    public List<UserFundsCommand> getUserTodayStatus(String fundId, String userId, String investmentId, String fDate, String tDate) {
        String sql = " SELECT * FROM user_today_status where active = 'Y' \n";
        if (fundId != null) {
            sql += " and fund_id = " + fundId;
        }
        if (userId != null) {
            sql += " and user_id = " + userId;
        }
        if (investmentId != null) {
            sql += " and investment_id = " + investmentId;
        }
        sql += " and curr_date between '" + fDate + "' and now() ; ";
        List<UserFundsCommand> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            UserFundsCommand coin = new UserFundsCommand();
            coin.setCurrent_date(rs.getString("curr_date"));
            coin.setUser_id(rs.getString("user_id"));
            coin.setInvestment_id(rs.getString("investment_id"));
            coin.setFund_id(rs.getString("fund_id"));
            coin.setInvestment_amount(rs.getString("investment_amount"));
            return coin;
        });
        return list;
    }

    @Override
    public List<ShareFund> dailyUpdates(String fundId, String userId, String investmentId, Boolean individuals) {
        String sql = null;
        if (individuals) {
            String individualSql = " SELECT *, bitcoin / minto as unit_price FROM \n"
                    + " (SELECT F.name, UCB.created_ts, sum(UCB.bitcoin) as bitcoin, sum(UCB.minto) as minto FROM user_curr_btc UCB left join fund F on (UCB.fund_id = F.id)\n"
                    + " WHERE UCB.id > 0\n";
            if (fundId != null && !fundId.isEmpty()) {
                individualSql += " and UCB.fund_id = " + fundId;
            }
            if (userId != null && !userId.isEmpty()) {
                individualSql += " and UCB.user_id = " + userId;
            }
            if (investmentId != null && !investmentId.isEmpty()) {
                individualSql += " and UCB.investment_id = " + investmentId;
            }
            individualSql += " GROUP BY name, created_ts ORDER BY name, created_ts) UCB ";
            sql = individualSql;
        } else {
            String totalSql = " SELECT *, bitcoin/ minto as unit_price FROM \n"
                    + " (SELECT UCB.created_ts, sum(UCB.bitcoin) as bitcoin, sum(UCB.minto) as minto FROM user_curr_btc UCB\n"
                    + " WHERE id > 0\n";
            if (fundId != null && !fundId.isEmpty()) {
                totalSql += " and fund_id = " + fundId;
            }
            if (userId != null && !userId.isEmpty()) {
                totalSql += " and user_id = " + userId;
            }
            if (investmentId != null && !investmentId.isEmpty()) {
                totalSql += " and investment_id = " + investmentId;
            }
            totalSql += " GROUP BY created_ts ORDER BY created_ts) UCB  ";
            sql = totalSql;
        }
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            if (individuals) {
                trade.setName(rs.getString("name"));
            }
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setMinto(rs.getString("minto"));
            trade.setQuantity(rs.getString("bitcoin"));
            trade.setPrice(rs.getString("unit_price"));
            return trade;
        });
        return list;
    }

    @Override
    public void saveCurrencyConversionRate(String currencyPair, String date, String price) {
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);
        jdbcTemplate.update((java.sql.Connection con) -> {
            PreparedStatement pstmnt = con.prepareStatement(SQLQueries.insertConversionRate);
            pstmnt.setString(1, date);
            pstmnt.setString(2, "usd_nzd".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(3, "usd_gbp".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(4, "usd_inr".equalsIgnoreCase(currencyPair) ? price : null);
            pstmnt.setString(5, "usd_aud".equalsIgnoreCase(currencyPair) ? price : null);
            return pstmnt;
        });
        transactionManager.commit(txStatus);
    }

    @Override
    public ShareFund currencyConversionRate(String currencyPair, String date) {
        String sql = " SELECT id, created_ts, " + currencyPair + " FROM conversion_rate WHERE created_ts = '" + date + "'";
        List<ShareFund> list = jdbcTemplate.query(sql, (ResultSet rs, int i) -> {
            ShareFund trade = new ShareFund();
            trade.setId(rs.getString("id"));
            trade.setCreatedDate(rs.getString("created_ts"));
            trade.setPrice(rs.getString(new String(currencyPair.toCharArray())));
            return trade;
        });
        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public void updateCurrencyConversionRate(String currencyPair, String id, String price) {
        String sql = "UPDATE `conversion_rate` SET `" + currencyPair + "`='" + price + "' WHERE `id` = " + id;
        jdbcTemplate.update(sql);
    }
}
