/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Service
public class FileUtility {

    @Autowired
    private ServletContext servletContext;

    public String rootDirectoryPath() {
        String path = servletContext.getRealPath("/resources");
        return path;
    }

    public String imagesRootDirectoryPath() {
        return rootDirectoryPath() + File.separator + "images";
    }

    public String usersDirectoryPath() {
        return rootDirectoryPath() + File.separator + "users";
    }

    public String userRootDirectoryPath(long userid) {
        return usersDirectoryPath() + File.separator + userid;
    }

    public String imagesDirectoryPath(long userid) {
        return userRootDirectoryPath(userid) + File.separator + "images";
    }

    public File rootDirectory() {
        // Creating or fetch the directory to store file
        File root = new File(rootDirectoryPath());
        if (!root.exists()) {
            root.mkdirs();
        }
        return root;
    }

    public File imagesRootDirectory() {
        // Creating or fetch the directory to store file
        File root_images = new File(imagesRootDirectoryPath());
        if (!root_images.exists()) {
            root_images.mkdirs();
        }
        return root_images;
    }

    public File userRootDirectory(long userid) {
        // Creating or fetch the directory to store file
        File userrootdir = new File(userRootDirectoryPath(userid));
        if (!userrootdir.exists()) {
            userrootdir.mkdirs();
        }
        return userrootdir;
    }

    public File imagesDirectory(long userid) {
        // Creating or fetch the directory to store file
        File user_images = new File(imagesDirectoryPath(userid));
        if (!user_images.exists()) {
            user_images.mkdirs();
        }
        return user_images;
    }

    public File storeImage(long createdBy, String imageSource, String imageName) throws IOException {
        byte[] imageByte = Base64.decode(imageSource);
        return storeImage(createdBy, imageByte, imageName);
    }

    public File storeImage(long createdBy, MultipartFile file, String imageName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(createdBy, imageByte, imageName);
        }
        return null;
    }

    public File storeImage(long createdBy, byte[] imageByte, String imageName) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();
        String dirPath = imagesDirectoryPath(createdBy);
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + imageName);
        int lastIndexOf = imageName.lastIndexOf(".") + 1;
        String extension = imageName.substring(lastIndexOf);
        ImageIO.write(image, extension, outputfile);
        return outputfile;
    }

    public String getImage(long userid, String imageName) throws IOException {
        File returnfile = new File(imagesDirectoryPath(userid) + File.separator + imageName);
        if (returnfile.exists()) {
            FileInputStream fis = new FileInputStream(returnfile);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] fileBytes = new byte[(int) returnfile.length()];
            inputStream.read(fileBytes);
            inputStream.close();
            String imageSource = Base64.encode(fileBytes);
            return imageSource;
        } else {
            return null;
        }
    }

    public String getFileExists(String fileDirectoryPath, String filePath, String noFile) {
        String fileExists = "";
        fileExists = filePath.replace('/', File.separatorChar);
        String path = fileDirectoryPath + File.separator + fileExists;
        File f = new File(path);
        if (f.exists()) {
            return fileExists;
        } else {
            return noFile;
        }
    }

    public Set<String> getFileNames(long userid) {
        return getFileNames(imagesDirectory(userid));
    }

    public Set<String> getFileNames(File folder) {
        File[] listOfFiles = folder.listFiles();
        Set<String> fileNames = new HashSet<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        return fileNames;
    }
}
