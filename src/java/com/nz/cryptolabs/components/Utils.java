package com.nz.cryptolabs.components;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Utils {

    public static Date date(String timeStamp) {
        LocalDateTime now = LocalDateTime.parse(timeStamp);
        return Date.from(now.atZone(ZoneId.systemDefault()).toInstant());
    }
}
