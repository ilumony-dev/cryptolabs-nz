/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class HTTPRequestManager {

    private final static String USER_AGENT = "Mozilla/5.0";

    public String sendHTTPGetRequest(String urlPath) {
        return sendHTTPGetRequest(urlPath, null);
    }

    public String sendHTTPGetRequest(String urlPath, Map<String, String> params) {
        return sendHTTPGetRequest(urlPath, params, null);
    }

    public String sendHTTPGetRequest(String urlPath, Map<String, String> params, Map<String, String> headers) {
        try {
            String urlParameters = "";
            if (params != null && !params.isEmpty()) {
                Set<Map.Entry<String, String>> paramSet = params.entrySet();
                int i = 0;
                for (Map.Entry<String, String> entry : paramSet) {
                    urlParameters += entry.getKey() + "=" + entry.getValue() + (i < paramSet.size() - 1 ? "&" : "");
                }
            }
            urlPath = urlPath + "?" + urlParameters;
            URL url = new URL(urlPath);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            if (headers != null && !headers.isEmpty()) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    con.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + urlPath);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
//            System.out.println(response.toString());
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return null;
    }

    // HTTP POST request
    public String sendHTTPPostRequest(String urlPath, String body) {
        return sendHTTPPostRequest(urlPath, body, null);
    }

    // HTTP POST request
    public String sendHTTPPostRequest(String urlPath, String body, Map<String, String> headers) {
        try {
            URL url = new URL(urlPath);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            if (body != null) {
                con.setRequestProperty("Content-Length", Integer.toString(body.length()));
            }
            if (headers != null && !headers.isEmpty()) {
                Set<Map.Entry<String, String>> headerSet = headers.entrySet();
                for (Map.Entry<String, String> header : headerSet) {
                    con.setRequestProperty(header.getKey(), header.getValue());
                }
            }
            if (body != null) {
                con.getOutputStream().write(body.getBytes("UTF8"));
            }
            try (DataOutputStream dataOutputStream = new DataOutputStream(con.getOutputStream())) {
                dataOutputStream.flush();
            }
            int responseCode = con.getResponseCode();
            System.out.println("\nSending 'POST' request to URL : " + urlPath);
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (IOException ex) {
            System.out.println(ex);
            return null;
        }
    }

    public String postHttp(String url, List<NameValuePair> params, List<NameValuePair> headers) throws IOException {
        HttpPost post = new HttpPost(url);
        post.setEntity(new UrlEncodedFormEntity(params, Consts.UTF_8));
        post.getEntity().toString();
        if (headers != null) {
            for (NameValuePair header : headers) {
                post.addHeader(header.getName(), header.getValue());
            }
        }
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(post);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            return EntityUtils.toString(entity);
        }
        return null;
    }

    public String getHttp(String url, List<NameValuePair> headers) throws IOException {
        HttpRequestBase request = new HttpGet(url);
        if (headers != null) {
            for (NameValuePair header : headers) {
                request.addHeader(header.getName(), header.getValue());
            }
        }
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpResponse response = httpClient.execute(request);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            return EntityUtils.toString(entity);
        }
        return null;
    }

}
