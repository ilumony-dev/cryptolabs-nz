/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import com.nz.cryptolabs.services.DatabaseService;
import java.util.Calendar;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Administrator
 */
@Component
public class TimeManager {

    private static long targetDate = 0L;
    @Autowired
    private DatabaseService databaseService;
    private AbstractTimeManager timeManager = null;

    public TimeManager() {
        timeManager = new AbstractTimeManager();
        timeManager.start();
    }

    class AbstractTimeManager extends Thread {

        @Override
        public void run() {
            if (targetDate < System.currentTimeMillis()) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR, 11);
                cal.set(Calendar.MINUTE, 45);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.set(Calendar.AM_PM, Calendar.PM);
                Date date = cal.getTime();
                targetDate = date.getTime();
            }
            if (targetDate == System.currentTimeMillis()) {
                databaseService.saveTodayUpdates();
            }
        }
    }

}
