/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.components;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Administrator
 */
@Component
public class FileMethods {

    @Autowired
    private PathDirector pathDirector;
    
    public File storeImage(long createdBy, String imageSource, String imageName) throws IOException {
        byte[] imageByte = Base64.decodeBase64(imageSource);
        return storeImage(createdBy, imageByte, imageName);
    }

    public File storeImage(long createdBy, MultipartFile file, String imageName) throws IOException {
        if (!file.isEmpty()) {
            byte[] imageByte = file.getBytes();
            return storeImage(createdBy, imageByte, imageName);
        }
        return null;
    }

    public File storeImage(long createdBy, byte[] imageByte, String imageName) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        BufferedImage image = ImageIO.read(bis);
        bis.close();
        String dirPath = pathDirector.imagesDirectoryPath(createdBy);
        File outputfile = new File(dirPath);
        if (!outputfile.exists()) {
            outputfile.mkdirs();
        }
        outputfile = new File(dirPath + File.separator + imageName);
        int lastIndexOf = imageName.lastIndexOf(".") + 1;
        String extension = imageName.substring(lastIndexOf);
        ImageIO.write(image, extension, outputfile);
        return outputfile;
    }

    public String getImage(long userid, String imageName) throws IOException {
        File returnfile = new File(pathDirector.imagesDirectoryPath(userid) + File.separator + imageName);
        if (returnfile.exists()) {
            FileInputStream fis = new FileInputStream(returnfile);
            BufferedInputStream inputStream = new BufferedInputStream(fis);
            byte[] fileBytes = new byte[(int) returnfile.length()];
            inputStream.read(fileBytes);
            inputStream.close();
            String imageSource = Base64.encodeBase64String(fileBytes);
            return imageSource;
        } else {
            return null;
        }
    }

    public String getFileExists(String fileDirectoryPath, String filePath, String noFile) {
        String fileExists = "";
        fileExists = filePath.replace('/', File.separatorChar);
        String path = fileDirectoryPath + File.separator + fileExists;
        File f = new File(path);
        if (f.exists()) {
            return fileExists;
        } else {
            return noFile;
        }
    }

    public Set<String> getFileNames(long userid) {
        return getFileNames(pathDirector.imagesDirectory(userid));
    }

    public Set<String> getFileNames(File folder) {
        File[] listOfFiles = folder.listFiles();
        Set<String> fileNames = new HashSet<String>();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames.add(listOfFiles[i].getName());
            }
        }
        return fileNames;
    }
}
