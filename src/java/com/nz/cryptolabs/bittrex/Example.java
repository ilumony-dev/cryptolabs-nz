package com.nz.cryptolabs.bittrex;

import com.google.gson.Gson;
import static com.nz.cryptolabs.bittrex.BittrexExchangeService.printJson;
import com.nz.cryptolabs.bittrex.modal.publicapi.GetMarketSummariesContainer;
import com.nz.cryptolabs.bittrex.modal.publicapi.MarketSummary;
import com.nz.cryptolabs.constants.Constants;

import java.util.List;

public class Example {

    public static void main(String[] args) {
        System.out.println("begin(bittrex);");
        BittrexExchangeService bittrex = new BittrexExchangeService(Constants.bittrexApiKey, Constants.bittrexSecret);
        System.out.println("printMarket(bittrex);");
        printMarket(bittrex);
        System.out.println("printJson(bittrex.getBalances());");
        printJson(bittrex.getBalances());
        System.out.println("findBiggest24HGainer(bittrex);");
        findBiggest24HGainer(bittrex);
        System.out.println("end(bittrex);");
    }

    private static void printMarket(BittrexExchangeService bittrex) {
        printJson(bittrex.getMarkets());
    }

    private static void findBiggest24HGainer(BittrexExchangeService bittrex) {
        Gson gson = new Gson();
        List<MarketSummary> marketSummaries = gson.fromJson(bittrex.getMarketSummaries(), GetMarketSummariesContainer.class).getMarketSummaries();
        String marketName = "";
        Double percentChange = Double.NEGATIVE_INFINITY;
        for (int i = 0; i < marketSummaries.size(); i++) {
            MarketSummary marketSummary = marketSummaries.get(i);
            Double lastVal = marketSummary.getLast();
            Double prevDayVal = marketSummary.getPrevDay();
            Double marketPercentChange = (lastVal - prevDayVal) / prevDayVal;
            if (marketPercentChange > percentChange) {
                marketName = marketSummary.getMarketName();
                percentChange = marketPercentChange;
            }
        }
        System.out.println("Biggest Gainer in the last 24H is " + bittrex.getMarket(marketName).getMarketCurrencyLong()
                + " with " + Double.toString(percentChange * 100).substring(0, 6) + "% change.");
    }
}
