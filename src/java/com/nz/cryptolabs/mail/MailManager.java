/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.mail;

/**
 *
 * @author Administrator
 */
public interface MailManager {

    public void sendConfirmationMailToEmail(String to, String firstName);
    
    public void sendWelcomeToNewClient(String email, String firstName);
    
    public void sendMail(String toEmail, String fromEmail, String subject, String htmlMessage);
    
    public void sendResetToUser(String email, String firstName);
}
