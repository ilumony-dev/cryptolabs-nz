/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.mail;

import com.nz.cryptolabs.beans.SecuredUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Palo Dev
 */
@Service
public class AdminMailManager {

    @Autowired
    private MailManager mailManager;

    public void allocateEmail(SecuredUser admin, String email, String inviteCode) {
        String mail = "Hi Dear,\n"
                + "\n"
                + "\n"
                + " Welcome to CryptoLabs"
                + "\n"
                + " This is your invite code " + inviteCode
                + "\n"
                + "\n"
                + "Best regards,\n"
                + admin.getFullName() + "@CryptoLabs\n"
                + "\n";
        mailManager.sendMail(email, admin.getUsername(), "Welcome to CryptoLabs", mail);
    }

}
