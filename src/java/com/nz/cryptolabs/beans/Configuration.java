/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

/**
 *
 * @author palo12
 */
public class Configuration {

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the localCurrency
     */
    public String getLocalCurrency() {
        return localCurrency;
    }

    /**
     * @param localCurrency the localCurrency to set
     */
    public void setLocalCurrency(String localCurrency) {
        this.localCurrency = localCurrency;
    }

    /**
     * @return the showCurrency
     */
    public String getShowCurrency() {
        return showCurrency;
    }

    /**
     * @param showCurrency the showCurrency to set
     */
    public void setShowCurrency(String showCurrency) {
        this.showCurrency = showCurrency;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the createdTs
     */
    public String getCreatedTs() {
        return createdTs;
    }

    /**
     * @param createdTs the createdTs to set
     */
    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    private String currency;
    private String localCurrency;
    private String showCurrency;    
    private String userId;
    private String createdTs;
}
