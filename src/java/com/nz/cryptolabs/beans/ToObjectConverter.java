/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public interface ToObjectConverter {

    public static String checkNull(Object... oArr) {
        if (oArr != null) {
            Object o = oArr[0];
            Boolean toObject = false;
            if (oArr.length > 1 && (Boolean) oArr[1]) {
                toObject = true;
            }
            if (o instanceof List) {
                List list = (List) o;
                String objects = "";
                for (int i = 0; i < list.size(); i++) {
                    Object obj = list.get(i);
                    String toString = null;
                    if (toObject && obj instanceof ToObjectConverter) {
                        toString = ((ToObjectConverter) obj).toObject();
                    } else {
                        toString = obj.toString();
                    }
                    objects = objects + ((i > 0 && i != list.size()) ? "," : "") + toString;
                }
                return "[" + objects + "]";
            } else if (o instanceof Map) {
                Map map = (Map) o;
                String objects = "";
                List<Map.Entry> list = new ArrayList<>(map.entrySet());
                for (int i = 0; i < list.size(); i++) {
                    Map.Entry entry = list.get(i);
                    Object key = entry.getKey();
                    Object value = entry.getValue();
                    String toString = null;
                    if (toObject && value instanceof ToObjectConverter) {
                        toString = key + ":" + ((ToObjectConverter) value).toObject();
                    } else {
                        toString = "\"" + key + "\"" + ":" + value.toString();
                    }
                    objects = objects + ((i > 0 && i != list.size()) ? "," : "") + toString;
                }
                return "{" + objects + "}";
            } else if (o instanceof Double) {
                return "" + o + "";
            } else if (o instanceof String) {
                String val = (String) o;
                if (val.equalsIgnoreCase("null")) {
                    return null;
                }
                return ("\""+val+"\"").trim();
            }
            return null;
        } else {
            return null;
        }
    }

    public String toObject();
}
