/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Maninderjit
 */
public class InvestmentBean implements ToObjectConverter {

    /**
     * @return the fundImage
     */
    public String getFundImage() {
        return fundImage;
    }

    /**
     * @param fundImage the fundImage to set
     */
    public void setFundImage(String fundImage) {
        this.fundImage = fundImage;
    }

    /**
     * @return the shareImage
     */
    public String getShareImage() {
        return shareImage;
    }

    /**
     * @param shareImage the shareImage to set
     */
    public void setShareImage(String shareImage) {
        this.shareImage = shareImage;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the localPrice
     */
    public String getLocalPrice() {
        return localPrice;
    }

    /**
     * @param localPrice the localPrice to set
     */
    public void setLocalPrice(String localPrice) {
        this.localPrice = localPrice;
    }

    /**
     * @return the localValue
     */
    public String getLocalValue() {
        return localValue;
    }

    /**
     * @param localValue the localValue to set
     */
    public void setLocalValue(String localValue) {
        this.localValue = localValue;
    }

    /**
     * @return the localInvestmentAmount
     */
    public String getLocalInvestmentAmount() {
        return localInvestmentAmount;
    }

    /**
     * @param localInvestmentAmount the localInvestmentAmount to set
     */
    public void setLocalInvestmentAmount(String localInvestmentAmount) {
        this.localInvestmentAmount = localInvestmentAmount;
    }

    /**
     * @return the localInvestedAmount
     */
    public String getLocalInvestedAmount() {
        return localInvestedAmount;
    }

    /**
     * @param localInvestedAmount the localInvestedAmount to set
     */
    public void setLocalInvestedAmount(String localInvestedAmount) {
        this.localInvestedAmount = localInvestedAmount;
    }

    /**
     * @return the units
     */
    public String getUnits() {
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(String units) {
        this.units = units;
    }

    /**
     * @return the percent1hr
     */
    public double getPercent1hr() {
        return percent1hr;
    }

    /**
     * @param percent1hr the percent1hr to set
     */
    public void setPercent1hr(double percent1hr) {
        this.percent1hr = percent1hr;
    }

    /**
     * @return the percent24hr
     */
    public double getPercent24hr() {
        return percent24hr;
    }

    /**
     * @param percent24hr the percent24hr to set
     */
    public void setPercent24hr(double percent24hr) {
        this.percent24hr = percent24hr;
    }

    /**
     * @return the percent7d
     */
    public double getPercent7d() {
        return percent7d;
    }

    /**
     * @param percent7d the percent7d to set
     */
    public void setPercent7d(double percent7d) {
        this.percent7d = percent7d;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the map
     */
    public Map<String, String> getMap() {
        return map;
    }

    /**
     * @param map the map to set
     */
    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    /**
     * @return the investedAmount
     */
    public String getInvestedAmount() {
        return investedAmount;
    }

    /**
     * @param investedAmount the investedAmount to set
     */
    public void setInvestedAmount(String investedAmount) {
        this.investedAmount = investedAmount;
    }

    /**
     * @return the interestRate
     */
    public String getInterestRate() {
        return interestRate;
    }

    /**
     * @param interestRate the interestRate to set
     */
    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    /**
     * @return the regularlyAmount
     */
    public String getRegularlyAmount() {
        return regularlyAmount;
    }

    /**
     * @param regularlyAmount the regularlyAmount to set
     */
    public void setRegularlyAmount(String regularlyAmount) {
        this.regularlyAmount = regularlyAmount;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

    /**
     * @return the reqId
     */
    public String getReqId() {
        return reqId;
    }

    /**
     * @param reqId the reqId to set
     */
    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    /**
     * @return the tranId
     */
    public String getTranId() {
        return tranId;
    }

    /**
     * @param tranId the tranId to set
     */
    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the fundId
     */
    public String getFundId() {
        return fundId;
    }

    /**
     * @param fundId the fundId to set
     */
    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    /**
     * @return the fundName
     */
    public String getFundName() {
        return fundName;
    }

    /**
     * @param fundName the fundName to set
     */
    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    /**
     * @return the shares
     */
    public String getShares() {
        return shares;
    }

    /**
     * @param shares the shares to set
     */
    public void setShares(String shares) {
        this.shares = shares;
    }

    /**
     * @return the shareName
     */
    public String getShareName() {
        return shareName;
    }

    /**
     * @param shareName the shareName to set
     */
    public void setShareName(String shareName) {
        this.shareName = shareName;
    }

    /**
     * @return the years
     */
    public String getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(String years) {
        this.years = years;
    }

    /**
     * @return the investmentId
     */
    public String getInvestmentId() {
        return investmentId;
    }

    /**
     * @param investmentId the investmentId to set
     */
    public void setInvestmentId(String investmentId) {
        this.investmentId = investmentId;
    }

    /**
     * @return the shareId
     */
    public String getShareId() {
        return shareId;
    }

    /**
     * @param shareId the shareId to set
     */
    public void setShareId(String shareId) {
        this.shareId = shareId;
    }

    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the investmentAmount
     */
    public String getInvestmentAmount() {
        return investmentAmount;
    }

    /**
     * @param investmentAmount the investmentAmount to set
     */
    public void setInvestmentAmount(String investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the bankAccount
     */
    public String getBankAccount() {
        return bankAccount;
    }

    /**
     * @param bankAccount the bankAccount to set
     */
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * @return the referenceNo
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * @param referenceNo the referenceNo to set
     */
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the customerNo
     */
    public String getCustomerNo() {
        return customerNo;
    }

    /**
     * @param customerNo the customerNo to set
     */
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    /**
     * @return the investmentType
     */
    public String getInvestmentType() {
        return investmentType;
    }

    /**
     * @param investmentType the investmentType to set
     */
    public void setInvestmentType(String investmentType) {
        this.investmentType = investmentType;
    }

    /**
     * @return the createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the timeFrame
     */
    public String getTimeFrame() {
        return timeFrame;
    }

    /**
     * @param timeFrame the timeFrame to set
     */
    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return the fundActualShares
     */
    public List<ShareFund> getFundActualShares() {
        return fundActualShares;
    }

    /**
     * @param fundActualShares the fundActualShares to set
     */
    public void setFundActualShares(List<ShareFund> fundActualShares) {
        this.fundActualShares = fundActualShares;
    }

    private String email;
    private String refId;
    private String reqId;
    private String tranId;
    private String userId;
    private String fundId;
    private String fundName;
    private String fundImage;
    private String shares;
    private String shareName;
    private String shareImage;
    private String years;
    private String investmentId;
    private String shareId;
    private String price;
    private String quantity;
    private String units;
    private String value;
    private String investmentAmount;
    private String regularlyAmount;
    private String bankName;
    private String bankAccount;
    private String referenceNo;
    private String customerName;
    private String customerNo;
    private String investmentType;
    private String createdDate;
    private String timeFrame;
    private String action;
    private String interestRate;
    private String investedAmount;
    private List<ShareFund> fundActualShares;
    private Map<String, String> map;
    private double percent1hr;
    private double percent24hr;
    private double percent7d;
    private String localInvestmentAmount;
    private String localInvestedAmount;
    private String localPrice;
    private String localValue;
    private String currency;

    @Override
    public String toString() {
        return "{"
                + "\"email\":" + checkNull(email) + ", "
                + "\"refId\":" + checkNull(getRefId()) + ", "
                + "\"reqId\":" + checkNull(getReqId()) + ", "
                + "\"tranId\":" + checkNull(getTranId()) + ", "
                + "\"userId\":" + checkNull(getUserId()) + ", "
                + "\"fundId\":" + checkNull(getFundId()) + ", "
                + "\"fundName\":" + checkNull(getFundName()) + ", "
                + "\"shares\":" + checkNull(getShares()) + ", "
                + "\"shareName\":" + checkNull(getShareName()) + ", "
                + "\"investmentId\":" + checkNull(getInvestmentId()) + ", "
                + "\"shareId\":" + checkNull(getShareId()) + ", "
                + "\"price\":" + checkNull(getPrice()) + ", "
                + "\"quantity\":" + checkNull(getQuantity()) + ", "
                + "\"bankAccount\":" + checkNull(getBankAccount()) + ", "
                + "\"bankName\":" + checkNull(getBankName()) + ", "
                + "\"referenceNo\":" + checkNull(getReferenceNo()) + ", "
                + "\"customerName\":" + checkNull(getCustomerName()) + ", "
                + "\"customerNo\":" + checkNull(getCustomerNo()) + ", "
                + "\"investmentAmount\":" + (getInvestmentAmount()) + ", "
                + "\"regularlyAmount\":" + checkNull(getRegularlyAmount()) + ", "
                + "\"interestRate\":" + checkNull(interestRate) + ", "
                + "\"years\":" + checkNull(years) + ", "
                + "\"investmentType\":" + checkNull(getInvestmentType()) + ", "
                + "\"createdDate\":" + checkNull(getCreatedDate()) + ", "
                + "\"timeFrame\":" + checkNull(getTimeFrame()) + ", "
                + "\"fundActualShares\":" + checkNull(fundActualShares) + ", "
                + "\"map\":" + checkNull(map) + ", "
                + "\"value\":" + (value) + ", "
                + "\"investedAmount\":" + (getInvestedAmount()) + ", "
                + "\"percent1hr\":" + checkNull(percent1hr) + ",\n"
                + "\"percent24hr\":" + checkNull(percent24hr) + ",\n"
                + "\"percent7d\":" + checkNull(percent7d) + ",\n"
                + "\"action\":" + checkNull(getAction()) + " "
                + "}";
    }

    @Override
    public String toObject() {
        return "{"
                + "email:" + checkNull(email) + ", "
                + "fundId:" + checkNull(getFundId()) + ", "
                + "refId:" + checkNull(getRefId()) + ", "
                + "reqId:" + checkNull(getReqId()) + ", "
                + "tranId:" + checkNull(getTranId()) + ", "
                + "userId:" + checkNull(getUserId()) + ", "
                + "fundName:" + checkNull(getFundName()) + ", "
                + "shares:" + checkNull(getShares()) + ", "
                + "shareName:" + checkNull(getShareName()) + ", "
                + "investmentId:" + checkNull(getInvestmentId()) + ", "
                + "shareId:" + checkNull(getShareId()) + ", "
                + "price:" + checkNull(getPrice()) + ", "
                + "quantity:" + checkNull(getQuantity()) + ", "
                + "bankAccount:" + checkNull(getBankAccount()) + ", "
                + "bankName:" + checkNull(getBankName()) + ", "
                + "referenceNo:" + checkNull(getReferenceNo()) + ", "
                + "customerName:" + checkNull(getCustomerName()) + ", "
                + "customerNo:" + checkNull(getCustomerNo()) + ", "
                + "investmentAmount:" + (getInvestmentAmount()) + ", "
                + "regularlyAmount:" + checkNull(getRegularlyAmount()) + ", "
                + "investmentType:" + checkNull(getInvestmentType()) + ", "
                + "createdDate:" + checkNull(getCreatedDate()) + ", "
                + "timeFrame:" + checkNull(getTimeFrame()) + ", "
                + "interestRate:" + checkNull(interestRate) + ", "
                + "years:" + checkNull(years) + ", "
                + "map:" + checkNull(map) + ", "
                + "value:" + (getValue()) + ", "
                + "investedAmount:" + (getInvestedAmount()) + ", "
                + "percent1hr:" + checkNull(percent1hr) + ", "
                + "percent24hr:" + checkNull(percent24hr) + ", "
                + "percent7d:" + checkNull(percent7d) + ", "
                + "fundActualShares:" + checkNull(fundActualShares, true) + ", "
                + "action:" + checkNull(getAction()) + " "
                + "}";
    }

}
