/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import java.util.List;

/**
 *
 * @author palo12
 */
public class StateCity {

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the phoneCode
     */
    public String getPhoneCode() {
        return phoneCode;
    }

    /**
     * @param phoneCode the phoneCode to set
     */
    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the detName
     */
    public String getDetName() {
        return detName;
    }

    /**
     * @param detName the detName to set
     */
    public void setDetName(String detName) {
        this.detName = detName;
    }

    /**
     * @return the detId
     */
    public String getDetId() {
        return detId;
    }

    /**
     * @param detId the detId to set
     */
    public void setDetId(String detId) {
        this.detId = detId;
    }

    /**
     * @return the details
     */
    public List<StateCity> getDetails() {
        return details;
    }

    /**
     * @param details the details to set
     */
    public void setDetails(List<StateCity> details) {
        this.details = details;
    }

    private String id;
    private String name;
    private String phoneCode;
    private String code;
    private String detName;
    private String detId;
    private List<StateCity> details;
        @Override
    public String toString() {
        return "{"
                + "\"id\":" + checkNull(id) + ", "
                + "\"name\":" + checkNull(name) + ", "
                + "\"phoneCode\":" + checkNull(phoneCode) + ", "
                + "\"code\":" + checkNull(code) + ", "
                + "\"detName\":" + checkNull(detName) + ", "
                + "\"detId\":" + checkNull(detId) + ", "
                + "\"details\":" + checkNull(details) + " "
                + "}";
    }
    
}
