/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

import static com.nz.cryptolabs.beans.ToObjectConverter.checkNull;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Administrator
 */
public class SecuredUser extends org.springframework.security.core.userdetails.User implements ToObjectConverter {

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the createdTs
     */
    public String getCreatedTs() {
        return createdTs;
    }

    /**
     * @param createdTs the createdTs to set
     */
    public void setCreatedTs(String createdTs) {
        this.createdTs = createdTs;
    }

    /**
     * @return the createdTime
     */
    public Long getCreatedTime() {
        return createdTime;
    }

    /**
     * @param createdTime the createdTime to set
     */
    public void setCreatedTime(Long createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * @return the config
     */
    public Configuration getConfig() {
        return config;
    }

    /**
     * @param config the config to set
     */
    public void setConfig(Configuration config) {
        this.config = config;
    }

    /**
     * @return the verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     * @param verified the verified to set
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public SecuredUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    private String userId;
    private String fullName;
    private String email;
    private String dob;
    private String createdTs;
    private Long createdTime;
    private String inviteCode;
    private String mobileNo;
    private String refId;
    private Boolean user;
    private Boolean admin;
    private Boolean verified;
    private Configuration config;

    @Override
    public String toString() {
        return "{"
                + "\"userId\":" + checkNull(userId) + ", "
                + "\"fullName\":" + checkNull(fullName) + ", "
                + "\"dob\":" + checkNull(dob) + ", "
                + "\"inviteCode\":" + checkNull(inviteCode) + ", "
                + "\"mobileNo\":" + checkNull(mobileNo) + ", "
                + "\"refId\":" + checkNull(refId) + ", "
                + "\"admin\":" + (admin) + ", "
                + "\"user\":" + (user) + " "
                + "}";
    }

    @Override
    public String toObject() {
        return "{"
                + "userId:" + checkNull(userId) + ", "
                + "fullName:" + checkNull(fullName) + ", "
                + "dob:" + checkNull(dob) + ", "
                + "inviteCode:" + checkNull(inviteCode) + ", "
                + "mobileNo:" + checkNull(mobileNo) + ", "
                + "refId:" + checkNull(refId) + ", "
                + "admin:" + (admin) + ", "
                + "user:" + (user) + " "
                + "}";
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the inviteCode
     */
    public String getInviteCode() {
        return inviteCode;
    }

    /**
     * @param inviteCode the inviteCode to set
     */
    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return the user
     */
    public Boolean getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(Boolean user) {
        this.user = user;
    }

    /**
     * @return the admin
     */
    public Boolean getAdmin() {
        return admin;
    }

    /**
     * @param admin the admin to set
     */
    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    /**
     * @return the refId
     */
    public String getRefId() {
        return refId;
    }

    /**
     * @param refId the refId to set
     */
    public void setRefId(String refId) {
        this.refId = refId;
    }

}
