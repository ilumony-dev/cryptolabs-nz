/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

/**
 *
 * @author palo12
 */
public class KickboxResponse {

    /**
     * @return the occurred_at
     */
    public String getOccurred_at() {
        return occurred_at;
    }

    /**
     * @param occurred_at the occurred_at to set
     */
    public void setOccurred_at(String occurred_at) {
        this.occurred_at = occurred_at;
    }

    /**
     * @return the last_sent
     */
    public String getLast_sent() {
        return last_sent;
    }

    /**
     * @param last_sent the last_sent to set
     */
    public void setLast_sent(String last_sent) {
        this.last_sent = last_sent;
    }

    /**
     * @return the geo_city
     */
    public String getGeo_city() {
        return geo_city;
    }

    /**
     * @param geo_city the geo_city to set
     */
    public void setGeo_city(String geo_city) {
        this.geo_city = geo_city;
    }

    /**
     * @return the geo_region
     */
    public String getGeo_region() {
        return geo_region;
    }

    /**
     * @param geo_region the geo_region to set
     */
    public void setGeo_region(String geo_region) {
        this.geo_region = geo_region;
    }

    /**
     * @return the geo_country
     */
    public String getGeo_country() {
        return geo_country;
    }

    /**
     * @param geo_country the geo_country to set
     */
    public void setGeo_country(String geo_country) {
        this.geo_country = geo_country;
    }

    /**
     * @return the ip_address
     */
    public String getIp_address() {
        return ip_address;
    }

    /**
     * @param ip_address the ip_address to set
     */
    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the track_token
     */
    public String getTrack_token() {
        return track_token;
    }

    /**
     * @param track_token the track_token to set
     */
    public void setTrack_token(String track_token) {
        this.track_token = track_token;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    private String status;
    private String track_token;
    private String id;
    private Boolean success;
    private String message;
    private String occurred_at;
    private String last_sent;
    private String geo_city;
    private String geo_region;
    private String geo_country;
    private String ip_address;
    private String reason;
    private String email;
    private String name;
}
