/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nz.cryptolabs.beans;

/**
 *
 * @author Administrator
 */
//Do not modify it, It use everytime.
public class TransactionBean {

    /**
     * @return the local_amount
     */
    public String getLocal_amount() {
        return local_amount;
    }

    /**
     * @param local_amount the local_amount to set
     */
    public void setLocal_amount(String local_amount) {
        this.local_amount = local_amount;
    }

    /**
     * @return the local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local the local to set
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return the usd
     */
    public String getUsd() {
        return usd;
    }

    /**
     * @param usd the usd to set
     */
    public void setUsd(String usd) {
        this.usd = usd;
    }

    /**
     * @return the btc
     */
    public String getBtc() {
        return btc;
    }

    /**
     * @param btc the btc to set
     */
    public void setBtc(String btc) {
        this.btc = btc;
    }

    /**
     * @return the minto
     */
    public String getMinto() {
        return minto;
    }

    /**
     * @param minto the minto to set
     */
    public void setMinto(String minto) {
        this.minto = minto;
    }

    /**
     * @return the units
     */
    public String getUnits() {
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(String units) {
        this.units = units;
    }

    /**
     * @return the txn_id
     */
    public String getTxn_id() {
        return txn_id;
    }

    /**
     * @param txn_id the txn_id to set
     */
    public void setTxn_id(String txn_id) {
        this.txn_id = txn_id;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    private String id;
    private String created_ts;
    private String user_id;
    private String investment_id;
    private String particulars;
    private String inc_dec;
    private String amount;
    private String local_amount;
    private String active;
    private String balance;
    private String txn_id;
    private String brand;
    private String units;
    private String local, usd, btc, minto;

    @Override
    public String toString() {
        return "{"
                + "\"id\":\"" + id + "\",\n"
                + "\"created_ts\":\"" + created_ts + "\",\n"
                + "\"user_id\":\"" + user_id + "\",\n"
                + "\"investment_id\":\"" + investment_id + "\",\n"
                + "\"particulars\":\"" + particulars + "\",\n"
                + "\"inc_dec\":\"" + inc_dec + "\",\n"
                + "\"amount\":\"" + amount + "\",\n"
                + "\"balance\":\"" + balance + "\",\n"
                + "\"txn_id\":\"" + getTxn_id() + "\",\n"
                + "\"brand\":\"" + getBrand() + "\" \n"
                + "}";
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the created_ts
     */
    public String getCreated_ts() {
        return created_ts;
    }

    /**
     * @param created_ts the created_ts to set
     */
    public void setCreated_ts(String created_ts) {
        this.created_ts = created_ts;
    }

    /**
     * @return the user_id
     */
    public String getUser_id() {
        return user_id;
    }

    /**
     * @param user_id the user_id to set
     */
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /**
     * @return the investment_id
     */
    public String getInvestment_id() {
        return investment_id;
    }

    /**
     * @param investment_id the investment_id to set
     */
    public void setInvestment_id(String investment_id) {
        this.investment_id = investment_id;
    }

    /**
     * @return the particulars
     */
    public String getParticulars() {
        return particulars;
    }

    /**
     * @param particulars the particulars to set
     */
    public void setParticulars(String particulars) {
        this.particulars = particulars;
    }

    /**
     * @return the inc_dec
     */
    public String getInc_dec() {
        return inc_dec;
    }

    /**
     * @param inc_dec the inc_dec to set
     */
    public void setInc_dec(String inc_dec) {
        this.inc_dec = inc_dec;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the active
     */
    public String getActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * @return the balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * @param balance the balance to set
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    public TransactionBean() {

    }

    public TransactionBean(StripeCharge charge) {
        this.created_ts = charge.getCreated_ts();
        this.user_id = charge.getUser_id();
        this.particulars = charge.getDescription();
//    this.inc_dec = ;
        this.amount = charge.getAmount();
        this.txn_id = charge.getTxn_id();
//    this.brand = charge.get;
    }
}
