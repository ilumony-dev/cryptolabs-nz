<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Investment Dashboard - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="User dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Portfolios</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>${investment.fundName}</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">${investment.fundName}</h6>
                                        <p>Hey User, welcome to Minto! Here's an overview of your investment. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-box">
                                        <div class="os-tabs-w">
                                            <div class="os-tabs-controls">
                                                <ul class="nav nav-tabs smaller">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" data-toggle="tab" href="#tab-balance">Balance</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-live">Live</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" data-toggle="tab" href="#tab-latestweek">Latest Week</a>
                                                    </li>
                                                </ul>
                                                <ul class="nav nav-pills smaller hidden-sm-down">
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab-balance">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Crypto Current Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">

                                                    </div>
                                                    <div class="el-chart-w" id="lineChart">
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab-live">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Live Balance
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">
                                                    </div>
                                                    <div class="el-chart-w" id="linegraph-live"></div>
                                                </div>
                                                <div class="tab-pane" id="tab-latestweek">
                                                    <div class="el-tablo">
                                                        <div class="label" style="display: inline-block; margin-right: 5px">
                                                            Latest Week
                                                        </div>
                                                        <img src="./resources/img/info-icon.png">
                                                    </div>
                                                    <div class="el-chart-w" id="lg-latestweek"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Portfolio Description</h6>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                ${portfolio.description}
                                                <!--                                                Le sommaire du portefeuille de placements peut changer en raison des op�rations courantes dans le portefeuille du fonds d'invest issement. 
                                                                                                Vous pouvez obtenir une mise � jour trimestrielle en consultant le site mintolabs.com/fondsmutuels. -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Performance Chart</h6>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="el-chart-w" id="hr24Chart"></div>
                                                <!--<div id="legend-3" style="position:absolute; center:0px; top:0px;"></div>-->
                                                <center>1 Day</center>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="el-chart-w" id="hr247Chart"></div>
                                                <!--<div id="legend-4" style="position:absolute; center:0px; top:0px;"></div>-->
                                                <center>1 Week</center>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="el-chart-w" id="hr1Chart"></div>
                                                <!--<div id="legend-2" style="position:absolute; center:0px; top:0px;"></div>-->
                                                <center>1 Month</center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" >
                                            <div class="element-box">
                                                <div class="element-wrapper">
                                                    <h6 class="element-header" >Your Investment Details</h6>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <h6 style="font-size: 14px;text-align: center;">Total Invested</h6>
                                                        <div class="col-md-12 color3 invest6 btn-hover">
                                                            <center>${currSymbol} ${investment.investmentAmount}</center>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h6 style="font-size: 14px;text-align: center;">Current Balance</h6>
                                                        <div class="col-md-12 invest1 color3 btn-hover">
                                                            <input type="hidden" id="actualInvested"/>
                                                            <center><span id="currentBalance">${investment.value}</span></center>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <h6 style="font-size: 14px;text-align: center;">Performance</h6>
                                                        <div class="col-md-12 invest1 color3 btn-hover">
                                                            <center id="performance">0.00%</center>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="row">
                                                                                                                                                        <div class="col-md-4 offset-2">
                                                                                                                                                            <h6 style="font-size: 14px;text-align: center;">Your Regularly Investment</h6>
                                                                                                                                                            <div class="col-md-12 color3 invest4">
                                                                                                                                                                <center>$ ${investment.regularlyAmount}</center>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                
                                                                                                </div>-->
                                                <!--                                                <div class="row">
                                                                                                    <div class="col-md-4 offset-4">
                                                                                                        <h6 style="font-size: 14px;text-align: center;">Investment time frame</h6>
                                                                                                        <div class="col-md-12 color3 invest5">
                                                                                                            <center>${investment.years} years</center>
                                                                                                        </div>
                                                                                                    </div>
                                                
                                                                                                </div>-->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Target Investment</h6>
                                        </div>
                                        <div class="el-chart-w" id="firstday-chart" data-highcharts-chart="0"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Current Investment</h6>
                                        </div>
                                        <div class="el-chart-w" id="today-chart"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="element-box">
                                        <div class="element-wrapper">
                                            <h6 class="element-header">Top 5 Investments</h6>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th style="" class="a">Share/Coin</th>
                                                        <th class="b">Invested</th>
                                                        <th class="b">Current</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${top5Shares}" var="share">
                                                        <tr>
                                                            <td style="text-transform: uppercase"><img src="./resources/img/coin-images/${share.shareName}.png" style="width:25px;height: 25px; margin-right: 5px">${share.shareName}</td>
                                                            <td style="">${currSymbol} ${share.investedAmount}</td>                                                
                                                            <td style="">${currSymbol} ${share.shareAmount}</td>                                                
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--                            <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="element-box">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Asset Class Performance</h6>
                                                                    </div>
                                                                    <table class="table table-bordered table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <td style=" " class="a">Share/Coin</td>
                                                                                <td class="b">1 Month</td>
                                                                                <td class="c">3 Month </td>
                                                                                <td class="d"> 1 year</td>
                            
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style=" ">bitcoin</td>
                                                                                <td style=" ">0.15%</td>
                                                                                <td style=" ">0.44%</td>
                                                                                <td style=" ">1.75%</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style=" ">litecoin</td>
                                                                                <td style=" ">0.49%</td>
                                                                                <td style=" ">0.91%</td>
                                                                                <td style=" ">2.66%</td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="element-box">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">Value of your Investment</h6>
                                                                    </div>
                                                                    <table class="table table-bordered table-fixed table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <td class="e">Share/Coin </td>
                                                                                <td class="f">Current value per unit</td>
                                                                                <td class="g">Units </td>
                                                                                <td class="i"> Total</td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                            <c:forEach items="${investment.fundActualShares}" var="share">
                                <tr>
                                    <td>${share.shareName}</td>
                                    <td>${share.price}</td>
                                    <td>${share.quantity}</td>
                                    <td>${share.shareAmount}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>-->














                            <!--                            <div class="floated-chat-btn">
                                                            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                                                        </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->

                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>


        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/investmentJs/SliderJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/MyPlanJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/CalculateMyBalanceJs.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="${home}/resources/js/cryptolabs/user-main.js"></script>
        <script>
        var endUser =${info};
        var latestWeek = ${latestWeek};
        </script>
        <script src="${home}/resources/js/cryptolabs/user-charts.js"></script>
        <script>
        $(document).ready(function () {
            var inv = ${investment.toObject()};
            var shares = inv.fundActualShares;
            firstdayChart(shares);
            todayChart(shares);
            var percent1hr = 0;
            var percent24hr = 0;
            var percent7d = 0;
            var amt = ${investment.value};
            $.each(shares, function (i, share) {
                percent1hr = percent1hr + share.percent1hr;
                percent24hr = percent24hr + share.percent24hr;
                percent7d = percent7d + share.percent7d;
            });
            percent1hr = (amt + (amt / 100 * (percent1hr / shares.length)));
            percent24hr = (amt + (amt / 100 * (percent24hr / shares.length)));
            percent7d = (amt + (amt / 100 * (percent7d / shares.length)));
            var max = 0;
            if (percent1hr > percent24hr && percent1hr > percent7d) {
                max = percent1hr;
            } else if (percent24hr > percent1hr && percent24hr > percent7d) {
                max = percent24hr;
            } else {
                max = percent7d;
            }
            hr24hChart(percent24hr, max);
            hr7dChart(percent7d, max);
            hr30dChart(percent1hr, max);
            var values = new Array();
            values.push(percent7d);
            values.push(percent24hr);
            values.push(percent1hr);
            values.push(amt);
            var labels = new Array();
            labels.push('Before 7 days');
            labels.push('Before 1 day');
            labels.push('Before 1 hour');
            labels.push('Current Amount');
            lineChart('lineChart', values, labels);
            linegraphlive(amt, true);
            setInterval(function () {
                amt = ${investment.value} + Math.random();
                var val = ${investment.investmentAmount};
                var inc = amt - val;
                var per = inc * 100 / val;
                $('#performance').text(per.toFixed(2) + '%');
                $('#currentBalance').text('${currSymbol}' + amt.toFixed(2));
                $("#actualInvested").val(amt.toFixed(2));
            }, 1000);
            var values = [];
            $.each(latestWeek, function (idx, day) {
                values.push({
                    x: new Date(day.current_date),
                    y: eval(day.investment_amount)
                });
            });
            values.push({
                x: new Date(),
                y: amt
            });
            lglatestweek(values);
        });
        </script>
    </body>
</html>
