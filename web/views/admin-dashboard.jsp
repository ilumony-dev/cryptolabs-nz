<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <style>
            .table-responsive .dataTables_wrapper .row:first-child {position: relative; left: -14%}
        </style>
    </head>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Summary Page</a>
                        </li>
                        <!--                        <li class="breadcrumb-item">
                                                    <span>Laptop with retina screen</span>
                                                </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <div class="element-actions">
                                            <form class="form-inline justify-content-sm-end">
                                                <select class="form-control form-control-sm rounded">
                                                    <option value="Pending">
                                                        Today
                                                    </option>
                                                    <option value="Active">
                                                        Last Week 
                                                    </option>
                                                    <option value="Cancelled">
                                                        Last 30 Days
                                                    </option>
                                                </select>
                                            </form>
                                        </div>
                                        <h6 class="element-header">
                                            Admin Dashboard
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Customers
                                                        </div>
                                                        <div class="value">
                                                            ${totalCustomers}
                                                        </div>
                                                        <div class="trending trending-up">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-up2"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Total Investments
                                                        </div>
                                                        <div class="value">
                                                            ${totalInvestments}
                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">
                                                            Current Balance
                                                        </div>
                                                        <div class="value currentBalance">
                                                            ${totalSharesValue}
                                                        </div>
                                                        <div class="trending trending-down-basic">
                                                            <!--<span>9%</span><i class="os-icon os-icon-graph-down"></i>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Pending Transactions
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Id
                                                            </th>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Investment Name
                                                            </th>
                                                            <th class="text-right">
                                                                Amount
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${pendingTransactions}" var="investment">
                                                            <tr>
                                                                <td>
                                                                    ${investment.refId}
                                                                </td>
                                                                <td>
                                                                    ${investment.customerName}
                                                                </td>
                                                                <td>
                                                                    ${investment.fundName}
                                                                </td>
                                                                <td class="text-right">
                                                                    $${investment.investmentAmount}
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-secondary btn-sm" href="#" onclick='purchasedShares2(${investment.toObject()});' data-target="#updateinvestmentshares" data-toggle="modal">
                                                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                                                        <span>${investment.action}</span>
                                                                    </a>
                                                                    <%--<c:choose>
                                                                        <c:when test="${investment.action eq 'WITHDRAWL'}">
                                                                            <a class="btn btn-secondary btn-sm" href="#" onclick="purchasedShares2(${investment.toObject()});" data-target="#updateinvestmentshares" data-toggle="modal">
                                                                                <i class="os-icon os-icon-wallet-loaded"></i>
                                                                                <span>${investment.action}</span>
                                                                            </a>                                                                           
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <a class='btn btn-secondary btn-sm' href='#' onclick='getShares2(${investment.toObject()});' data-target='#updateinvestmentshares' data-toggle='modal'>
                                                                                <i class="os-icon os-icon-wallet-loaded"></i>
                                                                                <span>${investment.action}</span>
                                                                            </a>                                                                           
                                                                        </c:otherwise>
                                                                    </c:choose>--%>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Pending Investments
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Id
                                                            </th>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Portfolio
                                                            </th>
                                                            <th>
                                                                Shares
                                                            </th>
                                                            <th>
                                                                Time Frame 
                                                            </th>
                                                            <th class="text-right">
                                                                Total Investment
                                                            </th>
                                                            <th>
                                                                Action 
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${pendingInvestments}" var="investment">
                                                            <tr>
                                                                <td>
                                                                    ${investment.refId}
                                                                </td>
                                                                <td>
                                                                    ${investment.customerName}
                                                                </td>
                                                                <td>
                                                                    ${investment.fundName}
                                                                </td>
                                                                <td>
                                                                    ${investment.shares}
                                                                </td>
                                                                <td>
                                                                    ${investment.timeFrame}
                                                                </td>
                                                                <td class="text-right">
                                                                    $${investment.investmentAmount}
                                                                </td>
                                                                <td>
                                                                    <a class='btn btn-secondary btn-sm' href='#' onclick='pendingShares2(${investment.toObject()});' data-target='#updateinvestmentshares' data-toggle='modal'>
                                                                        <i class="os-icon os-icon-wallet-loaded"></i>
                                                                        <span>Purchase</span>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Overview
                                        </h6>
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls" style="padding: 0 20px">
                                                    <div class="os-tabs-controls">
                                                        <ul class="nav nav-tabs smaller">
                                                            <li class="nav-item">
                                                                <a class="tab_lg_bitcoin nav-link active" data-toggle="tab" href="#tab_lg_bitcoin">Bitcoin</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="tab_lg_minto nav-link" data-toggle="tab" href="#tab_lg_minto">Minto</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="tab_lg_usd nav-link" data-toggle="tab" href="#tab_lg_usd">US$</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="tab_lg_nzd nav-link" data-toggle="tab" href="#tab_lg_nzd">NZ$</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="tab_lg_minto_nav nav-link" data-toggle="tab" href="#tab_lg_minto_nav">Minto NAV</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="dropdown">
                                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options</button>
                                                        <ul class="dropdown-menu" style="top:56%; position: absolute">
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_lg_bitcoin" onclick="removeActive('tab_lg_bitcoin')">Bitcoin</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_lg_minto" onclick="removeActive('tab_lg_minto')">Minto</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_lg_usd" onclick="removeActive('tab_lg_usd')">US$</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_lg_nzd" onclick="removeActive('tab_lg_nzd')">NZ$</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" data-toggle="tab" href="#tab_lg_minto_nav" onclick="removeActive('tab_lg_minto_nav')">Minto NAV</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="tab-pane menu-chart active" id="tab_lg_bitcoin">
                                                        <div class="el-chart-w"  id="linegraphBitcoin"></div>
                                                    </div>
                                                    <div class="tab-pane menu-chart" id="tab_lg_minto">
                                                        <div class="el-chart-w"  id="linegraphMinto"></div>
                                                    </div>
                                                    <div class="tab-pane menu-chart" id="tab_lg_usd">
                                                        <div class="el-chart-w"  id="linegraphUSD"></div>
                                                    </div>
                                                    <div class="tab-pane menu-chart" id="tab_lg_nzd">
                                                        <div class="el-chart-w"  id="linegraphNZD"></div>
                                                    </div>
                                                    <div class="tab-pane menu-chart" id="tab_lg_minto_nav">
                                                        <div class="el-chart-w"  id="linegraphMintoNav"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Minto Unit Summary
                                        </h6>
                                        <div class="element-box-tp">

                                            <div class="table-responsive">
                                                <table id="unit-summary-table" class="table table-bordered">
                                                    <thead id="unit-summary-head">
                                                        <tr>
                                                            <th style="width: 30px;">Sr.No.</th>
                                                            <th style="width: 30px;">Customer Id</th>
                                                            <th style="min-width: 150px;">Portfolio</th>
                                                            <th style="width: 30px;">Minto Units</th>
                                                            <th style="width: 30px;">Pur. Bitcoins</th>
                                                            <th style="width: 30px;">Current Bitcoins</th>
                                                            <th style="width: 30px;">N.A.V</th>
                                                            <th style="width: 30px;">US$</th>
                                                            <th style="width: 30px;">NZ$</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="unit-summary-body">
                                                        <c:forEach items="${totalUnits}" var="unit" varStatus="loop">
                                                            <tr>
                                                                <td>${loop.index+1}</td>
                                                                <td>${unit.reqId}</td>
                                                                <td>${unit.name}</td>
                                                                <td>${unit.minto}</td>
                                                                <td>${unit.btc}</td>
                                                                <td>${unit.quantity}</td>
                                                                <td>${unit.price}</td>
                                                                <td>${unit.usd}</td>
                                                                <td>${unit.local}</td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                    <tfoot id="unit-summary-foot">
                                                        <tr class="totals">
                                                            <th>Sr.No.</th>
                                                            <th>Customer Id</th>
                                                            <th>Portfolio</th>
                                                            <th id="unit-summary-foot-minto">Minto Units</th>
                                                            <th id="unit-summary-foot-btc">Pur. Bitcoins</th>
                                                            <th id="unit-summary-foot-quantity">Current Bitcoins</th>
                                                            <th>N.A.V</th>
                                                            <th id="unit-summary-foot-usd">US$</th>
                                                            <th id="unit-summary-foot-local">NZ$</th>
                                                        </tr>
                                                        <tr class="titles">
                                                            <th>Sr.No.</th>
                                                            <th>Customer Id</th>
                                                            <th>Investment</th>
                                                            <th>Minto Units</th>
                                                            <th>Pur. Bitcoins</th>
                                                            <th>Current Bitcoins</th>
                                                            <th>N.A.V</th>
                                                            <th>US$</th>
                                                            <th>NZ$</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="controls-below-table">
                                                <!--                                                <div class="table-records-info">
                                                                                                    Showing records 1 - 5
                                                                                                </div>-->
                                                <!--                                                <div class="table-records-pages">
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <a href="#">Previous</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a class="current" href="#">1</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">2</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">3</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">4</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                            <a href="#">Next</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Coin Summary
                                        </h6>
                                        <div class="element-box-tp">
                                            <div class="controls-above-table">
                                                <div class="row">
                                                    <!--                                                    <div class="col-sm-6">
                                                                                                            <a class="btn btn-sm btn-secondary" href="#">Download CSV</a><a class="btn btn-sm btn-secondary" href="#">Archive</a><a class="btn btn-sm btn-danger" href="#">Delete</a>
                                                                                                        </div>-->
                                                    <div class="col-sm-12">
                                                        <!--                                                        <form class="form-inline justify-content-sm-end">
                                                                                                                    <input class="form-control form-control-sm rounded bright" placeholder="Search" id="username" type="text" onblur="userCoinSummary();"/>
                                                                                                                    <select class="form-control form-control-sm rounded bright" id="refIds" onchange="onChangeEIC();">
                                                                                                                    </select>
                                                                                                                    <select class="form-control form-control-sm rounded bright" id="funds" onchange="onChangeEIC();">
                                                                                                                    </select>
                                                                                                                    <select class="form-control form-control-sm rounded bright" id="coins" onchange="onChangeEIC();">
                                                                                                                    </select>
                                                                                                                </form>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="coin-summary-table" class="table table-bordered">
                                                    <thead id="coin-summary-head">
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr>
                                                                    <th style="min-width:150px;">Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                    </thead>
                                                    <tbody id="coin-summary-body">
                                                    </tbody>
                                                    <tfoot id="coin-summary-foot">
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr class="titles">
                                                                    <th>Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                                            <c:if test="${loop.index eq 0}">
                                                                <tr class="totals">
                                                                    <th>Investment</th>
                                                                        <c:forEach items="${inv1.map}" var="coin" >
                                                                        <th>${coin.key}</th>
                                                                        </c:forEach>
                                                                </tr>
                                                            </c:if>
                                                        </c:forEach>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <!--                                            <div class="controls-below-table">
                                                                                            <div class="table-records-info">
                                                                                                Showing records 1 - 5
                                                                                            </div>
                                                                                            <div class="table-records-pages">
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <a href="#">Previous</a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a class="current" href="#">1</a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a href="#">2</a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a href="#">3</a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a href="#">4</a>
                                                                                                    </li>
                                                                                                    <li>
                                                                                                        <a href="#">Next</a>
                                                                                                    </li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            START - Chat Popup Box
                            -------------------->
<!--                            <div class="floated-chat-btn">
                                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                            </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a>
                                            <a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a>
                                            <a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/update-investment-shares.jsp" />

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="${home}/resources/js/cryptolabs/admin-charts.js"></script>
        <script>
                                                                    var totalUnits = ${totalUnits};
                                                                    var investments = ${investments};
                                                                    var refIds = ${refIds};
                                                                    var funds = ${funds};
                                                                    var coins = ${coins};
                                                                    var dailyUpdates = ${dailyUpdates};

                                                                    function removeActive(cls) {
                                                                        $(".nav-link").removeClass('active');
                                                                        $("." + cls).addClass('active');
                                                                    }
                                                                    
                                                                    function adminUnitSummary() {
                                                                        $('#unit-summary-table tfoot tr.titles th').each(function (i) {
                                                                            var th = $('#unit-summary-table thead th').eq($(this).index());
                                                                            var title = th.text();
                                                                            var thWidth = th.width();
                                                                            $(this).html('<input type="text" class="form-control-sm" style="width:' + thWidth + 'px;"  placeholder="Search ' + title + '" data-index="' + i + '" />');
                                                                        });
                                                                        var table1 = $('#unit-summary-table').DataTable({
                                                                            scrollY: "450px",
//                                                                        scrollX: true,
                                                                            scrollCollapse: true,
                                                                            paging: false,
                                                                            fixedColumns: true,
                                                                            drawCallback: function () {
                                                                                var api = this.api();
                                                                                $(api.table().column(3).footer()).html(api.column(3, {page: 'current'}).data().sum());
                                                                                $(api.table().column(4).footer()).html(api.column(4, {page: 'current'}).data().sum());
                                                                                $(api.table().column(5).footer()).html(api.column(5, {page: 'current'}).data().sum());
                                                                                $(api.table().column(7).footer()).html(api.column(7, {page: 'current'}).data().sum());
                                                                                $(api.table().column(8).footer()).html(api.column(8, {page: 'current'}).data().sum());
                                                                            }
                                                                        });
                                                                        $(table1.table().container()).on('keyup', 'tfoot input', function () {
                                                                            table1.column($(this).data('index')).search(this.value).draw();
                                                                        });
                                                                    }

                                                                    function adminCoinSummary() {
                                                                        var inv = investments[0];
                                                                        var htmlCode = '';
                                                                        htmlCode = htmlCode + '<tr style = ' + inv.action + '>';
                                                                        htmlCode = htmlCode + '<th style="min-width:60px;">Cust. Id</th>';
                                                                        htmlCode = htmlCode + '<th style="min-width:150px;">Investment</th>';
                                                                        $.each(inv.map, function (key, value) {
                                                                            htmlCode = htmlCode + '<th>' + key + '</th>';
                                                                        });
                                                                        $('#coin-summary-head').html(htmlCode);
                                                                        $('#coin-summary-foot').html(htmlCode);
                                                                        var keys = new Array();
                                                                        $.each(investments[0].map, function (key, value) {
                                                                            keys.push({name: key, value: 0});
                                                                        });
                                                                        var htmlCode = '';
                                                                        $.each(investments, function (i, inv) {
                                                                            htmlCode = htmlCode + '<tr style = ' + inv.action + '>';
                                                                            htmlCode = htmlCode + '<td>' + inv.refId + '</td>';
                                                                            htmlCode = htmlCode + '<td>' + inv.fundName + '</td>';
                                                                            $.each(inv.map, function (key, val) {
                                                                                htmlCode = htmlCode + '<td>' + val + '</td>';
                                                                                $.each(keys, function (idx, k) {
                                                                                    if (k.name === key) {
                                                                                        var value = k.value + val;
                                                                                        keys[idx] = {name: key, value: value};
                                                                                    }
                                                                                });
                                                                            });
                                                                            htmlCode = htmlCode + '</tr>';
                                                                        });
                                                                        $('#coin-summary-body').html(htmlCode);
                                                                        $('#coin-summary-table tfoot tr.titles th').each(function (i) {
                                                                            var th = $('#coin-summary-table thead th').eq($(this).index());
                                                                            var title = th.text();
                                                                            var thWidth = th.width();
                                                                            $(this).html('<input type="text" class="form-control" style="width:' + thWidth + 'px;" placeholder="Search ' + title + '" data-index="' + i + '" />');
                                                                        });
                                                                        // DataTable
                                                                        var table2 = $('#coin-summary-table').DataTable({
                                                                            scrollY: "450px",
//                                                                        scrollX: true,
                                                                            scrollCollapse: true,
                                                                            paging: false,
                                                                            fixedColumns: true
                                                                        });
                                                                        var arr = new Array();
                                                                        $(table2.table().container()).on('keyup', 'tfoot input', function () {
                                                                            table2.column($(this).data('index')).search(this.value).draw();
                                                                        });
                                                                    }

                                                                    function totals(table, arr) {
                                                                        console.log(table);
                                                                    }
//                                                                    function adminUnitSummaryOld() {
//                                                                    var refIds = new Array();
//                                                                    var funds = new Array();
//                                                                    $.each(totalUnits, function (idx, unt) {
//                                                                        if (refIds.includes(unt.reqId) === false) {
//                                                                            refIds.push(unt.reqId);
//                                                                        }
//                                                                        if (funds.includes(unt.name) === false) {
//                                                                            funds.push(unt.name);
//                                                                        }
//                                                                    });
//                                                                    var refIdHtmlCode = '<option value="0">Select Customer Id</option>';
//                                                                    $.each(refIds, function (i, refId) {
//                                                                        refIdHtmlCode = refIdHtmlCode + '<option>' + refId + '</option>';
//                                                                    });
//                                                                    $("#unit-refIds").html(refIdHtmlCode);
//                                                                    var fundHtmlCode = '<option value="0">Select Portfolio</option>';
//                                                                    $.each(funds, function (i, fund) {
//                                                                        fundHtmlCode = fundHtmlCode + '<option>' + fund + '</option>';
//                                                                    });
//                                                                    $("#unit-funds").html(fundHtmlCode);
//                                                                    }
//                                                                    function onChangeUIC() {
//                                                                        var refId = $("#unit-refIds").val();
//                                                                        var name = $("#unit-funds").val();
//                                                                        var refIdInvestment = new Array();
//                                                                        var fundInvestment = new Array();
//                                                                        if (refId !== '0') {
//                                                                            $.each(totalUnits, function (i, inv) {
//                                                                                if (inv.reqId === refId) {
//                                                                                    refIdInvestment.push(inv);
//                                                                                }
//                                                                            });
//                                                                        } else {
//                                                                            refIdInvestment = totalUnits;
//                                                                        }
//                                                                        if (name !== '0') {
//                                                                            $.each(refIdInvestment, function (i, inv) {
//                                                                                if (inv.name === name) {
//                                                                                    fundInvestment.push(inv);
//                                                                                }
//                                                                            });
//                                                                        } else {
//                                                                            fundInvestment = refIdInvestment;
//                                                                        }
//                                                                        var htmlCode = '';
//                                                                        var minto = 0, btc = 0, quantity = 0, usd = 0, local = 0;
//                                                                        $.each(fundInvestment, function (i, unit) {
//                                                                            htmlCode = htmlCode + '<tr>';
//                                                                            htmlCode = htmlCode + '<td>' + parseInt(i + 1) + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.reqId + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.name + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.minto + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.btc + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.quantity + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.price + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.usd + '</td>';
//                                                                            htmlCode = htmlCode + '<td>' + unit.local + '</td>';
//                                                                            htmlCode = htmlCode + '</tr>';
//                                                                            minto += eval(unit.minto);
//                                                                            btc += eval(unit.btc);
//                                                                            quantity += eval(unit.quantity);
//                                                                            usd += eval(unit.usd);
//                                                                            local += eval(unit.local);
//                                                                        });
//                                                                        $('#unit-summary-body').html(htmlCode);
//                                                                        $('#unit-summary-foot-minto').text(minto);
//                                                                        $('#unit-summary-foot-btc').text(btc.toFixed(8));
//                                                                        $('#unit-summary-foot-quantity').text(quantity.toFixed(16));
//                                                                        $('#unit-summary-foot-usd').text(usd.toFixed(4));
//                                                                        $('#unit-summary-foot-local').text(local.toFixed(4));
//                                                                        var datatable = $('#unit-summary-table').DataTable();
//                                                                    }
//                                                                function adminCoinSummaryOld() {
//                                                                    var inv = investments[0];
//                                                                    var htmlCode = '';
//                                                                    htmlCode = htmlCode + '<tr style = ' + inv.action + '>';
//                                                                    htmlCode = htmlCode + '<th>Cust. Id</th>';
//                                                                    htmlCode = htmlCode + '<th>Investment</th>';
//                                                                    $.each(inv.map, function (key, value) {
//                                                                        htmlCode = htmlCode + '<th>' + key + '</th>';
//                                                                    });
//                                                                    $('#coin-summary-head').html(htmlCode);
//                                                                    $('#coin-summary-foot').html(htmlCode);
////                                                                    var refIdHtmlCode = '<option value="0">Select Customer Id</option>';
////                                                                    $.each(refIds, function (i, refId) {
////                                                                        refIdHtmlCode = refIdHtmlCode + '<option>' + refId + '</option>';
////                                                                    });
////                                                                    $("#refIds").html(refIdHtmlCode);
////                                                                    var funds = new Array();
////                                                                    $.each(investments, function (i, inv) {
////                                                                        var fund = {id: inv.fundId, name: inv.fundName};
////                                                                        var exist = false;
////                                                                        var idx = 0;
////                                                                        while (idx < funds.length) {
////                                                                            var fund1 = funds[idx];
////                                                                            if (fund1.name === fund.name) {
////                                                                                exist = true;
////                                                                                break;
////                                                                            }
////                                                                            idx += 1;
////                                                                        }
////                                                                        if (exist === false) {
////                                                                            funds.push(fund);
////                                                                        }
////                                                                    });
////                                                                    var fundHtmlCode = '<option value="0">Select Portfolio</option>';
////                                                                    $.each(funds, function (i, fund) {
////                                                                        fundHtmlCode = fundHtmlCode + '<option value="' + fund.id + '">' + fund.name + '</option>';
////                                                                    });
////                                                                    $("#funds").html(fundHtmlCode);
////                                                                    var coinHtmlCode = '<option value="0">Select Coin</option>';
////                                                                    $.each(coins, function (i, coin) {
////                                                                        coinHtmlCode = coinHtmlCode + '<option>' + coin + '</option>';
////                                                                    });
////                                                                    $("#coins").html(coinHtmlCode);
//                                                                    method1(investments);
//                                                                }
//
//                                                                function method1(data) {
//                                                                    var keys = new Array();
//                                                                    $.each(data[0].map, function (key, value) {
//                                                                        keys.push({name: key, value: 0});
//                                                                    });
//                                                                    var htmlCode = '';
//                                                                    $.each(data, function (i, inv) {
//                                                                        htmlCode = htmlCode + '<tr style = ' + inv.action + '>';
//                                                                        htmlCode = htmlCode + '<td>' + inv.refId + '</td>';
//                                                                        htmlCode = htmlCode + '<td>' + inv.fundName + '</td>';
//                                                                        $.each(inv.map, function (key, val) {
//                                                                            htmlCode = htmlCode + '<td>' + val + '</td>';
//                                                                            $.each(keys, function (idx, k) {
////                                                                               if (k.name === key) {
//                                                                                    var value = k.value + val;
//                                                                                    keys[idx] = {name: key, value: value};
//                                                                                }
//                                                                            });
//                                                                        });
//                                                                        htmlCode = htmlCode + '</tr>';
//                                                                    });
////                                                                    {
////                                                                        var totalHtmlCode = totalHtmlCode + '<tr style = >';
////                                                                        totalHtmlCode = totalHtmlCode + '<td></td>';
////                                                                        totalHtmlCode = totalHtmlCode + '<td>TOTAL</td>';
////                                                                        $.each(keys, function (i, key) {
////                                                                            totalHtmlCode = totalHtmlCode + '<td>' + key.value.toFixed(4) + '</td>';
////                                                                        });
////                                                                        htmlCode = htmlCode + totalHtmlCode;
////                                                                    }
//                                                                    $('#coin-summary-body').html(htmlCode);
//                                                                    {
//                                                                        $('#coin-summary-table tfoot th').each(function (i) {
//                                                                            var th = $('#unit-summary-table thead th').eq($(this).index());
//                                                                            var title = th.text();
//                                                                            var thWidth = th.width();
//                                                                            $(this).html('<input type="text"  style="width:' + thWidth + 'px;" placeholder="Search ' + title + '" data-index="' + i + '" />');
//                                                                        });
//                                                                        // DataTable
//                                                                        var table = $('#coin-summary-table').DataTable({
//                                                                            scrollY: "400px",
////                                                                        scrollX: true,
//                                                                            scrollCollapse: true,
//                                                                            paging: false,
//                                                                            fixedColumns: true
//                                                                        });
//
//                                                                        $(table.table().container()).on('keyup', 'tfoot input', function () {
//                                                                            table.column($(this).data('index'))
//                                                                                    .search(this.value)
//                                                                                    .draw();
//                                                                        });
//                                                                    }
//                                                                    $('#coin-summary-table').DataTable();
//                                                                }
                                                                    function onChangeEIC() {
                                                                        var refId = $("#refIds").val();
                                                                        var fundId = $("#funds").val();
                                                                        var coin = $("#coins").val();
                                                                        var refIdInvestment = new Array();
                                                                        var fundInvestment = new Array();
                                                                        var coinInvestment = new Array();
                                                                        if (refId !== '0') {
                                                                            $.each(investments, function (i, inv) {
                                                                                if (inv.refId === refId) {
                                                                                    refIdInvestment.push(inv);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            refIdInvestment = investments;
                                                                        }
                                                                        if (fundId !== '0') {
                                                                            $.each(refIdInvestment, function (i, inv) {
                                                                                if (inv.fundId === fundId) {
                                                                                    fundInvestment.push(inv);
                                                                                }
                                                                            });
                                                                        } else {
                                                                            fundInvestment = refIdInvestment;
                                                                        }
                                                                        if (coin !== '0') {
                                                                            $.each(fundInvestment, function (i, inv) {
                                                                                $.each(inv.map, function (key, value) {
                                                                                    if (coin === key && value > 0) {
                                                                                        coinInvestment.push(inv);
                                                                                    }
                                                                                });
                                                                            });
                                                                        } else {
                                                                            coinInvestment = fundInvestment;
                                                                        }
                                                                        method1(coinInvestment);
                                                                    }

                                                                    function qpes(idx) {
                                                                        var q = $("#quantity" + idx).val();
                                                                        var p = $("#price" + idx).val();
                                                                        var s = p * q;
                                                                        $("#shareAmount" + idx).val(s.toFixed(2));
                                                                    }
                                                                    function sdpeq(idx) {
                                                                        var s = $("#shareAmount" + idx).val();
                                                                        var p = $("#price" + idx).val();
                                                                        var q = s / p;
                                                                        $("#quantity" + idx).val(q.toFixed(8));
                                                                    }
                                                                    function sdqep(idx) {
                                                                        var s = $("#shareAmount" + idx).val();
                                                                        var q = $("#quantity" + idx).val();
                                                                        var p = s / q;
                                                                        $("#price" + idx).val(p.toFixed(8));
                                                                    }
                                                                    function b2m(unitPrice) {
                                                                        var btc = $("#updateinvestmentshares-btc").val();
                                                                        var minto = btc * unitPrice;
                                                                        $("#updateinvestmentshares-minto").val(minto.toFixed(8));
                                                                    }
        </script>

        <script>
            $(document).ready(function () {
                adminUnitSummary();
                adminCoinSummary();
                var shareOptions = '';
                var shares = ${shares};
                $.each(shares, function (i, share) {
                    shareOptions = shareOptions + '<option value="${share.shareId}">${share.name}</option>';
                });
                var bitcoins = [];
                var mintos = [];
                var prices = [];
                var usds = [];
                var nzds = [];
                $.each(dailyUpdates, function (i, update) {
                    var time = (new Date(update.createdDate)).getTime();
                    var bitcoin = eval(update.quantity);
                    bitcoins.push({
                        x: time,
                        y: bitcoin
                    });
                    var minto = eval(update.minto);
                    mintos.push({
                        x: time,
                        y: minto
                    });
                    var price = eval(update.price);
                    prices.push({
                        x: time,
                        y: price
                    });
                    var usd = eval(update.usd);
                    usds.push({
                        x: time,
                        y: usd
                    });
                    var nzd = eval(update.local);
                    nzds.push({
                        x: time,
                        y: nzd
                    });
                });
                linegraphBitcoin(0, null, [{name: 'Bitcoins', data: bitcoins}]);
                linegraphMinto(0, null, [{name: 'Mintos', data: mintos}]);
                linegraphMintoNav(0, null, [{name: 'Prices', data: prices}]);
                linegraphUSD(0, null, [{name: 'US$', data: usds}]);
                linegraphNZD(0, null, [{name: 'NZ$', data: nzds}]);
            });
        </script>

    </body>
</html>
