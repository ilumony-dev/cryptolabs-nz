<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Wallet - MintoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Wallet</a>
                        </li>
                        <!--                        <li class="breadcrumb-item">
                                                    <span>Laptop with retina screen</span>
                                                </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">   
                            <!--                            <div class="row market">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <div class="element-box">
                                                                        <form>
                                                                            <fieldset class="form-group">
                                                                                <legend><span>Your Card Information</span></legend>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label>CREDIT CARD TYPE</label>
                                                                                            <select class="form-control">
                                                                                                <option>select a card</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label>CARD HOLDER NAME</label>
                                                                                            <input class="form-control" placeholder="CARD HOLDER NAME" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label > CARD NUMBER</label>
                            
                                                                                            <input class="form-control" placeholder="CARD NUMBER" type="text">
                            
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label> CARD IDENTIFICATION NUMBER</label>
                                                                                            <input class="form-control" placeholder="CARD IDENTIFICATION NUMBER" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label> EXPIRATION DATE</label>
                                                                                            <div class="row">
                                                                                                <div class="col-sm-6">
                                                                                                    <select class="form-control">
                                                                                                        <option>Month</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                                <div class="col-sm-6">
                                                                                                    <select class="form-control">
                                                                                                        <option>Year</option>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-6">
                                                                                        <div class="form-group">
                                                                                            <label> BILLING ZIP CODE </label>
                                                                                            <input class="form-control" placeholder="BILLING ZIP CODE" type="text">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                            <div class="form-check">
                                                                                <label class="form-check-label">
                                                                                    <input class="form-check-input" type="checkbox"> By continuing, you agree to the Terms and Conditions.</label>
                                                                            </div>
                                                                            <div class="form-buttons-w">
                                                                                <button class="btn btn-primary" type="submit"> Submit</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>            
                                                        </div>-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <!--                                        <div class="element-actions">
                                                                                    <form class="form-inline justify-content-sm-end">
                                                                                        <select class="form-control form-control-sm rounded">
                                                                                            <option value="Pending">Today</option>
                                                                                            <option value="Active">Last Week </option>
                                                                                            <option value="Cancelled">Last 30 Days</option>
                                                                                        </select>
                                                                                    </form>
                                                                                </div>-->
                                        <h6 class="element-header">My Wallet</h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <div class="col-sm-4" hrer="javascript:void(0)" id="accountbalance">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Wallet Balance</div>
                                                        <div class="value">$${walletBalance}</div>
                                                        <!--                                                        <div class="trending trending-up">
                                                                                                                    <span>12%</span>
                                                                                                                    <i class="os-icon os-icon-arrow-up2"></i>
                                                                                                                </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-sm-4" href="javascript:void(0)" id="lastdeposite">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Last Transaction</div>
                                                        <div class="value">$${lastDeposite}</div>
                                                        <!--                                                        <div class="trending trending-down-basic">
                                                                                                                    <span>12%</span>
                                                                                                                    <i class="os-icon os-icon-arrow-2-down"></i>
                                                                                                                </div>-->
                                                    </div>
                                                </div>
                                                <div class="col-sm-4" href="javascript:void(0)" id="accountstatus">
                                                    <div class="element-box el-tablo">
                                                        <div class="label">Account Status</div>
                                                        <div class="value">Active</div>
                                                        <!--                                                        <div class="trending trending-down-basic">
                                                                                                                    <span>9%</span>
                                                                                                                    <i class="os-icon os-icon-graph-down"></i>
                                                                                                                </div>-->
                                                    </div>
                                                </div>
<!--                                                <a class="col-sm-4" id="addbalances" href="javascript:void(0)">
                                                    <div class="box">
                                                        <div class="label">Add Balance</div>

                                                                                                                <div class="trending trending-down-basic">
                                                                                                                    <span>9%</span>
                                                                                                                    <i class="os-icon os-icon-graph-down"></i>
                                                                                                                </div>
                                                    </div>
                                                </a>-->
<!--                                                <a class="col-sm-4" href="javascript:void(0)" id="widthdrawn">
                                                    <div class="box">
                                                        <div class="label">Withdraw Money</div>

                                                                                                                <div class="trending trending-down-basic">
                                                                                                                    <span>9%</span>
                                                                                                                    <i class="os-icon os-icon-graph-down"></i>
                                                                                                                </div>
                                                    </div>
                                                </a>-->
                                                <a class="col-sm-4" id="mytransactions" href="javascript:void(0)">
                                                    <div class="box">
                                                        <div class="label">Wallet Transactions</div>

                                                                                                                <div class="trending trending-down-basic">
                                                                                                                    <span>9%</span>
                                                                                                                    <i class="os-icon os-icon-graph-down"></i>
                                                                                                                </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div id="addbalance">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <div class="row">
                                                                    <div class="col-md-3" style="border-right: 1px dotted #DEEAEE;">
                                                                        <img src="./resources/img/wallet/wallet.png" style="width: 50px; height: 50px">
                                                                        <span class="value" >$${walletBalance}</span>
                                                                        <span class="text">Your Wallet Balance</span>
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <form action='./user-stripe-charge' method='POST' id='checkout-form'>
                                                                            <div class="col-md-12">
                                                                                <p>&nbsp</p>
                                                                                <input type="number" class="form-control" name='money' id="money" placeholder="Enter Amount to be added in your Wallet">
                                                                            </div>
                                                                            <div class="col-md-12 pull-right">
                                                                                <p>&nbsp</p>
                                                                                <!--<button type="button" class="btn btn-primary">Add money</button>-->
                                                                                <input type='hidden' class="checkout-amount" name='amount' value='${amount}' />
                                                                                <!--<input type='hidden' class="checkout-fundId" name='fundId'/>-->
                                                                                <script
                                                                                    src='https://checkout.stripe.com/checkout.js'
                                                                                    class='stripe-button'
                                                                                    data-key='${stripePublicKey}' 
                                                                                    data-amount='${amount}' 
                                                                                    data-currency='${currency}'
                                                                                    data-name='Minto'
                                                                                    data-description='${description}'
                                                                                    data-image='http://www.baeldung.com/wp-content/themes/baeldung/favicon/android-chrome-192x192.png'
                                                                                    data-locale='auto'
                                                                                    data-zip-code='false'>
                                                                                </script>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="withdrawlbalance">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <h5>Available Wallet Balance <span>$1</span></h5>
                                                                <p>You can send this balance to any Bank account at a nominal charge</p>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="label" for="account">Account Number: </label>
                                                                            <input type="number" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="label" for="accountname">Account Holder Name: </label>
                                                                            <input type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="label" for="ifsccode">SWIFT CODE: </label>
                                                                            <input type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <p>&nbsp;</p>
                                                                        <button type="submit" class="btn btn-primary">Find code</button>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="label" for="">Amount:</label>
                                                                            <input type="number" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="label" for="">Description:</label>
                                                                            <input type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <button type="submit" class="btn btn-success">Proceed</button>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="mytransaction">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="element-wrapper">
                                                            <div class="element-box">
                                                                <h5 class="form-header">My Wallet Transactions</h5>
                                                                <div class="table-responsive">
                                                                    <table class="table table-lightborder">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    DateTime
                                                                                </th>
                                                                                <th>
                                                                                    Particulars
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Inc
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Dec
                                                                                </th>
                                                                                <th class="text-right">
                                                                                    Balance
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <c:forEach items="${walletSummary}" var="trans">
                                                                                <tr>
                                                                                    <td>
                                                                                        ${trans.created_ts}
                                                                                    </td>
                                                                                    <td>
                                                                                        ${trans.particulars}
                                                                                    </td>
                                                                                    <td  class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>

                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                                            ${trans.amount}    
                                                                                        </c:if>
                                                                                    </td>
                                                                                    <td class="text-right">
                                                                                        ${trans.balance}
                                                                                    </td>
                                                                                    <!--                                                                   <td class="text-right">
                                                                                                                                                            <a class="btn btn-secondary btn-sm" href="#" onclick="pendingShares('${investment.investmentId}', '${investment.userId}');" data-target="#updateinvestmentshares" data-toggle="modal">
                                                                                                                                                                <i class="os-icon os-icon-wallet-loaded"></i>
                                                                                                                                                                <span>Purchase</span>
                                                                                                                                                            </a>
                                                                                                                                                        </td>-->
                                                                                </tr>
                                                                            </c:forEach>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="element-box">
                                                <h6 class="element-header">
                                                    Account Statements
                                                </h6>
                                                <div class="table-responsive">
                                                    <table class="table table-lightborder">
                                                        <thead>
                                                            <tr>
                                                                <th>
                                                                    DateTime
                                                                </th>
                                                                <th>
                                                                    Particulars
                                                                </th>
                                                                <th class="text-right">
                                                                    Inc
                                                                </th>
                                                                <th class="text-right">
                                                                    Dec
                                                                </th>
                                                                <th class="text-right">
                                                                    Balance
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <c:forEach items="${accountSummary}" var="trans">
                                                                <tr>
                                                                    <td>
                                                                        ${trans.created_ts}
                                                                    </td>
                                                                    <td>
                                                                        ${trans.particulars}
                                                                    </td>
                                                                    <td  class="text-right">
                                                                        <c:if test="${trans.inc_dec eq 'Inc'}">
                                                                            ${trans.amount}    
                                                                        </c:if>

                                                                    </td>
                                                                    <td class="text-right">
                                                                        <c:if test="${trans.inc_dec eq 'Dec'}">
                                                                            ${trans.amount}    
                                                                        </c:if>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        ${trans.balance}
                                                                    </td>
                                                                    <!--                                                                   <td class="text-right">
                                                                                                                                            <a class="btn btn-secondary btn-sm" href="#" onclick="pendingShares('${investment.investmentId}', '${investment.userId}');" data-target="#updateinvestmentshares" data-toggle="modal">
                                                                                                                                                <i class="os-icon os-icon-wallet-loaded"></i>
                                                                                                                                                <span>Purchase</span>
                                                                                                                                            </a>
                                                                                                                                        </td>-->
                                                                </tr>
                                                            </c:forEach>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--------------------
                            START - Chat Popup Box
                            -------------------->
<!--                            <div class="floated-chat-btn">
                                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                            </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <jsp:include page="user-right-sidebar.jsp"/>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>    


        <script type="text/javascript">
                                                                                        $(document).ready(function () {
                                                                                            var val = "";

                                                                                            $.ajax({
                                                                                                type: "GET",
                                                                                                dataType: "json",
                                                                                                url: "${home}/rest/cryptolabs/api/marketcaps",
                                                                                                success: function (data) {
                                                                                                    console.log("response:" + data);

                                                                                                    $.each(data, function (j, pdata) {

                                                                                                        val = val + '<tr><td>' + pdata.name + '</td><td>' + pdata.market_cap_usd + '</td><td>' + pdata.price_usd + '</td><td>' + pdata.total_supply + ' ' + pdata.symbol + '</td><td>' + pdata.volume_usd_24h + '</td><td>' + pdata.percent_change_24h + '</td></tr>';
                                                                                                    });
                                                                                                    $("#data").html(val);
                                                                                                },
                                                                                                error: function (jqXHR, textStatus, errorThrown) {
                                                                                                    console.log(' Error in processing! ' + textStatus);
                                                                                                }
                                                                                            });

                                                                                        });
        </script>

        <script>

            $(".bitcoin").hide();
            $(".etherium").hide();
            $(".ripple").hide();
            $(".bitcoincash").hide();
            $(".litecoin").hide();
            $("#market").click(function () {
                $(".market").show();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoin").click(function () {
                $(".market").hide();
                $(".bitcoin").show();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#etherium").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").show();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#ripple").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").show();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoincash").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").show();
                $(".litecoin").hide();
            });
            $("#litecoin").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").show();
            });

        </script>
        <script>
            $(document).ready(function () {
                $("#addbalance").hide();
                $("#mytransaction").hide();
                $("#withdrawlbalance").hide();
                
                $("#addbalances").click(function () {
                    $("#addbalance").show();
                    $("#withdrawlbalance").hide();
                });
                $("#widthdrawn").click(function () {
                    $("#mytransaction").hide();
                    $("#addbalance").hide();
                    $("#withdrawlbalance").show();
                });
                $("#mytransactions").click(function () {
                    $("#mytransaction").show();
                    $("#addbalance").hide();
                    $("#withdrawlbalance").hide();
                });
                $("#money").keyup(function () {
                    var ele = document.getElementById("money");
                    $(".checkout-amount").val(ele.value * 100);
                });
            });
        </script>
    </body>
</html>
