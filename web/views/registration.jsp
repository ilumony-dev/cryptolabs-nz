<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Sign UP-CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="favicon.png" rel="shortcut icon">
        <link href="apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="./resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="./resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="./resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="./resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="./resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link rel="stylesheet" href="./resources/css/awesomplete.css" />
        <link rel="stylesheet" href="./resources/css/intl/intlTelInput.css">
        <link rel="stylesheet" href="./resources/css/intl/demo.css">
        <script src="./resources/js/awesomplete.js"></script>
        <script src="./resources/js/jquery.min.js"></script>
        <!--<script src="./resources/js/jquery_3.2.1.min.js"></script>-->
        <script src="./resources/js/jquery.easing.min.js"></script>
        <script src="./resources/js/v2-jquery.min.js"></script>
        <script src="./resources/js/jquery.powertip.js"></script>
        <script src="./resources/js/jquery-ui.js"></script>
        <script src="./resources/js/index.js"></script>
        <!--<script src="./resources/js/validation.js"></script>-->
        <style>
            ol {
                list-style-type: none;
                counter-reset: item;
                margin: 0;
                padding: 0;
            }

            ol > li {
                display: table;
                counter-increment: item;
                margin-bottom: 0.6em;
            }

            ol > li:before {
                content: counters(item, ".") ". ";
                display: table-cell;
                padding-right: 0.6em;
            }

            li ol > li {
                margin: 0;
            }

            li ol > li:before {
                content: counters(item, ".") " ";
            }
            tr td{ padding: 10px}
        </style>
    </head>
    <body>
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w wider1">
                <div class="logo-w">
                    <a href="./login"><img alt="" src="./resources/img/invsta-03(200px).png" style="width:40%"></a>
                </div>

                <h4 class="auth-header">
                    Create new account
                </h4>

                <div class="steps-w" style="padding:2em">
                    <form:form method="POST" id="msform" name="msRegform" action="./register" class="form-signup" modelAttribute="user" autocomplete="off" >


                        <!--                                                                <div class="step-triggers">

                                                                                            <a class="step-trigger active" href="#stepContent1">Step 1</a>
                                                                                            <a class="step-trigger" href="#stepContent2">Step 2</a>
                                                                                            <a class="step-trigger" href="#stepContent3">Step 3</a>
                                                                                            <a class="step-trigger" href="#stepContent4">Step 4</a>
                                                                                            <a class="step-trigger" href="#stepContent5">Step 5</a>

                                                                                        </div>-->
                        <ul id="progressbar">
                            <li class="active">Step</li>
                            <li>Step</li>
                            <li>Step</li>
                            <li>Step</li>
                            <li>Step</li>
                        </ul>

                        <!--<div class="step-contents">-->

                        <!--<div class="step-content active" id="stepContent1">-->
                        <!--                                        <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="fullName"> Full Name</label>
                                                                            <input type="text" class="form-control" id="fullName" name="fullName" required="required" placeholder="Enter full name"  >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="dob"> Date of Birth</label>
                                                                            <input type="text" class="single-daterange form-control" id="dob" name="dob" required="required" placeholder="Enter date of birth">
                                                                        </div>
                                                                    </div>
                                                                </div>-->
                        <fieldset id="firstfs">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email"> Email address</label>
                                        <input type="email" class="form-control email" id="email" name="email" required="required" placeholder="Enter email" onblur="isEmailAlreadyExist();">
                                        <!--<span style="display:none; " id="validationId" ></span>-->
                                        <span style="display:block; " id="validationId" ></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password-field" name="password" required="required" placeholder="Enter password">
                                        <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye-slash" title="Show password"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w row" id="div-form-button">
                                <!--<a class="btn btn-primary step-trigger-btn" href="#stepContent2"> Continue</a>-->
                                <div class="col-sm-6">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <input type="button" name="verify" id="firstfs-verify" class="btn btn-primary" value="Verify">
                                    <input type="button" name="next" id="firstfs-continue" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>

                        </fieldset>
                        <!--</div>-->
                        <!--<div class="step-content" id="stepContent2">-->
                        <fieldset id="secondfs">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fullName"> Full Name</label>
                                        <input type="text" class="form-control" style="text-transform:capitalize;" id="fullName" name="fullName" required="required" placeholder="Enter full name" onkeyup="myFunction()" />
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="dob"> Date of Birth</label>
                                        <input type="text" class="single-daterange form-control" id="dob" name="dob" required="required" placeholder="Enter date of birth">
                                    </div>
                                </div>
                            </div>
                            <!--                                        <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="email"> Email address</label>
                                                                                <input type="email" class="form-control" id="email" name="email" required="required" placeholder="Enter email">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group">
                                                                                <label for="password">Password</label>
                                                                                <input type="password" class="form-control" id="password-field" name="password" required="required" placeholder="Enter password">
                                                                                <span toggle="#password-field" class="fa fa-fw field-icon toggle-password fa-eye"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="previous" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <input type="button" name="next" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>
                            <!--</div>-->
                        </fieldset>

                        <!--<div class="step-content" id="stepContent3">-->
                        <fieldset  id="thirdfs">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label for="inviteCode" style="margin-right:10px"> Invite Code:</label>
                                            <input type="text" class="form-control" id="inviteCode" name="inviteCode" required="required" placeholder="Enter invite code"  onblur="isInviteCodeUsed();" style="margin-right:10px">
                                            <input type="button" class="btn btn-success" value="Submit">
                                            <span style="display:block; " id="validationId3" ></span>
                                        </div>
                                        <P STYLE="color:#999">Please contact us at ${adminEmailId} if you have not received your invite code.</P>
                                    </div>
                                </div>

                            </div>
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="previous" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <input type="button" id="thirdfs-continue" name="next" class="next btn btn-primary" value="Continue">
                                </div>
                            </div>
                            <!--</div>-->
                        </fieldset>
                        <!--<div class="step-content" id="stepContent4">-->
                        <fieldset  id="fourthfs">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="countryCode"> Country Code</label>
                                        <input type="text" class="form-control"  id="countryCode" name="countryCode" required="required" placeholder="Enter Country Code">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label for="mobileNo"> Mobile Number</label>
                                        <input type="text" class="form-control error" id="mobileNo" name="mobileNo" required="required" placeholder="Enter mobile number">
                                    </div>
                                </div>
                                <div class="col-sm-3" style="margin-top:10px">
                                    <input type="radio" class="senderType" name="senderType" value="sms" checked="true">SMS
                                </div>
                                <div class="col-sm-3" style="margin-top:10px">
                                    <input type="radio" class="senderType" name="senderType" value="call">CALL
                                </div>
                                <div class="col-sm-6">
                                    <!-- Button trigger modal -->
                                    <!--                                                <a href="#" onclick="otpGeneration(this);" class="btn btn-primary" data-target="#otp" data-toggle="modal">
                                                                                        Verify
                                                                                    </a>-->
                                    <!-- Modal -->
                                    <div class="modal fade" id="otp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Please Enter your 4-digit OTP</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-inline">
                                                                <div class="form-group">
                                                                    <label for="onetimepassword" style="margin-right:10px"> Enter your 4-digit OTP: </label>
                                                                    <input type="text" name="onetimepassword" id="onetimepassword" class="form-control" placeholder="Enter your 4-Digit OTP">
                                                                </div>
                                                                <small>If you do not receive your OTP within 1 mintue, you may request for another OTP by clicking on <a href="#" onclick="otpG#90A2CEeneration(this);" >Resend</a>.</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3" style="text-align: right">
                                                            <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Submit</button>-->
                                                            <a href="#" onclick="verifyOTP(this);" class="btn btn-primary">
                                                                Submit
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-buttons-w row">
                                <div class="col-sm-6">
                                    <input type="button" name="button" class="prev btn btn-primary" value="Back">
                                </div>
                                <div class="col-sm-6 text-right" id="verify-otp-btn">
                                    <a href="#" onclick="otpGeneration(this);" class="btn btn-primary" data-target="#otp" data-toggle="modal">
                                        Verify
                                    </a>
                                </div>
                                <div class="col-sm-6 text-right" id="continue-4to5-btn">
                                    <input type="button" class="next btn btn-primary" name="next" value="Continue">
                                </div>
                            </div>
                            <!--</div>-->
                        </fieldset>
                        <!--<div class="step-content" id="stepContent5">-->
                        <fieldset  id="fifthfs">
                            <!--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <h4 class="text-center">WEBSITE TERMS OF USE</h4>
                                                                <div style="overflow:auto; height: 200px;">
                                                                    <ol>
                                                                        <li><B>APPLICATION OF TERMS</B>
                                                                            <ol>
                                                                                <li>These Terms apply to your use of the Website.  By accessing and using the Website:
                                                                                    <ol>
                                                                                        <li>you agree to these Terms; and</li>
                                                                                        <li>where your access and use is on behalf of another person (e.g. a company), you confirm that you are authorised to, and do in fact, agree to these Terms on that person?s behalf and that, by agreeing to these Terms on that person?s behalf, that person is bound by these Terms.</li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>If you do not agree to these Terms, you are not authorised to access and use theWebsite, and you must immediately stop doing so.</li>
                                                                            </ol>
                                                                        </li>
                                                                        <li><b>CHANGES</b>
                                                                            <ol>
                                                                                <li>We may change these Terms at any time by updating them on the Website.  Unless stated otherwise, any change takes effect immediately.  You are responsible for ensuring you are familiar with the latest Terms.  By continuing to access and use theWebsite, you agree to be bound by the changed Terms.
                                                                                </li>
                                                                                <li>We may change, suspend, discontinue, or restrict access to, the Website without notice or liability.</li>
                                                                                <li><b>These Terms were last updated on 01 February 2018. </b></li>
                                                                            </ol>
                                                                        </li>
                            
                                                                        <li><b>DEFINITIONS</b>
                                                                            <p>
                                                                                In these Terms:
                                                                            </p>
                                                                            <p>including and similar words do not imply any limit</p>
                                                                            <p>Lossincludes loss of profits, savings, revenue or data, and any other claim, damage, loss, liability and cost, including legal costs on a solicitor and own client basis</p>
                                                                            <p>personal information means information about an identifiable, living person</p>
                                                                            <p>Terms means these terms and conditions titled Website Terms of Use</p>
                                                                            <p>Underlying System means any network, system, software, data or material that underlies or is connected to the Website </p>
                                                                            <p>User ID means a unique name and/or password allocated to you to allow you to access certain parts of the Website</p>
                                                                            <p>We, us or our means the trade name Invsta and Ilumony Ltd. </p>
                                                                            <p>Website means <a href="https://www.invsta.com">www.invsta.com</a> and <a href="https://www.invsta.io">www.invsta.io</a></p>
                                                                            <p>You means you or, if clause 1.1b applies, both you and the other person on whose behalf you are acting.</p>
                                                                        </li>
                                                                        <li><B>YOUR OBLIGATIONS </B>
                                                                            <ol>
                                                                                <li>You must provide true, current and complete information in your dealings with us (including when setting up an account), and must promptly update that information as required so that the information remains true, current and complete.</li>
                            
                                                                                <li>If you are given a User ID, you must keep your User ID secure and:
                                                                                    <ol>
                                                                                        <li>not permit any other person to use your User ID, including not disclosing or providing it to any other person; and </li>
                                                                                        <li>immediately notify us if you become aware of any unauthorised use or disclosure of your User ID, by sending an email to <b>info@invsta.com</b></li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>You must:
                                                                                    <ol>
                                                                                        <li>not act in a way, or use or introduce anything (including any virus, worm, Trojan horse, timebomb, keystroke logger, spyware or other similar feature) that in any way compromises, or may compromise,the Website or any UnderlyingSystem, or otherwise attempt to damage or interfere with the Website or any UnderlyingSystem; and</li>
                                                                                        <li>unless with our agreement, access the Website via standard web browsers only and not by any other method. Other methods include scraping, deep-linking, harvesting, data mining, use of a robot or spider, automation, or any similar data gathering, extraction or monitoring method. </li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>You must obtain our written permission to establish a link to our Website.  If you wish to do so, email your request to <a href="info@invsta.com">info@invsta.com</a></li>
                                                                                <li>4.5	You indemnify us against all Loss we suffer or incur as a direct or indirect result of your failure to comply with these Terms, including any failure of a person who accesses and uses our Website by using your User ID.</li>
                            
                                                                            </ol>
                                                                        <li><b>INTELLECTUAL PROPERTY</b>
                                                                            <OL>
                                                                                <li>We (and our licensors) own all proprietary and intellectual property rights in the Website (including all information, data, text, graphics, artwork, photographs, logos, icons, sound recordings, videos and look and feel), and the UnderlyingSystems.</li>
                                                                                <li>The website may also contain a number of trademarks, logos and symbols which are either owned by Invstaand Ilumony Ltdor used with the approval of the respective Company. Nothing on the website should be construed as granting any licence to use any trade mark without the permission of the trade mark owner. Any material on this website that is identified as being subject to copyright of a third party, authorisation to use or reproduce such material must be obtained from that third party. Unless stated otherwise, you may access and download the materials located on this website only for personal, non-commercial use.</li>
                                                                            </OL>
                                                                        </li>
                                                                        <li><b>DISCLAIMERS</b>
                                                                            <ol>
                                                                                <li>To the extent permitted by law, we and our licensors have no liability or responsibility to you or any other person for any Loss in connection with:
                                                                                    <ol>
                                                                                        <li>the Website being unavailable (in whole or in part) or performing slowly;</li>
                                                                                        <li>any error in, or omission from, any information made available through the Website;</li>
                                                                                        <li>any exposure to viruses or other forms of interference which may damage your computer system or expose you to fraud when you access or use the Website.  To avoid doubt, you are responsible for ensuring the process by which you access and use the Website protects you from this; and</li>
                                                                                        <li>any site linked from the Website.  Any link on the Website to other sites does not imply any endorsement, approval or recommendation of, or responsibility for, those sites or their contents, operations, products or operators.</li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>We make no representation or warranty that the Website is appropriate or available for use in all countries or that the content satisfies the laws of all countries.  You are responsible for ensuring that your access to and use of the Website is not illegal or prohibited, and for your own compliance with applicable local laws. </li>
                                                                                <li>While every effort is made to ensure the information on this website is up-to-date and correct, Invsta makes no representations or warranties of any kind, express or implied, about the accuracy, reliability, completeness, suitability or availability of the website or the information about the products and services provided on the website. The information on this website is subject to change at any time.</li>
                                                                                <li>Invsta is under no obligation to update any information on this website or correct any errors in the information after it is published on the website. </li>
                                                                                <li>Any reference on this website to historical information and performance of a product or service may not necessarily be a good guide to future performance. You are solely responsible for any actions you take or do not take by relying on such information.</li>
                                                                                <li>To the full extent legally allowable the directors, contractors, associates and staff of Invsta expressly disclaim all and any liability and responsibility to any person in respect of anything, and of the consequences of anything, done or omitted to be done by any such person in reliance, whether wholly or partially, upon the whole or any part of the contents of this website. </li>
                            
                                                                            </ol>
                                                                        </li>
                                                                        <li><b>LIABILITY</b>
                                                                            <ol>
                                                                                <li>To the maximum extent permitted by law:
                                                                                    <ol>
                                                                                        <li>you access and use the Website at your own risk; and</li>
                                                                                        <li>weare not liable or responsible to you or any other person for any Loss under or in connection with these Terms, the Website, or your access and use of (or inability to access or use) the Website.  This exclusion applies regardless of whether our liability or responsibility arises in contract, tort (including negligence), equity, breach of statutory duty, or otherwise.</li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>Except to the extent permitted by law, nothing in these Terms has the effect of contracting out of the New Zealand Consumer Guarantees Act 1993 or any other consumer protection law that cannot be excluded.  To the extent our liability cannot be excluded but can be limited, our liability is limited to NZD100.</li>
                                                                                <li>To the maximum extent permitted by law and only to the extent clauses 7.1 and 7.2 of these Terms do not apply, our total liability to you under or in connection with these Terms, or in connection with the Website, or your access and use of (or inability to access or use) the Website, must not exceed NZD100.</li>
                            
                                                                            </ol>
                                                                        </li>
                                                                        <li><b>PRIVACY POLICY</b>
                                                                            <OL>
                                                                                <li>You are not required to provide personal information to us, although in some cases if you choose not to do so then we will be unable to make certain sections of the Website available to you.  For example, we may need to have your contact information in order to provide you with updates from our Website.  </li>
                                                                                <li>When you provide personal information to us, we will comply with the New Zealand Privacy Act 1993.</li>
                                                                                <li>The personal information you provide to us (including any information provided if you register for an account) is collected and may be used for communicating with you, statistical analysis, the marketing by us of products and services to you, credit checks (if necessary), and research and development.</li>
                                                                                <li>We may also collect technical information whenever you log on to, or visit the public version of, our Website.  This may include information about the way users arrive at, browse through and interact with our Website.  We may collect this type of technical information through the use of cookies and other means.  Cookies are alphanumeric identifiers that we transfer to your computer?s hard drive to enable our systems to recognise your browser.  If you want to disable cookies, you may do so by changing the settings on your browser.  However, if you do so, you may not be able to use all of the functions on the Website.  We use the technical information we collect to have a better understanding of the way people use our Website, to improve the way it works and to personalise it to be more relevant and useful to your particular needs.  We may also use this information to assist in making any advertising we display on the Website more personalised and applicable to your interests.</li>
                                                                                <li>Generally, we do not disclose personal information to third parties for them to use for their own purposes.  However, some of the circumstances in which we may do this are:
                                                                                    <ol>
                                                                                        <li>to service providers and other persons working with us to make the Website available or improve or develop its functionality (e.g. we may use a third party supplier to host the Website);</li>
                                                                                        <li>in relation to the proposed purchase or acquisition of our business or assets; or</li>
                                                                                        <li>where required by applicable law or any court, or in response to a legitimate request by a law enforcement agency.</li>
                                                                                    </ol>
                                                                                </li>
                                                                                <li>Any personal information you provide to us may be stored on the secure servers of our trusted service providers, which may be located outside New Zealand.  This may involve the transfer of your personal information to countries which have less legal protection for personal information than New Zealand.</li>
                                                                                <li>You have the right to request access to and correction of any of the personal information we hold about you.  If you would like to exercise these rights, please email us at info@invsta.com</li>
                                                                            </OL>
                                                                        </li>
                                                                        <li><b>SUSPENSION AND TERMINATION</b>
                                                                            <ol>
                                                                                <li>Without prejudice to any other right or remedy available to us, if we consider that you have breached these Terms or we otherwise consider it appropriate, we may immediately, and without notice, suspend or terminate your access to the Website (or any part of it).</li>
                                                                                <li>On suspension or termination, you must immediately cease using the Websiteand must not attempt to gain further access.
                                                                                </li>
                                                                            </ol>
                                                                        </li>
                                                                        <li><b>GENERAL ADVICE</b>
                                                                            <OL>
                                                                                <li>The information provided and services described in this website are of a general nature and are not intended to be personalised financial advice to a retail client. The information provided in this website is not intended to be a substitue for professional advice. You may contact us to seek appropriate personalised financial advice from a qualified professional to suit your individual circumstances.
                                                                                </li>
                                                                                <li>Disclosure statements relating to Invsta and the advisers associated with this website are available on request and free of charge.
                                                                                </li>
                            
                                                                            </OL>
                                                                        </li>
                                                                        <li><b>GENERAL</b>
                                                                            <ol>
                                                                                <li>If we need to contact you, we may do so by email or by posting a notice on the Website.  You agree that this satisfies all legal requirements in relation to written communications.</li>
                                                                                <li>These Terms, and any dispute relating to these Terms or the Website, are governed by and must be interpreted in accordance with the laws of New Zealand.  Each party submits to the non-exclusive jurisdiction of the Courts of New Zealand in relation to any dispute connected with these Terms or the Website.</li>
                                                                                <li>For us to waive a right under these Terms, the waiver must be in writing.</li>
                                                                                <li>Clauses which, by their nature, are intended to survive termination of these Terms, including clauses 4.5, 5, 6, 7, 11.1, continue in force.</li>
                                                                                <li>If any part or provision of these Terms is or becomes illegal, unenforceable, or invalid, that part or provision is deemed to be modified to the extent required to remedy the illegality, unenforceability or invalidity.  If a modification is not possible, the part or provision must be treated for all purposes as severed from these Terms.  The remainder of these Terms will be binding on you.</li>
                                                                                <li>11.6	These Terms set out everything agreed by the parties relating to your use of the Website and supersede and cancel anything discussed, exchanged or agreed prior to you agreeing to these Terms.  The parties have not relied on any representation, warranty or agreement relating to the Website that is not expressly set out in the Terms, and no such representation, warranty or agreement has any effect from the date you agreed to these Terms.  </li>
                            
                                                                            </ol>
                                                                        </li>
                            
                            
                            
                                                                    </ol>
                            
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4 class="text-center">WEBSITE TERMS OF USE</h4>
                                    <div style="overflow:auto; height: 200px;">
                                        <ol>
                                            <li> <b>APPLICATION OF THESE TERMS AND CONDITIONS</b>
                                                <ol>
                                                    <li>If you create a User Profile and choose to access services via our Platform you accept and agree that you will be bound by these Terms and Conditions.</li>
                                                    <li>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition.</li>
                                                </ol>
                                            </li>
                                            <li> <b>SERVICES PROVIDED</b>
                                                <ol>
                                                    <li>Through the Platform you will have access to the following services ("Crypto Traded Portfolio Services"):
                                                        <ol>
                                                            <li>Model crypto currency portfolios on our Platform providing a range of crypto currency investment options and strategies;</li>
                                                            <li>High Frequency Trading (HFT) Algorithm based portfolios; </li>
                                                            <li>General information, including performance data, on the various portfolio options and any portfolios selected by you; </li>
                                                            <li>General information and education about crypto-currencies and related topics, including investment strategies; </li>
                                                            <li>Assistance with managing your Portfolio, including executing lump-sum investment Transactions and regular monthly investment Transactions on your behalf, and the ability to track and transfer your supported Digital Currencies;</li>
                                                            <li>Portfolio re-balancing to reflect the target asset allocation in the model portfolio selected and accepted by you;</li>
                                                            <li>Foreign and digital currency transactions as required to effect the purchase of selected Portfolios.</li>
                                                            <li>Digital currency wallets that hold the underlying digital currency assets of the various portfolios on offer, like Bitcoin or Ethereum ("Digital Currency").</li>
                                                            <li>Chat and user support services.</li>
                                                        </ol>
                                                    </li>
                                                </ol>
                                            </li>
                                            <li> <b>USER PROFILE</b>
                                                <ol>
                                                    <li>In order to access the Crypto Traded Portfolio Services via our Platform you need to create a User Profile and meet the following requirements:
                                                        <ol>
                                                            <li>be 18 years of age or older;</li>
                                                            <li>reside in a country that meets our eligibility requirements and is not on our excluded list; </li>
                                                            <li>complete our verification processes in relation to your identity and personal information to our satisfaction; and</li>
                                                            <li>meet any other requirements or provide information notified by us to you from time to time.</li>
                                                        </ol>
                                                    </li>
                                                    <li>To create a User Profile you must submit personal information, details and copies of documents via the Platform or via another process (such as email) as requested. We will rely on that information in order to provide Services to you. You agree that all information you submit to us is complete, accurate and not misleading. If any of the information provided by you changes, you must notify us immediately.</li>
                                                    <li>You agree to take personal responsibility for any actions in respect of instructions you give us through your User Profile.</li>
                                                    <li>The User Profile may be used only for the provision of  Services on your behalf.</li>
                                                    <li>Only you may operate the User Profile. You agree to keep any passwords, codes or other security information safe and secure and to not provide that information to any other person. You accept that we are not responsible for any loss or damage you suffer as a result of a person accessing your User Profile who you have not authorized to do so. </li>
                                                    <li>You must report any suspected loss, theft or misuse of your User Profile to us immediately. </li>
                                                    <li>You agree to give us clear, consistent and properly authorized instructions via the Platform.</li>
                                                </ol>
                                            <li><b>INSTRUCTIONS TO EXECUTE A TRANSACTION</b>
                                                <ol>
                                                    <li>You may make lump sum investments or regular monthly investments via our Platform.</li>
                                                    <li>All money paid by you into your Account will be allocated to and invested in accordance with your chosen Portfolio where possible (subject to clause 4.3 below) until such time you instruct us to withdraw your investments.</li>
                                                    <li>For lump sum investments:
                                                        <ol>
                                                            <li>Instructions to execute a Transaction may be given by you via our Platform using your User Profile;</li>
                                                        </ol>
                                                    <li>For regular monthly investments:
                                                        <ol>
                                                            <li>You may select a monthly contribution amount when you create your User Profile. This amount may be amended from time to time via our Platform.</li>
                                                        </ol>
                                                    <li>You agree that you authorize us to submit Transactions on your behalf to reflect the target allocation and strategy for your selected Portfolio at the time we receive the allocated funds. You acknowledge that we will buy the maximum number of units (including fractions of units, if available) of the relevant investments in accordance with your selected Portfolio. Any remaining money will be held in your Wallet. You acknowledge that your Portfolio may not exactly reflect the target asset allocation set out in your model portfolio.</li>
                                                    <li>We will execute transactions on best endeavors basis within 24 hours of receiving allocated funds into your wallet, subject to our sole discretion. The actual price of a particular investment is set by the issuer, provider or the market (as applicable) at the time the order is executed by us.</li>
                                                    <li>We are under no obligation to verify the authenticity of any instruction or purported instruction and may act on any instruction given using your User Profile without further enquiry.</li>
                                                    <li>Any Transaction order placed by you forms a commitment, which once you submit, cannot subsequently be amended or revoked by you. However, our acceptance of Transactions via the Platform and our execution of those Transactions is at our sole discretion and subject to change at any time without notice.</li>
                                                    <li>We reserve the right to void any Transaction which we consider contains any manifest error. In the absence of our fraud or willful default, we will not be liable to you for any loss, cost, claim, demand or expense following any manifest error. </li>
                                                    <li>We are not responsible for any delay in the settlement of a Transaction resulting from circumstances beyond our control, or the failure of any other person or party (including you) to perform all necessary steps to enable completion of the transaction.</li>
                                                </ol>
                                            <li><b>PORTFOLIO REBALANCING</b>
                                                <ol>
                                                    <li>All Portfolios are monitored and are rebalanced in line with the Portfolio?s mandate, and at the discretion of the Portfolio Manager. Subject to these Terms and Conditions, and by continuing to hold the Portfolio, you instruct us to automatically rebalance your portfolio back to the target asset allocation based on your selected portfolio. </li>
                                                    <li>We rebalance by buying and/or selling investments and any other assets within the portfolio, including by using any available cash balances, so that your Portfolio reflects the target asset allocation of your chosen model portfolio.  </li>
                                                </ol>
                                            </li>
                                            <li><b>TARGET ASSET ALLOCATION</b>
                                                <ol>
                                                    <li>Each Portfolio will have a Target Asset Allocation, which is set by the Portfolio Manager. The Portfolio Manager may choose to alter the Target Asset Allocation from time to time and at their sole discretion, in line with the mandate and strategy of the portfolio.</li>
                                                    <li>Notification of any such changes to the Target Asset Allocation are made via the platform by way of an update to the Target Asset Allocation details provided for each portfolio. </li>
                                                </ol>
                                            </li>

                                            <li><b>DIGITAL CURRENCY WALLET</b>
                                                <ol>
                                                    <li>Digital Currency Transactions. Invsta processes supported Digital Currency according to the instructions received from its users and we do not guarantee the identity of any user, receiver, requestee or other party. You should verify all transaction information prior to submitting instructions to Invsta. Once submitted to a Digital Currency network, a Digital Currency Transaction will be unconfirmed for a period of time pending sufficient confirmation of the transaction by the Digital Currency network. A transaction is not complete while it is in a pending state. Funds associated with transactions that are in a pending state will be designated accordingly, and will not be included in your Invsta Account balance or be available to conduct transactions. Invsta may charge network fees (miner fees) to process a Digital Currency transaction on your behalf. Invsta will calculate the network fee in its sole discretion.</li>
                                                    <li>Digital Currency Storage & Transmission Delays. Invsta securely stores all Digital Currency private keys in our control in a combination of online and offline storage. As a result, it may be necessary for Invsta to retrieve certain information from offline storage in order to facilitate a Digital Currency Transaction in accordance with your instructions, which may delay the initiation or crediting of such Digital Currency Transaction for 48 hours or more. You acknowledge and agree that a Digital Currency Transaction facilitated by Invsta may be delayed.</li>
                                                    <li>Operation of Digital Currency Protocols. Invsta does not own or control the underlying software protocols which govern the operation of Digital Currencies supported on our platform. In general, the underlying protocols are open source and anyone can use, copy, modify, and distribute them. By using the Invsta platform, you acknowledge and agree (i) that Invsta is not responsible for operation of the underlying protocols and that Invsta makes no guarantee of their functionality, security, or availability; and (ii) that the underlying protocols are subject to sudden changes in operating rules (a/k/a ?forks?), and that such forks may materially affect the value, function, and/or even the name of the Digital Currency you hold in the Invsta platform. In the event of a fork, you agree that Invsta may temporarily suspend Invsta operations (with or without advance notice to you) and that Invsta may, in its sole discretion, decide whether or not to support (or cease supporting) either branch of the forked protocol entirely. You acknowledge and agree that Invsta assumes absolutely no responsibility whatsoever in respect of an unsupported branch of a forked protocol.</li>
                                                    <li>Payments of money can be made to your Wallet electronically, including through credit card payments, e-Poli and bank transfers. The acceptable forms of payment will be set out on our Platform and may be subject to change from time to time.</li>
                                                    <li>You agree that investments and any other assets held on your behalf may be pooled with the investments and other assets of other clients and therefore your holdings may not be individually identifiable within the Digital Currency Wallet.</li>
                                                    <li>You agree money held in your Wallet (including money held pending investment as well as the proceeds and income from selling investments) <b>(?Client Money?)</b> may be held in pooled accounts, which means your money may be held in the same accounts as that of other clients using the Platform.</li>
                                                    <li>Withdrawals from the Service or of any Client Money can be made via the Platform by initiating a ?Withdrawal?. Withdrawals of will be processed on a best endeavors basis as outlined in Clause 6.3. Withdrawals of Client Money will be processed and paid into a bank account nominated by you in the same name of the User Account upon settlement, the timing of which depends on the market. We may set a maximum daily amount that can be withdrawn.</li>
                                                </ol>
                                            </li>

                                            <li><b>OUR WEBSITE POLICY</b>
                                                <ol>
                                                    <li>You agree to receive any and all advice, documents, information, or other communications from Invsta electronically through the Platform, by email, or otherwise over the internet,</li>
                                                    <li>You agree to receive any statements, confirmations, prospectuses, disclosures, tax reports, notices, documents, information, amendments to the agreements, or other communications transmitted to you from time to time electronically.</li>
                                                    <li>You agree that we may use the email address provided in your application for a User Profile or such other email address as you notify to us from time to time to provide such information to you. Any electronic communication will be deemed to have been received by you when it is transmitted by us.</li>
                                                    <li>Access to our Platform is at our absolute discretion. You acknowledge that access to our Platform may be interrupted and the Services may be unavailable in certain circumstances.</li>
                                                </ol>
                                            </li>

                                            <li><b>RISK WARNINGS</b>
                                                <ol>
                                                    <li>Investment in Crypto Currencies can present significant risks and there is a chance that you may loose some or all of your money. You need to be aware of these risks that may include but are not limited to market risk, company, sector and country exposure risk, currency exchange risk, regulation risk, and economic and political risk.</li>
                                                </ol>
                                            </li>

                                            <li><b>DISPUTE RESOLUTION</b>
                                                <ol>
                                                    <li>You will promptly inform us via the Platform and/or by email of any complaint you have regarding the standard of service we provide to you. We will promptly respond to any complaint we receive from you.</li>
                                                    <li>If you are unsatisfied with our response, you may direct any complaints to:</br>
                                                        <b>Financial Services Complaints Limited (FSCL)</b></br>
                                                        PO Box 5967, Lambton Quay</br>
                                                        Wellington, 6145</br> 
                                                        Email: info@fscl.org.nz</br>
                                                        FSCL is our independent external dispute resolution scheme that has been approved by the Minister of Consumer Affairs under the Financial Service Providers (Registration and Dispute Resolution) Act 2008. This service costs you nothing.
                                                    </li>
                                                </ol>
                                            </li>

                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                <ol>
                                                    <li>You authorize us to:
                                                        <ol>
                                                            <li>Collect, hold and disclose personal information about you for the purpose of providing Services to you, creating your User Profile and for our own marketing purposes;</li>
                                                            <li>Aggregate and anonymize your data along with that of other users, and use that data ourselves or sell or supply that anonymized data to other financial service providers for marketing, product design and other commercial purposes;</li>
                                                            <li>Keep records of all information and instructions submitted by you via the Platform or by email;</li>
                                                            <li>Record all telephone conversations with you;</li>
                                                            <li>Record and identify the calling telephone from which you instruct us;</li>
                                                            <li>Record and retain copies of all information and documents for the purposes of the various Financial Market Regulations under which we may operate;</li>
                                                            <li>Obtain credit information concerning you if we consider it relevant to determine whether to agree to perform Services or administer your User Profile.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree to give us any information we ask you for if we (or any affiliates or third parties with whom you are dealing with through us) believe we need it in order to comply with any laws in New Zealand or overseas. You agree that we can use information that we have about you to:
                                                        <ol>
                                                            <li>Assess whether we will provide you with a User Profile;</li>
                                                            <li>Provide you with, or manage any of, our Services;</li>
                                                            <li>Comply with any laws in New Zealand or overseas applying to us or the Services we provide to you; or</li>
                                                            <li>Compare with publicly available information about you or information held by other reputable companies or organizations we have a continuing relationship with, for any of the above reasons.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree that we can obtain information about you from or give your information to any of the following people or organizations:
                                                        <ol>
                                                            <li>Our agents or third parties (whether in New Zealand or overseas) that provide services to, through or via us such as execution, data hosting (including cloud-based storage providers) and processing, tax services, anti-money laundering services or support services; or</li>
                                                            <li>A regulator or exchange for the purposes of carrying out its statutory functions.</li>
                                                        </ol>
                                                    </li>
                                                    <li>You agree that where required to help us comply with laws in New Zealand or overseas or if we believe giving the information will help prevent fraud, money laundering or other crimes, we may give information we hold about you to others including:
                                                        <ol>
                                                            <li>Police or government agencies in New Zealand and overseas; or</li>
                                                            <li>The issuers of Financial Products in order for them to satisfy their obligations under New Zealand anti-money laundering laws and regulations.</li>
                                                        </ol>
                                                    </li>
                                                    <li>We may not be allowed to tell you if we do give out information about you. We are not responsible to you or anyone else if we give information for the purposes above. We will not disclose information about you except as authorized by you or as required or authorized by law.</li>
                                                </ol>
                                            </li>
                                            <li><b>USE AND DISCLOSURE OF INFORMATION</b>
                                                <ol>
                                                    <li>You have rights of access to, and correction of, personal information supplied to and held by us.</li>
                                                </ol>
                                            </li>
                                            <li><b>ANTI-MONEY LAUNDERING</b>
                                                <ol>
                                                    <li>We may need to identify you in order to comply with laws in New Zealand and overseas.</li>
                                                    <li>We are required to comply with all applicable New Zealand or overseas anti-money laundering laws and regulations and may ask for information identifying you, and then verification for such identity, including references and written evidence. We may ask you for details of the source or destination of your funds.</li>
                                                    <li>You agree to complete our identification and verification processes in relation to your identity and personal information to our satisfaction. We reserve the right to refuse to provide you Services or to cancel your User Profile if this information is not provided on request.</li>
                                                    <li>You agree that we may use personal information provided by you for the purpose of electronic identity verification using third party contractors and databases including the Department of Internal Affairs, NZ Transport Agency, Companies Office, electronic role, a credit reporting agency or other entity for that purpose.</li>
                                                </ol>
                                            </li>

                                            <li><b>LIMITATION OF LIABILITY</b>
                                                <ol>
                                                    <li>You agree that where our Services are acquired for business purposes, or where you hold yourself out as acquiring our Services for business purposes, the Consumer Guarantees Act 1993 (?the CGA?) will not apply to any supply of products or services by us to you. Nothing in these Terms and Conditions will limit or abrogate your rights and remedies under the CGA except to the extent that contracting out is permitted under the CGA and all provisions of these Terms and Conditions will be modified to the extent necessary to give effect to that intention.</li>
                                                    <li>Subject to any terms implied by law which cannot be excluded and in the absence of our fraud or willful default, we will not be liable in contract, tort (including negligence), equity, or otherwise for any direct, indirect, incidental, consequential, special or punitive damage, or for any loss of profit, income or savings, or any costs or expenses incurred or suffered by you or any other person in respect of Services supplied to you or in connection with your use of our Platform.</li>
                                                    <li>You acknowledge that:
                                                        <ol>
                                                            <li>Our advice may be based on information provided to us by you or by third parties which may not have been independently verified by us (?Information from Third Parties?);</li>
                                                            <li>We are entitled to rely on Information from Third Parties and we are under no obligation to verify or investigate that information. We will not be liable under any circumstances where we rely on Information from Third Parties;</li>
                                                            <li>Our Services do not include tax advice. We recommend that you consult your tax adviser before making a decision to invest or trade in Financial Products;</li>
                                                            <li>Without limiting any obligations we have under the various Financial Market Regulations, it is your responsibility to:
                                                                <ol>
                                                                    <li>Satisfy yourself that our Crypto Traded Portfolios are appropriate to your circumstances; and</li>
                                                                    <li>Make further enquiries as should reasonably be made by you before making a decision to invest or trade in Crypto Currency Products.</li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li>We will be under no liability for any loss or expense which arises as a result of a delay by us in executing a Transaction via the Platform (due to a network outage, system failure or otherwise or for any reason whatsoever).</li>
                                                    <li>We will not be liable for any failure to provide products or services to you or to perform our obligations to you under these Terms and Conditions if such failure is caused by any event of force majeure beyond our reasonable control, or the reasonable control of our employees, agents or contractors. For the purposes of this clause, an event of force majeure includes (but is not limited to) a network outage, an inability to communicate with other financial providers, brokers, financial intermediaries, a failure of any computer dealing or settlement system, an inability to obtain the necessary supplies for the proper conduct of business, and the actions or failures of any counterparty or any other broker or agent, or the systems of that broker or agent.</li>
                                                    <li>The provisions of this clause 13 will extend to all our employees, agents and contractors, and to all corporate entities in which we may have an interest and to all entities which may distribute our publications.</li>
                                                </ol>
                                            </li>

                                            <li><b>FEES AND CHARGES FOR SERVICES</b>
                                                <ol>
                                                    <li>We may charge an annual fee for accessing the Platform and associated services. Currently no such fee is charged. </li>
                                                    <li>Each Portfolio will charge various fees for investing into that portfolio. All portfolio fees are automatically deducted from the assets held within the Portfolio each month, these will not be charged to you directly. Each portfolio may charge, and must pay to us, on demand, the following fees and charges (?Fees?):
                                                        <ol>
                                                            <li>Crypto-Currency Portfolio Services:</li>
                                                            <table class="table-bordered">
                                                                <tr>
                                                                    <td>Portfolio Management Fee</td>
                                                                    <td>Description</td>
                                                                    <td>Other Expenses</td>
                                                                    <td>Performance Fee</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Up to 5% per annum of all funds invested (plus GST if any). Fees may be changed from time to time at our absolute discretion.</td>
                                                                    <td>Based on total investment value, charged monthly</td>
                                                                    <td>In operating the Portfolios and offering this service to you, we may incur other expenses such as transaction fees, bank charges, audit and legal fees. Such expenses will be an addition fees these are charged to the Portfolios. </td>
                                                                    <td>We may change a performance fee on some Portfolios, at a rate of up to 30% of outperformance against given hurdles. </td>
                                                                </tr>
                                                            </table>
                                                        </ol>
                                                    </li>
                                                    <li>You must also pay for any other fees and charges ("Fees and Charges") for any add on services you obtain via the Platform.</li>
                                                    <li>All Fees are automatically debited monthly in arrears from each Portfolio. You agree that we have the absolute right of sale of investments in each Portfolio to meet all amounts due to us.</li>
                                                </ol>
                                            </li>

                                            <li><b>TERMINATION OF YOUR USER PROFILE</b>
                                                <ol>
                                                    <li>Either you or we may cancel your User Profile at any time. If we cancel your User Profile we will notify you by email.   If you wish to cancel your User Profile you may do so using the facility available on the Platform.</li>
                                                    <li>Examples of when we will cancel your User Profile include (but are not limited to):
                                                        <ol>
                                                            <li>In order to comply with a court order;</li>
                                                            <li>If you have acted (or we have reasonable grounds to suspect that you are about to act) unlawfully;</li>
                                                            <li>If you have breached these Terms and Conditions;</li>
                                                            <li>If you are insolvent or in liquidation or bankruptcy; or</li>
                                                            <li>If you have not paid Fees due under these Terms and Conditions by the due date.</li>
                                                        </ol>
                                                    </li>
                                                    <li>If either you or we terminate your User Profile you will still be responsible for any Transaction made up to the time of termination, and Fees for Services rendered to you and our rights under these Terms and Conditions in respect of those matters will continue to apply accordingly.</li>
                                                    <li>You agree that we will not be liable for any loss you suffer where we act in accordance with this clause.</li>
                                                    <li>On termination of your User Profile, we will redeem all of the investments held in your Portfolio and transfer the proceeds of sale (less any applicable Fees) to a bank account nominated by you, unless you provide us with notice in writing of different instructions or to transfer the investments in your Portfolio to a different Digital Wallet within 7 days of the day of termination.</li>
                                                </ol>
                                            </li>

                                            <li><b>ASSIGNMENT</b>
                                                <ol>
                                                    <li>You agree that these Terms and Conditions bind you personally and you may not assign any of your rights or obligations under it. Any such purported assignment will be ineffective.</li>
                                                    <li>We may assign all or any of our rights, and transfer all or any of our obligations under these Terms and Conditions to any person, including a purchaser of the Platform or all or substantially all of our business.</li>
                                                </ol>
                                            </li>

                                            <li><b>INDEMNITY</b>
                                                <ol>
                                                    <li>You must, on demand being made by us and our partners, affiliated persons, officers and employees, indemnify those persons against any and all losses, costs, claims, damages, penalties, fines, expenses and liabilities: 
                                                        <ol>
                                                            <li>in the performance of their duties or exercise of their authorities, except to the extent arising as a result of their own negligence, fraud or willful default; and</li>
                                                            <li>which they may incur or suffer as a result of:
                                                                <ol>
                                                                    <li>relying in good faith on, and implementing instructions given by any person using your User Profile, unless there are reasonable grounds for us to doubt the identity or authority of that person; and</li>
                                                                    <li>relying in good faith on information you have either provided to us or made available to us.</li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                    </li>
                                                    <li>If any person who is not you (except for the Financial Markets Authority or any other regulatory authority of competent jurisdiction) makes any claim, or brings any proceedings in any Court, against us in connection with Services we provide to you, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                    <li>You must also indemnify us and our partners, affiliated persons, officers and their respective employees, agents and contractors in the case of any portfolio investment entity tax liability required to be deducted (at the Prescribed Investor Rate nominated by you or us) from your investment, even if that liability exceeds the value of their investments, or any incorrect notification or failure to notify or update annually your PIR or tax rates.</li>
                                                </ol>
                                            </li>

                                            <li><b>AMENDMENTS</b>
                                                <ol>
                                                    <li>We may, at our sole discretion, amend these Terms and Conditions (including our Fees) by giving ten working days' prior notice to you either by:
                                                        <ol>
                                                            <li>Notice on our website; or</li>
                                                            <li>Direct communication with you via email,</li>
                                                        </ol>
                                                        unless the change is immaterial (e.g. drafting and typographical amendments) or we are required to make the change sooner (e.g. for regulatory reasons), in which case the changes will be made immediately.
                                                    </li>
                                                    <li>You may request a copy of our latest Terms and Conditions by contacting us via the Platform or email.</li>
                                                    <li>If you access the Platform or otherwise use our Services after the expiry of the notice given in accordance with clause 17.1 you will be deemed to have accepted the amended Terms and Conditions.</li>
                                                </ol>
                                            </li>

                                            <li><b>NOTICES</b>
                                                <ol>
                                                    <li>Any notice or other communication (?Notice?) given for the purposes of these Terms and Conditions:
                                                        <ol>
                                                            <li>Must be in writing; and</li>
                                                            <li>Must be sent to the relevant party?s email address.</li>
                                                        </ol>
                                                    </li>
                                                    <li>Any notice is deemed served or received on the day it is sent to the correct email address.</li>
                                                    <li>Any notice that is served on a Saturday, Sunday or public holiday is deemed to be served on the first working day after that.</li>
                                                    <li>A notice may be given by an authorized officer, employee or agent.
                                                        <ol>
                                                            <li>Notice may be given personally to a director, employee or agent of the party at the party?s address or to a person who appears to be in charge at the time of delivery or according to section 387 to section 390 of the Companies Act 1993.</li>
                                                            <li>If the party is a natural person, partnership or association, the notice may be given to that person or any partner or responsible person. If they refuse to accept the notice, it may be brought to their attention and left in a place accessible to them.</li>
                                                        </ol>
                                                    </li>
                                                </ol>                
                                            </li>

                                            <li><b>GOVERNING LAW AND JURISDICTION</b>
                                                <ol>
                                                    <li>These Terms and Conditions are governed by and construed according to the current laws of New Zealand. The parties agree to submit to the non-exclusive jurisdiction of the Courts of New Zealand.</li>
                                                    <li>If you bring any claim or proceeding against us in any Court which is not a Court of New Zealand, you will indemnify us for and against, and pay to us on demand, all legal costs and other expenses that we incur in connection with that claim or proceeding.</li>
                                                </ol>
                                            </li>

                                            <li><b>DEFINITIONS</b></br>
                                                <b>"Account"</b> means the cash management account held within the Service by the Custodian on bare trust for the Client.</br> 

                                                <b>"Authorized Financial Adviser"</b> has the same meaning as in section 51 of the Financial Advisers Act.</br>

                                                <b>"Crypto Traded Portfolio Services"</b> means the services described in clause 2.1.</br>

                                                <b>"Client"</b> means the person in whose name a User Profile has been opened.</br>

                                                <b>"Fees"</b> means any fees or other charges charged for the Services, including, but not limited to the fees set out in clause 15.</br> 

                                                <b>"Financial Advisers Act"</b> means the Financial Advisers Act 2008.</br>

                                                <b>"Financial Adviser Service"</b> has the same meaning as in section 9 of the Financial Advisers Act.</br> 
                                                <b>"Financial Product"</b> has the same meaning as in section 7 of the Financial Markets Conduct Act 2013.</br> 
                                                <b>"Minor"</b> means a person under the age of 18.</br>
                                                <b>"Platform"</b> means the Invsta Investment Platform. (www.invsta.com) and any associated variations of this website </br>

                                                <b>"Portfolio"</b> means a portfolio of assets that is managed by Invsta via the Platform. </br>

                                                <b>"Services"</b> means a Service we provide to you via our Platform including the Crypto Traded Portfolio Services </br>

                                                <b>"Terms and Conditions"</b> means these Terms and Conditions.</br>

                                                <b>"Transaction"</b> means a transaction effected or to be effected using the Platform pursuant to your instructions.</br>

                                                <b>"User Profile"</b> means a User Profile in your name created by you in accordance with these Terms and Conditions through which you are entitled to gain access to our Platform.</br>

                                            </li>

                                            <li><b>GENERAL INTERPRETATION</b>
                                                <ol>
                                                    <li>In these Terms and Conditions:
                                                        <ol>
                                                            <li>Unless the context otherwise requires, references to:
                                                                <ol>
                                                                    <li>?we?, ?us?, Invsta, and ?Ilumony? refer to Ilumony Limited, trading as Invsta, and related companies (as defined in section 2(3) of the Companies Act 1993); and</li>
                                                                    <li>?you?, ?your? and ?yourself? are references to the Client and where appropriate any person who you have advised us are authorized to act on your behalf.</li>
                                                                </ol>
                                                            </li>
                                                            <li>A reference to these Terms and Conditions (including these Terms and Conditions) includes a reference to that agreement as novated, altered or replaced from time to time;</li>
                                                            <li>A reference to a party includes the party?s administrators, successors and permitted assigns;</li>
                                                            <li>Words in the plural include the singular and vice versa;</li>
                                                            <li>Headings are inserted for convenience only and will be ignored in construing these Terms and Conditions;</li>
                                                            <li>References to any legislation includes statutory regulations, rules, orders or instruments made pursuant to that legislation and any amendments, re-enactments, or replacements; and</li>
                                                            <li>Expressions referring to writing will be construed as including references to words printed, typewritten, or by email or otherwise traced, copied or reproduced.</li>
                                                        </ol>
                                                    </li>
                                                    <li>These Terms and Conditions are intended to benefit and be enforceable by Invsta Limited and any related companies (as defined in section 2(3) of the Companies Act 1993) in accordance with the Contracts (Privity) Act 1982.</li>
                                                </ol>
                                            </li>

                                        </ol>
                                    </div>
                                </div>
                            </div>
                            <!--                                        <div class="row">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-buttons-w text-left">
                                                                                <a class="btn btn-primary step-trigger-btn" href="#stepContent4"> Back</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-buttons-w text-right">
                                                                                <a href="javascript:submitForm()" onclick="termsAccepted(this);" class="btn btn-primary">
                                                                                    I Accept
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                            <div class="form-buttons-w">
                                <div class="row">
                                    <div class="col-sm-6"><a class="btn btn-primary step-trigger-btn" href="#stepContent4">Back</a></div>
                                    <div class="col-sm-6 text-right">
                                        <a href="javascript:submitForm()" onclick="termsAccepted(this);" class="btn btn-primary">
                                            I Accept
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-sm-12" style="text-align: center">
                                    <a href="./resources/Crypto-T&C.pdf" style=" color: #011c53;  font-size: 14px; font-weight: bold; padding: 10px;
                                       text-decoration: underline;" target="black"> Download Client Terms and Conditions</a>
                                </div>
                            </div>
                            <!--</div>-->
                            <!--</div>-->
                        </fieldset>

                    </form:form>
                </div>
            </div>
        </div>



        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/moment/moment.js"></script>
        <script src="./resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="./resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="./resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="./resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="./resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="./resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="./resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="./resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="./resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="./resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="./resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="./resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="./resources/js/main.js?version=3.5.1"></script>
        <script src="https://app.kickbox.com/authenticate/1/release.js"></script>
        <script src="./resources/js/cryptolabs/user-main.js"></script>
        <script src="./resources/js/intlTelInput.js"></script>
        <script>
                                            $("#countryCode").intlTelInput();
                                            $(document).ready(function () {
                                                $("#continue-4to5-btn").hide();
                                                $(".next").click(function () {
                                                    var valid = $("#msform").valid();
                                                    if (valid) {
                                                        MoveNext1(this);
                                                    }
                                                });
                                                $(".prev").click(function () {
                                                    MovePrevious1(this);
                                                });
                                                $("#firstfs-verify").click(function () {
                                                    var email = $('#email').val();
                                                    fingerprint(email);
                                                });
                                            });
                                            // First, fetch a fingerprint from Kickbox
                                            fingerprint = function (e) {
                                                Kickbox.fingerprint({
                                                    app: "${kickboxAppCode}",
                                                    email: e, // Email address to authenticate
                                                    onSuccess: function (fp) {
                                                        $.ajax({
                                                            url: "./rest/cryptolabs/api/verifyEmail",
                                                            type: "POST",
                                                            async: false,
                                                            data: {email: e, fingerprint: fp},
                                                            success: function (success) {
                                                                alert('success-->' + success);
                                                                if (success === true) {
                                                                    $('#firstfs-continue').removeAttr("disabled");
                                                                }
                                                            },
                                                            error: function (e) {
                                                                //alert('error' + e);
                                                            }
                                                        });
                                                    },
                                                    onError: function (err) {
                                                        // Handle error
                                                    }
                                                });
                                            };
                                            isEmailAlreadyExist = function () {
                                                var email = $('#email').val();
                                                $('#firstfs-continue').attr("disabled", "disabled");
                                                var url = './rest/cryptolabs/api/isEmailAlreadyExist?e=' + email;
                                                $.ajax({
                                                    url: url,
                                                    type: "GET",
                                                    async: false,
                                                    success: function (response) {
                                                        $("#validationId").css('display', 'inline');
                                                        if (response === 'Email is Valid.') {
                                                            $("#validationId").css('color', 'green').html(response);
                                                            $("#validationId").focus();
//                                                            $('#firstfs-continue').removeAttr("disabled");
                                                            return true;
                                                        } else {
                                                            $("#validationId").css('color', 'red').html(response);
                                                            $("#validationId").focus();
                                                            return false;
                                                        }
                                                    },
                                                    error: function (e) {
                                                        //handle error
                                                    }
                                                });
                                            }

                                            isInviteCodeUsed = function () {
                                                var inviteCode = $('#inviteCode').val();
                                                $('#thirdfs-continue').attr("disabled", "disabled");
                                                var url = './rest/cryptolabs/api/isInviteCode?ic=' + inviteCode;
                                                $.ajax({
                                                    url: url,
                                                    type: "GET",
                                                    async: false,
                                                    success: function (response) {
                                                        $("#validationId3").css('display', 'inline');
                                                        if (response === 'true') {
                                                            $("#validationId3").css('color', 'green').html('Invite Code is valid.');
                                                            $("#validationId3").focus();
                                                            $('#thirdfs-continue').removeAttr("disabled");
                                                            return true;
                                                        } else {
                                                            $("#validationId3").css('color', 'red').html('Invite Code is used or invalid.');
                                                            $("#validationId3").focus();
                                                            return false;
                                                        }
                                                    },
                                                    error: function (e) {
                                                        //handle error
                                                    }
                                                });
                                            }

                                            MoveNext1 = function (nextVal) {
                                                if (animating) {
                                                    return false;
                                                }
                                                animating = true;
                                                current_fs = $(nextVal).parent().parent().parent();
                                                next_fs = current_fs.next();
                                                //activate next step on progressbar using the index of next_fs
                                                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                                                //show the next fieldset
                                                next_fs.show();
                                                current_fs.hide();
                                                animating = false;
                                                //hide the current fieldset with style
                                                current_fs.animate({opacity: 0}, {
                                                    step: function (now, max) {
                                                        //as the opacity of current_fs reduces to 0 - stored in "now"
                                                        //1. scale current_fs down to 80%
                                                        scale = 1 - (1 - now) * 0.2;
                                                        //2. bring next_fs from the right(50%)
                                                        left = (now * 50) + "%";
                                                        //3. increase opacity of next_fs to 1 as it moves in
                                                        opacity = 1 - now;
                                                        current_fs.css({
                                                            'transform': 'scale(' + scale + ')',
                                                            'position': 'absolute'
                                                        });
                                                        next_fs.css({'left': left, 'opacity': opacity});
                                                    },
                                                    duration: 1500,
                                                    complete: function () {
                                                        current_fs.hide();
                                                        animating = false;
                                                    },
                                                    //this comes from the custom easing plugin
                                                    easing: 'easeInOutBack'
                                                });
                                            };
                                            MovePrevious1 = function (nextVal) {
                                                if (animating) {
                                                    return false;
                                                }
                                                animating = true;
                                                current_fs = $(nextVal).parent().parent().parent();
                                                previous_fs = current_fs.prev();
                                                //de-activate current step on progressbar
                                                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                                                //show the previous fieldset
                                                previous_fs.show();
                                                current_fs.hide();
                                                animating = false;
                                                //hide the current fieldset with style
                                                current_fs.animate({opacity: 0}, {
                                                    step: function (now, mx) {
                                                        //as the opacity of current_fs reduces to 0 - stored in "now"
                                                        //1. scale previous_fs from 80% to 100%
                                                        scale = 0.8 + (1 - now) * 0.2;
                                                        //2. take current_fs to the right(50%) - from 0%
                                                        left = ((1 - now) * 50) + "%";
                                                        //3. increase opacity of previous_fs to 1 as it moves in
                                                        opacity = 1 - now;
                                                        current_fs.css({'left': left});
                                                        previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                                                    },
                                                    duration: 1500,
                                                    complete: function () {
                                                        current_fs.hide();
                                                        animating = false;
                                                    },
                                                    //this comes from the custom easing plugin
                                                    easing: 'easeInOutBack'
                                                });
                                            };
        </script>

        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-XXXXXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
        <script src="./resources/js/jquery.validate.min.js"></script>
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script>
            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        </script>
        <script>

        </script>
        <script>
            function submitForm() {
                function termsAccepted(link) {
                    link.onclick = function (event) {
                        event.preventDefault();
                    }
                }
                document.msRegform.submit();
            }

            var EmailDomainSuggester = {

                domains: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],
                bindTo: $("#email"),
                init: function () {
                    this.addElements();
                    this.bindEvents();
                },
                addElements: function () {
                    // Create empty datalist
                    this.datalist = $("<datalist />", {
                        id: 'email-options'
                    }).insertAfter(this.bindTo);
                    // Corelate to input
                    this.bindTo.attr("list", "email-options");
                },
                bindEvents: function () {
                    this.bindTo.on("keyup", this.testValue);
                },
                testValue: function (event) {
                    var el = $(this),
                            value = el.val();
                    // email has @
                    // remove != -1 to open earlier
                    if (value.indexOf("@") != -1) {
                        value = value.split("@")[0];
                        EmailDomainSuggester.addDatalist(value);
                    } else {
                        // empty list
                        EmailDomainSuggester.datalist.empty();
                    }
                },
                addDatalist: function (value) {
                    var i, newOptionsString = "";
                    for (i = 0; i < this.domains.length; i++) {
                        newOptionsString +=
                                "<option value='" +
                                value +
                                "@" +
                                this.domains[i] +
                                "'>";
                    }

                    // add new ones
                    this.datalist.html(newOptionsString);
                }
            }
            EmailDomainSuggester.init();
        </script>
    </script>

    <script>
        function myFunction() {
            var name = $('input[name="fname"]').val();
            var first_name = name.split(' ')[0];
            var last_name = name.substring(first_name.length).trim();
            $("#Person_FirstName").val(first_name);
            $("#Person_LastName").val(last_name);
            //console.log(first_name)
            //console.log(last_name)
        }
    </script>
</body>
</html>
