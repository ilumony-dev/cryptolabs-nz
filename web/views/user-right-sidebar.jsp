<%-- 
    Document   : user-right-sidebar
    Created on : 31 Oct, 2017, 12:50:23 PM
    Author     : palo12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content-panel">
    <div class="content-panel-close">
        <i class="os-icon os-icon-close"></i>
    </div>
    <div class="element-wrapper">
        <h6 class="element-header">
            Quick Links
        </h6>
        <div class="element-box-tp">
            <div class="el-buttons-list full-width">
                <a class="btn btn-white btn-sm" href="#" data-target="" data-toggle="modal"><i class="os-icon os-icon-delivery-box-2"></i><span>Working on the Best Next thing</span></a>
            </div>
        </div>
    </div>
    <!--    <div class="element-wrapper">
            <h6 class="element-header">
                Support Agents
            </h6>
            <div class="element-box-tp">
                <div class="profile-tile">
                    <div class="profile-tile-box">
                        <div class="pt-avatar-w">
                            <img alt="" src="${home}/resources/img/avatar1.jpg">
                        </div>
                        <div class="pt-user-name">
                            Mark Parson
                        </div>
                    </div>
                    <div class="profile-tile-meta">
                        <ul>
                            <li>
                                Last Login:<strong>Online Now</strong>
                            </li>
                            <li>
                                Tickets:<strong>12</strong>
                            </li>
                            <li>
                                Response Time:<strong>2 hours</strong>
                            </li>
                        </ul>
                        <div class="pt-btn">
                            <a class="btn btn-success btn-sm" href="#">Send Message</a>
                        </div>
                    </div>
                </div>
                <div class="profile-tile">
                    <div class="profile-tile-box">
                        <div class="pt-avatar-w">
                            <img alt="" src="${home}/resources/img/avatar3.jpg">
                        </div>
                        <div class="pt-user-name">
                            John Mayers
                        </div>
                    </div>
                    <div class="profile-tile-meta">
                        <ul>
                            <li>
                                Last Login:<strong>Online Now</strong>
                            </li>
                            <li>
                                Tickets:<strong>9</strong>
                            </li>
                            <li>
                                Response Time:<strong>3 hours</strong>
                            </li>
                        </ul>
                        <div class="pt-btn">
                            <a class="btn btn-secondary btn-sm" href="#">Send Message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->

    <!--    <div class="element-wrapper">
            <h6 class="element-header">
                Team Members
            </h6>
            <div class="element-box-tp">
                <div class="input-search-w">
                    <input class="form-control rounded bright" placeholder="Search team members..." type="search">
                </div>
                <div class="users-list-w">
                    <div class="user-w with-status status-green">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="${home}/resources/img/avatar1.jpg">
                            </div>
                        </div>
                        <div class="user-name">
                            <h6 class="user-title">
                                John Mayers
                            </h6>
                            <div class="user-role">
                                Account Manager
                            </div>
                        </div>
                        <div class="user-action">
                            <div class="os-icon os-icon-email-forward"></div>
                        </div>
                    </div>
                    <div class="user-w with-status status-green">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="${home}/resources/img/avatar1.jpg">
                            </div>
                        </div>
                        <div class="user-name">
                            <h6 class="user-title">
                                Ben Gossman
                            </h6>
                            <div class="user-role">
                                Administrator
                            </div>
                        </div>
                        <div class="user-action">
                            <div class="os-icon os-icon-email-forward"></div>
                        </div>
                    </div>
                    <div class="user-w with-status status-red">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="${home}/resources/img/avatar1.jpg">
                            </div>
                        </div>
                        <div class="user-name">
                            <h6 class="user-title">
                                Phil Nokorin
                            </h6>
                            <div class="user-role">
                                HR Manger
                            </div>
                        </div>
                        <div class="user-action">
                            <div class="os-icon os-icon-email-forward"></div>
                        </div>
                    </div>
                    <div class="user-w with-status status-green">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="${home}/resources/img/avatar1.jpg">
                            </div>
                        </div>
                        <div class="user-name">
                            <h6 class="user-title">
                                Jenny Miksa
                            </h6>
                            <div class="user-role">
                                Lead Developer
                            </div>
                        </div>
                        <div class="user-action">
                            <div class="os-icon os-icon-email-forward"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
</div>
<jsp:include page = "modals/addtransaction.jsp"></jsp:include>
<script>
        window.intercomSettings = {
            app_id: "q28x66d9",
            name: '${endUser.fullName}', // Full name
            email: '${endUser.email}', // Email address
            created_at: ${endUser.createdTime} // Signup date as a Unix timestamp
        };
</script>
<script>(function () {
        var w = window;
        var ic = w.Intercom;
        if (typeof ic === "function") {
            ic('reattach_activator');
            ic('update', intercomSettings);
        } else {
            var d = document;
            var i = function () {
                i.c(arguments)
            };
            i.q = [];
            i.c = function (args) {
                i.q.push(args)
            };
            w.Intercom = i;
            function l() {
                var s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://widget.intercom.io/widget/q28x66d9';
                var x = d.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            }
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })()
</script>
