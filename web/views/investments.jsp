<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<!--<html>-->
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:th='http://www.thymeleaf.org'>
    <head>
        <title>Select Investment-CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/css/stripe-button-el.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Investment Options</a>
                        </li>

                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box" style="display: block">
                            <!--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <div class="element-actions">
                                                                        <form class="form-inline justify-content-sm-end">
                                                                            <select class="form-control form-control-sm rounded">
                                                                                <option value="Pending">
                                                                                    Today
                                                                                </option>
                                                                                <option value="Active">
                                                                                    Last Week 
                                                                                </option>
                                                                                <option value="Cancelled">
                                                                                    Last 30 Days
                                                                                </option>
                                                                            </select>
                                                                        </form>
                                                                    </div>
                                                                    <h6 class="element-header">
                                                                        Fund Investment Options
                                                                    </h6>
                                                                    <div class="element-content">
                                                                        <div class="row">
                            <c:forEach items="${funds}" var="fund">
                                <div class="col-sm-4">
                                    <div class="ilumony-box ilumony-bg1 text-center" onclick="setInfo('${fund.fundId}', '${fund.name}', '${fund.percentage}');" data-target=".investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">
                                        <h5 class="color3">${fund.name}</h5>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <!--                                        <div class="element-actions">
                                                                                    <form class="form-inline justify-content-sm-end">
                                                                                        <select class="form-control form-control-sm rounded">
                                                                                            <option value="Pending">
                                                                                                Today
                                                                                            </option>
                                                                                            <option value="Active">
                                                                                                Last Week 
                                                                                            </option>
                                                                                            <option value="Cancelled">
                                                                                                Last 30 Days
                                                                                            </option>
                                                                                        </select>
                                                                                    </form>
                                                                                </div>-->

                                        <h6 class="element-header">
                                            Crypto Traded Portfolio (CTP) Investment Options
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">
                                                <c:forEach items="${portfolios}" var="fund">
                                                    <div class="col-sm-3">
                                                        <div class="ilumony-box text-center" onclick="setInfo('${fund.fundId}', '${fund.name}', '${fund.percentage}','${fund.description}');" data-target=".investment-fund-modal-lg" data-toggle="modal"  style="cursor: pointer; background:url(./resources/img/${fund.master}) center center no-repeat; background-size: cover;" >
                                                            <h5 class="color3">${fund.name}</h5>
                                                        </div>
                                                    </div>
                                                </c:forEach>

                                                <c:forEach items="${funds}" var="fund">
                                                    <div class="col-sm-3">
                                                        <div class="ilumony-box ilumony-bg1 text-center" onclick="setInfo('${fund.fundId}', '${fund.name}', '${fund.percentage}','${fund.description}');" data-target=".investment-fund-modal-lg" data-toggle="modal" style="cursor: pointer">
                                                            <h5 class="color3">${fund.name}</h5>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--modals end-->
<!--                        <div class="floated-chat-btn">
                            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                        </div>-->
                        <div class="floated-chat-w">
                            <div class="floated-chat-i">
                                <div class="chat-close">
                                    <i class="os-icon os-icon-close"></i>
                                </div>
                                <div class="chat-head">
                                    <div class="user-w with-status status-green">
                                        <div class="user-avatar-w">
                                            <div class="user-avatar">
                                                <img alt="" src="${home}/resources/img/avatar1.jpg">
                                            </div>
                                        </div>
                                        <div class="user-name">
                                            <h6 class="user-title">
                                                John Mayers
                                            </h6>
                                            <div class="user-role">
                                                Account Manager
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-messages">
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, how can I help you?
                                        </div>
                                    </div>
                                    <div class="date-break">
                                        Mon 10:20am
                                    </div>
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, my name is Mike, I will be happy to assist you
                                        </div>
                                    </div>
                                    <div class="message self">
                                        <div class="message-content">
                                            Hi, I tried ordering this product and it keeps showing me error code.
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-controls">
                                    <input class="message-input" placeholder="Type your message here..." type="text">
                                    <div class="chat-extra">
                                        <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------
                        END - Chat Popup Box
                        -------------------->
                    </div>
                </div>
            </div>
        </div>
        <div class="display-type"></div>
        <jsp:include page="modals/investment-fund-modal-lg.jsp"></jsp:include>
        <!-- small Model-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="${home}/resources/bower_components/moment/moment.js"></script>
    <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
    <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
    <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
    <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
    <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
    <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
    <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
    <script src="${home}/resources/js/main.js?version=3.5.1"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
                var currency = '${paymentCurrency}';
                var currSymbol = '${currSymbol}';
    </script>
    <script src="${home}/resources/js/cryptolabs/user-main.js"></script> 
    <script>
                                                            function setInfo(fundId, fundName, interestRate, description) {
                                                                $("#investment-fund-modal-label").text('Add Investment');
                                                                $(".investment-fund-modal-fundName").text(fundName);
                                                                $(".investment-fund-modal-fundName").val(fundName);
                                                                $(".investment-fund-modal-fundDescription").text(description);
                                                                $("#investment-fund-modal-fundId").val(fundId);
                                                                $("#investment-type-fundId").val(fundId);
                                                                $("#via-wallet-fundId").val(fundId);
                                                                $("#investment-fund-modal-fundIR").val(interestRate);
                                                                $("#investment-fund-modal-continue").hide();
                                                                activeSummary();
                                                            }
                                                            
    </script>
</body>
</html>
