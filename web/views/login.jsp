<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Login - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="./resources/favicon.png" rel="shortcut icon">
        <link href="./resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">

        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">

        <link rel="stylesheet" href="./resources/css/awesomplete.css" />
        <script src="./resources/js/awesomplete.js"></script>  

        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/validation.js"></script>        
        <script src="./resources/js/v2-jquery.min.js"></script>
        <script src="./resources/js/jquery.powertip.js"></script>      
        <script src="./resources/js/jquery-ui.js"></script>
        <script src="./resources/js/index.js"></script>         


    </head>
    <body class="auth-wrapper">
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w">
                <div class="logo-w">
                    <a href="./login"><img alt="" src="./resources/img/invsta-03(200px).png" style="width:40%"></a>
                </div>
                <h4 class="auth-header">
                    Login
                </h4>

                <form id="msform" action="<c:url value='/j_spring_security_check'/>" method="post" class="form-login">
                    <fieldset>
                        <div class="form-group">
                            <label for="username">Email</label>
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                            <input type="email" class="form-control" id="email" name="username" data-error="Your email is invalid" required="required" placeholder="Enter your email" >
                            
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required="required" placeholder="Enter your password" >
                            <span toggle="#password" class="fa fa-fw field-icon toggle-password fa-eye-slash" title="Show password"></span>
                            <div class="pre-icon os-icon os-icon-fingerprint"></div>
                        </div>
                        <div class="buttons-w">
                            <button class="btn btn-primary">Log me in</button>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox">Remember Me
                                </label>
                            </div>           
                        </div>
                    </fieldset>
                    <div class="buttons-w">
                        <p> Don't have account <a  href='./register'>Sign Up</a></p>
                        <p><a  href='./forgetpwd'> Forget Password ?</a></p>
                    </div>
                </form>
            </div>
        </div>
        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>

        <script src="./resources/js/jquery.validate.min.js"></script>

        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script>
            $(".toggle-password").click(function () {

                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            var EmailDomainSuggester = {

                domains: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],

                bindTo: $("#email"),

                init: function () {
                    this.addElements();
                    this.bindEvents();
                },

                addElements: function () {
                    // Create empty datalist
                    this.datalist = $("<datalist />", {
                        id: 'email-options'
                    }).insertAfter(this.bindTo);
                    // Corelate to input
                    this.bindTo.attr("list", "email-options");
                },

                bindEvents: function () {
                    this.bindTo.on("keyup", this.testValue);
                },

                testValue: function (event) {
                    var el = $(this),
                            value = el.val();

                    // email has @
                    // remove != -1 to open earlier
                    if (value.indexOf("@") != -1) {
                        value = value.split("@")[0];
                        EmailDomainSuggester.addDatalist(value);
                    } else {
                        // empty list
                        EmailDomainSuggester.datalist.empty();
                    }
                },

                addDatalist: function (value) {
                    var i, newOptionsString = "";
                    for (i = 0; i < this.domains.length; i++) {
                        newOptionsString +=
                                "<option value='" +
                                value +
                                "@" +
                                this.domains[i] +
                                "'>";
                    }

                    // add new ones
                    this.datalist.html(newOptionsString);
                }
            }
            EmailDomainSuggester.init();
        </script>
        <!--        <script>new Awesomplete('input[type="email"]', {
                        list: ["aol.com", "att.net", "comcast.net", "facebook.com", "gmail.com", "gmx.com", "googlemail.com", "google.com", "hotmail.com", "hotmail.co.uk", "mac.com", "me.com", "mail.com", "msn.com", "live.com", "sbcglobal.net", "verizon.net", "yahoo.com", "yahoo.co.uk"],
                        data: function (text, input) {
                            return input.slice(0, input.indexOf("@")) + "@" + text;
                        },
                        filter: Awesomplete.FILTER_STARTSWITH
                    });</script> -->
    </body>
</html>

