<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Insights - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Insights</a>
                        </li>
                        <!--            <li class="breadcrumb-item">
                                      <span>Laptop with retina screen</span>
                                    </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">

                            <div class="row">
                                <div class="col-md-12">
                                    <img src="./resources/img/Coming-Soon.jpg" style="width:100%">
                                </div>
                            </div>

<!--                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">

                                        <h6 class="element-header">
                                            Insights
                                        </h6>
                                        <div class="element-content">
                                            <div class="row">

                                                <a class="col-sm-4" id="market" href="#" style="text-decoration: none">
                                                    <div class="box " >
                                                                                    <div class="label">
                                                                                        Market
                                                                                    </div>
                                                                                 <div class="trending trending-up">
                                                                                      <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                                                                                     </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/market.png"/>  Market
                                                        </div>

                                                    </div>
                                                </a>

                                                <a class="col-sm-4" id="bitcoin" href="#">
                                                    <div class=" box  ">
                                                                                    <div class="label">
                                                                                      Gross Profit
                                                                                    </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/bitcoin.png"/> BitCoin
                                                        </div>

                                                    </div>
                                                </a>
                                                <a class="col-sm-4" id="etherium" href="#">
                                                    <div class=" box  ">
                                                                                    <div class="label">
                                                                                      New Customers
                                                                                    </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/ethereum.png"/>Etherium
                                                        </div>

                                                    </div>
                                                </a>
                                                <a class="col-sm-4"  id="ripple" href="#">
                                                    <div class=" box  ">
                                                                                    <div class="label">
                                                                                      Products Sold
                                                                                    </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/ripple.png"/> Ripple
                                                        </div>

                                                    </div>
                                                </a>
                                                <a class="col-sm-4" id="bitcoincash" href="#">
                                                    <div class=" box  ">
                                                                                    <div class="label">
                                                                                      Gross Profit
                                                                                    </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/bitcoin-cash.png"/> Bitcoincash
                                                        </div>

                                                    </div>
                                                </a>
                                                <a class="col-sm-4" id="litecoin" href="#">
                                                    <div class=" box  ">
                                                                                    <div class="label">
                                                                                      New Customers
                                                                                    </div>
                                                        <div class="value">
                                                            <img src="${home}/resources/img/litecoin.png"/> LiteCoin
                                                        </div>

                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <!--                            <div class="row market">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        Market
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div id="chart"></div>
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>
                                                                                            Name
                                                                                        </th>
                                                                                        <th>
                                                                                            Market Cap
                                                                                        </th>
                                                                                        <th  >
                                                                                            Price usd
                                                                                        </th>
                                                                                        <th  >
                                                                                            circulating Supply
                                                                                        </th>
                                                                                        <th>
                                                                                            Valume(24h)
                                                                                        </th>
                                                                                        <th>
                                                                                            % Change(24h)
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody id="data">
                            
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>            
                                                        </div>-->


                            <div class="row bitcoin">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Bitcoin
                                        </h6>
                                        <div class="element-box">

                                            <!-- TradingView Widget BEGIN -->

                                            <script type="text/javascript">

                                                new TradingView.widget({
                                                    "width": 600,
                                                    "height": 500,
                                                    "symbol": "NASDAQ:AAPL",
                                                    "interval": "D",
                                                    "timezone": "Etc/UTC",
                                                    "theme": "Light",
                                                    "style": "1",
                                                    "locale": "en",
                                                    "toolbar_bg": "#f1f3f6",
                                                    "enable_publishing": false,
                                                    "hide_side_toolbar": false,
                                                    "allow_symbol_change": true,
                                                    "hideideas": true,
                                                });
                                            </script>
                                            <!-- TradingView Widget END -->

                                        </div>
                                    </div>
                                </div>            
                            </div>

                            <div class="row etherium">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Etherium 
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Products Ordered
                                                            </th>
                                                            <th class="text-center">
                                                                Status
                                                            </th>
                                                            <th class="text-right">
                                                                Order Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="nowrap">
                                                                John Mayers
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio2.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio12.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 5 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $354
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Kelly Brans
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio8.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $94
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Tim Howard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio16.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $156
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Joe Trulli
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio1.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio6.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 2 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $1,120
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Jerry Lingard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $856
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                            </div>

                            <div class="row ripple">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Ripple
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Products Ordered
                                                            </th>
                                                            <th class="text-center">
                                                                Status
                                                            </th>
                                                            <th class="text-right">
                                                                Order Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="nowrap">
                                                                John Mayers
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio2.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio12.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 5 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $354
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Kelly Brans
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio8.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $94
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Tim Howard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio16.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $156
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Joe Trulli
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio1.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio6.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 2 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $1,120
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Jerry Lingard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $856
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                            </div>

                            <div class="row bitcoincash">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Bitcoin Cash
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Products Ordered
                                                            </th>
                                                            <th class="text-center">
                                                                Status
                                                            </th>
                                                            <th class="text-right">
                                                                Order Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="nowrap">
                                                                John Mayers
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio2.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio12.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 5 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $354
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Kelly Brans
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio8.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $94
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Tim Howard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio16.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $156
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Joe Trulli
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio1.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio6.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 2 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $1,120
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Jerry Lingard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $856
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                            </div>

                            <div class="row litecoin">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Litecoin
                                        </h6>
                                        <div class="element-box">
                                            <div class="table-responsive">
                                                <table class="table table-lightborder">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Customer Name
                                                            </th>
                                                            <th>
                                                                Products Ordered
                                                            </th>
                                                            <th class="text-center">
                                                                Status
                                                            </th>
                                                            <th class="text-right">
                                                                Order Total
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="nowrap">
                                                                John Mayers
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio2.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio12.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 5 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $354
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Kelly Brans
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio8.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $94
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Tim Howard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio16.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio14.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $156
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Joe Trulli
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio1.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio5.jpg)"></div>
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio6.jpg)"></div>
                                                                    <div class="cell-img-more">
                                                                        + 2 more
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $1,120
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="nowrap">
                                                                Jerry Lingard
                                                            </td>
                                                            <td>
                                                                <div class="cell-image-list">
                                                                    <div class="cell-img" style="background-image: url(${home}/resources/img/portfolio9.jpg)"></div>
                                                                </div>
                                                            </td>
                                                            <td class="text-center">
                                                                <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
                                                            </td>
                                                            <td class="text-right">
                                                                $856
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>            
                            </div>

                            <!--------------------
                            START - Chat Popup Box
                            -------------------->
<!--                            <div class="floated-chat-btn">
                                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                            </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>    


        <script type="text/javascript">
                                                $(document).ready(function () {
                                                    var val = "";

                                                    $.ajax({
                                                        type: "GET",
                                                        dataType: "json",
                                                        url: "${home}/rest/cryptolabs/api/marketcaps",
                                                        success: function (data) {
                                                            console.log("response:" + data);

                                                            $.each(data, function (j, pdata) {

                                                                val = val + '<tr><td>' + pdata.name + '</td><td>' + pdata.market_cap_usd + '</td><td>' + pdata.price_usd + '</td><td>' + pdata.total_supply + ' ' + pdata.symbol + '</td><td>' + pdata.volume_usd_24h + '</td><td>' + pdata.percent_change_24h + '</td></tr>';
                                                            });
                                                            $("#data").html(val);
                                                        },
                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                            console.log(' Error in processing! ' + textStatus);
                                                        }
                                                    });

                                                });
        </script>

        <script>

            $(".bitcoin").hide();
            $(".etherium").hide();
            $(".ripple").hide();
            $(".bitcoincash").hide();
            $(".litecoin").hide();
            $("#market").click(function () {
                $(".market").show();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoin").click(function () {
                $(".market").hide();
                $(".bitcoin").show();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#etherium").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").show();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#ripple").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").show();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoincash").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").show();
                $(".litecoin").hide();
            });
            $("#litecoin").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").show();
            });

        </script>
    </body>
</html>
