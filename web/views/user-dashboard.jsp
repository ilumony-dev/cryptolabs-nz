<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>User Dashboard - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="User dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>
    <style>
        .table tbody tr:last-child{ background: #6599FF; color: #fff}
    </style>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${endUser.fullName} Profile</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${currSymbol}</a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Crypto Net Worth Overview
                                        </h6>
                                        <div class="element-box">
                                            <div class="os-tabs-w">
                                                <div class="os-tabs-controls">
                                                    <ul class="nav nav-tabs smaller">
                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab_overview">Daily Crypto Balance Snapshot
                                                            </a>
                                                        </li>
<!--                                                        <li class="nav-item">
                                                            <a class="nav-link active" data-toggle="tab" href="#tab_latest_week">Latest Week</a>
                                                        </li>-->
                                                    </ul>
                                                </div>
                                                <div class="tab-content">
                                                    <div class="label">
                                                        Current Balance of all Investments  <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom">
                                                    </div>
                                                    <div class="el-tablo">
                                                        <div class="value currentBalance">
                                                            ${totalSharesValue}
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane active" id="tab_overview">
                                                        <div class="el-chart-w"  id="linegraph">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane " id="tab_latest_week">
                                                        <div class="el-chart-w"  id="lg-latestweek">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invsta Portfolio Breakdown Current
                                        </h6>
                                        <div class="element-box">
                                            <div class="element-content">
                                                <div class="row">
                                                    <div class="col-sm-12 ">
                                                        <div class=" el-tablo">
                                                            <div class="label">Current Balance  <img src="./resources/img/info-icon.png" style="vertical-align:text-bottom"></div>
                                                            <div class="value currentBalance"></div>
                                                            <input type="hidden" id="actualInvested"/>
                                                            <div class="el-chart-w" id="cons-invs-pieChart">
                                                            </div>
                                                            <div id="legend-1" style="position:absolute; center:0px; top:0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--</div>-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">Invsta Crypto Portfolio Snapshot</h6>
                                    </div>
                                </div>
                            </div>
                            <c:forEach items="${userInvestments}" var="inv">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="element-wrapper">
                                            <div class="element-box">
                                                <h6 style="margin-bottom:20px; display: inline-block; margin-right: 5px;">${inv.fundName}</h6><img src="./resources/img/info-icon.png" style="vertical-align:text-top"> in ${currSymbol}
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="fund-img" style="width:100%;height: 150px;cursor: pointer; background:url(./resources/img/${inv.fundImage}) center center no-repeat; background-size: cover;">
                                                            <!--<img src="./resources/img/balance.png" >-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">

                                                        <!--<div id="easypie" style="width:150px; height: 150px"></div>-->

                                                        <div id="initialInvestment${inv.investmentId}" class="centered" style="width: 150px; height: 150px;"></div>
                                                        <!--<div class="circle1"><div id="text"><b>Investment Portfolio Size</b><br><span class="color1"><b>$${inv.investedAmount}</b></span></div></div>-->
                                                    </div>
                                                    <div class="col-md-3">
                                                        <!--<div id="easypie1" style="width:150px; height: 150px"></div>-->
                                                        <input type="hidden" class="hdn-currentAmount${inv.investmentId}"/>
                                                        <div id="currentAmount${inv.investmentId}" class="centered" style="width: 150px; height: 150px;"></div>
                                                        <!--<div class="circle1"><div id="text"><b>Investment Portfolio Current Size</b><br><span class="color1"><b>$${inv.value}</b></span></div></div>-->
                                                    </div>
                                                    <div class="col-md-3">
                                                        <!--<div id="easypie2" style="width:150px; height: 150px"></div>-->
                                                        <div id="performance${inv.investmentId}" class="centered" style="width: 150px; height: 150px;"></div>
                                                        <!--                                                        <div class="circle1">
                                                                                                                    <div id="text"><b>Investment Portfolio Performance</b><br>
                                                                                                                        <span class="color1" id="percentange${inv.investmentId}"><b>0%</b></span>
                                                                                                                                                                                        <span style="position:absolute; text-align:center;">
                                                                                                                                                                                            <span class="trending trending-up" id="percentange${inv.investmentId}" style="position:relative; top:50px;">
                                                                                                                                                                                                12%<i class="os-icon os-icon-arrow-up2"></i>
                                                                                                                                                                                            </span>
                                                                                                                                                                                        </span>
                                                                                                                    </div>
                                                                                                                </div>-->
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top:20px">
                                                    <div class="col-md-12 text-right">
                                                        <c:choose>
                                                            <c:when test="${info.admin}">
                                                                <a class="btn btn-primary" href="./suflsdjweafkjsdlkj?ok=${inv.investmentId}&un=${endUser.userId}">Show Me More</a>
                                                                <a class="btn btn-danger" href="#" onclick="purchasedShares4Edit('${inv.investmentId}', '${endUser.userId}');" data-target="#edit-investment-shares" data-toggle="modal"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                                            </c:when>
                                                            <c:otherwise>
                                                                <a class="btn btn-primary" href="./suflsdjweafkjsdlkj?ok=${inv.investmentId}">Show Me More</a>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <!--<a class="btn btn-danger" href='#' data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('DEPOSITE', '${inv.investmentId}')"><i class="fa fa-plus" aria-hidden="true"></i></a>-->
                                                        <!--<a class="btn btn-warning" href="#" data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('WITHDRAWL', '${inv.investmentId}')">Withdrawal</a>-->
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 10px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                            <!--                            <div class="row" >
                                                            <div class="col-md-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">Minto Crypto Portfolio Snapshot</h6>
                                                                </div>
                                                            </div>
                            <c:forEach items="${portfolioInvestments}" var="inv">
                                <div class="col-md-6">
                                    <div class="element-wrapper">
                                        <div class="element-box">
                                            <div class="fund-img">
                                                <img src="./resources/img/balance.png" style="width:100%">
                                            </div>
                                            <center><h6 style="margin-top:10px">${inv.fundName}</h6></center>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="col-md-6">
                                                    <div class=" color3 Initial">
                                                        <center>Initial Investment</center>
                                                        <center>$ ${inv.investmentAmount}</center>
                                                    </div>
                                                </div>
                                                                                                    <div class="col-md-6">
                                                                                                        <div class=" color3 Monthly">
                                                                                                            <center>Regular Contribution</center>
                                                                                                            <center>$ ${inv.regularlyAmount}</center>
                                                                                                        </div>
                                                                                                    </div>
                                                <div class="col-md-6">
                                                    <div class=" color3 Current">
                                                        <center>Current Value</center>
                                                        <center>$ ${inv.value}</center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                                                                    <div class="col-md-6">
                                                                                                        <div class=" color3 Predict">
                                                                                                            <center>Forecast <br>Value</center>
                                                                                                            <center id="predictValue${inv.investmentId}"></center>
                                                                                                        </div>
                                                                                                    </div>
    
                                            </div>
                                                                                            <div class="row" style="margin-top:10px">
                                                                                                <div class="col-md-6" style="text-align:right">
                                                                                                    <a class="btn btn-success" href="#" data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('WITHDRAWL','${inv.investmentId}')">Withdrawl</a>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <a class="btn btn-danger" href='#' data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('DEPOSITE','${inv.investmentId}')">Deposite</a>
                                                                                                </div>
                                                                                            </div>
                                            <div class="row" style="margin-top: 10px">
                                                <div class="col-md-6 offset-3" >
                                                    <a href="./ajflsdjafkjsdlkj?ok=${inv.investmentId}" class="btn btn-primary">More...</a>
                                                    <div class="dropdown">
                                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">View More
                                                            <span class="caret"></span></button>
                                                        <ul class="dropdown-menu dropdown-menu-left">
                                                            <li><a href="./ajflsdjafkjsdlkj?ok=${inv.investmentId}">Details</a></li>
                                                            <li><a  href="#" data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('WITHDRAWL', '${inv.investmentId}')">Withdrawl</a></li>
                                                            <li><a href='#' data-target="#addtransaction" data-toggle="modal" onclick="addTransaction('DEPOSITE', '${inv.investmentId}')">Deposite</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                                                                    <div class="col-md-6">
                                                                                                        <a href="./ajflsdjafkjsdlkj?ok=${inv.investmentId}" class="btn btn-primary">View Details</a>
                                                                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>                                
                        </div>-->
                            <!--                           <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        Latest Requests
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>
                                                                                            Date
                                                                                        </th>
                                                                                        <th>
                                                                                            Transaction Type
                                                                                        </th>
                                                                                        <th>
                                                                                            Amount
                                                                                        </th>
                                                                                        <th class="text-right">
                                                                                            Status
                                                                                        </th>
                                                                                        <th>
                                                                                            Description 
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                            <c:forEach items="${latestTransactions}" var="latest">
                                <tr>
                                    <td>
                                ${latest.createDate}
                            </td>
                            <td>
                                ${latest.actionType}
                            </td>
                            <td>
                                $${latest.amount}
                            </td>
                            <td class="text-right">
                                ${latest.status}
                            </td>
                            <td class="text-right">
                                ${latest.description}
                            </td>
                                    <a class="btn btn-secondary btn-sm" href="#" onclick="pendingShares('${investment.investmentId}', '${investment.userId}');" data-target="#updateinvestmentshares" data-toggle="modal">
                                    <i class="os-icon os-icon-wallet-loaded"></i>
                                    <span>Purchase</span>
                                </a>
                            </td>
                        </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>-->

                            <!-- JDIHDFI -->
                            <!--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        My Investments Status
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-lightborder">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>
                                                                                            Fund Name
                                                                                        </th>
                                                                                        <th>
                                                                                            Shares
                                                                                        </th>
                                                                                        <th>
                                                                                            Time Frame 
                                                                                        </th>
                                                                                        <th class="text-right">
                                                                                            Investment Total
                                                                                        </th>
                                                                                        <th>
                                                                                            Action 
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                            <c:forEach items="${userInvestments}" var="investment">
                                <tr>
                                    <td>
                                ${investment.fundName}
                            </td>
                            <td>
                                ${investment.shares}
                            </td>
                            <td>
                                ${investment.timeFrame}
                            </td>
                            <td class="text-right">
                                $${investment.investmentAmount}
                            </td>
                            <td class="text-right">
                                ${investment.action}
                            </td>
                                    <a class="btn btn-secondary btn-sm" href="#" onclick="pendingShares('${investment.investmentId}', '${investment.userId}');" data-target="#updateinvestmentshares" data-toggle="modal">
                                    <i class="os-icon os-icon-wallet-loaded"></i>
                                    <span>Purchase</span>
                                </a>
                            </td>
                        </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>-->
                            <!-- end -->

                            <!--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        Total Investment Value
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div class="os-tabs-w">
                                                                            <div class="os-tabs-controls">
                                                                                <ul class="nav nav-tabs smaller">
                                                                                    <li class="nav-item">
                                                                                        <a class="nav-link active" data-toggle="tab" href="#tab_overview">Overview</a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="tab-content">
                                                                                <div class="tab-pane active" id="tab_overview">
                                                                                    <div class="el-tablo">
                                                                                        <div class="label">
                                                                                            Total Investments
                                                                                        </div>
                                                                                        <div class="value">
                                                                                            $10,000
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="el-chart-w">
                                                                                        <canvas height="250px" id="lineChart" width="600px"></canvas>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tab-pane" id="tab_sales"></div>
                                                                                <div class="tab-pane" id="tab_conversion"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->

                            <!--                            <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header currentBalance">
                                                                        
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div class="el-chart-w" id="cons-invs-pieChart">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="element-wrapper">
                                                                    <h6 class="element-header">
                                                                        Consolidated Investment
                                                                    </h6>
                                                                    <div class="element-box">
                                                                        <div class="el-chart-w" id="lineChart">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <%--<c:forEach items="${purchasedInvestments}" var="investment">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="element-wrapper">
                                                                        <h6 class="element-header">
                                                                            ${investment.investmentAmount}
                            <%--${investment.bankName}
                            ${investment.bankAccount}
                            ${investment.referenceNo}
                            ${investment.customerName}
                            ${investment.timeFrame}
                        </h6>
                        <div class="element-box">
                            <div class="el-chart-w" id="donutChart${investment.investmentId}">
                            </div>
                            <!--                                                <div class="el-chart-w" style="width: 150px ;margin:0 auto" >
                                                                                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                <canvas id="donutChart${investment.investmentId}"  width="150px" height="150px" style="display: block; width: 150px; height: 150px;"></canvas>
                                                                                <div class="inside-donut-chart-label">
                                                                                    <strong>$${investment.investmentAmount}</strong>
                                                                                    <span>${investment.fundName}</span>
                                                                                </div>
                                                                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="element-wrapper">
                        <h6 class="element-header">
                            ${investment.fundName}
                            <%--${investment.bankName}
                            ${investment.bankAccount}
                            ${investment.referenceNo}
                            ${investment.customerName}
                            ${investment.timeFrame}-->
                        </h6>
                        <div class="element-box">
                            <div class="el-chart-w" id="lineChart${investment.investmentId}">
                                <!--                                                    <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                    <canvas id="lineChart${investment.investmentId}" width="450px" height="150px" style="display:block"></canvas>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>--%>
                            <!--------------------
                            START - Chat Popup Box
                            -------------------->

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invsta Crypto Asset Overview
                                        </h6>
                                    </div>
                                    <div class="element-box">
                                        <!--<div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="label">Customer Name: </label>
                                                    <input type="text" class="form-control" placeholder="Customer Name">
                                                </div> 
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label">Date: </label>
                                                    <input type="date" class="form-control" placeholder="Date">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="label">&nbsp</label>
                                                    <a href="javascript:void(0)" class="btn btn-warning">Submit</a>
                                                </div>
                                            </div>
                                        </div>-->

                                        <!--                                        <div class="controls-above-table">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-6">
                                                                                            <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text" style="margin:0">
                                                                                        </div>
                                                                                        <div class="col-sm-6">
                                                                                            <form class="form-inline justify-content-sm-end">
                                                                                                <select class="form-control form-control-sm rounded bright">
                                                                                                    <option selected="selected" value="">Sort By</option>
                                                                                                    <option value="">Bitcoin</option>
                                                                                                    <option value="">Bitcoin-lite</option>
                                                                                                    <option value="">Bitcoin-pro</option>
                                                                                                </select>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->

                                        <!--                                        <div class="table-responsive">
                                                                                    <table class="table table-lightborder">
                                                                                        <thead>
                                                                                                                                                <tr>
                                                                                                                                                    <th>Investment</th>
                                                                                                                                                    <th></th>
                                                                                                                                                    <th></th>
                                                                                                                                                    <th></th>
                                                                                                                                                </tr>
                                        <%--<c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                            <c:if test="${loop.index eq 0}">
                                                <tr>
                                                    <th>Investment</th>
                                                <c:forEach items="${inv1.map}" var="coin" >
                                                <th>${coin.key}</th>
                                                </c:forEach>
                                        </tr>
                                            </c:if>
                                        </c:forEach>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${coinsByInvestments}" var="inv">
                                            <tr>
                                                <td>${inv.fundName}</td>
                                            <c:forEach var="coin" items="${inv.map}">
                                                <td>${coin.value}</td>
                                            </c:forEach>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                        <c:forEach items="${coinsByInvestments}" var="inv1" varStatus="loop">
                                            <c:if test="${loop.index eq 0}">
                                                <tr>
                                                    <th>Investment</th>
                                                <c:forEach items="${inv1.map}" var="coin" >
                                                <th>${coin.key}</th>
                                                </c:forEach>
                                        </tr>
                                            </c:if>
    </c:forEach>--%>
                                    </tfoot>
                                </table>
                            </div>-->
                                        <div class="row">
                                            <c:forEach items="${totalcoins}" var="coin" varStatus="loop">
                                                <div class="col-md-4" style="margin-bottom:20px">
                                                    <div class="circle2 centered" style="background:url(./resources/img/coin-images/${coin.coinId}2.png) center top no-repeat; background-size:cover">
                                                        <div id="text1">
                                                            <!--<img src="./resources/img/coin-images/${coin.coinId}.png" style="border-radius: 50%;width: 100px;height: 100px;"><br>-->
                                                            <h5 style="margin-top:10px;" class="color0">${coin.shareName}</h5>
                                                            <span class="color2"><b>${coin.percentage}% </b></span><b class="color1">of Portfolio</b><br>
                                                            <span class="color1" style="font-weight:500">Units: <b class="color2">${coin.quantity}</b></span><br>
                                                            <!--<span class="color1"><b>Market Price</b> ${coin.price}</span>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--                            <div class="floated-chat-btn">
                                                            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                                                        </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <jsp:include page = "modals/edit-investment-shares.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>


        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="${home}/resources/js/cryptolabs/investmentJs/SliderJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/MyPlanJs.js"></script> 
        <script src="${home}/resources/js/cryptolabs/investmentJs/CalculateMyBalanceJs.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
        <script src="${home}/resources/js/cryptolabs/user-main.js"></script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <script>
                                                            var endUser =${endUser};
                                                            var currency = '${currency}';
                                                            var currSymbol = '${currSymbol}';
                                                            var latestWeek = ${latestWeek};
        </script>
        <script src="${home}/resources/js/cryptolabs/user-charts.js"></script>
        <script>
                                                            $(document).ready(function () {
                                                                var data = ${userInvestments};
                                                                initSetDataInCharts(data);
                                                                var actualAmount = 0;
                                                                for (var i = 0; i < data.length; i++) {
                                                                    var y = eval(data[i].value);
                                                                    if (y > 0 || y < 0) {
                                                                        actualAmount += y;
                                                                    }
                                                                }
                                                                var values = [];
                                                                $.each(latestWeek, function (idx, day) {
                                                                    values.push({
                                                                        x: new Date(day.current_date),
                                                                        y: eval(day.investment_amount)
                                                                    });
                                                                });
                                                                var y = eval($("#actualInvested").val());
                                                                values.push({
                                                                    x: new Date(),
                                                                    y: actualAmount
                                                                });
                                                                lglatestweek(values);
                                                            });
        </script>
        <script>
            $(function () {
                $('#easypie').highcharts({
                    chart: {
                        margin: [0, 0, 0, 0],
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: '',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 70
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },

                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black'
                                }
                            },

                            center: ['50%', '50%']
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '80%',
                            data: [{
                                    name: '',
                                    y: 150,
                                    color: 'red' // Jane's color
                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 50,
//                                                                                    color: 'green' // Jane's color
//                                                                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 10,
//                                                                                    color: 'red' // Jane's color
//                                                                                }


                            ]
                        }]
                });
            });

            $(function () {
                $('#easypie1').highcharts({
                    chart: {
                        margin: [0, 0, 0, 0],
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: '',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 70
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black'
                                }
                            },
                            center: ['50%', '50%']
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '80%',
                            data: [{
                                    name: '',
                                    y: 150,
                                    color: 'green' // Jane's color
                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 50,
//                                                                                    color: 'green' // Jane's color
//                                                                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 10,
//                                                                                    color: 'red' // Jane's color
//                                                                                }


                            ]
                        }]
                });
            });
            $(function () {
                $('#easypie2').highcharts({
                    chart: {
                        margin: [0, 0, 0, 0],
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false
                    },
                    title: {
                        text: '',
                        align: 'center',
                        verticalAlign: 'middle',
                        y: 70
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    exporting: {
                        enabled: false
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black'
                                }
                            },
                            center: ['50%', '50%']
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '80%',
                            data: [{
                                    name: '',
                                    y: 150,
                                    color: 'yellow' // Jane's color
                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 50,
//                                                                                    color: 'green' // Jane's color
//                                                                                },
//                                                                                {
//                                                                                    name: '',
//                                                                                    y: 10,
//                                                                                    color: 'red' // Jane's color
//                                                                                }


                            ]
                        }]
                });
            });
        </script>
    </body>
</html>
