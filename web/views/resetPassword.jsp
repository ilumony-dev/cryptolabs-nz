<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Login - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="./resources/favicon.png" rel="shortcut icon">
        <link href="./resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">

        <link href="./resources/css/main.css?version=3.5.1" rel="stylesheet">
    </head>
    <body class="auth-wrapper">
        <div class="all-wrapper menu-side with-pattern">
            <div class="auth-box-w">
                <div class="logo-w">
                    <a href="./login"><img alt="" src="./resources/img/logo-big.png" style="width:40%"></a>
                </div>
                <h4 class="auth-header">
                    Enter your new Password
                </h4>
                <form action="./resetpwd" method="post" class="form-login">
                    <input type="hidden" name="userId" value="userId">
                    <input type="hidden" name="token" value="token">
                    <input type="hidden" name="csrf" value="csrf">
                        <div class="form-group">
                            <label for="newPassword">New password!</label>
                            <input type="password" class="form-control" id="newPassword" name="newPassword" data-error="Your username is invalid" required="required" placeholder="Enter your password" >
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                        </div>
                        
                        <div class="form-group">
                            <label for="rePassword">Re password!</label>
                            <input type="password" class="form-control" id="rePassword" name="rePassword" data-error="Your re-password is invalid" required="required" placeholder="Enter your password" >
                            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                        </div>
                        
                        <div class="buttons-w">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                        <div class="buttons-w">
                            <p> Don't have account <a  href='./register'>Sign Up</a></p>
                            <p> Already have account? <a  href='./login'> Login</a></p>
                        </div>
                    </form>
            </div>
        </div>
        <script src="./resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="./resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
    </body>
</html>
