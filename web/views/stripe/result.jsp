<%-- 
    Document   : result
    Created on : 28 Nov, 2017, 12:37:00 PM
    Author     : palo12
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html SYSTEM "http://www.thymeleaf.org/dtd/xhtml1-strict-thymeleaf-4.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:th='http://www.thymeleaf.org'>
    <title>Result</title>
</head>
<body>

    <c:if test="${error.length() gt 0}">
        <h3 style='color: red;'>${error}</h3>
    </c:if>
    <c:if test="${error eq null}">
        <div>
            <h3 style='color: green;'>Success!</h3>
            <div>Id.: <span th:text='${id}' /></div>
            <div>Status: <span th:text='${status}' /></div>
            <div>Charge id.: <span th:text='${chargeId}' /></div>
            <div>Balance transaction id.: <span th:text='${balance_transaction}' /></div>
        </div>
    </c:if>        
    <a href='./checkout'>Checkout again</a>
</body>
</html>