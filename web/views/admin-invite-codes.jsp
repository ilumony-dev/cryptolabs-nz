<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Dashboard</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    </head>

    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Products</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span>Laptop with retina screen</span>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Invite Codes
                                        </h6>
                                        <div class="element-box-tp">
                                            <div class="controls-above-table">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <a class="btn btn-sm btn-secondary" href="#" data-target="#addinvitecode" data-toggle="modal">Create Invite Code</a>
                                                        <!--<a class="btn btn-sm btn-secondary" href="#">Archive</a>-->
                                                        <!--<a class="btn btn-sm btn-danger" href="#">Delete</a>-->
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <form class="form-inline justify-content-sm-end">
                                                            <input class="form-control form-control-sm rounded bright" placeholder="Search" type="text">
                                                            <select class="form-control form-control-sm rounded bright">
                                                                <option selected="selected" value="">
                                                                    Select Status
                                                                </option>
                                                                <option value="available">
                                                                    Available
                                                                </option>
                                                                <option value="used">
                                                                    Used
                                                                </option>
                                                                <option value="expired">
                                                                    Expired
                                                                </option>
                                                            </select>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-lg table-v2 table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>
                                                                Invite Code
                                                            </th>
                                                            <th>
                                                                Purpose/ Expiry Date
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach items="${inviteCodes}" var="code">
                                                            <tr>
                                                                <td class="text-center">
                                                                    ${code.inviteCode}
                                                                </td>
                                                                <td class="text-center">
                                                                    ${code.purpose}/ ${code.expiryTs}
                                                                </td>
                                                                <td class="text-center">
                                                                    <c:choose>
                                                                        <c:when test="${code.emailId.length() gt 0}">
                                                                            ${code.emailId}
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <form action="./admin-allocate-email" method="post">
                                                                                <input type="hidden" name="id" value="${code.id}"/>
                                                                                <input type="hidden" name="inviteCode" value="${code.inviteCode}"/>
                                                                                <input placeholder="Enter email" type="text" name="emailId" id="emailId"/>
                                                                                <button class="btn btn-success btn-sm" type="submit">Apply & Send Email</button>
                                                                            </form>
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="controls-below-table">
                                                <div class="table-records-info">
                                                    Showing records 1 - 5
                                                </div>
                                                <div class="table-records-pages">
                                                    <ul>
                                                        <li>
                                                            <a href="#">Previous</a>
                                                        </li>
                                                        <li>
                                                            <a class="current" href="#">1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">3</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">4</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Next</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--------------------
                            START - Chat Popup Box
                            -------------------->
<!--                            <div class="floated-chat-btn">
                                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                            </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a>
                                            <a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a>
                                            <a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <!--------------------START - Sidebar------------------>
                        <jsp:include page = "admin-right-sidebar.jsp" />
                        <jsp:include page = "modals/addinvitecode.jsp" />
                        <!--------------------END - Sidebar-------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <jsp:include page = "modals/update-investment-shares.jsp" />

        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script>
        var shareOptions = '';
            <c:forEach items="${shares}" var="share" varStatus="loop">
        shareOptions = shareOptions + '<option value="${share.shareId}">${share.name}</option>';
            </c:forEach>
        function newInviteCode() {
            var url = './rest/cryptolabs/api/randomstring';
            $.ajax({
                url: url,
                type: 'GET',
                async: true,
                dataType: "json",
                success: function (data) {
                    $("#txnTitle").text(data);
                }
            });
        }
        </script>
        <script src="${home}/resources/js/cryptolabs/admin-main.js"></script>
    </body>
</html>
