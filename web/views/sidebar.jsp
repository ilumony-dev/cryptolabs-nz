  <!--------------------
            START - Sidebar
            -------------------->
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Quick Links
                </h6>
                <div class="element-box-tp">
                  <div class="el-buttons-list full-width">
                    <a class="btn btn-white btn-sm" href="#"><i class="os-icon os-icon-wallet-loaded"></i><span>Account Summary</span></a><a class="btn btn-white btn-sm" href="#"><i class="os-icon os-icon-delivery-box-2"></i><span>Sell/Shift Funds</span></a>
                  </div>
                </div>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Tiwitter Feeds 
                </h6>
                <div class="element-box-tp">
                  <div>
		<span class="first-title"> 
			@marceloag
			<span class="icon-edit new"></span>
		</span>
		<ul class="timeline">
			<li>
				<div class="avatar">
               <img src="http://www.croop.cl/UI/twitter/images/doug.jpg">
					<div class="hover">
						<div class="icon-twitter"></div>
					</div>
				</div>
				<div class="bubble-container">
					<div class="bubble">
					<div class="retweet">
						<div class="icon-retweet"></div>
					</div>
						<h3>@carlf</h3><br/> 
                                              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						<div class="over-bubble">
							<div class="icon-mail-reply action"></div>
							<div class="icon-retweet action"></div>
							<div class="icon-star"></div>
						</div>
					</div>
					
					 
				</div>
			</li>
			<li>
				<div class="avatar">
					<img src="http://www.croop.cl/UI/twitter/images/carl.jpg">
					<div class="hover">
						<div class="icon-twitter"></div>
					</div>
				</div>
				<div class="bubble-container">
					<div class="bubble">
						<h3>@carlf</h3><br/>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, iusto, maxime, ullam autem a voluptate rem quos repudiandae.
						<div class="over-bubble">
							<div class="icon-mail-reply action"></div>
							<div class="icon-retweet action"></div>
							<div class="icon-star"></div>
						</div>
					</div>
<!--					<div class="arrow"></div>-->
				</div>
			</li>
			<li>
				<div class="avatar">
					<img src="http://www.croop.cl/UI/twitter/images/russel.jpg">
					<div class="hover">
						<div class="icon-twitter"></div>
					</div>
				</div>
				<div class="bubble-container">
					<div class="bubble">
					<h3>@russel</h3><br/>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, iusto, maxime, ullam autem a voluptate rem quos repudiandae.
						<div class="over-bubble">
							<div class="icon-mail-reply action"></div>
							<div class="icon-retweet action"></div>
							<div class="icon-star"></div>
						</div>
					</div>
					 
				</div>
			</li>
			<li>
				<div class="avatar">
					<img src="http://www.croop.cl/UI/twitter/images/carl.jpg">
					<div class="hover">
						<div class="icon-twitter"></div>
					</div>
				</div>
				<div class="bubble-container">
					<div class="bubble">
						<h3>@carlf</h3><br/>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea, iusto, maxime, ullam autem a voluptate rem quos repudiandae.
						<div class="over-bubble">
							<div class="icon-mail-reply action"></div>
							<div class="icon-retweet action"></div>
							<div class="icon-star"></div>
						</div>
					</div>
					 
				</div>
			</li>
		</ul>

	</div>
                   
                </div>
              </div>
                      <div class="element-wrapper">
                <h6 class="element-header">
                  Support Agents
                </h6>
                <div class="element-box-tp">
                  <div class="profile-tile">
                    <div class="profile-tile-box">
                      <div class="pt-avatar-w">
                        <img alt="" src="${home}/resources/img/avatar1.jpg">
                      </div>
                      <div class="pt-user-name">
                        Mark Parson
                      </div>
                    </div>
                    <div class="profile-tile-meta">
                      <ul>
                        <li>
                          Last Login:<strong>Online Now</strong>
                        </li>
                        <li>
                          Tickets:<strong>12</strong>
                        </li>
                        <li>
                          Response Time:<strong>2 hours</strong>
                        </li>
                      </ul>
                      <div class="pt-btn">
                        <a class="btn btn-success btn-sm" href="#">Send Message</a>
                      </div>
                    </div>
                  </div>
                  <div class="profile-tile">
                    <div class="profile-tile-box">
                      <div class="pt-avatar-w">
                        <img alt="" src="${home}/resources/img/avatar3.jpg">
                      </div>
                      <div class="pt-user-name">
                        John Mayers
                      </div>
                    </div>
                    <div class="profile-tile-meta">
                      <ul>
                        <li>
                          Last Login:<strong>Online Now</strong>
                        </li>
                        <li>
                          Tickets:<strong>9</strong>
                        </li>
                        <li>
                          Response Time:<strong>3 hours</strong>
                        </li>
                      </ul>
                      <div class="pt-btn">
                        <a class="btn btn-secondary btn-sm" href="#">Send Message</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                      
              <div class="element-wrapper">
                <h6 class="element-header">
                  Recent Activity
                </h6>
                    <div class="row">
                   <div class="col-sm-12">
          <div class="os-progress-bar primary">
            <div class="bar-labels">
              <div class="bar-label-left">
                <span>Progress</span><span class="positive">+25%</span>
              </div>
              <div class="bar-label-right">
                <span class="info">100%</span>
              </div>
            </div>
            <div class="bar-level-1" style="width: 100%">
              <div class="bar-level-2" style="width:100%">
                <div class="bar-level-3" style="width:25%"></div>
              </div>
            </div>
          </div>
        </div>
              </div>
                <div class="element-box-tp">
                  <div class="activity-boxes-w">
                    <div class="activity-box-w">
                      <div class="activity-time">
                      +25%
                      </div>
                      <div class="activity-box">
                        <div class="activity-avatar">
                            <img alt="" src="${home}/resources/img/verification.png"> 
                        </div>
                        <div class="activity-info">
                          <div class="activity-role">
                        
                          </div>
                          <strong class="activity-title">   User Verification</strong>
                        </div>
                      </div>
                    </div>
                    <div class="activity-box-w">
                      <div class="activity-time">
                       +25%

                      </div>
                      <div class="activity-box">
                        <div class="activity-avatar">
                            <img alt="" src="${home}/resources/img/fundselection.png">
                        </div>
                        <div class="activity-info">
                          <div class="activity-role">
                        
                          </div>
                          <strong class="activity-title">  Fund Selection</strong>
                        </div>
                      </div>
                    </div>
                    <div class="activity-box-w">
                      <div class="activity-time">
                       +25%

                      </div>
                      <div class="activity-box">
                        <div class="activity-avatar">
                            <img alt="" src="${home}/resources/img/fundallocation.png">
                        </div>
                        <div class="activity-info">
<!--                          <div class="activity-role">
                           Fund Allocation
                          </div>-->
                          <strong class="activity-title"> Fund Allocation</strong>
                        </div>
                      </div>
                    </div>
                    <div class="activity-box-w">
                      <div class="activity-time">
                       +25%

                      </div>
                      <div class="activity-box">
                        <div class="activity-avatar">
                            <img alt="" src="${home}/resources/img/makingmoney.png">
                        </div>
                        <div class="activity-info">
<!--                          <div class="activity-role">
                          Making Money
                          </div>-->
                          <strong class="activity-title">  Making Money</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="element-wrapper">
                <h6 class="element-header">
                  Team Members
                </h6>
                <div class="element-box-tp">
                  <div class="input-search-w">
                    <input class="form-control rounded bright" placeholder="Search team members..." type="search">
                  </div>
                  <div class="users-list-w">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="${home}/resources/img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                      <div class="user-action">
                        <div class="os-icon os-icon-email-forward"></div>
                      </div>
                    </div>
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="${home}/resources/img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          Ben Gossman
                        </h6>
                        <div class="user-role">
                          Administrator
                        </div>
                      </div>
                      <div class="user-action">
                        <div class="os-icon os-icon-email-forward"></div>
                      </div>
                    </div>
                    <div class="user-w with-status status-red">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="${home}/resources/img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          Phil Nokorin
                        </h6>
                        <div class="user-role">
                          HR Manger
                        </div>
                      </div>
                      <div class="user-action">
                        <div class="os-icon os-icon-email-forward"></div>
                      </div>
                    </div>
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="${home}/resources/img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          Jenny Miksa
                        </h6>
                        <div class="user-role">
                          Lead Developer
                        </div>
                      </div>
                      <div class="user-action">
                        <div class="os-icon os-icon-email-forward"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--------------------
            END - Sidebar
            -------------------->