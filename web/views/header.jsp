<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!--------------------
START - Mobile Menu
-------------------->
<div class="menu-mobile menu-activated-on-click color-scheme-dark">
    <div class="mm-logo-buttons-w">
        <a class="mm-logo" href="#"><img src="./resources/img/invsta-03(200px).png"><span>Information panel</span></a>
        <div class="mm-buttons">
            <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
            </div>
            <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
        </div>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="avatar-w">
                <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    ${info.fullName}
                </div>
                <div class="logged-user-role">
                    <c:choose>
                        <c:when test="${info.admin}">
                            ADMINISTRATOR
                        </c:when>
                        <c:otherwise>
                            USER
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
        <!--------------------
        START - Mobile Menu List
        -------------------->

        <!--------------------
        END - Mobile Menu List
        -------------------->
        <div class="mobile-menu-magic">
            <h4>
                Light Admin
            </h4>
            <p>
                Clean Bootstrap 4 Template
            </p>
            <!--              <div class="btn-w">
                            <a class="btn btn-white btn-rounded" href="https://themeforest.net/item/light-admin-clean-bootstrap-dashboard-html-template/19760124?ref=Osetin" target="_blank">Purchase Now</a>
                          </div>-->
        </div>
    </div>
</div>
<!--------------------
END - Mobile Menu
-------------------->

<!--------------------
START - Menu side 
-------------------->
<div class="desktop-menu menu-side-w menu-activated-on-click">
    <div class="logo-w">
        <a class="logo" href="#">
            <img src="./resources/img/invsta-03(200px).png"> </a>
    </div>
    <div class="menu-and-user">
        <div class="logged-user-w">
            <div class="logged-user-i">
                <div class="avatar-w">
                    <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
                </div>
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        ${info.fullName}
                    </div>
                    <div class="logged-user-role">
                        <c:choose>
                            <c:when test="${info.admin}">
                                ADMINISTRATOR
                            </c:when>
                            <c:otherwise>
                                USER
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="logged-user-menu">
                    <div class="logged-user-avatar-info">
                        <div class="avatar-w">
                            <img alt="" src="https://api.adorable.io/avatars/50/${info.fullName}">
                        </div>
                        <div class="logged-user-info-w">
                            <div class="logged-user-name">
                                ${info.fullName}
                            </div>
                            <div class="logged-user-role">
                                <c:choose>
                                    <c:when test="${info.admin}">
                                        ADMINISTRATOR
                                    </c:when>
                                    <c:otherwise>
                                        USER
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <div class="bg-icon">
                        <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
                    <ul>
                        <li>
                            <a href="./prsdldfjlsjrjewli"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                        </li>
                        <li>
                            <a href="./verify-new"><i class="os-icon os-icon-user-male-circle2"></i><span>Verify Journey</span></a>
                        </li>
                        <li>
                            <a href="./coasdflkerewrweasd"><i class="icon-settings"></i><span>Configuration</span></a>
                        </li>
                        <li>
                            <a href="./login?logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="main-menu">
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>Admin Dashboard</span>
                    </a>
                </li>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')">
                <li class="sub-menu">
                    <a href="./welcome">
                        <div class="icon-w">
                            <div class="os-icon os-icon-window-content"></div>
                        </div>
                        <span>User Dashboard</span>
                    </a>
                </li>
                <li class="sub-menu">
                    <a href="./inoetreoirpewirp">
                        <div class="icon-w">
                            <div class="os-icon os-icon-hierarchy-structure-2"></div>
                        </div>
                        <span>Investment Options</span>
                    </a>
                </li>
            </sec:authorize>                
            <li class="sub-menu">
                <a href="./inksdjflsdjrjwel">
                    <div class="icon-w">
                        <div class="icon-eye"></div>
                    </div>
                    <span>Insights</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="./paksdjfljdsfudskd">
                    <div class="icon-w">
                        <div class="icon-wallet"></div>
                    </div>
                    <span>Wallet</span>
                </a>                 
            </li>
            <li class="sub-menu">
                <a href="#">
                    <div class="icon-w">
                        <div class="os-icon os-icon-newspaper"></div>
                    </div>
                    <span>Talk to us</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="./rehjkgjgtfdqwgfss">
                    <div class="icon-w">
                        <div class="icon-people"></div>
                    </div>
                    <span>Referral</span>
                </a>
            </li>
            <script>
                var HW_config = {
                    selector: "#headway-logo", // CSS selector where to inject the badge
                    account: "7zwMgJ"
                }
            </script>
            <script async src="//cdn.headwayapp.co/widget.js"></script>
            <script>
                setInterval(function () {
                    $("#HW_badge_cont").html('Invsta-Gram');
                }, 1000);
            </script>
            <li class="sub-menu">
                <a href="" >
                    <div class="icon-w">
                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                    </div>
                    <span id="headway-logo" style="margin-top: -20px; margin-left: -16px"></span>
                </a>
            </li>
            <!--            <li class="sub-menu">
                            <a href="./heasdfjsdajflsdeay">
                                <div class="icon-w">
                                    <div class="icon-people"></div>
                                </div>
                                <span>HeadWay</span>
                            </a>
                        </li>-->
        </ul>
    </div>
</div>

<!--------------------
END - Menu side 
-------------------->