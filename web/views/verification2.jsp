<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>	
        <title>Verification</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous"/>
        <link href="${home}/resources/css/verify-style.css" rel="stylesheet"  type="text/css"/>
        <link href="${home}/resources/css/verify-animate.css" rel="stylesheet" type="text/css" />
        <style>
            .showhide {
                display: none;
            }    
            .showhide2 {
                display: none;
            }  
            .showhide3 {
                display: none;
            }
            .showhide5{
                display: none;
            } 
            .showhide10{
                display: none;
            } 
            .showhide15{
                display: none;
            }
            .verification_body:before{
                content: "";
                position: absolute;
                z-index: -1;
                background-image: url(${home}/resources/img/verify-images/banner3.jpg);
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                background-size: cover;

                background-repeat: no-repeat;
            }
            .form_cont {
                max-width: 600px;
                margin: auto;
                position: absolute;
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            .app{
                width: 100%;
                position: relative;
            }

            .app #start-camera{
                display: none;
                border-radius: 3px;
                max-width: 400px;
                color: #fff;
                background-color: #448AFF;
                text-decoration: none;
                padding: 15px;
                opacity: 0.8;
                margin: 50px auto;
                text-align: center;
            }
            .btn_section.take-selff .btn_area1 {
                margin-bottom: 24px;
            }

            .app video#camera-stream{
                display: none;
                width: 100%;
            }

            .app img#snap{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                z-index: 10;
                display: none;
            }

            .app #error-message{
                width: 100%;
                background-color: #ccc;
                color: #9b9b9b;
                font-size: 28px;
                padding: 200px 100px;
                text-align: center;
                display: none;
            }

            .app .controls{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                z-index: 20;

                display: flex;
                align-items: flex-end;
                justify-content: space-between;
                padding: 30px;
                display: none;
            }

            .app .controls a{
                border-radius: 50%;
                color: #fff;
                background-color: #111;
                text-decoration: none;
                padding: 15px;
                line-height: 0;
                opacity: 0.7;
                outline: none;
                -webkit-tap-highlight-color: transparent;
            }

            .app .controls a:hover{
                opacity: 1;
            }

            .app .controls a.disabled{
                background-color: #555;
                opacity: 0.5;
                cursor: default;
                pointer-events: none;
            }

            .app .controls a.disabled:hover{
                opacity: 0.5;
            }

            .app .controls a i{
                font-size: 18px;
            }

            .app .controls #take-photo i{
                font-size: 32px;
            }

            .app canvas{
                display: none;
            }

            .app video#camera-stream.visible,
            .app img#snap.visible,
            .app #error-message.visible
            {
                display: block;
            }

            .app .controls.visible{
                display: flex;
            }

            @media(max-width: 1000px){
                .app-container{
                    margin: 40px;
                }

                .app #start-camera.visible{
                    display: block;
                }

                .app .controls a i{
                    font-size: 16px;
                }

                .app .controls #take-photo i{
                    font-size: 24px;
                }
            }

            @media(max-width: 600px){
                .app-container{
                    margin: 10px;
                }

                .app #error-message{
                    padding: 80px 50px;
                    font-size: 18px;
                }

                .app .controls a i{
                    font-size: 12px;
                }

                .app .controls #take-photo i{
                    font-size: 18px;
                }
            }
        </style>
    </head>
    <body class="verification_body">
        <!-- MultiStep Form -->
        <div class="container">
            <div class="form_cont">
                <form id="msform" action="" method="post" autocomplete="off" >

                    <fieldset class="animated bounceInLeft showhide1 ">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">INVSTA SELF VERIFICATION PORTAL</h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container form_text">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <a id="go" class=" action-button" href="#">START ID CHECK</a>
                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide3" id="show8">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">INVSTA SELF VERIFICATION PORTAL</h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container form_text">
                            <div class="form-style-6">
                                <div class="Hi_Name">
                                    <p>Hi,Name</p>
                                    <p>Can you please confirm the following are correct?</p>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-label">
                                        <i class="fas fa-envelope"></i>
                                    </span>
                                    <input type="text" name="fullName" placeholder="Your Name" value="${user.fullName}" />
                                </div>
                                <div class="input-group">
                                    <span class="input-group-label">
                                        <i class="fas fa-birthday-cake"></i>
                                    </span>
                                    <input type="text" name="dob" class="textbox-n" onfocus="(this.type = 'date')" id="date" placeholder="Date of Birth"/>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-label">
                                        <i class="fas fa-map-marker-alt"></i>
                                    </span>
                                    <textarea name="address" placeholder="Address"></textarea>
                                </div>
                                <a id="go8" class=" action-button-Next" href="#">Next</a>

                            </div>

                        </div>

                    </fieldset>

                    <fieldset class="animated bounceInLeft showhide" id="show">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">
                            <div class="where_reside">
                                <h2>WHERE DO YOU RESIDE?</h2>
                            </div>
                        </div>
                        <div class="btn_section">
                            <a id="go1" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/Flag_of_New_Zealand.png">
                                    </div>
                                    <span class="btn_area1_span">New Zealand</span>
                                </div>
                            </a>
                            <a id="go5" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/2-2-globe-free-download-png.png">
                                    </div>
                                    <span class="btn_area1_span">Rest of the world</span>
                                </div>
                            </a>
                        </div>
                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide2 ">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">
                            <div class="where_reside">
                                <h2>WHAT NEW ZEALAND FROM OF ID WOULD YOU LIKE TO USE FOR IDENTIFICATION ?</h2>
                            </div>
                        </div>
                        <div class="btn_section">
                            <a id="go2" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/passport.png">
                                    </div>
                                    <span class="btn_area1_span">Nz pasport </span>
                                </div>
                            </a>

                            <a id="go3" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/linces.png">
                                    </div>
                                    <span class="btn_area1_span">Nz driving 
                                        licence</span>
                                </div>
                            </a>
                            <a id="go4" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/passport.png">
                                    </div>
                                    <span class="btn_area1_span">NO NZ ID? </span>
                                </div>
                            </a>
                        </div>

                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide5 ">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">
                            <div class="where_reside">
                                <h2>WHAT  FROM OF ID WOULD YOU LIKE TO USE FOR IDENTIFICATION ?</h2>
                            </div>
                        </div>
                        <div class="btn_section">
                            <a id="go21" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/passport.png">
                                    </div>
                                    <span class="btn_area1_span"> pasport </span>
                                </div>
                            </a>

                            <a id="go31" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/linces.png">
                                    </div>
                                    <span class="btn_area1_span"> driving 
                                        licence</span>
                                </div>
                            </a>
                            <a id="go41" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/passport.png">
                                    </div>
                                    <span class="btn_area1_span">NO  ID? </span>
                                </div>
                            </a>
                        </div>
                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide" id="show2">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">
                            <div class="where_reside">
                                <h2>OK, I SEE YOU WANT TO USE YOUR PASSPORT! WHICH METHOD WOULD YOU LIKE TO USE?</h2>

                            </div>
                        </div>
                        <div class="btn_section take-selff">
                            <a id="go6" href="#">
                                <div class="btn_area1">
                                    <div class="btn_area1_img">
                                        <img src="${home}/resources/img/verify-images/photo-camera-7.png">
                                    </div>
                                    <!--<span class="btn_area1_span">TAKE A SELFIE! </span>-->
                                    <a href="#" id="start-camera" class="btn_area1_span visible">TAKE A SELFIE!</a>
                                </div>
                            </a>
                            <div class="app-container">
                                <div class="app">
                                    <video id="camera-stream"></video>
                                    <img id="snap" name="selfieImage"></img>
                                    <p id="error-message"></p>
                                    <div class="controls">
                                        <a href="#" id="delete-photo" title="Delete Photo" class="disabled"><i class="fas fa-trash-alt"></i></a>
                                        <a href="#" id="take-photo" title="Take Photo"><i class="fas fa-camera"></i></a>
                                        <a href="#" id="download-photo" download="selfie.png" title="Save Photo" class="disabled"><i class="fas fa-download"></i></a>  
                                    </div>
                                    <canvas></canvas>
                                </div>
                            </div>
                        </div>
                    </fieldset>     
                    <fieldset class="animated bounceInLeft showhide10" id="show10">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">
                            <div class="where_reside">
                                <h2>Thanks for the photoshot.We just need a little more information from you?</h2>
                            </div>
                        </div>
                        <div class="form-style-3">

                            <div class="form-style-3_form">
                                <label for="field1"><span>Your are Tax Resident for Which Country? </span><input type="text" class="input-field" name="field1" value="" /></label>
                                <label for="field2"><span>You TAX ID is </span><input type="email" class="input-field" name="field2" value="" /></label>

                                <label for="field4"><span>Source of Fund?</span>

                                    <select id="field4"  name="field4" class="select-field">
                                        <option value="Appointment">Appointment</option>
                                        <option value="Interview">Interview</option>
                                        <option value="Regarding a post">Regarding a post</option>
                                        <option id="other" value="Other">Other</option>


                                    </select>
                                </label>
                                <div id="text" class="animated zoomIn">
                                    <label for="field2"><span>Other </span><input type="email" class="input-field" /></label>

                                </div>



                            </div>
                            <div class="btn_section12">
                                <a id="go15" href="#">
                                    <div class="btn_area1">

                                        <div class="btn_area1_span">Submit </div>
                                    </div>
                                </a>

                            </div>




                        </div>
                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide15" id="show15">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text ">
                            <img class="right_image" src="${home}/resources/img/verify-images/check-clipart-animated-gif-1.gif">
                                <div class="where_reside">
                                    <h2>Thanks for giving us all the infomation 
                                        we will be in touch with you soon.<br>
                                            Thanks</h2>

                                </div>
                        </div>

                    </fieldset>
                    <fieldset class="animated bounceInLeft showhide" id="show4">
                        <div class="header_form_s in_form_container ">
                            <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                            <img src="${home}/resources/img/verify-images/header-dp.png">
                        </div>
                        <div class="in_form_container in_form2 form_text">

                            <div class="where_reside">
                                <h2>Thanks for letting us know.Please contact us at info@invsta.coom to get verified manually </h2>
                            </div>
                        </div>
                        <div class="btn_section">
                            <a id="go15" href="#">
                                <div class="btn_area1">

                                    <span class="btn_area1_span">CLOSE </span>
                                </div>
                            </a>
                        </div>
                    </fieldset>
                    <!--              <fieldset class="animated bounceInLeft showhide" id="show3">
                                    <div class="header_form_s in_form_container ">
                                       <h2 class="fs-title1">VERIFY YOUR IDENTITY </h2>
                                       <img src="${home}/resources/img/verify-images/header-dp.png">
                                    </div>
                                          <div class="in_form_container in_form2 form_text">
                                       <div class="where_reside">
                                          <h2>UPLOAD THE FOLLOWING DOCUMENTATIONS </h2>
                                       </div>
                                    </div>
                                                        <div class="btn_section">
                                       <a id="go6" href="#">
                                          <div class="btn_area1">
                                             <div class="btn_area1_img">
                                                <img src="${home}/resources/img/verify-images/uploadli.png">
                                             </div>
                                             <span class="btn_area1_span">upload scanned driving licence (front)<span>
                                          </div>
                                       </a>
                                       <a id="go7" href="#">
                                          <div class="btn_area1">
                                             <div class="btn_area1_img">
                                                <img src="${home}/resources/img/verify-images/uploadli.png">
                                             </div>
                                             <span class="btn_area1_span">upload scanned driving licence (back)<span>
                                          </div>
                                       </a>
                                    </div>
                                 </fieldset> -->
                </form>
                <!-- link to designify.me code snippets --> 
                <!--<div class="dme_link">
                   <p><a href="http://designify.me/code-snippets-js/" target="_blank">More Code Snippets</a></p>
                   </div>--> 
                <!-- /.link to designify.me code snippets --> 
            </div>

        </div>
        <!-- /.MultiStep Form -->
    </body>
    <script src="http://invsta-dev.ap-southeast-2.elasticbeanstalk.com/resources/js/validation.js"></script>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script>
                                        $(function () {
                                            $('#go').click(function () {
                                                $('#show8').css("display", "block");

                                            });
                                            $('#go').click(function () {
                                                $('.showhide1').css("display", "none");

                                            });
                                            $('#go1').click(function () {
                                                $('#show').css("display", "none");

                                            });
                                            $('#go1').click(function () {
                                                $('.showhide2').css("display", "block");

                                            });
                                            $('#go5').click(function () {
                                                $('#show').css("display", "none");

                                            });
                                            $('#go5').click(function () {
                                                $('.showhide5').css("display", "block");

                                            });
                                            $('#go2').click(function () {
                                                $('#show2').css("display", "block");

                                            });
                                            $('#go2').click(function () {
                                                $('.showhide2').css("display", "none");

                                            });
                                            $('#go3').click(function () {
                                                $('#show2').css("display", "block");

                                            });
                                            $('#go3').click(function () {
                                                $('.showhide2').css("display", "none");

                                            });
                                            $('#go4').click(function () {
                                                $('#show4').css("display", "block");

                                            });
                                            $('#go4').click(function () {
                                                $('.showhide2').css("display", "none");

                                            });
                                            $('#go2').click(function () {
                                                $('#show2').css("display", "block");

                                            });
                                            $('#go21').click(function () {
                                                $('.showhide5').css("display", "none");

                                            });
                                            $('#go31').click(function () {
                                                $('#show2').css("display", "block");

                                            });
                                            $('#go31').click(function () {
                                                $('.showhide5').css("display", "none");

                                            });
                                            $('#go41').click(function () {
                                                $('#show4').css("display", "block");

                                            });
                                            $('#go41').click(function () {
                                                $('.showhide5').css("display", "none");

                                            });
                                            $('#go8').click(function () {
                                                $('#show').css("display", "block");

                                            });
                                            $('#go8').click(function () {
                                                $('.showhide3').css("display", "none");

                                            });
                                            $('#go6').click(function () {
                                                $('#show10').css("display", "block");

                                            });
                                            $('#go6').click(function () {
                                                $('.showhide').css("display", "none");

                                            });

                                            $('#go15').click(function () {
                                                $('#show15').css("display", "block");

                                            });
                                            $('#go15').click(function () {
                                                $('.showhide10').css("display", "none");

                                            });
                                        });
                                        // References to all the element we will need.
                                        var video = document.querySelector('#camera-stream'),
                                                image = document.querySelector('#snap'),
                                                start_camera = document.querySelector('#start-camera'),
                                                controls = document.querySelector('.controls'),
                                                take_photo_btn = document.querySelector('#take-photo'),
                                                delete_photo_btn = document.querySelector('#delete-photo'),
                                                download_photo_btn = document.querySelector('#download-photo'),
                                                error_message = document.querySelector('#error-message');

                                        $(document).ready(function () {
                                            $("#text").hide();
                                            $("#field4").change(function () {
                                                var field4 = $("#field4").val();
                                                //alert( "Handler for .change() called." + field4);
                                                if (field4 === 'Other') {
                                                    $("#text").show();
                                                }
                                            });
                                            $("#other").click(function () {
                                                $("#text").show();
                                            });
                                            
                                            // The getUserMedia interface is used for handling camera input.
                                            // Some browsers need a prefix so here we're covering all the options
                                            navigator.getMedia = (navigator.getUserMedia ||
                                                    navigator.webkitGetUserMedia ||
                                                    navigator.mozGetUserMedia ||
                                                    navigator.msGetUserMedia);
                                            if (!navigator.getMedia) {
                                                displayErrorMessage("Your browser doesn't have support for the navigator.getUserMedia interface.");
                                            } else {
                                                // Request the camera.
                                                navigator.getMedia(
                                                        {
                                                            video: true
                                                        },
                                                        // Success Callback
                                                                function (stream) {
                                                                    // Create an object URL for the video stream and
                                                                    // set it as src of our HTLM video element.
                                                                    video.src = window.URL.createObjectURL(stream);
                                                                    // Play the video element to start the stream.
                                                                    video.play();
                                                                    video.onplay = function () {
                                                                        showVideo();
                                                                    };
                                                                },
                                                                // Error Callback
                                                                        function (err) {
                                                                            displayErrorMessage("There was an error with accessing the camera stream: " + err.name, err);
                                                                        }
                                                                );
                                                            }
                                                    // Mobile browsers cannot play video without user input,
                                                    // so here we're using a button to start it manually.
                                                    start_camera.addEventListener("click", function (e) {
                                                        e.preventDefault();
                                                        // Start video playback manually.
                                                        video.play();
                                                        showVideo();
                                                    });


                                                    take_photo_btn.addEventListener("click", function (e) {
                                                        e.preventDefault();
                                                        var snap = takeSnapshot();  // Show image. 
                                                        image.setAttribute('src', snap);
                                                        image.classList.add("visible");
                                                        // Enable delete and save buttons
                                                        delete_photo_btn.classList.remove("disabled");
                                                        download_photo_btn.classList.remove("disabled");
                                                        // Set the href attribute of the download button to the snap url.
                                                        download_photo_btn.href = snap;
                                                        // Pause video playback of stream.
                                                        video.pause();
                                                    });


                                                    delete_photo_btn.addEventListener("click", function (e) {

                                                        e.preventDefault();

                                                        // Hide image.
                                                        image.setAttribute('src', "");
                                                        image.classList.remove("visible");

                                                        // Disable delete and save buttons
                                                        delete_photo_btn.classList.add("disabled");
                                                        download_photo_btn.classList.add("disabled");

                                                        // Resume playback of stream.
                                                        video.play();
                                                    });

                                                });

                                        showVideo = function () {
                                            // Display the video stream and the controls.
                                            hideUI();
                                            video.classList.add("visible");
                                            controls.classList.add("visible");
                                        };


                                        takeSnapshot = function () {
                                            // Here we're using a trick that involves a hidden canvas element.  

                                            var hidden_canvas = document.querySelector('canvas'),
                                                    context = hidden_canvas.getContext('2d');

                                            var width = video.videoWidth, height = video.videoHeight;

                                            if (width && height) {
                                                // Setup a canvas with the same dimensions as the video.
                                                hidden_canvas.width = width;
                                                hidden_canvas.height = height;
                                                // Make a copy of the current frame in the video on the canvas.
                                                context.drawImage(video, 0, 0, width, height);
                                                // Turn the canvas image into a dataURL that can be used as a src for our photo.
                                                return hidden_canvas.toDataURL('image/png');
                                            }
                                        };

                                        displayErrorMessage = function(error_msg, error) {
                                            error = error || "";
                                            if (error) {
                                                console.log(error);
                                            }
                                            error_message.innerText = error_msg;
                                            hideUI();
                                            error_message.classList.add("visible");
                                        };

                                        hideUI = function () {
                                            controls.classList.remove("visible");
                                            start_camera.classList.remove("visible");
                                            video.classList.remove("visible");
                                            snap.classList.remove("visible");
                                            error_message.classList.remove("visible");
                                        };
    </script>
</html>