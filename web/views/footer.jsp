<%-- 
    Document   : footer
    Created on : Oct 7, 2017, 12:24:10 PM
    Author     : maninderjit
--%>
<script>
    window.intercomSettings = {
        app_id: "q28x66d9",
        name: '${user.fullName}', // Full name
        email: '${user.username}', // Email address
        created_at: ${user.createdTime} // Signup date as a Unix timestamp
    };
</script>
<script>(
            function () {
                var w = window;
                var ic = w.Intercom;
                if (typeof ic === "function") {
                    ic('reattach_activator');
                    ic('update', intercomSettings);
                } else {
                    var d = document;
                    var i = function () {
                        i.c(arguments)
                    };
                    i.q = [];
                    i.c = function (args) {
                        i.q.push(args)
                    };
                    w.Intercom = i;
                    function l() {
                        var s = d.createElement('script');
                        s.type = 'text/javascript';
                        s.async = true;
                        s.src = 'https://widget.intercom.io/widget/q28x66d9';
                        var x = d.getElementsByTagName('script')[0];
                        x.parentNode.insertBefore(s, x);
                    }
                    if (w.attachEvent) {
                        w.attachEvent('onload', l);
                    } else {
                        w.addEventListener('load', l, false);
                    }
                }
            })()
</script>