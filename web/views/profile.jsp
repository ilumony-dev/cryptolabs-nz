<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Profile - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welocome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Profile</a>
                        </li>
                        <!--                        <li class="breadcrumb-item">
                                                    <span>Laptop with retina screen</span>
                                                </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">   
                            <div class="row market">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">
                                            Your Personal Information
                                        </h6>
                                        <div class="element-box">
                                            <form>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label> FIRST NAME</label>
                                                            <input class="form-control" placeholder="First Name" name="firstName" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label  >LAST NAME</label>
                                                            <input class="form-control" placeholder="LAST NAME" name="lastName" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label  >PHONE NUMBER  </label>
                                                            <input class="form-control" placeholder="PHONE NUMBER" name="lastName" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label  > EMAIL ADDRESS</label>
                                                            <input class="form-control" placeholder="EMAIL ADDRESS" name="email" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>DATE OF BIRTH</label>
                                                            <div class="date-input">
                                                                <input class="single-daterange form-control" placeholder="Date of birth"  name="dob" type="text" value="04/12/1978">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label >GENDER</label>    </br>           
                                                            <div class="form-check col-sm-6">
                                                                <label class="form-check-label   ">
                                                                    <input checked="" class="form-check-input" name="gender" type="radio" value="option1">Male
                                                                </label>
                                                            </div>  
                                                            <div class="form-check col-sm-6" style="display:inline">
                                                                <label class="form-check-label ">
                                                                    <input checked="" class="form-check-input" name="gender" type="radio" value="option1">Female
                                                                </label>
                                                            </div>  
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label> COUNTRY OF CITIZENSHIP</label>
                                                            <select class="form-control" id="countries" onchange="populateStates(this.value);">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>STATE  </label>
                                                            <select class="form-control" id="states">
                                                                <option>
                                                                    Select State
                                                                </option>
                                                                <option>
                                                                    Auckland
                                                                </option>
                                                                <option>
                                                                    Wellington
                                                                </option>
                                                                <option>
                                                                    Christchurch
                                                                </option>
                                                                <option>
                                                                    Hamilton
                                                                </option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="form-group">
                                                            <label > HOME ADDRESS</label>
                                                            <input class="form-control" placeholder="HOME ADDRESS" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <label> ZIPCODE </label>
                                                            <input class="form-control" placeholder="ZIPCODE" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                                <div class="form-check">
                                                                                                    <label class="form-check-label">
                                                                                                        <input class="form-check-input" type="checkbox">
                                                                                                        I want to receive Travelo promotional offers in the future.
                                                                                                    </label>
                                                                                                </div>-->
                                                <div class="form-buttons-w">
                                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>            
                            </div>
                            <div class="element-wrapper">
                                <h6 class="element-header">
                                    Account Progress
                                </h6>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="os-progress-bar primary">
                                            <div class="bar-labels">
                                                <div class="bar-label-left">
                                                    <span>Progress</span><span class="positive">+${progress}%</span>
                                                </div>
                                                <div class="bar-label-right">
                                                    <span class="info">100%</span>
                                                </div>
                                            </div>
                                            <div class="bar-level-1" style="width: 100%">
                                                <div class="bar-level-2" style="width:100%">
                                                    <div class="bar-level-3" style="width:${progress}%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="element-box-tp">
                                    <div class="activity-boxes-w">
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/verification.png"> 
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty verification.pp_passport_number}">
                                                            <img alt="" src="./resources/img/pp.png"  style="width:30%">
                                                        </c:if>
                                                        <c:if test="${not empty verification.pa_number}">
                                                            <img alt="" src="./resources/img/pa.png"  style="width:30%"> 
                                                        </c:if>
                                                        <c:if test="${not empty verification.address}">
                                                            <img alt="" src="./resources/img/pa.png"  style="width:30%"> 
                                                        </c:if>
                                                        <c:if test="${not empty verification.dl_license_number}">
                                                            <img alt="" src="./resources/img/dl.png"  style="width:30%"> 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">   User Verification</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/fundselection.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userFunds}">
                                                            <img alt="" src="./resources/img/portfolio13.jpg"  style="width:30%">+${userFunds.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">  Fund Selection</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/fundselection.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userInvestments}">
                                                            <img alt="" src="./resources/img/portfolio14.jpg"  style="width:30%">+${userInvestments.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title"> Fund Allocation</strong>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="activity-box-w">
                                            <div class="activity-time active">
                                                +25%
                                            </div>
                                            <div class="activity-box">
                                                <div class="activity-avatar">
                                                    <img alt="" src="./resources/img/makingmoney.png">
                                                </div>
                                                <div class="activity-info">
                                                    <div class="activity-role">
                                                        <c:if test="${not empty userInvestments}">
                                                            <img alt="" src="./resources/img/ilumony-bg5.png"  style="width:30%">+${totalcoins.size()} 
                                                        </c:if>
                                                    </div>
                                                    <strong class="activity-title">  Making Money</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            START - Chat Popup Box
                            -------------------->
<!--                            <div class="floated-chat-btn">
                                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                            </div>-->
                            <div class="floated-chat-w">
                                <div class="floated-chat-i">
                                    <div class="chat-close">
                                        <i class="os-icon os-icon-close"></i>
                                    </div>
                                    <div class="chat-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="${home}/resources/img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    John Mayers
                                                </h6>
                                                <div class="user-role">
                                                    Account Manager
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-messages">
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, how can I help you?
                                            </div>
                                        </div>
                                        <div class="date-break">
                                            Mon 10:20am
                                        </div>
                                        <div class="message">
                                            <div class="message-content">
                                                Hi, my name is Mike, I will be happy to assist you
                                            </div>
                                        </div>
                                        <div class="message self">
                                            <div class="message-content">
                                                Hi, I tried ordering this product and it keeps showing me error code.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat-controls">
                                        <input class="message-input" placeholder="Type your message here..." type="text">
                                        <div class="chat-extra">
                                            <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--------------------
                            END - Chat Popup Box
                            -------------------->
                        </div>
                        <jsp:include page="user-right-sidebar.jsp"/>
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/bower_components/moment/moment.js"></script>
        <script src="${home}/resources/bower_components/chart.js/dist/Chart.min.js"></script>
        <script src="${home}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
        <script src="${home}/resources/bower_components/ckeditor/ckeditor.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-validator/dist/validator.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="${home}/resources/bower_components/dropzone/dist/dropzone.js"></script>
        <script src="${home}/resources/bower_components/editable-table/mindmup-editabletable.js"></script>
        <script src="${home}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="${home}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="${home}/resources/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="${home}/resources/bower_components/tether/dist/js/tether.min.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/util.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/alert.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/button.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/carousel.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/collapse.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/dropdown.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/modal.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tab.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/tooltip.js"></script>
        <script src="${home}/resources/bower_components/bootstrap/js/dist/popover.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>    


        <script type="text/javascript">
                                                                var countries = new Array();
                                                                $(document).ready(function () {
                                                                    $.ajax({
                                                                        type: "GET",
                                                                        dataType: "json",
                                                                        url: "${home}/rest/cryptolabs/api/countries",
                                                                        success: function (data) {
                                                                            countries = data;
                                                                            console.log("response:" + data);
                                                                            var val = '<option>Select Country</option>';
                                                                            $.each(countries, function (index, obj) {
                                                                                val = val + '<option value=' + obj.code + '>' + obj.name + '</option>';
                                                                            });
                                                                            $("#countries").html(val);
                                                                        },
                                                                        error: function (jqXHR, textStatus, errorThrown) {
                                                                            console.log(' Error in processing! ' + textStatus);
                                                                        }
                                                                    });

                                                                });

                                                                function populateStates(code) {
                                                                    $("#states").html('');
                                                                    var val = '<option>Select State</option>';
                                                                    $.each(countries, function (index, c) {
                                                                        if (c.code === code) {
                                                                            $.each(c.details, function (index, s) {
                                                                                val = val + '<option value=' + s.detId + '>' + s.detName + '</option>';
                                                                            });
                                                                        }
                                                                    });
                                                                    $("#states").html(val);
                                                                }
        </script>

        <script>

            $(".bitcoin").hide();
            $(".etherium").hide();
            $(".ripple").hide();
            $(".bitcoincash").hide();
            $(".litecoin").hide();
            $("#market").click(function () {
                $(".market").show();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoin").click(function () {
                $(".market").hide();
                $(".bitcoin").show();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#etherium").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").show();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#ripple").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").show();
                $(".bitcoincash").hide();
                $(".litecoin").hide();
            });
            $("#bitcoincash").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").show();
                $(".litecoin").hide();
            });
            $("#litecoin").click(function () {
                $(".market").hide();
                $(".bitcoin").hide();
                $(".etherium").hide();
                $(".ripple").hide();
                $(".bitcoincash").hide();
                $(".litecoin").show();
            });

        </script>
    </body>
</html>
