<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>Referral - CryptoLabs</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Tamerlan Soziev" name="author">
        <meta content="Admin dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
        <!--<link href="https://netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">-->
        <!--<script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>-->
        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script>
//            var firstName = '${fullName}';
//            var email = '${email}';
//            setInterval(function () {
//                var iframe = document.getElementsByTagName("iframe")[0];
//                alert('iframe-->' + iframe);
//              x.document.getElementsByTagName("body")[0].style.backgroundColor = "blue";
//            }, 1000);
//            campaign.identify({
//                firstname: "${fullName}",
//                email: "${email}"
//            }, function (err, details) {
//                campaign.widgets.load();
//            });
        </script>
    </head>
    <body>
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Referral</a>
                        </li>
                        <!--            <li class="breadcrumb-item">
                                      <span>Laptop with retina screen</span>
                                    </li>-->
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>
                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="element-box">
                                        <div data-vl-widget="embedForm"></div>
                                        <div data-vl-widget="milestoneWidget"></div>
                                        <div data-vl-widget="referralCountWidget"></div>
                                        <script>
                                            !function () {
                                                var a = window.VL = window.VL || {};
                                                return a.instances = a.instances || {}, a.invoked ? void(window.console && console.error && console.error("VL snippet loaded twice.")) : (a.invoked = !0, void(a.load = function (b, c, d) {
                                                    var e = {};
                                                    e.publicToken = b, e.config = c || {};
                                                    var f = document.createElement("script");
                                                    f.type = "text/javascript", f.id = "vrlps-js", f.defer = !0, f.src = "https://app.viral-loops.com/client/vl/vl.min.js";
                                                    var g = document.getElementsByTagName("script")[0];
                                                    return g.parentNode.insertBefore(f, g), f.onload = function () {
                                                        a.setup(e), a.instances[b] = e
                                                    }, e.identify = e.identify || function (a, b) {
                                                        e.afterLoad = {identify: {userData: a, cb: b}}
                                                    }, e.pendingEvents = [], e.track = e.track || function (a, b) {
                                                        e.pendingEvents.push({event: a, cb: b})
                                                    }, e.pendingHooks = [], e.addHook = e.addHook || function (a, b) {
                                                        e.pendingHooks.push({name: a, cb: b})
                                                    }, e.$ = e.$ || function (a) {
                                                        e.pendingHooks.push({name: "ready", cb: a})
                                                    }, e
                                                }))
                                            }();
                                            var campaign = VL.load("qsUUK4cUR0BNRWEHa8aMbrtbSeg", {autoLoadWidgets: !0});
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <jsp:include page = "user-right-sidebar.jsp" />
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>
                </div>
            </div>
            <div class="display-type"></div>
        </div>
    </body>

</html>
