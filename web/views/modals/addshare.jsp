
<%-- 
    Document   : addshare
    Created on : Oct 15, 2017, 9:49:26 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="addshare" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addshare-title">
                    Create New Share
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./admin-add-share" method="post" id="addShareForm">
                <div class="modal-body">
                    <input type="hidden" id="addshare-shareId" name="shareId"/>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Share Name</label>
                                <input class="form-control" placeholder="Enter Share Name" type="text" name="name" id="addshare-name"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input class="form-control" placeholder="Enter Description" type="text" name="description" id="addshare-description"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="exchangeCode">Exchange Code</label>
                                <input class="form-control" placeholder="Enter Exchange Code" type="text" name="exchangeCode" id="addshare-exchangeCode"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="custodianId">Custodian Id</label>
                                <input class="form-control" placeholder="Enter Custodian Id" type="text" name="custodianId" id="addshare-custodianId"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button" ondblclick="addShareSubmit();">Proceed</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
