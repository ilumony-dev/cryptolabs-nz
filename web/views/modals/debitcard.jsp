<%-- 
    Document   : debitcard
    Created on : Oct 15, 2017, 10:12:33 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="debitcard" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ilumony Crypto Debit Card
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12 text-center">  

                        <img src="${home}/resources/img/card.png" style="width:80%"/>


                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                <button class="btn btn-warning" data-target="#linkdebitcard" data-toggle="modal"   type="button">Link your Debit Card</button>
                <button class="btn btn-primary" data-target="#requestcard" data-toggle="modal"  type="button">Request Card</button>
            </div>

        </div>
    </div>
</div>