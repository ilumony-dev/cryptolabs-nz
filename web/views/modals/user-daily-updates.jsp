<%-- 
    Document   : addfund
    Created on : Oct 15, 2017, 9:50:50 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="user-daily-updates" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Individual User Charts
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="user-daily-updates-username">Username</label>
                            <input class="form-control" placeholder="Enter Username" type="text" name="username" id="user-daily-updates-username"/>
                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-top: 27px">
                        <div class="form-group" style="float: right">
                            <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                            <a class="btn btn-primary" href="#" onclick="userDailyUpdates();">Submit</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="element-wrapper">
                            <h6 class="element-header">
                                Overview
                            </h6>
                            <div class="element-box">
                                <div class="os-tabs-w">
                                    <div class="os-tabs-controls" style="padding: 0 20px">
                                        <div class="os-tabs-controls">
                                            <ul class="nav nav-tabs smaller">
                                                <li class="nav-item">
                                                    <a class="tab_lg_bitcoin nav-link active" data-toggle="tab" href="#tab_lg_bitcoin">Bitcoin</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="tab_lg_minto nav-link" data-toggle="tab" href="#tab_lg_minto">Minto</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="tab_lg_usd nav-link" data-toggle="tab" href="#tab_lg_usd">US$</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="tab_lg_nzd nav-link" data-toggle="tab" href="#tab_lg_nzd">NZ$</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="tab_lg_minto_nav nav-link" data-toggle="tab" href="#tab_lg_minto_nav">Minto NAV</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Options</button>
                                            <ul class="dropdown-menu" style="top:56%; position: absolute">
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lg_bitcoin" onclick="removeActive('tab_lg_bitcoin')">Bitcoin</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lg_minto" onclick="removeActive('tab_lg_minto')">Minto</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lg_usd" onclick="removeActive('tab_lg_usd')">US$</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lg_nzd" onclick="removeActive('tab_lg_nzd')">NZ$</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tab_lg_minto_nav" onclick="removeActive('tab_lg_minto_nav')">Minto NAV</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane menu-chart active" id="tab_lg_bitcoin">
                                            <div class="el-chart-w"  id="linegraphBitcoin"></div>
                                        </div>
                                        <div class="tab-pane menu-chart" id="tab_lg_minto">
                                            <div class="el-chart-w"  id="linegraphMinto"></div>
                                        </div>
                                        <div class="tab-pane menu-chart" id="tab_lg_usd">
                                            <div class="el-chart-w"  id="linegraphUSD"></div>
                                        </div>
                                        <div class="tab-pane menu-chart" id="tab_lg_nzd">
                                            <div class="el-chart-w"  id="linegraphNZD"></div>
                                        </div>
                                        <div class="tab-pane menu-chart" id="tab_lg_minto_nav">
                                            <div class="el-chart-w"  id="linegraphMintoNav"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <!--                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Create New Fund</button>-->
            </div>
        </div>
    </div>
</div>