<%-- 
    Document   : updateinvestmentshares
    Created on : Oct 15, 2017, 9:47:52 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade" id="updateinvestmentshares" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <form method="post" action="./" id="updateSharesForm">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title " id="txnTitle">
                        Purchase Shares/ Coins
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Customer Id
                                                    </th>
                                                    <th>
                                                        Customer Name
                                                    </th>
                                                    <th>
                                                        Fund Name
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="refId">

                                                    </td>
                                                    <td class="customerName">

                                                    </td>
                                                    <td class="fundName">

                                                    </td>
                                                    <td class="investmentAmount">

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Local Currency
                                                    </th>
                                                    <th>
                                                        US Dollars
                                                    </th>
                                                    <th>
                                                        Bitcoin Currency
                                                    </th>
                                                    <th>
                                                        Minto Units
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" class="form-control investmentAmount" name="local" id="updateinvestmentshares-local"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="usd"  id="updateinvestmentshares-usd"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="btc" id="updateinvestmentshares-btc" onkeyup="b2m(10000);"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" name="minto" id="updateinvestmentshares-minto"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-lightborder">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Share/Coin
                                                    </th>
                                                    <th>
                                                        Amount
                                                    </th>
                                                    <th>
                                                        Price
                                                    </th>
                                                    <th>
                                                        Quantity 
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody id="pendingShareTable">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary" type="submit">Proceed</button>
                    </div>
                </div>

            </div>
        </form>    
    </div>
</div>