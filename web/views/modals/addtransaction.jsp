<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="home" value="${pageContext.request.contextPath}"/>
<%-- 
    Document   : addtransaction
    Created on : 1 Nov, 2017, 4:08:34 PM
    Author     : palo12
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="addtransaction" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Investment Transactions
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./" method="post">
                <div class="modal-body">
                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-sm-6">
                            <label for="description">Transaction Type</label>
                            <select class="form-control" name="actionType" id="addtransaction-actionType">
<!--                                <option value="DEPOSITE">
                                    DEPOSITE
                                </option>-->
                                <option value="WITHDRAWL">
                                    WITHDRAWL
                                </option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label for="description">Select Investment</label>
                            <select class="form-control" name="investmentId"  id="addtransaction-investmentId">
                                <c:forEach items="${userFunds}" var="fund">
                                    <option value="${fund.investment_id}">
                                        ${fund.name}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="amount">Amount</label>
                                <input class="form-control" placeholder="Enter Amount" type="text" name="amount" id="amount"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input class="form-control" placeholder="Enter Description" type="text" name="description" id="description"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    function addTransaction(action, invId) {
        $("#addtransaction-actionType").val(action);
        $("#addtransaction-investmentId").val(invId);
    }
</script>