
<%-- 
    Document   : update-conversion-pairs.jsp
    Created on : Oct 15, 2017, 9:49:26 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="updateConversionPairs" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateConversionPairs-title">
                    Update Conversation Pairs
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true"> &times;</span>
                </button>
            </div>
            <form action="./admin-update-conversion-pairs" method="post" id="updateConversionPairs-form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fromCurrSym">From Symbol</label>
                                <select class="form-control" name="fromCurrSym" id="updateConversionPairs-fromCurrSym">
                                    <option value=""> -Select- </option>
                                    <c:forEach items="${currencies}" var="currency">
                                        <option value="${currency.currencySymbol}">${currency.currencySymbol}</option>
                                    </c:forEach> 
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fromQty">Quantity</label>
                                <input class="form-control" type="text" name="fromQty" id="updateConversionPairs-fromQty"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="toCurrSym">To Symbol</label>
                                <select class="form-control" name="toCurrSym" id="updateConversionPairs-toCurrSym">
                                    <option value=""> -Select- </option>
                                    <c:forEach items="${currencies}" var="currency">
                                        <option value="${currency.currencySymbol}">${currency.currencySymbol}</option>
                                    </c:forEach> 
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="toQty">Quantity</label>
                                <input class="form-control" type="text" name="toQty" id="updateConversionPairs-toQty"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button" ondblclick="updateConversionPairsSubmit();">Proceed</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
