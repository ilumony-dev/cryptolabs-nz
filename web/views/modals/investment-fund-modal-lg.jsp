<%-- 
    Document   : investment-fund-modal-lg
    Created on : Oct 15, 2017, 9:58:39 AM
    Author     : Administrator
--%>


<!--bd-example-modal-lg-->
<div aria-hidden="true" aria-labelledby="myLargeModalLabel" class="modal fade investment-fund-modal-lg" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title " id="investment-fund-modal-label">
                    Add Investment
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <section id="summary" style="display:block;">
                    <div class="label investment-fund-modal-fundName" style="font-size: 1.5rem; color:forestgreen;"></div>
                    <div class="label investment-fund-modal-fundDescription" style="font-size: 1rem;"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="highchart"></div>
                        </div>
                    </div>
                    <div class="row investment-content" >
                        <div class="col-sm-4">
                            <div class="element-box el-tablo1">
                                <div class="color3">Total Invested</div>
                                <div class=" color3 value">$100</div>
                                <p>&nbsp;</p>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="element-box el-tablo1">
                                <div class="color3">Performance Since Inception</div>
                                <p class="color3">For Hypothetical</p>
                                <div class=" color3 value">$100</div>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="element-box el-tablo1">
                                <div class="color3">User Investment Performance</div>
                                <p class="color3">1st Day</p>
                                <div class=" color3 value">$100</div>

                            </div>
                        </div>

                        <div class="col-sm-4 offset-2">
                            <div class="element-box el-tablo1">
                                <div class="color3">User Investment Performance</div>
                                <p class="color3">1st Month</p>
                                <div class=" color3 value">$100</div>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="element-box el-tablo1">
                                <div class="color3">User Investment Performance</div>
                                <p class="color3">3rd Month</p>
                                <div class=" color3 value">$100</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="element-box el-tablo">
                                <div class="label">Target Asset Allocation</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="element-box el-tablo">
                                <div class="label">Current Asset Allocation</div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="element-box el-tablo">
                                <div class="label">Top 5 Holdings</div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <!--<button class="btn btn-primary next" type="button" onclick="activePage('investments');">Continue</button>-->
                    </div>
                </section>
                <section id="investments" style="display:none;">
                    <div class="label" class="investment-fund-modal-fundName" style="font-size: 1.5rem; color:forestgreen;">
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name='fundId' id="investment-fund-modal-fundId" value="0"/>
                            <!--<input type="hidden" name='fundName' class='investment-fund-modal-fundName' value="null"/>-->
                            <input type="hidden" id="investment-fund-modal-fundIR" value="0"/>
                            <h3 class="color1">How much do you want to invest now?</h3>
                            <p style="font-size:12px; color: #666">(Minimum investment amount of $50. Either $50 now, or $50 as a regular payment)</p>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div id="slider1"></div>
                                    <input type="range" class="form-control" id="slider1" value="0" min="0" max="100000" onchange="setCheckoutAmount();"/>
                                </div><span style="margin-top: 10px">${currSymbol}</span>
                                <div class="col-sm-4">
                                    <input type="number" class="form-control" id="investment-fund-modal-upFrontValue" name="upFrontAmount" value="0.00" step="0.01" onkeyup="setCheckoutAmount();" onchange="setCheckoutAmount();"/>
                                </div>
                                <div class="col-sm-2">
                                    <!--<button id="btnload">reset</button>-->
                                    <button id="submit" class="btn btn-warning" onclick="displayContinue(this);">Submit Amount</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12" id="investment-fund-modal-hideImg" style="display:none;">
                                    <center><img src="./resources/img/loading.gif" style="width:15%"></center>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <%--                            <h3 class="color1">How much to you want to invest on a regular basis? (per month)</h3>
                                                        <div class="row">
                                                            <div class="col-sm-7">
                                                                <div id="slider2"></div>
                                                                <input type="range" class="form-control" id="slider2" value="0" min="0" max="100000"/>
                        </div>
                                                            <div class="col-sm-5">
                                                                <input type="number" class="form-control" id="monthlyValue" name="monthlyValue" value="0"/>
                    </div>
                                                        </div>
                                                        <h3 class="color1">What's your time frame with this investment?</h3>
                                                        <div class="row">
                                                            <div class="col-sm-7">
                                                                <div id="slider3"></div>
                                                                <input type="range" class="form-control" id="slider3" value="1" min="1" max="65"/>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <input class="form-control" type="number" id="years" name="years" value="1"/>
                                                            </div>
                                                        </div>
                                                        <div class="row bg-primary" style="padding:10px">
                                                            <div class="col-sm-12" style="border:1px solid #fff; border-radius: 5px; margin-bottom: 10px">
                                                                <p style="color:#fff;margin:10px 0"><b>Forecast Crypto Value</b><span class="pull-right">After <span class="spnYear"></span> years</span></p>
                                                                <div class="row"  style="padding:10px">
                                                                    <div class="col-sm-5">
                                                                        <img src="./resources/img/preferences/money-bag.png" style="width:40px; height: 40px">
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <p style="color:#fff; font-size: 24px" class="predictValue"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12" style="border:1px solid #fff; border-radius: 5px; margin-bottom: 10px">
                                                                <p style="color:#fff;margin:10px 0"><b>Forecast Bank Value</b><span class="pull-right">After <span class="spnYear"></span> years</span></p>
                                                                <div class="row"  style="padding:10px">
                                                                    <div class="col-sm-5">
                                                                        <img src="./resources/img/preferences/bank.png" style="width:40px; height: 40px">
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <p style="color:#fff; font-size: 24px" class="bankValue"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12" style="border:1px solid #fff; border-radius: 5px">
                                                                <p style="color:#fff;margin:10px 0"><b>Different Value</b><span class="pull-right">After <span class="spnYear"></span> years</span> </p>
                                                                <div class="row"  style="padding:10px">
                                                                    <div class="col-sm-5">
                                                                        <img src="./resources/img/preferences/difference.png" style="width:40px; height: 40px">
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <p style="color:#fff; font-size: 24px" class="diffValue"></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>--%>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button"> Cancel</button>
                        <button class="btn btn-primary next" id="investment-fund-modal-continue" type="button" onclick="activePage('payGateways');">Pay Now</button>
                    </div>
                </section>
                <%--                <section id="prefrences" style="display:none;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="element-box"  style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                <div class="row">
                                                    <div class="col-sm-6"> 
                                                        <div class="os-tabs-w">
                                                            <div class="os-tabs-controls">
                                                                <ul class="nav nav-tabs smaller">
                                                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview" aria-expanded="true">Portfolio Details</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="tab-content">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="tab-pane active" id="tab_overview" aria-expanded="true">
                                                                            <div class="element-box">
                                                                                <div class="el-chart-w" style="width:150px">
                                                                                    <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                    <canvas height="152" id="donutChart1" width="152" style="display: block; width: 152px; height: 152px;"></canvas>
                                                                                    <div class="inside-donut-chart-label"><strong class="upFrontValue"></strong><span class="upFrontValue">Initial Investment</span></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                                                                        <div class="col-sm-5">
                                                                                                                            <div class="el-legend">
                                                                                                                                <div class="label" style="margin-bottom: 40px">Current Shares Allocation</div>
                                                                                                                                <div class="legend-value-w">
                                                                                                                                    <div class="legend-pin" style="background-color: #6896f9;"></div>
                                                                                                                                    <div class="legend-value">Processed</div>
                                                                                                                                </div>
                                                                                                                                <div class="legend-value-w">
                                                                                                                                    <div class="legend-pin" style="background-color: #85c751;"></div>
                                                                                                                                    <div class="legend-value">Cancelled</div>
                                                                                                                                </div>
                                                                                                                                <div class="legend-value-w">
                                                                                                                                    <div class="legend-pin" style="background-color: #d97b70;"></div>
                                                                                                                                    <div class="legend-value">Pending</div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                </div>
                                                                <div class="tab-pane" id="tab_sales" aria-expanded="false"></div>
                                                                <div class="tab-pane" id="tab_conversion"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="os-tabs-controls">
                                                            <ul class="nav nav-tabs smaller">
                                                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tab_overview" aria-expanded="true">Risk Profile</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-7"><div class="label" style="text-transform:capitalize; font-size: 1rem">Hello there</div><p>Your Profile is Super-Risky</p></div>
                                                            <div class="col-sm-5"><p><img src="./resources/img/nervous/risk.png" style="width:39%"></p></div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                                <div class="row">
                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                <div class="os-tabs-w">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview" aria-expanded="true">
                                                            <div class="el-tablo">
                                                                <div class="label">Projected Yearly Return</div>
                                                            </div>
                                                            <div class="el-chart-w">
                                                                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                <canvas class="lineChart1" height="161" id="lineChart1" width="645" style="display: block; width: 645px; height: 161px;"></canvas>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_sales" aria-expanded="false"></div>
                                                        <div class="tab-pane" id="tab_conversion"></div>
                                                    </div>                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                <div class="os-tabs-w">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview" aria-expanded="true">
                                                            <div class="el-tablo">
                                                                <div class="label">Bank Yearly Return</div>
                                                            </div>
                                                            <div class="el-chart-w">
                                                                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                <canvas class="lineChart2" height="161" id="lineChart2" width="645" style="display: block; width: 645px; height: 161px;"></canvas>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_sales" aria-expanded="false"></div>
                                                        <div class="tab-pane" id="tab_conversion"></div>
                                                    </div>                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                <div class="os-tabs-w">
                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="tab_overview" aria-expanded="true">
                                                            <div class="el-tablo">
                                                                <div class="label">Actual Investments Yearly</div>
                                                            </div>
                                                            <div class="el-chart-w">
                                                                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                <canvas class="lineChart3" height="161" id="lineChart3" width="645" style="display: block; width: 645px; height: 161px;"></canvas>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane" id="tab_sales" aria-expanded="false"></div>
                                                        <div class="tab-pane" id="tab_conversion"></div>
                                                    </div>                                    
                                                </div>
                                            </div>
                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);"><div class="os-tabs-w"><div class="tab-content"><div class="tab-pane active" id="tab_overview" aria-expanded="true"><div class="el-tablo"><div class="label">Projected Yearly Profit</div></div><div class="el-chart-w"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                                <canvas  class="lineChart2" height="161" id="lineChart2" width="645" style="display: block; width: 645px; height: 161px;"></canvas></div></div><div class="tab-pane" id="tab_sales" aria-expanded="false"></div><div class="tab-pane" id="tab_conversion"></div></div></div></div>
                                        
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);"><div class="os-tabs-w"><div class="tab-content"><div class="tab-pane active" id="tab_overview" aria-expanded="true"><div class="el-tablo"><div class="label">Original<br> Investment</div></div><div class="el-chart-w"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                                <canvas  class="lineChart3" height="161" id="lineChart3" width="645" style="display: block; width: 645px; height: 161px;"></canvas></div></div><div class="tab-pane" id="tab_sales" aria-expanded="false"></div><div class="tab-pane" id="tab_conversion"></div></div></div>\
                                                                            </div>
                                                                        </div>
                                    </div>
                
                
                                                        <div class="row">
                                                            <div class="col-sm-7">
                                                                <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                                    <div class="os-tabs-w"><div class="tab-content"><div class="tab-pane active" id="tab_overview" aria-expanded="true"><div class="el-tablo"><div class="label">Original Investment</div></div><div class="el-chart-w"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                    <canvas  height="56" id="liteLineChart" width="228" style="display: block; width: 228px; height: 56px;"></canvas></div></div><div class="tab-pane" id="tab_sales" aria-expanded="false"></div><div class="tab-pane" id="tab_conversion"></div></div></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="element-box" style="border:1px solid #ccc;box-shadow: 0 0 10px rgba(0,0,0, .65);">
                                                                    <div class="row">
                                                                        <div class="col-sm-6">
                                    
                                                                            <div class="el-chart-w"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                                                                <canvas height="170" id="donutChart2" width="170" style="display: block; width: 170px; height: 170px;"></canvas>
                                                                                <div class="inside-donut-chart-label"><strong>142</strong><span>Total Orders</span></div>
                                                                            </div>
                                    
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="el-legend">
                                                                                <div class="legend-value-w">
                                                                                    <div class="legend-pin" style="background-color: #6896f9;"></div>
                                                                                    <div class="legend-value">Processed</div>
                                                                                </div>
                                                                                <div class="legend-value-w">
                                                                                    <div class="legend-pin" style="background-color: #85c751;"></div>
                                                                                    <div class="legend-value">Cancelled</div>
                                                                                </div>
                                                                                <div class="legend-value-w">
                                                                                    <div class="legend-pin" style="background-color: #d97b70;"></div>
                                                                                    <div class="legend-value">Pending</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                                        <button class="btn btn-warning previous" type="button" onclick="activePage('investments');">Back</button>
                                        <button class="btn btn-primary next" type="button" onclick="activePage('payGateways');">Continue</button>
                                    </div>
                                </section>--%>
                <section id="payGateways" style="display:none;">
                    <h3 class="color1">How would you like to pay?</h3>
                    <div class="row">
                        <div class="col-md-4" style="margin-top: 28px">
                            <a id="NET_BANKING" href="">
                                <img src="./resources/img/preferences/payment-1.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px;" alt="Internet Banking">
                            </a>
                        </div>  
                        <div class="col-md-4" style="margin-top: 28px">
                            <form action='./user-stripe-charge-add-investment' method='POST' id='checkout-form'>
                                <input type='hidden' class="checkout-amount" name='amount' value="0.00" step="0.01"/>
                                <input type='hidden' class="checkout-fundId" name='fundId'/>
                                <script
                                    src='https://checkout.stripe.com/checkout.js'
                                    class='stripe-button'
                                    data-key='${stripePublicKey}' 
                                    data-amount='${amount}' 
                                    data-currency='${paymentCurrency}'
                                    data-name='Minto'
                                    data-description='${description}'
                                    data-image='${domain}/resources/img/invsta-03(200px).png'
                                    data-locale='auto'
                                    data-zip-code='false'>
                                </script>
                            </form>
                        </div> 
                        <!--                        <div class="col-md-3">
                                                    <input type="button" name="payGateway" value="BANK_TRANSFER" id="payGateway3" class="input-hidden getway radio-btn" data-target="#bank-transfer" data-toggle="modal" disabled="true"/>
                                                    <label for="payGateway3">
                                                        <img src="./resources/img/preferences/payment-4.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Bank Transfer">
                                                    </label>
                                                </div>-->
                        <div class="col-md-4">
                            <input type="button" name="payGateway" value="WALLET" id="payGateway4" class="input-hidden getway radio-btn" data-target="#via-wallet" data-toggle="modal"/>
                            <label for="payGateway4">
                                <img src="./resources/img/preferences/mywallet.png" style="width: 100%;box-shadow: 0 0 8px #011c53;border-radius: 20px; " alt="Wallet">
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn btn-warning previous" type="button" onclick="activePage('investments');">Back</button>
                        <!--<button class="btn btn-warning previous" type="button" onclick="activePage('prefrences');">Back</button>
                        <!--<button class="btn btn-primary" type="button" data-target="#investment-type" data-toggle="modal" onclick="showRegulary('')">Continue</button>-->
                    </div>
                </section>
            </div>
        </div>

    </div>
</div>
<jsp:include page="bank-transfer.jsp"></jsp:include>
<script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<script src="${home}/resources/js/cryptolabs/highcharts/highcharts.js"></script>
<script src="${home}/resources/js/cryptolabs/highcharts/highcharts-more.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>

                            $('#slider1').change(function () {
                                var newValue = this.value;
                                $('#upFrontValue').html(newValue);
                            });

                            Highcharts.chart('highchart', {
                                title: {
                                },
                                subtitle: {
                                },
                                yAxis: {
                                    title: {
                                        text: 'Number of Shares'
                                    }
                                },
                                legend: {
                                    layout: 'vertical',
                                    align: 'right',
                                    verticalAlign: 'middle'
                                },
                                plotOptions: {
                                    series: {
                                        label: {
                                            connectorAllowed: false
                                        },
                                        pointStart: 2010
                                    }
                                },
                                series: [{
                                        name: 'share1',
                                        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                                    }, {
                                        name: 'share2',
                                        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                                    }, {
                                        name: 'share3',
                                        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                                    }, {
                                        name: 'share4',
                                        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                                    }, {
                                        name: 'Other',
                                        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
                                    }],
                                responsive: {
                                    rules: [{
                                            condition: {
                                                maxWidth: 500
                                            },
                                            chartOptions: {
                                                legend: {
                                                    layout: 'horizontal',
                                                    align: 'center',
                                                    verticalAlign: 'bottom'
                                                }
                                            }
                                        }]
                                }

                            });</script>
<script>
    function activeSummary() {
        activePage("summary");
    }
    function activePage(id) {
        var sections = document.getElementsByTagName("section");
        for (var i = 0; i < sections.length; i++) {
            var section = sections[i];
            section.style.display = "none";
        }
        var cP = document.getElementById(id);
        cP.style.display = "block";
        onFundChange();
    }
    function onFundChange() {
        var fundId = $("#investment-fund-modal-fundId").val();
        var iAmount = $("#investment-fund-modal-upFrontValue").val();
        var url = './rest/cryptolabs/api/aksadjfljdslfjds999?fId=' + fundId + '&iAmt=' + iAmount;
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            dataType: "json",
            success: function (data) {
                donutSharesChart(data);
                chart2();
            }
        });
    }

    function donutSharesChart(db) {
        var shares = new Array();
        var amounts = new Array();
        $.each(db, function (i, share) {
            shares.push(share.shareName);
            amounts.push(share.shareAmount);
        });
        var donutChart = $("#donutChart1");
        // donut chart data
        var data = {
            labels: shares,
            datasets: [{
                    data: amounts,
                    backgroundColor: ["#5797fc", "#7e6fff", "#4ecc48", "#ffcc29", "#f37070"],
                    hoverBackgroundColor: ["#5797fc", "#7e6fff", "#4ecc48", "#ffcc29", "#f37070"],
                    borderWidth: 0
                }]
        };
        // -----------------
        // init donut chart
        // -----------------
        new Chart(donutChart, {
            type: 'doughnut',
            data: data,
            options: {
                legend: {
                    display: false
                },
                animation: {
                    animateScale: true
                },
                cutoutPercentage: 80
            }
        });
    }

    function chart2() {
        var currentmonth = new Date().getMonth() + 1;
        var currentYear = new Date().getYear() + 1900;
        var year = $('#years').val();
        var upFrontValue = $('#upFrontValue').val();
        var monthlyValue = $('#monthlyValue').val();
        var investmentInterest = $('#investmentFundModalFundIR').val();
        upFrontValue = upFrontValue.replace(/,/g, "");
        monthlyValue = monthlyValue.replace(/,/g, "");
        investmentInterest = investmentInterest.replace(/,/g, "");
        var yearsArr = new Array();
        var ilumony = new Array();
        var bank = new Array();
        var actual = new Array();
        var actualBal = 0;
        var curr = 0;
        var yearsNum = eval(year);
        var monthNum = 0;
        var pmonths = 0;
        while (curr <= yearsNum) {
            if (curr === 0) {
                monthNum = (12 - currentmonth);
                actualBal = eval(upFrontValue) + eval(monthlyValue * monthNum);
            } else if (curr === yearsNum) {
                monthNum = (currentmonth);
                actualBal = eval(actualBal) + eval(monthlyValue * monthNum);
            } else {
                monthNum = (12);
                actualBal = eval(actualBal) + eval(monthlyValue * monthNum);
            }
            pmonths = pmonths + monthNum;
            var balance = calculateMonthsBal(pmonths, upFrontValue, investmentInterest, monthlyValue);
            var yearlypridicted = balance.principalVal;
            var yearlybank = balance.bankValue;
            ilumony.push(parseInt(yearlypridicted));
            bank.push(parseInt(yearlybank));
            actual.push(parseInt(actualBal));
            var cY = eval(currentYear + curr);
            yearsArr.push(parseInt(cY));
            curr++;
        }
        lineChart("#lineChart1", ilumony, yearsArr);
        lineChart("#lineChart2", bank, yearsArr);
        lineChart("#lineChart3", actual, yearsArr);
    }
    function lineChart(id, values, yearsArr) {
        var lineChart = $(id);
        // line chart data
        var lineData = {
            labels: yearsArr,
            datasets: [{
//                                                                            label: "Crypto Balance",
                    fill: true,
                    lineTension: 0,
                    backgroundColor: "#fff",
                    borderColor: "#6896f9",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "#2a2f37",
                    pointBorderWidth: 3,
                    pointHoverRadius: 10,
                    pointHoverBackgroundColor: "#FC2055",
                    pointHoverBorderColor: "#fff",
                    pointHoverBorderWidth: 3,
                    pointRadius: 6,
                    pointHitRadius: 10,
                    data: values,
                    spanGaps: false
                }]
        };
        // line chart init
        var myLineChart = new Chart(lineChart, {
            type: 'line',
            data: lineData,
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                            ticks: {
                                fontSize: '11',
                                fontColor: '#969da5'
                            },
                            gridLines: {
                                color: 'rgba(0,0,0,0.05)',
                                zeroLineColor: 'rgba(0,0,0,0.05)'
                            }
                        }],
                    yAxes: [{
                            display: false,
                            ticks: {
                                beginAtZero: true,
                                max: values[values.length - 1]
                            }
                        }]
                }
            }
        });
    }

    function setCheckoutAmount() {
        var ufeleVal = $("#investment-fund-modal-upFrontValue").val();
        var amount = eval(ufeleVal) * 100;
        var fundId = $("#investment-fund-modal-fundId").val();
        alert('amount' + amount);
        document.getElementsByClassName("checkout-amount")[0].value = amount;
        $(".checkout-fundId").val(fundId);
        $(".span-checkout-amount").text(amount);
    }

    function showRegulary(text) {
//        var x = document.getElementById("regularly");
//        x.style.display = "block";
//        $("#monthly").prop("checked", true);
//        $("#investOptionTitle").text(text);
        onShowSetModal();
    }
    function hideRegulary(text) {
//        var x = document.getElementById("regularly");
//        x.style.display = "none";
//        $("#fixed").prop("checked", true);
//        $("#investOptionTitle").text(text);
        onShowSetModal();
    }
    function onShowSetModal() {
        $("#stepContent1").addClass("active");
        $("#trigger1").removeClass("complete");
        $("#trigger1").addClass("active");
        $("#stepContent2").removeClass("active");
        $("#trigger2").removeClass("active");
    }


</script>
<script>
    $("#hideImg").hide();
    $("#submit").click(function () {
        $("#hideImg").show();
    });
</script>