<%-- 
    Document   : addinvitecode
    Created on : Oct 25, 2017, 10:19:26 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="addinvitecode" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Create Invite Code
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <form action="./admin-add-invite-code" method="post" id="addInviteCodeForm">
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-sm-6">                            
                            <div class="form-group">
                                <label for="emailId">Email Id</label>
                                <input class="form-control" placeholder="Enter email" type="text" name="emailId" id="emailId"/>
                            </div>
                        </div>
                        <div class="col-sm-6">                            
                            <div class="form-group">
                                <label for="inviteCode">Invite Code</label>
                                <input class="form-control" placeholder="Enter invite code" type="text" name="inviteCode" id="inviteCode"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="timeFrame">Expired Time Frame</label>
                                <select class="form-control" name="timeFrame" id="timeFrame">
                                    <option value="1">1 Year</option>
                                    <option value="2">2 Years</option>
                                    <option value="3">3 Years</option>
                                    <option value="4">4 Years</option>
                                    <option value="5">5 Years</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="purpose">Purpose</label>
                                <select class="form-control" name="purpose" id="purpose">
                                    <option value="REGISTRATION">Registration</option>
                                    <option value="INVESTMENT">Investment</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"> 
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Cancel</button>
                        <button class="btn btn-primary" type="button" ondblclick="addInviteCodeSubmit();">Create Invite Code</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>