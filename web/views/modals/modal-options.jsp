<%-- 
    Document   : invite-code
    Created on : Oct 15, 2017, 10:08:12 AM
    Author     : Administrator
--%>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal3" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ilumony Trading Bot Crypto Fund
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="">Enter Invite Code</label>
                            <input class="form-control" placeholder="Enter Invite code" type="number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                <button class="btn btn-primary" data-target="#validate" data-toggle="modal" type="button" >Validate </button>
            </div>

        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="validate" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ilumony Trading Bot Crypto Fund
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12 text-center">  

                        <img src="${home}/resources/img/cancel.png" style="width:20%"/>

                        <h5 >
                            <b>  Incorrect Invite Code</b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>

            </div>

        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="validate" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Ilumony Trading Bot Crypto Fund
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12 text-center">  

                        <img src="${home}/resources/img/cancel.png" style="width:20%"/>

                        <h5 >
                            <b>  Incorrect Invite Code</b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>

            </div>

        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="invite-code" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Enter Your Invite Code
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div>
                    <p>
                        This Fund is under algorithm testing mode ? We only allow investments from advanced investors. Please enter your fund invite code.
                    </p>
                </div>
            </div>
            <div class="modal-footer">

                <button class="btn btn-warning"  data-target="#exampleModal3" data-toggle="modal" type="button">Enter Your Invite Code</button>
                <button class="btn btn-primary" data-target="#register" data-toggle="modal" type="button" >Register Interest </button>
            </div>

        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="exampleModal2" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Enter Your Invite Code
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div>
                    <p>
                        This Fund is under algorithm testing mode ? We only allow investments from advanced investors. Please enter your fund invite code.
                    </p>

                </div>


            </div>
            <div class="modal-footer">

                <button class="btn btn-warning"  data-target="#exampleModal3" data-toggle="modal" type="button">Enter Your Invite Code</button>
                <button class="btn btn-primary" data-target="#register" data-toggle="modal" type="button" >Register Interest </button>
            </div>

        </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="validate1" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Validate Unique Number
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12 text-center">  

                        <img src="${home}/resources/img/cancel.png" style="width:20%"/>

                        <h5 >
                            <b>  Incorrect Unique Number</b>
                        </h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
            </div>
        </div>
    </div>
</div>                    
<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="requestcard" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Request Card
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12 text-center">             

                        <img src="${home}/resources/img/massage.gif" style="width: 50%"/>
                        <h6 >
                            Thanks for requesting Debit Card. We will be in touch with you soon. 

                        </h6>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 

                <button class="btn btn-primary"   type="button">Cool</button>
            </div>

        </div>
    </div>
</div>                        


<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="register" role="dialog" tabindex="-1">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Register Interest
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="">Full Name</label>
                            <input class="form-control" placeholder="Enter Full Name" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="">Email </label>
                            <input class="form-control" placeholder="Enter Email  " type="email">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer"> 
                <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                <button class="btn btn-primary"   type="button">Register</button>
            </div>

        </div>
    </div>
</div>