<%-- 
    Document   : user-units-summary
    Created on : Feb 01, 2018, 9:50:50 AM
    Author     : Administrator
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>

<div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="user-units-summary" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Minto Unit Summary
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <!--<form action="./admin-user-account-summary" method="post">-->
                    <div class="col-md-4">
                        <div class="form-group  col-sm-12">
                            <label for="username">Username</label>
                            <input class="form-control" placeholder="Enter Username" type="text" name="username" id="user-units-summary-username"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label for="fromDate">From Date</label>
                            <input type="date" class="form-control"  placeholder="Enter From date" name="fromDate" id="user-units-summary-fromDate" required="required"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label for="toDate">To Date</label>
                            <input type="date" class="form-control"  placeholder="Enter To date" name="toDate" id="user-units-summary-toDate" required="required"/>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group" style="float: right">
                            <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                            <a class="btn btn-primary" href="#" onclick="userUnitSummary();">Submit</a>
                            <button id="btnExport" class="btn btn-success" onclick="javascript:xport.toCSV('user-Units-Summary-Table',$('#user-units-summary-username').val());" title="Export the table to CSV for all browsers.">Export to CSV</button>
                        </div>
                    </div>
                    <!--</form>-->
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-lightborder" id="user-Units-Summary-Table" summary="" rules="groups">
                                <thead>
                                    <tr style="display:none">
                                        <th>
                                            Customer Id
                                        </th>
                                        <th  id="user-units-summary-thead-refId">

                                        </th>
                                        <th>
                                            From Date
                                        </th>
                                        <th id="user-units-summary-thead-fromDate">

                                        </th>
                                        <th>
                                            To Date
                                        </th>
                                        <th id="user-units-summary-thead-toDate">

                                        </th>
                                        <th>
                                            To Date
                                        </th>
                                        <th>

                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            DateTime
                                        </th>
                                        <th>
                                            Portfolio
                                        </th>
                                        <th>
                                            Pur. Minto
                                        </th>
                                        <th>
                                            Pur. Bitcoin
                                        </th>
                                        <th>
                                            Cur. Bitcoin
                                        </th>
                                        <th>
                                            U.S.D
                                        </th>
                                        <th>
                                            N.Z.D
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                    </tr>
                                </thead>
                                <tbody id="user-units-summary-UAS">
                                    <tr>
                                        <td class="nowrap">
                                        </td>
                                        <td class="nowrap">
                                        </td>
                                        <td class="text-right">
                                        </td>
                                        <td class="text-right">
                                        </td>
                                        <td class="text-right">
                                        </td>
                                        <td class="text-right">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer"> 
                <!--                    <button class="btn btn-danger" data-dismiss="modal" type="button" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Create New Fund</button>-->
            </div>
        </div>
    </div>
</div>