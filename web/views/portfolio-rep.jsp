<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="home" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <title>${title}</title>
        <meta charset="utf-8">
        <meta content="ie=edge" http-equiv="x-ua-compatible">
        <meta content="template language" name="keywords">
        <meta content="Template" name="author">
        <meta content="User dashboard html template" name="description">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="${home}/resources/favicon.png" rel="shortcut icon">
        <link href="${home}/resources/apple-touch-icon.png" rel="apple-touch-icon">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
        <link href="${home}/resources/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="${home}/resources/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
        <link href="${home}/resources/css/style.css" rel="stylesheet">
        <link href="${home}/resources/css/main.css?version=3.5.1" rel="stylesheet">
        <link href="${home}/resources/icon_fonts_assets/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    </head>
    <style>
        .table tbody tr:last-child{ background: #6599FF; color: #fff}
    </style>
    <body onload="balChart('today');">
        <div class="all-wrapper menu-side with-side-panel">
            <div class="layout-w">
                <jsp:include page="header.jsp"/>
                <div class="content-w">
                    <!--------------------
                    START - Breadcrumbs
                    -------------------->
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="./welcome">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${endUser.fullName} Profile</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">${currency}</a>
                        </li>
                    </ul>
                    <!--------------------
                    END - Breadcrumbs
                    -------------------->
                    <div class="content-panel-toggler">
                        <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                    </div>

                    <div class="content-i">
                        <div class="content-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="element-wrapper">
                                        <h6 class="element-header">${title}</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div id="linechart1"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart2"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="linechart3"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart4"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="piechart1"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="piechart2"></div>
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-md-6">
                                    <div id="linechart5"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="linechart6"></div>
                                </div>
                            </div>


                        </div>
                        <!--------------------
                        START - Sidebar
                        -------------------->
                        <%--<jsp:include page = "user-right-sidebar.jsp" />--%>
                        <!--------------------
                        END - Sidebar
                        -------------------->
                    </div>

                </div>
            </div>
            <div class="display-type"></div>
        </div>


        <script src="${home}/resources/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="${home}/resources/js/main.js?version=3.5.1"></script>
        <script src="https://use.fontawesome.com/ac51fe8084.js"></script>
        <!--<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>-->
        <!--<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.5/angular.min.js'></script>-->
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/series-label.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script>
        var linecharts = [1, 2, 3, 4, 5, 6];
        var piecharts = [1, 2];
        for (var i = 1; i <= linecharts.length; i++) {
            Highcharts.chart('linechart' + i, {

                title: {
                    text: 'Solar Employment Growth by Sector, 2010-2016'
                },

                subtitle: {
                    text: 'Source: thesolarfoundation.com'
                },

                yAxis: {
                    title: {
                        text: 'Number of Employees'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2010
                    }
                },

                series: [{
                        name: 'Installation',
                        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
                    }, {
                        name: 'Manufacturing',
                        data: [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]
                    }, {
                        name: 'Sales & Distribution',
                        data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
                    }, {
                        name: 'Project Development',
                        data: [null, null, 7988, 12169, 15112, 22452, 34400, 34227]
                    }, {
                        name: 'Other',
                        data: [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]
                    }],

                responsive: {
                    rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                }

            });
        }

        for (var j = 1; j <= piecharts.length; j++){

            Highcharts.chart('piechart' + j, {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                title: {
                    text: 'Browser<br>shares<br>2017',
                    align: 'center',
                    verticalAlign: 'middle',
                    y: 40
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            distance: -50,
                            style: {
                                fontWeight: 'bold',
                                color: 'white'
                            }
                        },
                        startAngle: -90,
                        endAngle: 270,
                        center: ['50%', '50%']
                    }
                },
                series: [{
                        type: 'pie',
                        name: 'Browser share',
                        innerSize: '80%',
                        data: [
                            ['Chrome', 58.9],
                            ['Firefox', 13.29],
                            ['Internet Explorer', 13],
                            ['Edge', 3.78],
                            ['Safari', 3.42],
                            {
                                name: 'Other',
                                y: 7.61,
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        ]
                    }]
            });


        }
        </script>

    </body>
</html>