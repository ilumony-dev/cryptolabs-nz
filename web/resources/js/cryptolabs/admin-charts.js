function linechart(id, valId, value, values, series) {
    var data = [];
    if (values !== null) {
        data = [{
                name: 'Live data',
                data: values
            }];
    }
    if (series !== null) {
        data = series;
    }
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    Highcharts.chart(id, {
        chart: {
            type: 'line',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10
//            events: {
//                load: function () {
//                    // set up the updating of the chart each second
//                    var series = this.series[0];
//                    setInterval(
//                            function () {
//                                var x = (new Date()).getTime();
//                                var y = eval($(valId).val());
//                                series.addPoint([x, y], true, true);
//                            }
//                    , 1000);
//                }
//            }
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Value'
            },
            plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
        },
        credits: {
            enabled: false
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 12);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: data
    });
}


function linegraphBitcoin(value, values, series) {
    var id = 'linegraphBitcoin';
    var valId = "#actual-btc-invested";
    linechart(id, valId, value, values, series);
}

function linegraphMinto(value, values, series) {
    var id = 'linegraphMinto';
    var valId = "#actual-minto-units";
    linechart(id, valId, value, values, series);
}

function linegraphUSD(value, values, series) {
    var id = 'linegraphUSD';
    var valId = "#actual-usd-units";
    linechart(id, valId, value, values, series);
}

function linegraphNZD(value, values, series) {
    var id = 'linegraphNZD';
    var valId = "#actual-nzd-units";
    linechart(id, valId, value, values, series);
}

function linegraphMintoNav(value, values, series) {
    var id = 'linegraphMintoNav';
    var valId = "#actual-minto-nav";
    linechart(id, valId, value, values, series);
}