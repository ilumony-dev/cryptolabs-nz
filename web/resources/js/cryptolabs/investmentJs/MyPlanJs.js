$(document).ready(function () {
    setSliderValue("#slider1", "#upFrontValue");
    setSliderValue("#slider2", "#monthlyValue");
    setSliderValue("#slider3", "#years");
});


function setSliderValue(slider, value) {
    var rg = $(slider).val();
    $(value).val(rg);
    $(slider).change(function () {
        rg = $(slider).val();
        $(value).val(rg);
        changePlan();
    });
    $(value).keyup(function () {
        vl = $(value).val();
        $(slider).val(vl);
        changePlan();
    });
}

function changePlan()
{
    var year = $('#years').val();
    var InitialInvestment = $('#upFrontValue').val();
    var monthlyInvest = $('#monthlyValue').val();
    var investmentInterest = $('#investment-fund-modal-fundIR').val();
    $('#upFrontValue').val(InitialInvestment);
    $('#monthlyValue').val(monthlyInvest);

    $('.spnYear').text('');
    $('.predictValue').text('');
    $('.bankValue').text('');
    $('.diffValue').text('');

    var balance = CalculatePlan(year, InitialInvestment, investmentInterest, monthlyInvest);
    var yearlypridicted = balance.principalVal;
    var yearlybank = balance.bankValue;
    var yearlyilumony = balance.ilumnyValue;

    $('.upFrontValue').text('$' + InitialInvestment);
    $('.monthlyValue').text('$' + monthlyInvest);
    $('.spnYear').text(year);
    $('.predictValue').text('$' + yearlypridicted);
    $('.bankValue').text('$' + yearlybank);
    $('.diffValue').text('$' + yearlyilumony);
    
    $('.upFrontValue').val(InitialInvestment);
    $('.monthlyValue').val(monthlyInvest);
    $('.years').val(year);

    $('#predictAmount').val(yearlypridicted.replace(/,/g, ""));
    $('#bankAmount').val(yearlybank.replace(/,/g, ""));
    $('#diffAmount').val(yearlyilumony.replace(/,/g, ""));

}	