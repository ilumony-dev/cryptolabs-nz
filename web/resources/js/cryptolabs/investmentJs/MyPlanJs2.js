var yAxisFirstAmount = 0;
var yAxisLastAmount = 0;
var categoryData = [];
$(document).ready(function () {
    //var plan = CalculatePlan(2,100,5,50);
    //console.log('principalVal: ' +plan.principalVal + ' || ' + ' bankValue: ' +plan.bankValue + ' || '+'ilumnyValue: ' + plan.ilumnyValue);
    SetupPlan();
});

function SetupPlan() {
    var year = $('#years').val();
    var InitialInvestment = $('#upFrontValue').val();
    var monthlyInvest = $('#monthlyValue').val();
    var investmentInterest = $('.count:checked').val();
    setupPlan06(year, InitialInvestment, monthlyInvest, investmentInterest);
}

function setupPlan06(year, InitialInvestment, investmentInterest, monthlyInvest)
{
//	var year = 5,InitialInvestment = 100,investmentInterest = 5.30,monthlyInvest = 50;
    var balance = CalculatePlan(year, InitialInvestment, investmentInterest, monthlyInvest);
    var yearlypridicted = balance.principalVal;
    var yearlybank = balance.bankValue;
    var yearlyilumony = balance.ilumnyValue;


    // set the value to UI
    $('#hPlanName').text("Groth");
    $('#hInitialInvestment').text('$' + InitialInvestment);
    $('#hMonthlyInvestment').text('$' + monthlyInvest);
    $('#hInvestmentStyle').text('$' + investmentInterest);
    $('.spnYear').text(year);
    $('#PpridictedValue').text('$' + yearlypridicted);
    $('#PbankValue').text('$' + yearlybank);
    $('#PilumonyValue').text('$' + yearlyilumony);


    // ------------- line chart----------------------
    var mySeries = CreateSearies(year, InitialInvestment, investmentInterest, monthlyInvest);
    LoadChart(mySeries);

    // ------------ Pie Chart-------------------
    // 1 is the investment type which will come from previous page
    var instType = InvestmentStyleData(1);
    LoadPieChart(instType);

    //---------------Map-------
    LoadMap();
}

//------------------------------------- Line Chart ----------------------------------------
function LoadChart(mySeries) {
    $("#chart-2").html('');
    Highcharts.chart('chart-2', {
        chart: {
            type: 'spline',
            backgroundColor: 'transparent',
            style: {
                fontFamily: 'monospace',
                color: "#FFFFFF",
                margin: 0
            }
        },
        legend: {
            itemStyle: {
                font: '12px Trebuchet MS, Verdana, sans-serif',
                color: '#FFFFFF'
            },
            itemHoverStyle: {
                color: '#FFF'
            }
        },
        /*title: {
         text: 'KiwiSaver Calculator',
         style: {
         color: '#FFFFFF',
         font: 'bold 25px "Trebuchet MS", Verdana, sans-serif'
         }
         },
         subtitle: {
         text: 'Potential value of Grafts advice by the time you retire (when compared to receiving no advice in a default fund): <br/> <b>$' + lastAmount +'<b> in your Retirement',
         style: {
         color: '#FFFFFF',
         fontWeight: 'bold',
         fontSize: '12px',
         fontFamily: 'Trebuchet MS, Verdana, sans-serif'
         
         } 
         },*/
        title: {
            text: '',
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: categoryData,
            /*labels: {
             formatter: function() {
             
             return this.value;
             },
             style: {
             color: '#FFFFFF',
             font: '11px Trebuchet MS, Verdana, sans-serif'
             }
             //enable: false
             },*/
            overflow: 'justify'
        },
        yAxis: {
            title: {
                text: 'Value',
                style: {
                    color: '#FFFFFF',
                    font: '11px Trebuchet MS, Verdana, sans-serif'
                }
            },
            labels: {
                /*formatter: function () {
                 return this.value / 10;
                 },*/
                format: '{value:,.0f}',
                style: {
                    color: '#FFFFFF',
                    font: '11px Trebuchet MS, Verdana, sans-serif'
                }
            },
            minRange: yAxisFirstAmount,
            maxRange: yAxisLastAmount,
            tickInterval: yAxisLastAmount / 5,
            minorGridLineWidth: 1,
            gridLineWidth: 1,
            alternateGridColor: null,
            style: {
                color: '#FFFFFF',
                fontWeight: 'bold',
                fontSize: '12px',
                fontFamily: 'Trebuchet MS, Verdana, sans-serif'
            }
        },
        tooltip: {
            pointFormat: '{series.name} : <b>${point.y:,.0f}</b><br/> at age <b>{point.name}</b>'
        },
        plotOptions: {
            spline: {
                lineWidth: 4,
                states: {
                    hover: {
                        lineWidth: 5
                    }
                },
                marker: {
                    enabled: false
                }
            }
        },
        legend: {
            itemStyle: {
                fontFamily: 'Trebuchet MS, Verdana, sans-serif',
                color: '#FFFFFF'
            }
        },
        series: mySeries
    });

    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });
//    $('.highcharts-credits').html('');
}

function CreateSearies(year, InitialInvestment, investmentInterest, monthlyInvest) {
    var predictedBalanceList = GetSeriesData(CalculatePlanByYear(year, InitialInvestment, investmentInterest, monthlyInvest));
    var ilumnBalanceList = GetSeriesData(CalculatePlanByYear(year, InitialInvestment, 3.50, monthlyInvest));
    var series = [{
            name: 'Pridicted Balance',
            data: predictedBalanceList,
            lineWidth: 6,
            color: '#ffde00',
        }
        , {
            name: 'ilumony Balance',
            data: ilumnBalanceList,
            lineWidth: 5,
            color: '#ffffff',
        }];
    yAxisFirstAmount = predictedBalanceList[0].y;
    yAxisLastAmount = predictedBalanceList[predictedBalanceList.length - 1].y;
    return series;
}

function GetSeriesData(calculatedBalance) {
    var array = [];
    for (var i = 0; i < calculatedBalance.length; i++) {
        var currentValue = calculatedBalance[i].balance;
        var currentYear = calculatedBalance[i].year;
        array.push({
            name: currentYear,
            y: currentValue
        });
        categoryData.push(currentYear);
    }
    return array;
}


//---------------------- Pie Chart----------------
function LoadPieChart(inst)
{
    Highcharts.chart('chart-1', {
        chart: {
            type: 'pie',
            height: 250,
            width: 250,
            marginTop: 0,
            backgroundColor: 'transparent',
            color: '#fff'
        },
        credits: {enabled: false},
        colors: [
            '#5485BC', '#AA8C30', '#5C9384', '#981A37', '#FCB319', '#86A033', '#614931', '#00526F', '#594266', '#cb6828', '#aaaaab', '#a89375'
        ],
        title: {text: null},
        plotOptions: {
            pie: {
                innerSize: 0,
                depth: 30,
                allowPointSelect: true,
                cursor: 'pointer',
                showInLegend: true,
                dataLabels: {
                    enabled: false,
                    formatter: function () {
                        return this.percentage.toFixed(2) + '%';
                    }
                }
            }
        },
        legend: {
            enabled: true,
            layout: 'vertical',
            align: 'right',
            width: 100,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function () {
                return '<div style="text-align:left;width:100px; color:#fff;float:left;">' + this.name + '</div> ';
            }
        },
        series: [{
                name: 'Investment Type',
                style: {
                    color: '#fff'},
                data: [
                    ['Cash', inst.Cash],
                    ['NZ Fixed Interest', inst.NZFixedInterest],
                    ['Global Fixed Interest', inst.GlobalFixedInterest],
                    ['NZ Property', inst.NZProperty],
                    ['Global Property', inst.GlobalProperty],
                    ['Australaian Equity', inst.AustralaianEquity],
                    ['Global Equity', inst.GlobalEquity],
                    ['Alternative Assets', inst.AlternativeAssets],
                    ['Global Infrastructure', inst.GlobalInfrastructure],
                    ['Other', inst.Other]
                ]
            }]
    });
}

//------------------------------ high map---------------------


function LoadMap()
{
// Create the chart
    Highcharts.mapChart('highMapDiv', {
        chart: {
            map: 'custom/world-continents'
        },
        title: {
            text: 'Highmaps basic demo'
        },
        subtitle: {
            text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world-continents.js">World continents</a>'
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },
        colorAxis: {
            min: 0
        },
        series: [{
                data: data,
                name: 'Random data',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }]
    });
}
var data = [
    ['eu', 0],
    ['oc', 1],
    ['af', 2],
    ['as', 3],
    ['na', 4],
    ['sa', 5]
];

