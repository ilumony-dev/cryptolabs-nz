function CalculatePlan(year, InitialInvestment, investmentInterest, monthlyInvest)
{
    var bal = calculateMyBal(year, InitialInvestment, investmentInterest, monthlyInvest);

    var principalVal = (bal.principalVal).formatMoney(0, '.', ',');
    var bankValue = (bal.bankValue).formatMoney(0, '.', ',');
    var ilumnyValue = (bal.ilumnyValue).formatMoney(0, '.', ',');
    return {
        principalVal: principalVal, bankValue: bankValue, ilumnyValue: ilumnyValue
    };
}

function calculateMyBal(year, InitialInvestment, investmentInterest, monthlyInvest)
{
    var pmts = eval(year * 12);
    return calculateMonthsBal(pmts, InitialInvestment, investmentInterest, monthlyInvest);

}

function calculateMonthsBal(pmts, InitialInvestment, investmentInterest, monthlyInvest)
{
    var interest = investmentInterest; // this intereset rate eg : 5%
    interest = investmentInterest / 100;
    interest /= 12;
    var bankinterest = 3.5;
    bankinterest = 3.5 / 100;
    bankinterest /= 12;
    var ma = eval(monthlyInvest);
    //alert(ma)
    var prin = eval(InitialInvestment);
    var bprin = prin;
//    var pmts = eval(year * 12);
    var count = 0;
    while (count < pmts) {
        newprin = prin + ma;        
        prin = (newprin * interest) + eval(prin + ma);
        bankprincipal = bprin + ma;
        bprin = (bankprincipal * bankinterest) + eval(bprin + ma);
        count = count + 1;
    }
     //alert(prin)
    return {
        principalVal: prin, bankValue: bprin, ilumnyValue: eval(prin - bprin)
    };
}
// this data is created for line chart
function CalculatePlanByYear(year, InitialInvestment, investmentInterest, monthlyInvest){
    var calArray = [];
    var i = investmentInterest; // this intereset rate eg : 5%
    i = investmentInterest / 100;
    i /= 12;
    var ma = eval(monthlyInvest);
    var prin = eval(InitialInvestment);
    for (j = 1; j <= year; j++)
        {
        var count = 0;
        while (count < 12) {
            newprin = prin + ma;
            prin = (newprin * i) + eval(prin + ma);
            count = count + 1;
             }
        calArray.push({year: j, balance: prin});
    }
    return calArray;
}

// format the number with comma seprated and round up of last value
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};