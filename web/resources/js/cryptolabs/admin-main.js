var shareOptions = '';
var coinOptions = '';
var data = {};
$.each(shares, function (index, share) {
    shareOptions = shareOptions + '<option value=' + share.shareId + '>' + share.name + '</option>';
});
$.each(coins, function (index, coin) {
    coinOptions = coinOptions + '<option value=' + coin.uniqueId + '>' + coin.name + '</option>';
});
coinOptions = coinOptions + shareOptions;

var xport = {
    _fallbacktoCSV: true,
    toXLS: function (tableId, filename) {
        this._filename = (typeof filename == 'undefined') ? tableId : filename;

        //var ieVersion = this._getMsieVersion();
        //Fallback to CSV for IE & Edge
        if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
            return this.toCSV(tableId);
        } else if (this._getMsieVersion() || this._isFirefox()) {
            alert("Not supported browser");
        }

        //Other Browser can download xls
        var htmltable = document.getElementById(tableId);
        var html = htmltable.outerHTML;

        this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
    },
    toCSV: function (tableId, filename) {
        this._filename = (typeof filename === 'undefined') ? tableId : filename;
        // Generate our CSV string from out HTML Table
        var csv = this._tableToCSV(document.getElementById(tableId));
        // Create a CSV Blob
        var blob = new Blob([csv], {type: "text/csv"});

        // Determine which approach to take for the download
        if (navigator.msSaveOrOpenBlob) {
            // Works for Internet Explorer and Microsoft Edge
            navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
        } else {
            this._downloadAnchor(URL.createObjectURL(blob), 'csv');
        }
    },
    _getMsieVersion: function () {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf("MSIE ");
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf("rv:");
            return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        }

        // other browser
        return false;
    },
    _isFirefox: function () {
        if (navigator.userAgent.indexOf("Firefox") > 0) {
            return 1;
        }
        return 0;
    },
    _downloadAnchor: function (content, ext) {
        var anchor = document.createElement("a");
        anchor.style = "display:none !important";
        anchor.id = "downloadanchor";
        document.body.appendChild(anchor);
        // If the [download] attribute is supported, try to use it
        if ("download" in anchor) {
            anchor.download = this._filename + "." + ext;
        }
        anchor.href = content;
        anchor.click();
        anchor.remove();
    },
    _tableToCSV: function (table) {
        // We'll be co-opting `slice` to create arrays
        var slice = Array.prototype.slice;

        return slice
                .call(table.rows)
                .map(function (row) {
                    return slice
                            .call(row.cells)
                            .map(function (cell) {
                                return '"t"'.replace("t", cell.textContent.split('  ').join(''));
                            })
                            .join(",");
                })
                .join("\r\n");
    }
};

function balChart(type) {
    var url = './rest/cryptolabs/api/balChart?type=' + type;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            $(".currentBalance").text(data.totalBalance);
            lineChart(data);
            if (type === 'today') {
                $(".todayActive").addClass("active");
            } else if (type === 'week') {
                $(".weekActive").addClass("active");
            } else if (type === 'month') {
                $(".monthActive").addClass("active");
            }
        }
    });
}
// init line chart if element exists
function lineChart(data) {
    var lineChart = $("#lg-latestweek");
    // line chart data
    var lineData = {
        labels: data.labels,
        datasets: [{
                label: "Balance",
                fill: false,
                lineTension: 0,
                backgroundColor: "#fff",
                borderColor: "#6896f9",
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: "#fff",
                pointBackgroundColor: "#2a2f37",
                pointBorderWidth: 3,
                pointHoverRadius: 10,
                pointHoverBackgroundColor: "#FC2055",
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 3,
                pointRadius: 6,
                pointHitRadius: 10,
                data: data.values,
                spanGaps: false
            }]
    };
    // line chart init
    var myLineChart = new Chart(lineChart, {
        type: 'line',
        data: lineData,
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                        ticks: {
                            fontSize: '11',
                            fontColor: '#969da5'
                        },
                        gridLines: {
                            color: 'rgba(0,0,0,0.05)',
                            zeroLineColor: 'rgba(0,0,0,0.05)'
                        }
                    }],
                yAxes: [{
                        display: false,
                        ticks: {
                            beginAtZero: true,
                            max: data.totalBalance
                        }
                    }]
            }
        }
    });
}

function onAddFundChangeMaster(ele) {
    var val = ele.value;
    $("#addfund-details").html('');
    if (val === 'FUND') {
        onAddFundShare();
    } else if (val === 'PORTFOLIO') {
        onAddFundCoin();
    }
}

function onAddFundShare() {
    var htmlCode = '<tr>';
    htmlCode = htmlCode + '<td class="nowrap">';
    var shareDropDown = '';
    shareDropDown = shareDropDown + '<select class="form-control" name="shareId">';
    shareDropDown = shareDropDown + shareOptions;
    shareDropDown = shareDropDown + '</select>';
    htmlCode = htmlCode + shareDropDown;
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage"/>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity"/>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<a href="#" onclick="onAddFundShare();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
    htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '</tr>';
    $("#addfund-details").append(htmlCode);
}

function onAddFundCoin() {
    var htmlCode = '<tr>';
    htmlCode = htmlCode + '<td class="nowrap">';
    var shareDropDown = '';
    shareDropDown = shareDropDown + '<select class="form-control" name="coinId">';
    shareDropDown = shareDropDown + coinOptions;
    shareDropDown = shareDropDown + '</select>';
    htmlCode = htmlCode + shareDropDown;
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage"/>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity"/>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '<td class="text-right">';
    htmlCode = htmlCode + '<a href="#" onclick="onAddFundCoin();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
    htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
    htmlCode = htmlCode + '</td>';
    htmlCode = htmlCode + '</tr>';
    $("#addfund-details").append(htmlCode);
}

function onShareById(data) {
    $("#addshare-title").text('Update Share');
    $("#addshare-shareId").val(data.shareId);
    $("#addshare-name").val(data.name);
    $("#addshare-description").val(data.description);
    $("#addshare-exchangeCode").val(data.exchangeCode);
    $("#addshare-custodianId").val(data.custodianId);
}

function deActiveShare(data) {
    var url = './rest/cryptolabs/api/met98765678w356?sId=' + data.shareId;
//    alert(url);
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            document.location.href = './welcome';
        }
    });
}

function deActiveFund(data) {
    var url = './rest/cryptolabs/api/met5456876w789?fId=' + data.fundId;
//    alert(url);
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            document.location.href = './welcome';
        }
    });
}

function onAddFundDeleteRow(idx) {
    document.getElementById("addfund-table").deleteRow(idx);
}

function onFundById(data) {
    $("#addfund-first-row-div").hide();
    $("#addfund-title").text('Updation Form');
    $("#addfund-fundId").val(data.fundId);
    $("#addfund-name").val(data.name);
    $("#addfund-description").val(data.description);
    $("#addfund-exchangeCode").val(data.exchangeCode);
    $("#addfund-custodianId").val(data.custodianId);
    var shares = new Array();
    var url = './rest/cryptolabs/api/akljhgtfdghjkdsfadsf?fId=' + data.fundId;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            shares = data;
            var sharesHtmlCode = '';
            $("#addfund-details").html(sharesHtmlCode);
            $.each(shares, function (index, share) {
                var htmlCode = '';
                var htmlCode = '<tr>';
                htmlCode = htmlCode + '<td class="nowrap">';
                var shareDropDown = '';
                shareDropDown = shareDropDown + '<select class="form-control" name="shareId" id="opt_' + index + '">';
                shareDropDown = shareDropDown + shareOptions;
                shareDropDown = shareDropDown + '</select>';
                htmlCode = htmlCode + shareDropDown;
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage" value="' + share.percentage + '"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity" value="' + share.quantity + '"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundShare();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(index + 1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '</tr>';
                sharesHtmlCode = sharesHtmlCode + htmlCode;
            });
            if (sharesHtmlCode === '') {
                var htmlCode = '';
                var htmlCode = '<tr>';
                htmlCode = htmlCode + '<td class="nowrap">';
                var shareDropDown = '';
                shareDropDown = shareDropDown + '<select class="form-control" name="shareId" id="opt_0">';
                shareDropDown = shareDropDown + shareOptions;
                shareDropDown = shareDropDown + '</select>';
                htmlCode = htmlCode + shareDropDown;
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage" value="100"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity" value="100"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundShare();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '</tr>';
                sharesHtmlCode = sharesHtmlCode + htmlCode;
            }
            $("#addfund-details").html(sharesHtmlCode);
            $.each(shares, function (index, share) {
                document.getElementById("opt_" + index).value = share.shareId;
            });
        }
    });
}

function onPortfolioById(data) {
    $("#addfund-first-row-div").hide();
    $("#addfund-title").text('Updation Form');
    $("#addfund-fundId").val(data.fundId);
    $("#addfund-name").val(data.name);
    $("#addfund-description").val(data.description);
    $("#addfund-exchangeCode").val(data.exchangeCode);
    $("#addfund-custodianId").val(data.custodianId);
    var sharesHtmlCode = '';
    $("#addfund-shares").html(sharesHtmlCode);
    var coins = new Array();
    var url = './rest/cryptolabs/api/ksdajklfjkdsjfldsjf?fId=' + data.fundId;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            coins = data;
//            alert(coins);
            $.each(coins, function (index, coin) {
                var htmlCode = '';
                var htmlCode = '<tr>';
                htmlCode = htmlCode + '<td class="nowrap">';
                var shareDropDown = '';
                shareDropDown = shareDropDown + '<select class="form-control" name="coinId" id="opt_' + index + '">';
                shareDropDown = shareDropDown + coinOptions;
                shareDropDown = shareDropDown + '</select>';
                htmlCode = htmlCode + shareDropDown;
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage" value="' + coin.percentage + '"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity" value="' + coin.quantity + '"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundCoin();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(index + 1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '</tr>';
                sharesHtmlCode = sharesHtmlCode + htmlCode;
            });
            if (sharesHtmlCode === '') {
                var htmlCode = '';
                var htmlCode = '<tr>';
                htmlCode = htmlCode + '<td class="nowrap">';
                var shareDropDown = '';
                shareDropDown = shareDropDown + '<select class="form-control" name="coinId" id="opt_0">';
                shareDropDown = shareDropDown + coinOptions;
                shareDropDown = shareDropDown + '</select>';
                htmlCode = htmlCode + shareDropDown;
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="percentage" value="100"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<input class="form-control" type="text" name="quantity" value="100"/>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '<td class="text-right">';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundCoin();"><span class="extra-tooltip"></span><i class="fa fa-plus-circle"></i></a>';
                htmlCode = htmlCode + '<a href="#" onclick="onAddFundDeleteRow(' + eval(1) + ');"><span class="extra-tooltip"></span><i class="fa fa-minus-circle"></i></a>';
                htmlCode = htmlCode + '</td>';
                htmlCode = htmlCode + '</tr>';
                sharesHtmlCode = sharesHtmlCode + htmlCode;
            }
            $("#addfund-details").html(sharesHtmlCode);
            $.each(coins, function (index, coin) {
//                alert(coin.coinId);
                document.getElementById("opt_" + index).value = coin.coinId;
            });
        }
    });
}

function setPendingShares(data, rId) {
    var details = data.details;
    var unitPrice = data.unitPrice;
//    alert(unitPrice);
    var pendingSharesHtmlCode = '';
    $("#pendingShareTable").html(pendingSharesHtmlCode);
    $.each(details, function (index, obj) {
        if (index === 0) {
            pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="reqId" value="' + rId + '">';
            pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="investmentId" value="' + obj.investmentId + '">';
            pendingSharesHtmlCode = pendingSharesHtmlCode + '<input class="form-control" type="hidden" name="userId" value="' + obj.userId + '">';
        }
        var htmlCode = '<tr>';
        htmlCode = htmlCode + '<input type="hidden" name="shareId" value="' + obj.shareId + '">';
        htmlCode = htmlCode + '<input type="hidden" name="coinId" value="' + obj.coinId + '">';
        htmlCode = htmlCode + '<td>' + obj.name + '</td>';
        htmlCode = htmlCode + '<td> <input class="form-control shareAmount" type="number" name="shareAmount" id="shareAmount' + index + '" step="0.01" value="' + obj.shareAmount + '" onkeyup="sdqep(' + index + ')"></td>';
        htmlCode = htmlCode + '<td> <input class="form-control" type="number" name="price" id="price' + index + '" step="0.00000001" value="' + obj.price + '" onkeyup="sdpeq(' + index + ')"></td>';
        htmlCode = htmlCode + '<td> <input class="form-control" type="number" name="quantity" id="quantity' + index + '" step="0.00000001" value="' + obj.quantity + '" onkeyup="sdqep(' + index + ')"></td>';
        htmlCode = htmlCode + '</tr> ';
        pendingSharesHtmlCode = pendingSharesHtmlCode + htmlCode;
        if (index === details.length - 1) {
            htmlCode = '<tr>';
            htmlCode = htmlCode + '<td>Total</td>';
            htmlCode = htmlCode + '<td id=shareAmountTotal></td>';
            htmlCode = htmlCode + '<td></td>';
            htmlCode = htmlCode + '<td></td>';
            htmlCode = htmlCode + '</tr> ';
            pendingSharesHtmlCode = pendingSharesHtmlCode + htmlCode;
        }
    });
    $("#pendingShareTable").html(pendingSharesHtmlCode);
    $.each(details, function (index, obj) {
        sdpeq(index);
    });
    var shareAmountTotal = 0;
    var shareAmounts = document.getElementsByClassName("shareAmount");
    $.each(shareAmounts, function (index, obj) {
        shareAmountTotal = shareAmountTotal + eval(obj.value);
    });
    $("#shareAmountTotal").text(shareAmountTotal);
}

function pendingShares(mId, uId) {
    var url = './rest/cryptolabs/api/fjxzdjfhxckhfkldshffhfkhds?uId=' + uId + '&mId=' + mId;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            setPendingShares(data, '');
            $("#txnTitle").text('Purchase Shares');
            document.getElementById('updateSharesForm').action = "./admin-purchase-pending-shares";
        }
    });
}

function purchasedShares(mId, uId, rId) {
    var url = './rest/cryptolabs/api/jsdlfjsldjfwer324rrrre?uId=' + uId + '&mId=' + mId;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
//            alert('succeed data' + data);
            setPendingShares(data, rId);
            $("#txnTitle").text('WITHDRAWAL-(Sell Shares)');
            document.getElementById('updateSharesForm').action = "./admin-sell-shares";
        }
    });
}

function getShares(mId, uId, amt, rId) {
    var url = './rest/cryptolabs/api/getShares?un=' + uId + '&ok=' + mId + '&amt=' + amt;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            setPendingShares(data, rId);
            $("#txnTitle").text('DEPOSITE-(Purchase Shares)');
            document.getElementById('updateSharesForm').action = "./admin-purchase-shares";
        }
    });
}

function purchasedShares2(obj) {
    purchasedShares(obj.investmentId, obj.userId, obj.reqId);
    $(".refId").text(obj.refId);
    $(".customerName").text(obj.customerName);
    $(".fundName").text(obj.fundName);
    $(".investmentAmount").text(obj.investmentAmount);
}

function getShares2(obj) {
    getShares(obj.investmentId, obj.userId, obj.investmentAmount, obj.reqId);
    $(".refId").text(obj.refId);
    $(".customerName").text(obj.customerName);
    $(".fundName").text(obj.fundName);
    $(".investmentAmount").text(obj.investmentAmount);
}

function pendingShares2(obj) {
    pendingShares(obj.investmentId, obj.userId);
    $(".refId").text(obj.refId);
    $(".customerName").text(obj.customerName);
    $(".fundName").text(obj.fundName);
    $(".investmentAmount").text(obj.investmentAmount);
    $(".investmentAmount").val(obj.investmentAmount);
}

function registeredShares(fId, fname) {
    var url = './rest/cryptolabs/api/akljhgtfdghjkdsfadsf?fId=' + fId;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (shares) {
            var registeredSharesHtmlCode = '';
            $("#registeredShares").html(registeredSharesHtmlCode);
            $("#fundTotalShares").text(fname + ' (Total Shares)');
            $.each(shares, function (index, share) {
                var htmlCode = '<tr>';
                htmlCode = htmlCode + '<td>' + share.name + '</td>';
                htmlCode = htmlCode + '<td>' + share.description + '</td>';
                htmlCode = htmlCode + '<td>' + share.exchangeCode + '</td>';
                htmlCode = htmlCode + '<td>' + share.custodianId + '</td>';
                htmlCode = htmlCode + '<td>' + share.percentage + '</td>';
                htmlCode = htmlCode + '</tr> ';
                registeredSharesHtmlCode = registeredSharesHtmlCode + htmlCode;
            });
            $("#registeredShares").html(registeredSharesHtmlCode);
        }
    });
}

function userAccountSummary() {
    var un = $("#username").val();
    var url = './rest/cryptolabs/api/userAccountSummary?un=' + un;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            setUserAccountSummary(data);
        }
    });
}

function setUserAccountSummary(data) {
    $("#UAS").html('');
    var totalHtmlCode = '';
    $.each(data, function (index, obj) {
        var htmlCode = '<tr>';
        htmlCode = htmlCode + '<td>' + obj.created_ts + '</td>';
        htmlCode = htmlCode + '<td>' + obj.particulars + '</td>';
        if (obj.inc_dec === 'Inc') {
            htmlCode = htmlCode + '<td>' + obj.amount + '</td>';
        } else {
            htmlCode = htmlCode + '<td></td>';
        }
        if (obj.inc_dec === 'Dec') {
            htmlCode = htmlCode + '<td>' + obj.amount + '</td>';
        } else {
            htmlCode = htmlCode + '<td></td>';
        }
        htmlCode = htmlCode + '<td>' + obj.balance + '</td>';
        htmlCode = htmlCode + '</tr> ';
        totalHtmlCode = totalHtmlCode + htmlCode;
    });
    $("#UAS").html(totalHtmlCode);
}

function userDailyUpdates() {
    var un = $("#user-daily-updates-username").val();
    var url = './rest/cryptolabs/api/userDailyUpdates?un=' + un;
    $.ajax({
        url: url,
        type: 'GET',
        async: true,
        dataType: "json",
        success: function (data) {
            setUserDailyUpdates(data);
        }
    });
}

function setUserDailyUpdates(dailyUpdates) {
    var bitcoins = null, mintos = null, prices = null, usds = null, nzds = null;
    var name = null;
    var bitcoinSeries = [], mintosSeries = [], pricesSeries = [], usdsSeries = [], nzdsSeries = [];
    $.each(dailyUpdates, function (i, update) {
        if (name !== update.name) {
            if (name !== null) {
                bitcoinSeries.push({name: 'Bitcoins of ' + name, data: bitcoins});
                mintosSeries.push({name: 'Mintos of' + name, data: mintos});
                pricesSeries.push({name: 'Prices of' + name, data: prices});
                usdsSeries.push({name: 'US$ of' + name, data: usds});
                nzdsSeries.push({name: 'NZ$ of' + name, data: nzds});
            }
            bitcoins = new Array();
            mintos = new Array();
            prices = new Array();
            usds = new Array();
            nzds = new Array();
            name = update.name;
        }
        console.log(' index ' + i + ' % name-->' + name + ' % bitcoins-->' + bitcoins.length + ' % bitcoinSeries-->' + bitcoinSeries);
        var time = (new Date(update.createdDate)).getTime();
        var bitcoin = eval(update.quantity);
        bitcoins.push({
            x: time,
            y: bitcoin
        });
        var minto = eval(update.minto);
        mintos.push({
            x: time,
            y: minto
        });
        var price = eval(update.price);
        prices.push({
            x: time,
            y: price
        });
        var usd = eval(update.usd);
        usds.push({
            x: time,
            y: usd
        });
        var nzd = eval(update.local);
        nzds.push({
            x: time,
            y: nzd
        });
    });
    bitcoinSeries.push({name: 'Bitcoins of ' + name, data: bitcoins});
    mintosSeries.push({name: 'Mintos of' + name, data: mintos});
    pricesSeries.push({name: 'Prices of' + name, data: prices});
    usdsSeries.push({name: 'US$ of' + name, data: usds});
    nzdsSeries.push({name: 'NZ$ of' + name, data: nzds});
    console.log(' out of index ' + ' % name-->' + name + ' % bitcoins-->' + bitcoins.length + ' % bitcoinSeries-->' + bitcoinSeries);
    linegraphBitcoin(0, null, bitcoinSeries);
    linegraphMinto(0, null, mintosSeries);
    linegraphMintoNav(0, null, pricesSeries);
    linegraphUSD(0, null, usdsSeries);
    linegraphNZD(0, null, nzdsSeries);
}

function onSummaryClick(un) {
    $("#username").val(un);
    $("#UAS").html('');
}

function setAddFundModal(lbl) {
    $("#addfund-modalLabel").text(lbl);
}

function addShareSubmit() {
    $("#addShareForm").submit();
}

function addCurrencySubmit() {
    $("#addCurrencyForm").submit();
}

function updateConversionPairsSubmit() {
    $("#updateConversionPairs-form").submit();
}
function addFundSubmit() {
    $("#addFundForm").submit();
}

function addInviteCodeSubmit() {
    $("#addInviteCodeForm").submit();
}
