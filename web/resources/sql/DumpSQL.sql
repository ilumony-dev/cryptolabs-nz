CREATE DATABASE  IF NOT EXISTS `cryptolabsinternal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cryptolabsinternal`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: cryptolabsinternal
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fund`
--

DROP TABLE IF EXISTS `fund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `active` varchar(3) DEFAULT NULL,
  `approved` varchar(3) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `asset_id` int(11) DEFAULT NULL,
  `custodian_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund`
--

LOCK TABLES `fund` WRITE;
/*!40000 ALTER TABLE `fund` DISABLE KEYS */;
INSERT INTO `fund` VALUES (1,'Ilumony Balanced Crypto Fund','fund 01 desc','Y','Y',2,NULL,'fund 01 custodian'),(2,'Ilumony Pure Bitcoin Fund','fund 02 desc','Y','Y',2,NULL,'fund 02 custodian');
/*!40000 ALTER TABLE `fund` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fund_detail`
--

DROP TABLE IF EXISTS `fund_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fund_id` int(11) NOT NULL,
  `share_id` int(11) NOT NULL,
  `percentage` decimal(20,5) DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  `active` varchar(3) DEFAULT 'N',
  `quantity` decimal(20,4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund_detail`
--

LOCK TABLES `fund_detail` WRITE;
/*!40000 ALTER TABLE `fund_detail` DISABLE KEYS */;
INSERT INTO `fund_detail` VALUES (1,2,1,25.00000,NULL,'Y',250.0000),(2,2,2,25.00000,NULL,'Y',450.0000),(3,2,3,50.00000,NULL,'Y',500.0000);
/*!40000 ALTER TABLE `fund_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investment`
--

DROP TABLE IF EXISTS `investment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `customer_name` varchar(100) DEFAULT NULL,
  `investment_amount` varchar(100) DEFAULT NULL,
  `bank_acc` varchar(50) DEFAULT NULL,
  `reference_no` varchar(50) DEFAULT NULL,
  `fund_id` int(11) DEFAULT NULL,
  `active` varchar(3) DEFAULT 'N',
  `created_ts` timestamp NULL DEFAULT NULL,
  `time_frame` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investment`
--

LOCK TABLES `investment` WRITE;
/*!40000 ALTER TABLE `investment` DISABLE KEYS */;
INSERT INTO `investment` VALUES (1,2,'Rahul Sharma','12000','12345987654','REF12345567',2,'Y','2017-10-14 08:58:06','MONTHLY'),(2,2,'Rahul Sharma','24000','12907544677','REF-09876543',2,'Y','2017-10-14 10:42:33','WEEKLY'),(3,2,'Rahul Sharma','100000','09123456789','BIT-976432',2,'Y','2017-10-14 10:53:41','FIXED'),(4,2,'Rahul Sharma','50000','1234567890876','Ref-08823456',2,'Y','2017-10-16 12:16:56','FIXED');
/*!40000 ALTER TABLE `investment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investment_request`
--

DROP TABLE IF EXISTS `investment_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investment_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `investment_id` int(11) DEFAULT NULL,
  `amount` decimal(20,4) DEFAULT NULL,
  `action` varchar(10) DEFAULT NULL COMMENT '''WITHDRAWL,DEPOSITE''',
  `created_ts` timestamp NULL DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `active` varchar(3) DEFAULT 'N',
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investment_request`
--

LOCK TABLES `investment_request` WRITE;
/*!40000 ALTER TABLE `investment_request` DISABLE KEYS */;
INSERT INTO `investment_request` VALUES (1,2,3,10000.0000,'WITHDRAWL','2017-10-15 09:52:20',NULL,'Y','PENDING'),(2,2,3,10000.0000,'DEPOSITE','2017-10-15 10:03:20',NULL,'Y','DONE'),(3,2,3,15000.0000,'DEPOSITE','2017-10-16 12:14:17','Demo','Y','PENDING');
/*!40000 ALTER TABLE `investment_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_master`
--

LOCK TABLES `role_master` WRITE;
/*!40000 ALTER TABLE `role_master` DISABLE KEYS */;
INSERT INTO `role_master` VALUES (1,1,'ROLE_ADMIN'),(2,2,'ROLE_ADMIN'),(3,3,'ROLE_USER'),(4,4,'ROLE_USER'),(5,5,'ROLE_USER'),(6,6,'ROLE_USER'),(7,7,'ROLE_USER');
/*!40000 ALTER TABLE `role_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share`
--

DROP TABLE IF EXISTS `share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `active` varchar(3) DEFAULT NULL,
  `approved` varchar(3) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `exchange_code` varchar(45) DEFAULT NULL,
  `custodian_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share`
--

LOCK TABLES `share` WRITE;
/*!40000 ALTER TABLE `share` DISABLE KEYS */;
INSERT INTO `share` VALUES (1,'Share 01','Share 01 Desc','Y','Y',2,'SHARE-01','SHARE-0001'),(2,'Share 02','Share 02 Desc','Y','Y',2,'ex code 02','Custodian ID 02'),(3,'Share 03','Share 03 Desc','Y','Y',2,'Share 03 ExCode','Custodian ID 03'),(4,NULL,NULL,'Y','Y',2,NULL,NULL);
/*!40000 ALTER TABLE `share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `share_price`
--

DROP TABLE IF EXISTS `share_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `share_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `share_id` int(11) NOT NULL,
  `price` decimal(20,5) DEFAULT NULL,
  `created_ts` timestamp NULL DEFAULT NULL,
  `active` varchar(3) DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `share_price`
--

LOCK TABLES `share_price` WRITE;
/*!40000 ALTER TABLE `share_price` DISABLE KEYS */;
INSERT INTO `share_price` VALUES (1,1,10.00000,'2017-10-12 05:30:08','Y'),(2,2,10.00000,'2017-10-13 05:53:08','Y'),(3,3,10.00000,'2017-10-14 05:43:08','Y');
/*!40000 ALTER TABLE `share_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_ts` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `investment_id` int(11) DEFAULT NULL,
  `particulars` varchar(100) DEFAULT NULL,
  `inc_dec` varchar(5) DEFAULT NULL COMMENT 'INC/DEC',
  `amount` decimal(20,2) DEFAULT NULL,
  `active` varchar(3) DEFAULT NULL COMMENT 'Y/N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (35,'2017-10-16 09:41:23',7,NULL,'Opening Account...','Inc',0.00,'Y'),(36,'2017-10-16 11:27:28',2,3,'Deposite-(Purchased Shares)','Inc',10000.00,'Y');
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_master` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `invite_code` varchar(20) DEFAULT NULL,
  `mobile_no` varchar(20) DEFAULT NULL,
  `created_ts` timestamp NULL DEFAULT NULL,
  `active` varchar(3) DEFAULT 'Y',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_master`
--

LOCK TABLES `user_master` WRITE;
/*!40000 ALTER TABLE `user_master` DISABLE KEYS */;
INSERT INTO `user_master` VALUES (1,'Maninderjit Singh','1987-01-19','maninderjit@ice.red','PASSW0RD','0987654321','9646237869','2017-10-10 18:30:00','Y'),(2,'Rahul Sharma','2000-10-12','rahul@ice.red','PASSW0RD','1234567890','0987654321','2017-10-10 18:30:00','Y'),(3,'manider','1990-10-16','manider@ice.red','PASSW0RD','12345','1234567789','2017-10-10 18:30:00','Y'),(4,'HDSFJH','2017-10-21','SAD@FD.SDH','VDSHVDHJS','12345','2342345','2017-10-10 18:30:00','Y'),(5,'NBSFDJH','2017-10-17','SDMNB@bejg','nmssvdjh','123213','234234','2017-10-10 18:30:00','Y'),(6,'jhgfds','2017-10-16','kjs@jhd.sjdg','jahdfgus','2134','123424','2017-10-10 18:30:00','Y'),(7,'Rahul Sharma','2017-10-16','RAHUL@IDBM.COM','passw0rd','9876543','098765432','2017-10-16 09:41:23','Y');
/*!40000 ALTER TABLE `user_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_shares_pending_transaction`
--

DROP TABLE IF EXISTS `user_shares_pending_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shares_pending_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_ts` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `investment_id` int(11) DEFAULT NULL,
  `share_id` int(11) DEFAULT NULL,
  `price` decimal(20,4) DEFAULT NULL,
  `quantity` decimal(20,4) DEFAULT NULL,
  `active` varchar(3) DEFAULT 'Y',
  `action` varchar(10) DEFAULT NULL COMMENT 'PENDING/ DONE',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_shares_pending_transaction`
--

LOCK TABLES `user_shares_pending_transaction` WRITE;
/*!40000 ALTER TABLE `user_shares_pending_transaction` DISABLE KEYS */;
INSERT INTO `user_shares_pending_transaction` VALUES (4,'2017-10-14 10:53:41',2,3,1,10.0000,2500.0000,'N','PENDING'),(5,'2017-10-14 10:53:41',2,3,2,10.0000,2500.0000,'N','PENDING'),(6,'2017-10-14 10:53:41',2,3,3,10.0000,5000.0000,'N','PENDING'),(7,'2017-10-15 09:25:20',2,3,1,10.0000,2500.0000,'Y','DONE'),(8,'2017-10-15 09:25:20',2,3,2,10.0000,2500.0000,'Y','DONE'),(9,'2017-10-15 09:25:20',2,3,3,10.0000,5000.0000,'Y','DONE'),(10,'2017-10-16 05:37:12',2,3,1,10.0000,2500.0000,'Y','DONE'),(11,'2017-10-16 05:37:12',2,3,2,10.0000,2500.0000,'Y','DONE'),(12,'2017-10-16 05:37:12',2,3,3,10.0000,5000.0000,'Y','DONE'),(13,'2017-10-16 12:16:56',2,4,1,10.0000,1250.0000,'Y','PENDING'),(14,'2017-10-16 12:16:56',2,4,2,10.0000,1250.0000,'Y','PENDING'),(15,'2017-10-16 12:16:56',2,4,3,10.0000,2500.0000,'Y','PENDING');
/*!40000 ALTER TABLE `user_shares_pending_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_shares_transaction`
--

DROP TABLE IF EXISTS `user_shares_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_shares_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_ts` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `investment_id` int(11) DEFAULT NULL,
  `share_id` int(11) DEFAULT NULL,
  `price` decimal(20,4) DEFAULT NULL,
  `quantity` decimal(20,4) DEFAULT NULL,
  `active` varchar(3) DEFAULT 'Y',
  `action` varchar(10) DEFAULT NULL COMMENT 'SOLD/ PURCHASED',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_shares_transaction`
--

LOCK TABLES `user_shares_transaction` WRITE;
/*!40000 ALTER TABLE `user_shares_transaction` DISABLE KEYS */;
INSERT INTO `user_shares_transaction` VALUES (4,'2017-10-15 09:25:20',2,3,1,10.0000,2500.0000,'Y','PURCHASED'),(5,'2017-10-15 09:25:20',2,3,2,10.0000,2500.0000,'Y','PURCHASED'),(6,'2017-10-15 09:25:20',2,3,3,10.0000,5000.0000,'Y','PURCHASED'),(7,'2017-10-16 05:37:12',2,3,1,10.0000,2500.0000,'Y','PURCHASED'),(8,'2017-10-16 05:37:12',2,3,2,10.0000,2500.0000,'Y','PURCHASED'),(9,'2017-10-16 05:37:12',2,3,3,10.0000,5000.0000,'Y','PURCHASED'),(10,'2017-10-16 06:11:57',2,3,1,10.0000,250.0000,'Y','SOLD'),(11,'2017-10-16 06:11:57',2,3,2,10.0000,250.0000,'Y','SOLD'),(12,'2017-10-16 06:11:57',2,3,3,10.0000,500.0000,'Y','SOLD'),(13,'2017-10-16 11:27:28',2,3,1,10.0000,250.0000,'Y','PURCHASED'),(14,'2017-10-16 11:27:28',2,3,2,10.0000,250.0000,'Y','PURCHASED'),(15,'2017-10-16 11:27:28',2,3,3,10.0000,500.0000,'Y','PURCHASED');
/*!40000 ALTER TABLE `user_shares_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cryptolabsinternal'
--

--
-- Dumping routines for database 'cryptolabsinternal'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-16 18:05:54
